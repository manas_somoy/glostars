//
//  FollowingUserMapper.swift
//  Glostars
//
//  Created by Sanzid iOS on 7/12/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import Foundation
import ObjectMapper

class FollowingUserMapper : Mappable {
    
    
    dynamic var name : String = ""
    dynamic var proPic : String = ""
    dynamic var is_mutual = Bool()
    dynamic var he_follow = Bool()
    dynamic var me_follow = Bool()
    dynamic var id : String = ""
    dynamic var lastName : String = ""

    

    
    
    required convenience init?(map: Map) {
        
        
        self.init()
        
        
    }
    
    func mapping(map: Map) {
        
        name <- map["name"]
        proPic <- map["profilemediumPath"]
        is_mutual <- map["is_mutual"]
        he_follow <- map["he_follow"]
        me_follow <- map["me_follow"]
        id <- map["id"]
        lastName <- map["lastName"]

   

        
        
    }
}
