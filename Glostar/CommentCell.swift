import UIKit

class CommentCell: UITableViewCell {
    @IBOutlet weak var uploadTime: UILabel!
    
    @IBOutlet var editableTextField: UITextField!
    
    
    @IBOutlet var editCommentOutlet: UIButton!
    
    @IBOutlet weak var userLastName: UILabel!
    
    @IBOutlet var deleteButtonOutlet: UIButton!
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var commentMsg: UITextView!
    
    var onProfileTapped: (() -> Void)? = nil
    var onProfileTapped1: (() -> Void)? = nil
    var onProfileTapped2: (() -> Void)? = nil
    
    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var fontView: UIView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        userImage.setRounded()
        
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        //deleteButtonOutlet.isHidden = true
        
    }
    @IBAction func sendToProfile(_ sender: Any) {
        
        if let onProfileTapped = self.onProfileTapped {
            
            onProfileTapped()
            
        }
        
        
    }
    
    @IBAction func editprofile(_ sender: Any) {
        
        if let onProfileTapped2 = self.onProfileTapped2 {
            
            onProfileTapped2()
            
        }
        
    }
    
    @IBAction func deleteComment(_ sender: Any) {
        
        if let onProfileTapped1 = self.onProfileTapped1 {
            
            onProfileTapped1()
            
        }
        
    }
    @IBAction func sendToProfile1(_ sender: Any) {
        
        
        
        
    }
    
}
