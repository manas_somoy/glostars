//
//  GaleryMapper.swift
//  Glostars
//
//  Created by Sanzid Ashan on 5/19/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

//
//  ImageMapper.swift
//  Glostars
//
//  Created by Sanzid Ashan on 5/17/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import Foundation


import Foundation
import ObjectMapper

class TopImageMappre : Mappable {
    
    
    dynamic var descriptionTwo : String = ""
    dynamic var emailTwo : String = ""
    dynamic var uploadedUserIdTwo : String = ""
    dynamic var userProfilePicLinkTwo : String = ""
    dynamic var usernameTwo : String = ""
    dynamic var votecounterTwo : Int = 0
    dynamic var weekPictureIdTwo : Int = 0
    dynamic var photourlTwo : String = ""


    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        descriptionTwo <- map["descriptionTwo"]
        emailTwo <- map["emailTwo"]
        uploadedUserIdTwo <- map["uploadedUserIdTwo"]
        userProfilePicLinkTwo <- map["userProfilePicLinkTwo"]
        usernameTwo <- map["usernameTwo"]
        votecounterTwo <- map["votecounterTwo"]
        weekPictureIdTwo <- map["weekPictureIdTwo"]
        photourlTwo <- map["photourlTwo"]


    }
}



