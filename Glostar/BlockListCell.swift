//
//  BlockListCell.swift
//  Glostars
//
//  Created by sadidur rahman on 23/1/19.
//  Copyright © 2019 Sanzid Ashan. All rights reserved.
//

import UIKit

class BlockListCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var unblockButton: DesignableButton!
    
    var onUnFollowTapped: (() -> Void)? = nil
    var onImageTapped: (() -> Void)? = nil
    var onLabelTapped: (() -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        userImageView.setRounded()
        // Configure the view for the selected state
    }
    
    @IBAction func unBlockButtonTapped(_ sender: Any) {
        if let onUnFollowTapped = self.onUnFollowTapped {
            onUnFollowTapped()
        }
    }
    
    
    @IBAction func onImageTapped(_ sender: Any) {
        if let onImageTapped = self.onImageTapped {
            onImageTapped()
        }
    }
    
    @IBAction func onLabelTapped(_ sender: Any) {
        if let onLabelTapped = self.onLabelTapped {
            onLabelTapped()
        }
        
    }
    
}

