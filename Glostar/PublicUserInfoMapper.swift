//
//  SportArticleMapper.swift
//  NewsApp
//
//  Created by Sanzid Ashan on 5/2/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import Foundation
import ObjectMapper

class PublicUserInfoMapper : Mappable {
    
    
    dynamic var profilePicURL : String = ""
    dynamic var name : String = ""
    dynamic var userid : String = ""
    
    
    
    
    required convenience init?(map: Map) {
        
        
        self.init()
        
        
    }
    
    func mapping(map: Map) {
        
        profilePicURL <- map["profilePicURL"]
        name <- map["name"]
        userid <- map["userId"]
        
        
        
    }
}

