//
//  FollowerVC.swift
//  Glostars
//
//  Created by Sanzid iOS on 7/12/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

//
//  FollowingViewController.swift
//  Glostars
//
//  Created by Sanzid Ashan on 3/30/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import ObjectMapper

class FollowerVC: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    var FollowingNotifyArray : Array< FollowingMapper > = []
    var FollowingstatusArray : Array <FollowingStatusMapper> = []
    var FollowerArray : Array<FollowUserMapper> = []

    var PageNumber = 1
    var totalItems = 1000

    @IBOutlet weak var tableView: UITableView!
    var is_mutual = Bool()
    var me_follow = Bool()
    var he_follow = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tableView.tableFooterView = UIView(frame: .zero)
        
        
        print(userID)
        
        self.navigationItem.title = "Followers"
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.white,
             NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 18)!]
        
        
        //cintainerView.isHidden = true
        //tableView2.isHidden = true
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        FollowerArray.removeAll()
        tableView.contentInset = UIEdgeInsetsMake(40, 0, 0, 0)

        gettingFollowerUser()
//        PageNumber = 2
//        gettingFollowerUser()

    }
    
    
    func gettingFollowerUser() {
        
        
    let url = "https://testglostarsdevelopers.azurewebsites.net/api/account/GetUserFollowById?userId=\(userID)&page=\(PageNumber)"
        Alamofire.request(url, method: .get , encoding: JSONEncoding.default)
            
            
            .responseJSON{ response in
                
                
                if let jsonRoot = response.result.value as? [String:Any]!{
                    
                    if let resultPayload = jsonRoot!["resultPayload"] as? [String : Any]{
                        
                        if let followList = resultPayload["followerList"] as? [[String : Any]] {
                            print(followList)
                            for data in followList{
                                
                                guard let followList = Mapper<FollowUserMapper>().map(JSON: data) else{
                                    
                                    continue
                                }
                                
                                self.FollowerArray.append(followList)
                                
                            }

                            
                        }
                        
                        if let followingList = resultPayload["followingList"] as? [[String : Any]] {
                            
                            
                            
                        }
                        
                        
                        
                    }
                    
                    
                }
                self.tableView.reloadData()

        }
        
        
        
        
    }
    
//    func gettingFollowerUserWithout() {
//        
//        
//        let url = "https://testglostarsdevelopers.azurewebsites.net/api/account/GetUserFollowById?userId=\(userID)&page=\(PageNumber)"
//        Alamofire.request(url, method: .get , encoding: JSONEncoding.default)
//            
//            
//            .responseJSON{ response in
//                
//                
//                if let jsonRoot = response.result.value as? [String:Any]!{
//                    
//                    if let resultPayload = jsonRoot["resultPayload"] as? [String : Any]{
//                        
//                        if let followList = resultPayload["followerList"] as? [[String : Any]] {
//                            print(followList)
//                            for data in followList{
//                                
//                                guard let followList = Mapper<FollowUserMapper>().map(JSON: data) else{
//                                    
//                                    continue
//                                }
//                                
//                                //self.FollowerArray.append(followList)
//                                self.tableView.reloadData()
//                                
//                            }
//                            
//                            
//                        }
//                        
//                        if let followingList = resultPayload["followingList"] as? [[String : Any]] {
//                            
//                            
//                            
//                        }
//                        
//                        
//                        
//                    }
//                    
//                    
//                }
//        }
//        
//        
//        
//    }

    
    
    
    
    
    // this method will be called by the table view to know the count of rows for each section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return FollowerArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        
        
        // create the table view cell design from the prototype cell we have created in storyboard. Reuseidentifier will have same string as in storyboard
        let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellTwo", for: indexPath) as! FollowerCell
        
        //tableViewCell.lastname.text = FollowerArray[indexPath.row].lastName
        tableViewCell.nameProfile.text = FollowerArray[indexPath.row].name + " " + FollowerArray[indexPath.row].lastName
        
        let url = FollowerArray[indexPath.row].proPic
        
        
        if url == "/Content/Profile/Thumbs/   Male.jpg"{
            
            tableViewCell.proPicImage.image = UIImage(named: "male")
            
        }
        else if url == "/Content/Profile/Thumbs/   Female  .jpg"{
            
            tableViewCell.proPicImage.image = UIImage(named: "female")
            
        }
            
            
        else{
            
            
            
            let nsurl = NSURL(string: FollowerArray[indexPath.row].proPic)
            
            tableViewCell.proPicImage.af_setImage(withURL: nsurl! as URL, placeholderImage: #imageLiteral(resourceName: "male"), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .crossDissolve(0.5), runImageTransitionIfCached: true, completion: nil)
            
        }
        
        
      
        
        
        
        tableViewCell.onImageTapped = {
            
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            
            passData.getid = self.FollowerArray[indexPath.row].id
            
            
            
            self.navigationController?.pushViewController(passData, animated: true)
        }
        
        tableViewCell.onLabelTapped = {
            
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            
            passData.getid = self.FollowerArray[indexPath.row].id
            
            
            
            self.navigationController?.pushViewController(passData, animated: true)
            
        }
        
//        if self.FollowerArray[indexPath.row].is_mutual == true {
//            
//            tableViewCell.followButtonOutlet.setTitle("Mutual", for: .normal)
//            tableViewCell.followButtonOutlet.backgroundColor = UIColor(hex: 0x660066, alpha: 1)
//            
//        }
//        
//        
//        if self.FollowerArray[indexPath.row].is_mutual == false && self.FollowerArray[indexPath.row].me_follow == false && self.FollowerArray[indexPath.row].he_follow == false {
//            
//            tableViewCell.followButtonOutlet.setTitle("Follow", for: .normal)
//                 tableViewCell.followButtonOutlet.backgroundColor = UIColor(hex: 0x4d94ff, alpha: 1)
//            
//            
//        }
//        
//        if self.FollowerArray[indexPath.row].is_mutual == false && self.FollowerArray[indexPath.row].me_follow == true && self.FollowerArray[indexPath.row].he_follow == false {
//            
//            tableViewCell.followButtonOutlet.setTitle("Following", for: .normal)
//            tableViewCell.followButtonOutlet.backgroundColor = UIColor(hex: 0xcc99ff, alpha: 1)
//            
//        }
//        
//        if self.FollowerArray[indexPath.row].is_mutual == false && self.FollowerArray[indexPath.row].me_follow == false && self.FollowerArray[indexPath.row].he_follow == true {
//            
//            tableViewCell.followButtonOutlet.setTitle("Follow Back", for: .normal)
//            tableViewCell.followButtonOutlet.backgroundColor = UIColor.darkGray
//        }
        if self.self.FollowerArray[indexPath.row].is_mutual == true  {
            
            tableViewCell.followButtonOutlet.setTitle("Mutual", for: .normal)
            tableViewCell.followButtonOutlet.backgroundColor = UIColor(hex: 0x660066, alpha: 1)
            
        }
            
            
        else if self.FollowerArray[indexPath.row].is_mutual == false && self.FollowerArray[indexPath.row].me_follow == false && self.FollowerArray[indexPath.row].he_follow == false {
            
            tableViewCell.followButtonOutlet.setTitle("Follow", for: .normal)
            tableViewCell.followButtonOutlet.backgroundColor = UIColor.init(hex: "00c800")
            
            
        }
            
        else if self.FollowerArray[indexPath.row].is_mutual == false && self.FollowerArray[indexPath.row].me_follow == true && self.FollowerArray[indexPath.row].he_follow == false {
            
            tableViewCell.followButtonOutlet.setTitle("Following", for: .normal)
            tableViewCell.followButtonOutlet.backgroundColor = UIColor(hex: 0xcc99ff, alpha: 1)
            
        }
            
        else if self.FollowerArray[indexPath.row].is_mutual == false && self.FollowerArray[indexPath.row].me_follow == false && self.FollowerArray[indexPath.row].he_follow == true {
            
            tableViewCell.followButtonOutlet.setTitle("Follower", for: .normal)
            tableViewCell.followButtonOutlet.backgroundColor = UIColor.init(hex: "0371e1")
                
        }

//    
        tableViewCell.onFollowTapped = {
            
            if tableViewCell.followButtonOutlet.title(for: .normal) == "Mutual" {
                
                let unfollowUrl = "https://testglostarsdevelopers.azurewebsites.net/api/Follower/Unfollowing?id=\(self.FollowerArray[indexPath.row].id)"
                Alamofire.request(unfollowUrl, method: .post).responseJSON { response in
                    
                    if let jsonRoot = response.result.value as? [String:Any]!{
                        
                        if let resultPayload = jsonRoot!["resultPayload"] as! [String :Any]!{
                            
                            let he_follow = resultPayload["he_follow"] as! Bool
                            let is_mutual = resultPayload["is_mutual"] as! Bool
                            let me_follow = resultPayload["me_follow"] as! Bool
                            
                            if is_mutual == true && he_follow == true && me_follow == true {
                                tableViewCell.followButtonOutlet.setTitle("Mutual", for: .normal)
                                tableViewCell.followButtonOutlet.backgroundColor = UIColor(hex: 0x660066, alpha: 1)
                                
                            }
                                
                            else if is_mutual == false && self.me_follow == false && he_follow == false {
                                tableViewCell.followButtonOutlet.setTitle("Follow", for: .normal)
                                tableViewCell.followButtonOutlet.backgroundColor = UIColor(hex: 0x4d94ff, alpha: 1)
                                
                                
                            }
                                
                            else if is_mutual == false && self.me_follow == true && he_follow == false {
                                
                                tableViewCell.followButtonOutlet.setTitle("Following", for: .normal)
                                tableViewCell.followButtonOutlet.backgroundColor = UIColor(hex: 0xcc99ff, alpha: 1)
                                
                            }
                                
                            else if is_mutual == false && self.me_follow == false && he_follow == true {
                                
                                tableViewCell.followButtonOutlet.setTitle("Follow Back", for: .normal)
                                tableViewCell.followButtonOutlet.backgroundColor = UIColor.darkGray
                            }
                            
                            
                            
                        }
                        
                        
                        
                        //
                        //                            }
                        //
                        //                    }
                        
                        
                        
                    }
                    
                    
                    
                }
            }
                
            else {
                
                
                let url = "https://testglostarsdevelopers.azurewebsites.net/api/Follower/Following?id=\(self.FollowerArray[indexPath.row].id)"
                
                //print(getid)
                
                Alamofire.request(url, method: .post).responseJSON { response in
                    
                    if let jsonRoot = response.result.value as? [String:Any]!{
                        if let resultPayload = jsonRoot!["resultPayload"] as! [String :Any]!{
                            
                            let he_follow = resultPayload["he_follow"] as! Bool
                            let is_mutual = resultPayload["is_mutual"] as! Bool
                            let me_follow = resultPayload["me_follow"] as! Bool
                            
                            if is_mutual == true && he_follow == true && me_follow == true {
                                
                                tableViewCell.followButtonOutlet.setTitle("Mutual", for: .normal)
                                tableViewCell.followButtonOutlet.backgroundColor = UIColor(hex: 0x660066, alpha: 1)
                                
                            }
                                
                                
                            else if is_mutual == false && self.me_follow == false && he_follow == false {
                                
                                tableViewCell.followButtonOutlet.setTitle("Follow", for: .normal)
                                tableViewCell.followButtonOutlet.backgroundColor = UIColor(hex: 0x4d94ff, alpha: 1)
                                
                                
                            }
                                
                            else if is_mutual == false && self.me_follow == true && he_follow == false {
                                
                                tableViewCell.followButtonOutlet.setTitle("Following", for: .normal)
                                tableViewCell.followButtonOutlet.backgroundColor = UIColor(hex: 0xcc99ff, alpha: 1)
                                
                            }
                                
                            else if is_mutual == false && self.me_follow == false && he_follow == true {
                                
                                tableViewCell.followButtonOutlet.setTitle("Follow Back", for: .normal)
                                tableViewCell.followButtonOutlet.backgroundColor = UIColor.darkGray
                            }
                            
                            
                            
                        }
                        
                        
                        
                        
                    }
                    
                }
                
                
                
            }

            
//            if self.me_follow == true{
//                
//                let unfollowUrl = "https://testglostarsdevelopers.azurewebsites.net/api/Follower/Unfollowing?id=\(self.FollowerArray[indexPath.row].id)"
//                Alamofire.request(unfollowUrl, method: .post).responseJSON { response in
//                    
//                    if let jsonRoot = response.result.value as? [String:Any]!{
//                        
//                        print(jsonRoot)
//                        
//                        if let resultPayload = jsonRoot["resultPayload"] as? [String : Any]{
//                            
//                            self.is_mutual = (resultPayload["is_mutual"] as? Bool)!
//                            self.me_follow = (resultPayload["me_follow"] as? Bool)!
//                            self.he_follow = (resultPayload["he_follow"] as? Bool)!
//                            
//                            if self.is_mutual == true {
//                                
//                    tableViewCell.followButtonOutlet.setTitle("Mutual", for: .normal)
//                    tableViewCell.followButtonOutlet.backgroundColor = UIColor(hex: 0x660066, alpha: 1)
//                                
//                            }
//                            
//                            
//                            if self.is_mutual == false && self.me_follow == false && self.he_follow == false {
//                                
//                    tableViewCell.followButtonOutlet.setTitle("Follow", for: .normal)
//                                
//                    tableViewCell.followButtonOutlet.backgroundColor = UIColor(hex: 0x4d94ff, alpha: 1)
//                            }
//                            
//                            if self.is_mutual == false && self.me_follow == true && self.he_follow == false {
//                                
//                    tableViewCell.followButtonOutlet.setTitle("Following", for: .normal)
//                    tableViewCell.followButtonOutlet.backgroundColor = UIColor(hex: 0xcc99ff, alpha: 1)
//                                
//                            }
//                            if self.is_mutual == false && self.me_follow == false && self.he_follow == true {
//                                
//                        tableViewCell.followButtonOutlet.setTitle("Follow Back", for: .normal)
//                    tableViewCell.followButtonOutlet.backgroundColor = UIColor.gray
//                                
//                            }
//                            
//                        }
//                        
//                        
//                    }
//                    
//                    
//                    
//                }
//                
//                
//                
//            }
//                
//            else {
//            
//            let url = "https://testglostarsdevelopers.azurewebsites.net/api/Follower/Following?id=\(self.FollowerArray[indexPath.row].id)"
//            
//            
//            Alamofire.request(url, method: .post).responseJSON { response in
//                
//                
//                if let jsonRoot = response.result.value as? [String:Any]!{
//                    
//                    if let resultPayload = jsonRoot["resultPayload"] as? [String : Any]{
//                        
//                        self.is_mutual = (resultPayload["is_mutual"] as? Bool)!
//                        self.me_follow = (resultPayload["me_follow"] as? Bool)!
//                        self.he_follow = (resultPayload["he_follow"] as? Bool)!
//                        
//                        if self.is_mutual == true {
//                            
//                        tableViewCell.followButtonOutlet.setTitle("Mutual", for: .normal)
//                            tableViewCell.followButtonOutlet.backgroundColor = UIColor(hex: 0x660066, alpha: 1)
//                            
//                        }
//                        
//                        
//                        if self.is_mutual == false && self.me_follow == false && self.he_follow == false {
//                            
//                        tableViewCell.followButtonOutlet.setTitle("Follow", for: .normal)
//                            
//                            tableViewCell.followButtonOutlet.backgroundColor = UIColor(hex: 0x4d94ff, alpha: 1)
//                        }
//                        
//                        if self.is_mutual == false && self.me_follow == true && self.he_follow == false {
//                            
//                        tableViewCell.followButtonOutlet.setTitle("Following", for: .normal)
//                            tableViewCell.followButtonOutlet.backgroundColor = UIColor(hex: 0xcc99ff, alpha: 1)
//                            
//                        }
//                        if self.is_mutual == false && self.me_follow == false && self.he_follow == true {
//                            
//                        tableViewCell.followButtonOutlet.setTitle("Follow Back", for: .normal)
//                            tableViewCell.followButtonOutlet.backgroundColor = UIColor.gray
//                            
//                        }
//                        
//                    }
//                    
//                }
//
//                
//                
//             
//
//                
//                }
//
//                
//                
//                
//                
//            }
//            
//
//            
//            
        }
        if indexPath.row == FollowerArray.count - 1 {
            // last cell
            
            if totalItems > FollowerArray.count {
                PageNumber = PageNumber + 1
                print(PageNumber)
                // more items to fetch
                gettingFollowerUser()
            }
        }
        
    
        return tableViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileSearchVC") as! ProfileSearchVC
        
        passData.getid = self.FollowerArray[indexPath.row].id
        passData.getName = self.FollowerArray[indexPath.row].name
        passData.getProfileUrl = self.FollowerArray[indexPath.row].proPic
        passData.getLastName = self.FollowerArray[indexPath.row].lastName
        
        
        self.navigationController?.pushViewController(passData, animated: true)
        
        
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        
        navigationController?.isNavigationBarHidden = false
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.panGestureRecognizer.translation(in: scrollView.superview).y > 0 {
            //scrolling downwards
            
            navigationController?.isNavigationBarHidden = false
            
            
            if scrollView.contentOffset.y < 0 {
                //this means we are at top of the scrollView
                navigationController?.isNavigationBarHidden = false
                
                
            }
        }
        else {
            //we are scrolling upward
            
            navigationController?.isNavigationBarHidden = true
            
            
        }
      
    }
    
    @IBAction func accesstypeButtonPressed(_ sender: Any) {
        print("Pressed")
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
