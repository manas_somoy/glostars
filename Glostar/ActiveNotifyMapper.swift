//
//  InfoMapper.swift
//  Glostars
//
//  Created by Sanzid Ashan on 5/14/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//


import Foundation
import ObjectMapper

class ActiveNotifyMapper : Mappable {
    

    dynamic var userPicUrl : String = ""
    dynamic var PictureUrl : String = ""
    dynamic var UserName : String = ""
    dynamic var Description : String = ""
    dynamic var pictureID : Int = 0
    dynamic var notifyID : String = ""
    dynamic var timeUpload :String = ""
    dynamic var userId : String = ""
    
    
    
    required convenience init?(map: Map) {
        
        
        self.init()
        
        
    }
    
    func mapping(map: Map) {
        
        userPicUrl <- map["profilePicURL"]
        PictureUrl <- map["picUrl"]
        UserName <- map["name"]
        Description <- map["description"]
        pictureID <- map["pictureId"]
        notifyID <- map["originatedById"]
        timeUpload <- map["date"]
        userId <- map["userId"]
       
 
    }
}

