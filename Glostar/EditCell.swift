//
//  EditCell.swift
//  EditedProject
//
//  Created by sadidur rahman on 4/2/19.
//  Copyright © 2019 sadidur rahman. All rights reserved.
//

import UIKit

class EditCell: UICollectionViewCell {
    
    @IBOutlet weak var cellPhotoImage: UIImageView!
    @IBOutlet weak var cellPhoto: UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var editImage: UIImageView!
    @IBOutlet weak var editLabel: UILabel!
    
    
    override func awakeFromNib() {
//        editImage.setRounded()
        userImage.setRounded()
//        overlay.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)

    }
    
    
}


//extension UIImageView {
//    
//    func setRounded() {
//        self.layer.cornerRadius = (self.frame.width / 2) //instead of let radius = CGRectGetWidth(self.frame) / 2
//        self.layer.masksToBounds = true
//    }
//}
