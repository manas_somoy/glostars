//
//  NotificationViewController.swift
//  Glostars
//
//  Created by Sanzid Ashan on 3/30/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

extension UITextField {
    
    func useUnderline() {
        
        let border = CALayer()
        let borderWidth = CGFloat(1.0)
        border.borderColor = UIColor.gray.cgColor
        border.frame = CGRect(x:0, y: self.frame.size.height - borderWidth, width : self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = borderWidth
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}

class ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout , UITextFieldDelegate , UITableViewDataSource,UITableViewDelegate,FloatyDelegate {
    
    @IBOutlet var mainView: UIView!
    var NameArray : Array<ProfileMapper> = []
    var totalItems = 1000

    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet var searchButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet var hidingView: UIView!
    @IBOutlet weak var achivementButton: UIButton!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var uploadButton: UIButton!
    
    @IBOutlet weak var floatingMainButton: UIButton!

    var publicInfo : Array<ImageMapper> = []
    var IDArray : Array<ImageIdMapper> = []


    var SearchText = ""
    
    @IBOutlet weak var searchFieldPhoto: UITextField!

    
    var pageNumber = 1
    
    
    var container: UIView = UIView()
    var loadingView: UIView = UIView()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    var getSearchUserid = String()
    var fab = Floaty()

    override func viewDidLoad() {
        super.viewDidLoad()
        layoutFAB()
        hiddenFloatingButtons()
        getRequest()

        
        tableView.isHidden = true
        
        
        
//        let urlString = URL(string: userPic)!
//
//
//        profileButton.af_setBackgroundImage(for: [] , url: urlString )
        

     
        //getRequest()
       
        
      
        
        
    }
    

    func layoutFAB() {
        let item = FloatyItem()
        item.buttonColor = UIColor(displayP3Red: 1, green: 1, blue: 1, alpha: 0.7)
        //item.circleShadowColor = UIColor.red
        //item.titleShadowColor = UIColor.blue
        
        //fab.addItem(title: "I got a title")
        //fab.addItem("I got a icon", icon: UIImage(named: "icShare"))
        fab.addItem("", icon: UIImage(named: "notif+"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "photo-camera"), titlePosition: .left) { (item) in

            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "FAImageCropperVC") as! FAImageCropperVC
            
            self.navigationController?.pushViewController(passData, animated: true)
            
            
            
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "search-1"), titlePosition: .left) { (item) in
            
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "competiton"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "GaleryViewController") as! GaleryViewController
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "Profile_male"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            passData.getid = userID
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "home+"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "NewsVCViewController") as! NewsVCViewController
            
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        
        self.fab.sticky = true
        self.fab.paddingX = self.view.frame.width/20
        
        self.fab.fabDelegate = self
        
        
        self.view.addSubview(self.fab)
    }
    

    override func viewWillAppear(_ animated: Bool) {
        searchFieldPhoto.isHidden = true
        searchFieldPhoto.useUnderline()
        searchFieldPhoto.keyboardAppearance = .dark
        searchFieldPhoto.resignFirstResponder()
        hidingView.isHidden = true
        floatingMainButton.setRounded()
        notificationButton.setRounded()
        uploadButton.setRounded()
        achivementButton.setRounded()
        homeButton.setRounded()
        profileButton.setRounded()
        searchButton.setRounded()
        self.navigationController?.setNavigationBarHidden(false, animated: true)

        if userPic == "/Content/Profile/Thumbs/   Male.jpg"{
            
            profileButton.setBackgroundImage(UIImage(named:"male"), for: .normal)
            
        }
        else if userPic == "/Content/Profile/Thumbs/   Female  .jpg"{
            
            profileButton.setBackgroundImage(UIImage(named:"female"), for: .normal)
        }
            
            
        else{
            let urlString = URL(string:String(describing: UserDefaults.standard.value(forKey: "pic")!))
            profileButton.af_setBackgroundImage(for: [] , url: urlString! )
            
        }

        if (searchFieldPhoto.text?.isEmpty)!{
            
            tableView.isHidden = true
            
        }
        

        self.navigationItem.title = "Search Photos"
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.white,
             NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 17)!]
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //searchItem()
        tableView.isHidden = true
        searchFieldPhoto.text = ""
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NameArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NameCell
        
        if NameArray.count > 0 {
        
        let array = NameArray[indexPath.row]
        
        
          
        let Url = NSURL(string:array.proLink)
            
        
            if Url != nil{
        cell.userName.text = NameArray[indexPath.row].userName
        cell.lastName.text = NameArray[indexPath.row].lastName
            cell.searchProfilePic.sd_setShowActivityIndicatorView(true)
            cell.searchProfilePic.sd_setIndicatorStyle(.gray)
                cell.searchProfilePic.sd_setImage(with: Url!as URL, placeholderImage: UIImage(named:"male"), options: .scaleDownLargeImages, completed: nil)
            }
            else{
                
                print("Url Error ")
            }
    
        }
        
        else {
            
            print ("Error")
            
        }
        return cell
        
        
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        //        navigationController?.isNavigationBarHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //        navigationController?.isNavigationBarHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        
    
        passData.getid = NameArray[indexPath.row].id

        self.navigationController?.pushViewController(passData, animated: true)
        
        searchFieldPhoto.resignFirstResponder()
        
    }
   
    
   func getRequest(){
    
    
    let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/public/\(pageNumber)"
    print(url)
    
    Alamofire.request(url).responseJSON { response in
    
    if let jsonRoot = response.result.value as? [String:Any]!{
        if response.result.value != nil {
    
            if let resultPayload = jsonRoot!["resultPayload"] as? [String:Any]!{
    
    
                if let picReturn = resultPayload!["picsToReturn"] as? [[String:Any]]!{
    
                    for data in picReturn! {
    
    guard  let imageInfo = Mapper<ImageMapper>().map(JSON: data) else {
        
        
    continue
        
        }
    
    self.publicInfo.append(imageInfo)
    }
     
                    for dic in picReturn! {
            
            
            if let picID = dic["poster"] as? [String:Any]!{

                guard  let imageID = Mapper<ImageIdMapper>().map(JSON: picID!) else {
                    
                    
                    continue
                    
                }
                
            self.IDArray.append(imageID)
            }
                
            
            
            
            
        }
        
    
    
    }
    
    
    
    }
 
                self.collectionView.reloadData()
            //self.collectionView.
           
    
        }
        else{
            
            self.getRequest()
        }
        
    }
        
    else {
        
        
        }

    }
    
    }
    
    @IBAction func onClinkProfile(_ sender: Any) {
        
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        
        
        passData.getid = userID
        
        self.navigationController?.pushViewController(passData, animated: true)
        
        
    }
    
    func searchItem(){
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/account/GetUserListByName?name=\(searchFieldPhoto.text!)&page=0"
        let safeURL =  url.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
        Alamofire.request(safeURL!).responseJSON { response in
            
            if let jsonRoot = response.result.value as? [String:Any]!{
                
                if response.result.value != nil {
                    if let resultPayload = jsonRoot!["resultPayload"] as? [[String:Any]]!{
                    
                    if resultPayload != nil {
                    
                        for array in resultPayload! {
                        
                        
                        guard  let profile = Mapper<ProfileMapper>().map(JSON: array) else {
                            continue
                        }
                        self.NameArray.append(profile)
                        self.tableView.reloadData()
                        
                    }
                    }
                    else {
                        
                    }
                    
                    
                }
            }
            }
            else {
                
                
            }
            
        }
    }

   
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        //searchFieldPhoto.text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        //let text = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        
        let text = (searchFieldPhoto.text! as NSString).replacingCharacters(in: range, with: string)

            self.tableView.isHidden = false
            


        
        
//        if text.isEmpty {
//
//            tableView.isHidden = true
//            searchFieldPhoto.text = ""
//
//        }
        self.SearchText = textField.text!
        //self.tableView.reloadData()

        NameArray.removeAll()
        searchItem()
        
        //self.tableView.reloadData()

        return true
    }

    func hiddenFloatingButtons(){
        
        uploadButton.isHidden = true
        achivementButton.isHidden = true
        profileButton.isHidden = true
        notificationButton.isHidden = true
        homeButton.isHidden = true
        searchButton.isHidden = true
        
    }
    
    func animatingPopUp()
    {
        UIView.animate(withDuration: 1, animations: {
            self.uploadButton.isHidden =  !self.uploadButton.isHidden
            
        }) { (true) in
            
            UIView.animate(withDuration: 1, animations: {
                self.achivementButton.isHidden = !self.achivementButton.isHidden
                self.searchButton.isHidden = !self.searchButton.isHidden

                
            }, completion: { (true) in
                UIView.animate(withDuration: 1, animations: {
                    
                    self.profileButton.isHidden = !self.profileButton.isHidden
                    
                }, completion: { (true) in
                    self.notificationButton.isHidden = !self.notificationButton.isHidden
                    
                    self.homeButton.isHidden = !self.homeButton.isHidden
                    
                    //                    let image1 = UIImage(named: "Circleopen")
                    //                    self.floatingMainButton.setBackgroundImage(image1, for: UIControlState.normal)
                    
                    
                    
                    
                })
            })
        }
        
        
        
    }
    
   
    
    
    @IBAction func mainButtonAction(_ sender: UIButton) {
        
        animatingPopUp()
        //blurView.isHidden = !blurView.isHidden
        
        
        
        
    }

    
    
    

    @IBAction func searchFieldButton(_ sender: Any) {
        self.NameArray.removeAll()
        self.searchFieldPhoto.isHidden = !self.searchFieldPhoto.isHidden
        //self.tableView.isHidden = !self.tableView.isHidden
        searchFieldPhoto.becomeFirstResponder()
        searchFieldPhoto.useUnderline()
        
        searchFieldPhoto.isHidden = false
        hiddenFloatingButtons()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return publicInfo.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CollectionViewCell
        
        cell.layer.shouldRasterize = true
        
        cell.layer.rasterizationScale = UIScreen.main.scale
        
        
        if publicInfo.count > 0 {

        
        let allinfo = self.publicInfo[indexPath.row]

        
        let url = NSURL(string: allinfo.picUrl)
        
       
      
        //cell.imgImage.af_imageDownloader?.download([url as! URLRequestConvertible])
        
            cell.imgImage.sd_setShowActivityIndicatorView(true)
            cell.imgImage.sd_setIndicatorStyle(.gray)
            cell.imgImage.sd_setImage(with: url! as URL, placeholderImage: nil, options: .scaleDownLargeImages, completed: nil)
            
            
            

        
        //cell.imgImage = nil
        
        hiddenFloatingButtons()
        }
        else {
            
        }
        if indexPath.row == publicInfo.count - 1 {
            // last cell
            
            if totalItems > publicInfo.count {
                pageNumber = pageNumber + 1
                // more items to fetch
                getRequest() // increment `fromIndex` by 20 before server call
            }
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 3 - 1

        return CGSize(width: width, height: width)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
//    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let offsetY = scrollView.contentOffset.y
//        let contentHeight = scrollView.contentSize.height
//        
//        if offsetY > contentHeight - scrollView.frame.size.height {
//            pageNumber += 1
//            getRequest()
//            
//            self.collectionView.reloadData()
////            hidingView.isHidden = false
////            IJProgressView.shared.showProgressView(view)
////
////            setCloseTimer()
//        }
//    }

 
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let offsetY = scrollView.contentOffset.y
//        let contentHeight = scrollView.contentSize.height
//
//        if offsetY > contentHeight - scrollView.frame.size.height {
//            DispatchQueue.global(qos: .background).async {
//                self.pageNumber += 1
//                self.getRequest()
//
//                DispatchQueue.main.async {
//
//                    self.collectionView?.performBatchUpdates(nil, completion: nil)
//
//
//                    self.collectionView.reloadData()
//
//                }
//            }
    
            //self.collectionView.reloadData()
            
            
//            IJProgressView.shared.showProgressView(view)
//
//            setCloseTimer()
            

//    func close() {
//        IJProgressView.shared.hideProgressView()
//        hidingView.isHidden = true
//
//    }
//
//    func setCloseTimer() {
//        _ = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(close), userInfo: nil, repeats: false)
//
//    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "NewsVCViewController") as! NewsVCViewController
        passData.indexNumber = CGFloat(indexPath.row)
        passData.fromPublic = "true"

        

        //passData.getprofilePicUrl = publicInfo[indexPath.row].
        
        self.navigationController?.pushViewController(passData, animated: true)
        hiddenFloatingButtons()

        searchFieldPhoto.resignFirstResponder()

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

  

}

class DiskCache: URLCache {
    
    
    private let constSecondsToKeepOnDisk = 10*24*60*60 // numDaysToKeep is your own constant or hard-coded value
    
    override func storeCachedResponse(_ cachedResponse: CachedURLResponse, for request: URLRequest) {
        
        var customCachedResponse = cachedResponse
        // Set custom Cache-Control for Image-Response
        let response = cachedResponse.response as! HTTPURLResponse
        
        if let contentType = response.allHeaderFields["Content-Type"] as? String,
            var newHeaders = response.allHeaderFields as? [String: String], contentType.contains("image") {
            newHeaders["Cache-Control"] = "public, max-age=\(constSecondsToKeepOnDisk)"
            if let url = response.url, let newResponse = HTTPURLResponse(url: url, statusCode: response.statusCode, httpVersion: "HTTP/1.1", headerFields: newHeaders) {
                customCachedResponse = CachedURLResponse(response: newResponse, data: cachedResponse.data, userInfo: cachedResponse.userInfo, storagePolicy: cachedResponse.storagePolicy)
            }
        }
        super.storeCachedResponse(customCachedResponse, for: request)
    }
}

