//
//  SettingTerms.swift
//  Glostars
//
//  Created by Sanzid iOS on 6/18/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit

class RegistrationTermVC: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var childView: UIView!
    @IBOutlet weak var achivementButton: UIButton!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var uploadButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
    let backButton : UIBarButtonItem = UIBarButtonItem(image: UIImage(named:"left-arrow"), style: .plain, target: self, action:  #selector(back))
//        let backButton: UIBarButtonItem = UIBarButtonItem(title: "Create Account", style: .plain, target: self, action: #selector(back))
        self.navigationItem.leftBarButtonItem = backButton
        
       
        
        self.navigationItem.title = "Terms Of Use"
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.white,
             NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 20)!]
        
        let heightOfScroll : CGFloat = 1500.0
        
        self.scrollView.contentSize = CGSize(width: self.view.frame.width , height: heightOfScroll)
        // Do any additional setup after loading the view, typically from a nib.
        
        self.childView.frame = CGRect(x: 0, y: 0, width:self.view.frame.width , height: heightOfScroll)
        
        scrollView.contentSize.height = heightOfScroll
      
        
        // Do any additional setup after loading the view.
    }
    func back() {
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLayoutSubviews() {
        
        
        self.view.translatesAutoresizingMaskIntoConstraints = true
        self.scrollView.translatesAutoresizingMaskIntoConstraints = true
        self.childView.translatesAutoresizingMaskIntoConstraints = true
        self.view.needsUpdateConstraints()
        self.scrollView.needsUpdateConstraints()
        self.childView.needsUpdateConstraints()
        
        
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
