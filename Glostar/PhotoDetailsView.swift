//
//  PhotoDetailsView.swift
//  Glostars
//
//  Created by Sanzid iOS on 14/7/18.
//  Copyright © 2018 Sanzid Ashan. All rights reserved.
//

import UIKit
import SDWebImage

class PhotoDetailsView: UIView {
    @IBOutlet var mainView: UIView!
    
    @IBOutlet weak var imageView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadFromXib()
        
        
        
    }
    
    @IBAction func viewCrossAction(_ sender: Any) {
        

        UIView.animate(withDuration: 1, animations: {
            self.mainView.frame.origin.y = 1000

           

            
        }) { (true) in
            self.removeFromSuperview()

            
        }
     
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadFromXib()
        //let nib = UINib(nibName: "TableViewCell", bundle: nil)
        fatalError("init(coder:) has not been implemented")
    }
    
    func loadFromXib() -> Void {
        //NSBundle.mainBundle().loadNibNamed("CustomView", owner: self, options: nil)
        Bundle.main.loadNibNamed("PhotoDetailsView", owner: self, options: nil)
        mainView.frame = self.frame
        self.addSubview(mainView)
        let url = NSURL(string: pictureUrlNotification)
        imageView.sd_setImage(with: url as? URL, completed: nil)
    }
    


}
