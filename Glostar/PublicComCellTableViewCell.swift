

import UIKit

class PublicComCellTableViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var imgImage: UIImageView!
    
    
    @IBOutlet var starCount: UILabel!
    @IBOutlet var starCellButton: UIButton!
    
    var starCellButtonTap: (() -> Void)? = nil
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    
    
    //    override func setSelected(_ selected: Bool, animated: Bool) {
    //        super.setSelected(selected, animated: animated)
    //
    //
    //
    //
    //
    //    }
    
    @IBAction func starCellButton(_ sender: UIButton) {
        
        if let starCellButtonTap = self.starCellButtonTap {
            
            starCellButtonTap()
            
        }
        
        
        
        
    }
}
