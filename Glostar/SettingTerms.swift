//
//  SettingTerms.swift
//  Glostars
//
//  Created by Sanzid iOS on 6/18/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit

class SettingTerms: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var childView: UIView!
    @IBOutlet weak var achivementButton: UIButton!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var uploadButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        hiddenFloatingButtons()
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        let urlString = URL(string: userPic)!
        
        profileButton.af_setBackgroundImage(for: [] , url: urlString )
        
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        
        self.navigationItem.title = "Terms Of Use"
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.white,
             NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 20)!]
        
        let heightOfScroll : CGFloat = 1500.0
        
        self.scrollView.contentSize = CGSize(width: self.view.frame.width , height: heightOfScroll)
        // Do any additional setup after loading the view, typically from a nib.
        
        self.childView.frame = CGRect(x: 0, y: 0, width:self.view.frame.width , height: heightOfScroll)
        
        scrollView.contentSize.height = heightOfScroll
        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
        tap.numberOfTapsRequired = 1
        scrollView.addGestureRecognizer(tap)
        
        // Do any additional setup after loading the view.
    }
    func doubleTapped(){
        
        hiddenFloatingButtons()
        
    }
    override func viewDidLayoutSubviews() {
        
        
        self.view.translatesAutoresizingMaskIntoConstraints = true
        self.scrollView.translatesAutoresizingMaskIntoConstraints = true
        self.childView.translatesAutoresizingMaskIntoConstraints = true
        self.view.needsUpdateConstraints()
        self.scrollView.needsUpdateConstraints()
        self.childView.needsUpdateConstraints()
        
        
    }
    func hiddenFloatingButtons(){
        
        uploadButton.isHidden = true
        achivementButton.isHidden = true
        profileButton.isHidden = true
        notificationButton.isHidden = true
        homeButton.isHidden = true
        
        
    }
    func animatingPopUp()
    {
        UIView.animate(withDuration: 1, animations: {
            self.uploadButton.isHidden =  !self.uploadButton.isHidden
            
        }) { (true) in
            
            UIView.animate(withDuration: 1, animations: {
                self.achivementButton.isHidden = !self.achivementButton.isHidden
                
            }, completion: { (true) in
                UIView.animate(withDuration: 1, animations: {
                    
                    self.profileButton.isHidden = !self.profileButton.isHidden
                    self.homeButton.isHidden = !self.homeButton.isHidden
                    
                }, completion: { (true) in
                    self.notificationButton.isHidden = !self.notificationButton.isHidden
                    //                    let image1 = UIImage(named: "Circleopen")
                    //                    self.floatingMainButton.setBackgroundImage(image1, for: UIControlState.normal)
                    
                    
                    
                    
                })
            })
        }
        
        
        
    }
    
    @IBAction func mainButtonAction(_ sender: Any) {
        
        
        animatingPopUp()
        
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
