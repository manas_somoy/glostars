//
//  GaleryMapper.swift
//  Glostars
//
//  Created by Sanzid Ashan on 5/19/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

//
//  ImageMapper.swift
//  Glostars
//
//  Created by Sanzid Ashan on 5/17/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import Foundation


import Foundation
import ObjectMapper

class GaleryMapperFull : Mappable {
    
    
    dynamic var picUrl : String = ""
    dynamic var photoId : Int = 0
    dynamic var myStarCount : Int = 0
    dynamic var starcount : Int = 0
    dynamic var userId : String = ""
    dynamic var description : String = ""
    dynamic var pictureId : String = ""
    dynamic var totalEditPhoto : Int = 0



    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        picUrl <- map["picUrl"]
        photoId <- map["id"]
        myStarCount <- map["myStarCount"]
        starcount <- map["starsCount"]
        userId <- map["userId"]
        description <- map["description"]
        pictureId <- map["pictureId"]
        totalEditPhoto <- map["totalEditPhoto"]



    }
}

