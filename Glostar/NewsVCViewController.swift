//
//  ViewController.swift
//  Glostar
//
//  Created by Sanzid Ashan on 3/19/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

var starCount = ""
var bottomType: BottomType!
var emojis: [EmojiCategory]?

//  intro for the first time
var notification = ""
var upload = ""
var search = ""
var competition = ""
var profile = ""
var home = ""

var emojiButtonPressed: Bool = false

import UIKit
import Alamofire
import ObjectMapper
import AlamofireImage
import SDWebImage
import YPImagePicker
import Foundation
import ISEmojiView
import iOSPhotoEditor

func color(_ rgbColor: Int) -> UIColor{
    return UIColor(
        red:   CGFloat((rgbColor & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbColor & 0x00FF00) >> 8 ) / 255.0,
        blue:  CGFloat((rgbColor & 0x0000FF) >> 0 ) / 255.0,
        alpha: CGFloat(1.0)
    )
}
enum Colors {
    
    static let red = UIColor(red: 1.0, green: 0.0, blue: 77.0/255.0, alpha: 1.0)
    static let blue = UIColor.blue
    static let green = UIColor(red: 35.0/255.0 , green: 233/255, blue: 173/255.0, alpha: 1.0)
    static let yellow = UIColor(red: 1, green: 209/255, blue: 77.0/255.0, alpha: 1.0)
    
}

enum Images {
    
    static let box = UIImage(named: "Star small 2")!
    static let triangle = UIImage(named: "Star small 2")!
    static let circle = UIImage(named: "Star small 2")!
    static let swirl = UIImage(named: "Star small 2")!
    
}


var picImageUrl = String()
var ArrayItemCount = 0
var PicURL = ""


//public protocol SkeletonTableViewDataSource: UITableViewDataSource {
//    func numSections(in collectionSkeletonView: UITableView) -> Int
//    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int
//    func collectionSkeletonView(_ skeletonView: UITableView, cellIdenfierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier
//}
class NewsVCViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,DZNEmptyDataSetSource, DZNEmptyDataSetDelegate,FloatyDelegate, UITextViewDelegate, EmojiViewDelegate,PhotoEditorDelegate{
    func doneEditing(image: UIImage) {
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "ImageViewController") as! ImageViewController
        passData.getImageData = image
        passData.chooseCtegeory = ""
        passData.copetionPost = String(photoId)
        
        self.navigationController?.pushViewController(passData, animated: true)
    }
    
    func canceledEditing() {
        
        
    }
    
    //    func numSections(in collectionSkeletonView: UITableView) -> Int {
    //        return 1
    //    }
    //
    //    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int {
    //
    //        return 10
    //    }
    //
    //    func collectionSkeletonView(_ skeletonView: UITableView, cellIdenfierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
    //
    //        return "FeedTableViewCell"
    //    }
    
   
    
    @IBOutlet private weak var emojiView: EmojiView! {
        didSet {
            emojiView.delegate = self
        }
    }
    
    
    var commentIdCell = ""
    
    @IBOutlet weak var commentViewTopConstarints: NSLayoutConstraint!
    @IBOutlet var commentsImageView: UIImageView!
    @IBOutlet var commentText: UITextView!
    
    @IBOutlet var commentTextVie: UITextView!
    
    @IBOutlet var postUserImage: UIImageView!
    
    @IBOutlet var tableViewComment: UITableView!
    var CommentsPic : Array<DetailComMapper> = []
    
    var photoId = 0
    var getID = 0
    var getImage = ""
    var fromPublic = ""
    var TitleShow = ""
    var scrollEnable = "True"



    
    
    @IBOutlet var mainView: UIView!
    
    var starCountName = 0
    var starShow = 0
    @IBOutlet var leftSearch: UIBarButtonItem!
    
    var commentCount = [Int]()
    var ListPhoto = [Int]()
    
    @IBOutlet var searchButton: DesignableButton!
    
    
    var pageNumber = 1
    var totalItems = 1000
    
    var Articles : Array<FeedMapper> = []
    var ArticlesInfo : Array<InfoMapper> = []
    var commentInfo : Array< CommentMapper > = []
    var editPhotoArray : Array<EditPhotoMapper> = []

    var idArray = [Int]()
    var indexNumber :CGFloat = 0.0

    
    var fab = Floaty()
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet var commentView: UIView!
    
    
    
    @IBOutlet var sendCommenterView: UIView!
    
    var starCountValue = 0
    
    
    
    // create the data source
    
    @IBOutlet weak var achivementButton: UIButton!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var uploadButton: UIButton!
    
    @IBOutlet weak var floatingMainButton: UIButton!
    
    
    @IBOutlet weak var introView: UIView!
    @IBOutlet weak var thirdIntroView: UIView!
    
    @IBOutlet weak var menuIntroBottomConstraints: NSLayoutConstraint!
    @IBOutlet weak var menuIntroHorizontalConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var floatingMainButtonTrailing: NSLayoutConstraint!
    
    var PublicInfo : Array< PublicFeeedMapper > = []
    var UserInfoArray : Array< UserInfoMapper > = []
    
    @IBOutlet weak var floatingMainButtonBottomCOnstraints: NSLayoutConstraint!
    
    
    
    
    //tableview data source methods
    
    // return how many sections in tableview
    //    func numberOfSections(in tableView: UITableView) -> Int {
    //        return R
    //
    // return title string for each section
    
    
    
    
    @IBAction func barLogo(_ sender: Any) {
        
        
        hiddenFloatingButtons()
        
        
    }
    @IBAction func barButtonLogo(_ sender: Any) {
        
        
        hiddenFloatingButtons()
        
        
        
    }
    
    
    @IBAction func onClinkProfile(_ sender: Any) {
        
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        
        
        passData.getid = UserDefaults.standard.value(forKey: "id") as! String
        
        self.navigationController?.pushViewController(passData, animated: true)
        
        
    }
    
    
    // this method will be called by the table view to know the count of rows for each section
    
    
    //table view delegate methods
    var emitter = CAEmitterLayer()
    
    var colors:[UIColor] = [
        Colors.red,
        Colors.blue,
        Colors.green,
        Colors.yellow
    ]
    
    var images:[UIImage] = [
        Images.box,
        Images.triangle,
        Images.circle,
        Images.swirl
    ]
    
    var velocities:[Int] = [
        100,
        90,
        150,
        200
    ]
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firstTimeLaunch()
        
        
        //tableView.reloadData()
        hiddenFloatingButtons()
        layoutFAB()
        
     
        
        
        tableView.delegate = self
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGesture.minimumPressDuration = 0.5
        longPressGesture.delegate = self as? UIGestureRecognizerDelegate
        self.tableView.addGestureRecognizer(longPressGesture)
        
        //print(UserDefaults.standard.value(forKey: "access") as! String)
        
        commentView.layer.cornerRadius = 16
        sendCommenterView.layer.cornerRadius = 16
        
        
        
        
        commentText.delegate = self
        commentText.text! = "Write a comment"
        commentText.textColor = UIColor.lightGray
        

        
        
        
        
        
    }
    
    func firstTimeLaunch(){
        let firstLaunch = FirstLaunch()
        
        if firstLaunch.isFirstLaunch{
            introView.isHidden = false
            
            UIView.transition(with: view, duration: 1.5, options: .transitionCrossDissolve, animations: {
                
                
                let deviceType = UIDevice().type
                
                switch deviceType {
                case .iPhoneSE,.iPhone5:
                    print("default value")
                case.iPhone5S:
                    self.cutCircle(inView: self.introView, withRect: CGRect(x: UIScreen.main.bounds.width * 0.75, y: UIScreen.main.bounds.height * 0.863 , width: 70, height: 70))
                case .iPhone6:
                    self.cutCircle(inView: self.introView, withRect: CGRect(x: UIScreen.main.bounds.width * 0.78, y: UIScreen.main.bounds.height * 0.885 , width: 70, height: 70))
                case .iPhone6S:
                    print("6")
                case .iPhone7:
                    self.cutCircle(inView: self.introView, withRect: CGRect(x: UIScreen.main.bounds.width * 0.78, y: UIScreen.main.bounds.height * 0.885 , width: 70, height: 70))
                case .iPhone8:
                    self.cutCircle(inView: self.introView, withRect: CGRect(x: UIScreen.main.bounds.width * 0.78, y: UIScreen.main.bounds.height * 0.885 , width: 70, height: 70))
                
                case .iPhoneX:
                    self.cutCircle(inView: self.introView, withRect: CGRect(x: UIScreen.main.bounds.width * 0.78, y: UIScreen.main.bounds.height * 0.863 , width: 70, height: 70))
                    
                case .iPhoneXR:
                    self.cutCircle(inView: self.introView, withRect: CGRect(x: UIScreen.main.bounds.width * 0.795, y: UIScreen.main.bounds.height * 0.878 , width: 70, height: 70))
                case .iPhoneXS:
                   self.cutCircle(inView: self.introView, withRect: CGRect(x: UIScreen.main.bounds.width * 0.78, y: UIScreen.main.bounds.height * 0.863 , width: 70, height: 70))
                case .iPhoneXSMax:
                   self.cutCircle(inView: self.introView, withRect: CGRect(x: UIScreen.main.bounds.width * 0.794, y: UIScreen.main.bounds.height * 0.877 , width: 70, height: 70))
                default:break
                }
                
                
                // menu
               
                
                notification = "Notification"
                upload = "Upload"
                search = "Search"
                competition = "Competition"
                profile = "Profile"
                home = "Home"
                
            })
        }
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(hideSecondIntroGesture))
        introView.isUserInteractionEnabled = true
        introView.addGestureRecognizer(gesture)
    }
    
    
    
    @objc func hideSecondIntroGesture(_ sender: UITapGestureRecognizer){
        UIView.transition(with: view, duration: 1.5, options: .transitionCrossDissolve, animations: {
            //self.view.addSubview(self.introView)
            self.introView.isHidden = true
            self.thirdIntroView.isHidden = false
            
            
            let deviceType = UIDevice().type
            
            switch deviceType {
            case .iPhoneSE,.iPhone5:
                print("default value")
            case.iPhone5S:
                self.cutCircle(inView: self.thirdIntroView,
                               withRect: CGRect(x: self.view.bounds.width * 0.77, y: self.thirdIntroView.frame.height - 174, width: 70, height: 70))
            case .iPhone6:
                self.cutCircle(inView: self.thirdIntroView,
                               withRect: CGRect(x: self.view.bounds.width * 0.805, y: self.view.bounds.height - 273, width: 70, height: 70))
            case .iPhone6S:
                print("6")
                
            case .iPhone7:
                self.cutCircle(inView: self.thirdIntroView,
                               withRect: CGRect(x: self.view.bounds.width * 0.805, y: self.view.bounds.height - 273, width: 70, height: 70))
            case .iPhone8:
                self.cutCircle(inView: self.thirdIntroView,
                               withRect: CGRect(x: self.view.bounds.width * 0.805, y: self.view.bounds.height - 273, width: 70, height: 70))
           
            case .iPhoneX:
                self.cutCircle(inView: self.thirdIntroView,
                               withRect: CGRect(x: self.view.bounds.width * 0.805, y: self.view.bounds.height - 393, width: 70, height: 70))
                
            case .iPhoneXR:
                self.cutCircle(inView: self.thirdIntroView,
                               withRect: CGRect(x: UIScreen.main.bounds.width * 0.82, y: self.view.bounds.height - 475, width: 70, height: 70))
            case .iPhoneXS:
                self.cutCircle(inView: self.thirdIntroView,
                               withRect: CGRect(x: self.view.bounds.width * 0.805, y: self.view.bounds.height - 393, width: 70, height: 70))
            case .iPhoneXSMax:
                self.cutCircle(inView: self.thirdIntroView,
                               withRect: CGRect(x: UIScreen.main.bounds.width * 0.82, y: self.view.bounds.height - 480, width: 70, height: 70))
            default:break
            }
            
            
           // self.cutCircle(inView: self.thirdIntroView, withRect: CGRect(x: self.view.bounds.width * 0.81, y: self.thirdIntroView.frame.height - 390, width: 70, height: 70))
            
            let hideallIntroGesture = UITapGestureRecognizer(target: self, action: #selector(self.hideallIntroGesture))
            self.thirdIntroView.isUserInteractionEnabled = true
            self.thirdIntroView.addGestureRecognizer(hideallIntroGesture)
            
        }
        )
        
    }
    
    @objc func hideallIntroGesture(_ sender: UITapGestureRecognizer){
        UIView.transition(with: view, duration: 1.5, options: .transitionCrossDissolve, animations: {
            self.view.addSubview(self.introView)
            self.thirdIntroView.isHidden = true
        }
        )
        
    }
    
    
    
    
    func cutCircle(inView view: UIView, withRect rect: CGRect) {
        
        // Create new path and mask
        let newMask = CAShapeLayer()
        let newPath = UIBezierPath(ovalIn: rect)
        
        // Create path to clip
        //let newClipPath = UIBezierPath(rect: UIScreen.main.bounds)
        let newClipPath = UIBezierPath(rect: mainView.bounds)
        newClipPath.append(newPath)
        
        // If view already has a mask
        if let originalMask = view.layer.mask,
            let originalShape = originalMask as? CAShapeLayer,
            let originalPath = originalShape.path {
            
            // Create bezierpath from original mask's path
            let originalBezierPath = UIBezierPath(cgPath: originalPath)
            
            // Append view's bounds to "reset" the mask path before we re-apply the original
            //newClipPath.append(UIBezierPath(rect: view.bounds))
            newClipPath.append(UIBezierPath(rect: UIScreen.main.bounds))
            
            // Combine new and original paths
            newClipPath.append(originalBezierPath)
            
        }
        
        // Apply new mask
        newMask.path = newClipPath.cgPath
//        let evenOdd: CAShapeLayerFillRule
        newMask.fillRule = kCAFillRuleEvenOdd
            //CAShapeLayerFillRule.evenOdd
        view.layer.mask = newMask
        
        
    }
    
//}
    
    // for first time intro
    
    
    
    
    func emojiViewDidSelectEmoji(_ emoji: String, emojiView: EmojiView) {

        commentText.text = ""
        commentText.insertText(emoji)
        print(commentText.text!)
    }

    func emojiViewDidPressChangeKeyboardButton(_ emojiView: EmojiView) {
        commentText.inputView = nil
        commentText.keyboardType = .default
        commentText.reloadInputViews()
    }

    func emojiViewDidPressDeleteBackwardButton(_ emojiView: EmojiView) {
        commentText.deleteBackward()
    }

    func emojiViewDidPressDismissKeyboardButton(_ emojiView: EmojiView) {
        commentText.resignFirstResponder()
    }
    
//    override func viewDidLayoutSubviews() {
//        //print("a")
//       // tableView.reloadData()
//        tableViewComment.reloadData()
//    }

    func layoutFAB() {
        let item = FloatyItem()
        item.buttonColor = UIColor(displayP3Red: 1, green: 1, blue: 1, alpha: 0.7)
        //item.circleShadowColor = UIColor.red
        //item.titleShadowColor = UIColor.blue
        
        //fab.addItem(title: "I got a title")
        //fab.addItem("I got a icon", icon: UIImage(named: "icShare"))
        fab.addItem(notification, icon: UIImage(named: "notif+"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            notification = ""
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem(upload, icon: UIImage(named: "photo-camera"), titlePosition: .left) { (item) in
            
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "FAImageCropperVC") as! FAImageCropperVC
            
            self.navigationController?.pushViewController(passData, animated: true)
            
            
            
            self.fab.close()
        }
        fab.addItem(search, icon: UIImage(named: "search-1"), titlePosition: .left) { (item) in
            
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem(competition, icon: UIImage(named: "competiton"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "GaleryViewController") as! GaleryViewController
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem(profile, icon: UIImage(named: "Profile_male"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            passData.getid = userID
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem(home, icon: UIImage(named: "home+"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "NewsVCViewController") as! NewsVCViewController
            
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        
        // For Image
        
        
        self.fab.sticky = true
        self.fab.paddingX = self.view.frame.width/20
        
        self.fab.fabDelegate = self
        
        print(tableView.frame)
        
        self.view.addSubview(self.fab)
    }

    
    override func viewWillAppear(_ animated: Bool) {
        
        notification = ""
        upload = ""
        search = ""
        competition = ""
        profile = ""
        home = ""
        //tableView.reloadData()
        
        if fromPublic == "true"{
            
            print(indexNumber)
            pageNumber = Int(ceil((indexNumber/10)))
            
            //print(indexNumber/10)
            //print(Int(ceil((indexNumber/10))))
            print(pageNumber)
            
            
            if indexNumber >= 10{
                
                indexNumber = CGFloat(Int(indexNumber) % 10)
                
                print(indexNumber)
                
            }
        }
        
        
        self.navigationController?.navigationBar.isHidden = true
        self.inputViewController?.dismissKeyboard()
        emojiButtonPressed = false
        self.view.removeBlurEffect()
        if self.fromPublic == "true"{
            self.getRequesPublic()
            
        }
        else{
            self.getRequest()
            
        }
        
        commentView.isHidden = true
        
       // postUserImage.setRounded()
        tableViewComment.emptyDataSetSource = self
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.view.backgroundColor = .clear
        
        
        
        
        //        if (UserDefaults.standard.value(forKey: "star"))as? String != nil{
        //
        //            UserDefaults.value(forKey: "star")
        //
        //        }
        hiddenFloatingButtonsScrool()
        self.navigationController?.isNavigationBarHidden = true
        commentText.layer.borderWidth = 1.0
        commentText.layer.borderColor = UIColor.black.cgColor
        commentText.layer.cornerRadius = 8.0
        commentText.textContainer.lineBreakMode = .byTruncatingTail
        
        if userPic == "/Content/Profile/Thumbs/   Male.jpg"{
            
            profileButton.setBackgroundImage(UIImage(named:"male"), for: .normal)
           // postUserImage.image = UIImage(named: "male")
            
            
        }
        else if userPic == "/Content/Profile/Thumbs/   Female  .jpg"{
            
            profileButton.setBackgroundImage(UIImage(named:"female"), for: .normal)
           // postUserImage.image = UIImage(named: "female")
            
        }
            
            
        else{
            let urlString = URL(string:String(describing: UserDefaults.standard.value(forKey: "pic")!))
            //profileButton.af_setBackgroundImage(for: [] , url: urlString! )
           // postUserImage.af_setImage(withURL: urlString!)
            
            
        }
        
        
        floatingMainButton.setRounded()
        notificationButton.setRounded()
        uploadButton.setRounded()
        achivementButton.setRounded()
        homeButton.setRounded()
        profileButton.setRounded()
        searchButton.setRounded()
        
        

        
    }
    
    
    @IBAction func commentBackAction(_ sender: Any) {
       // print(commentCount)
        self.view.removeBlurEffect()
        self.commentViewTopConstarints.constant = 1000.00
        UIView.animate(withDuration: 1, animations: {
            
            self.view.layoutIfNeeded()
            
        }, completion: { (true) in
            
            self.commentView.isHidden = true
            if self.fromPublic == "true"{
                self.getRequesPublic()

            }
            else{
                self.getRequest()

            }
            
            self.tableView.reloadData()
            
        })
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "NewsVCViewController") as! NewsVCViewController
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionReveal
            //kCATransitionFromBottom
        view.window!.layer.add(transition, forKey: kCATransition)
        
        //self.navigationController?.pushViewController(passData, animated: true)
        self.navigationController?.present(passData, animated: true, completion: nil)
        self.fab.close()
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        
        let text = NSAttributedString(string: "No Comments")
        tableViewComment.separatorColor = UIColor.clear
        return text
        
    }
    
    @IBAction func sendCommentAction(_ sender: Any) {
        
//        if self.commentText.text != "" || self.commentText.text != "Write a comment"{
//        if self.commentText.text == "Nice"{
//            (0...10).forEach { (_) in
//                self.generateAnimatedViewsComments()
//            }
//        }
        
        if self.commentText.text != "" {
            if self.commentText.text != "Write a comment"{
                let parameters: [String : Any]? = [
                    "CommentText": commentText.text!,
                    "photoId": getID ,
                    ]
                let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/comment"
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json"
                ]
                Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
                    switch response.result {
                    case .success:
                        print(response)
                        self.CommentsPic.removeAll()
                        self.getImageInfo()
                        self.commentText.resignFirstResponder()
                    case .failure(let error):
                        print(error)
                    }
                }
            }
            //}
        }
        
        
        commentText.text = "Write a comment"
        commentText.textColor = UIColor.lightGray
       
        
    }
    
    func getRequesPublic(){
        
        
        
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/public/\(pageNumber)"
        
        Alamofire.request(url).responseJSON { response in
            
            if let jsonRoot = response.result.value as? [String:Any]!{
                //print(jsonRoot)
                if response.result.value != nil {
                    
                    if let resultPayload = jsonRoot!["resultPayload"] as? [String:Any]!{
                        
                        print(resultPayload)
                        
                        if let picReturn = resultPayload!["picsToReturn"] as? [[String:Any]]!{
                            
                            for data in picReturn! {
                                print(data)
                                
                                guard  let imageInfo = Mapper<PublicFeeedMapper>().map(JSON: data) else {
                                    
                                    
                                    continue
                                    
                                }
                                
                                self.PublicInfo.append(imageInfo)
                            }
                            
                            for dic in picReturn! {
                                
                                print(dic)
                                if let poster = dic["poster"] as? [String:Any]!{
                                    
                                    guard  let userinfo = Mapper<UserInfoMapper>().map(JSON: poster!) else {
                                        
                                        
                                        continue
                                        
                                    }
                                    self.UserInfoArray.append(userinfo)
                                    
                                }
                                if let comments = dic["comments"] as![[String : Any]]! {
                                    
                                    self.commentCount.append(comments.count)
                                    
                                    
                                    for array in comments {
                                        //print(comments)
                                        
                                        
                                        
                                        //print(self.commentArray)
                                        
                                        guard let comments = Mapper<CommentMapper>().map(JSON: array) else{
                                            
                                            continue
                                        }
                                        
                                        self.commentInfo.append(comments)
                                        
                                    }
                                    
                                }
                                if let comments = dic["editPhotos"] as![[String : Any]]! {
                                    
                                    
                                    
                                    for array in comments {
                                        //print(comments)
                                        
                                        
                                        
                                        //print(self.commentArray)
                                        
                                        guard let comments = Mapper<EditPhotoMapper>().map(JSON: array) else{
                                            
                                            continue
                                        }
//                                        self.editPhotoArray.removeAll()
                                        self.editPhotoArray.append(comments)
                                        
                                    }
                                    
                                }
                                
                                
                                
                                
                                
                                
                            }
                            
                            
                            //print(picReturn)
                            
                        }
                        
                        //print(resultPayload)
                        
                        
                    }
                    
                    
                    
                    self.tableView.reloadData()
                    print(Int(self.indexNumber))
                    
                if  self.scrollEnable == "True"{
                        
                        self.tableView.scrollToRow(at: IndexPath.init(row: Int(self.indexNumber), section: 0), at: .top, animated: true)
                    }
                    
                }
                
            }
                
            else {
                
                print("Network Error ")
                
            }
            
            
            
            
        }
        
    }
    
    
    func BackButtonClick(_ title:String,message:String){
        
        let alertController = UIAlertController(title: title, message: message , preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            print("OK Pressed")
            
            let DeletePicUrl = "https://testglostarsdevelopers.azurewebsites.net/api/images/delete/\(self.photoId)"
            print(self.photoId)
            Alamofire.request(DeletePicUrl, method: .post).responseJSON { response in
                
                switch response.result {
                    
                case .success:
                    print(response)
                    
                    let nextViewController =                                  self.storyboard!.instantiateViewController(withIdentifier:"NewsVCViewController")
                    
                    let nvc = UINavigationController(rootViewController: nextViewController)
                    self.present(nvc, animated: false, completion: nil)
                    
                case .failure(let error):
                    print(error)
                }
                
            }
            
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
            print("Cancel Pressed")
            //self.dismiss(animated: true, completion: nil)
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    func handleLongPress(longPressGesture:UILongPressGestureRecognizer) {
        
        
        
        
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        navigationController?.navigationBar.isHidden = false
        
    }
    
    func getRequest(){
        
        let parameter = [
            
            "ListPhoto": ListPhoto,
            
            ]
        
        
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/mutualpic/\(String(describing: UserDefaults.standard.value(forKey: "id")!))/\(pageNumber)"
        print(url)
        
        
        
        //        if (UserDefaults.standard.value(forKey: "access"))as? String == nil {
        
        let headers: HTTPHeaders = [
            
            //            "Authorization": tokenType + accessToken ,
            //            "Content-Type": "application/json",
            
            
            "Authorization": String(describing: UserDefaults.standard.value(forKey: "id")!) + String(describing: UserDefaults.standard.value(forKey: "access")!),
            "Content-Type": "application/json",
            
            ]
        
        Alamofire.request(url, method:.post, parameters : parameter,encoding:JSONEncoding.default,headers:headers).responseJSON { response in
            
            
            if response.result.value != nil{
                
                if let jsonRoot = response.result.value as? [String:Any]!{
                    
                    //print(jsonRoot)
                    if response.result.value != nil{
                        if let resultPayload = jsonRoot!["resultPayload"] as? [String: Any]!{
                            
                            let count = resultPayload!["count"] as! Int
                            print(count)
                            //self.totalItems = count
                        
                            
                            // print(jsonRoot)
                            
                            if let data = resultPayload!["data"] as! [[String:Any]]!{
                                
                                
                                print(data)
                                
                         
                                
                                
                                for array in data {
                                    
                                    
                                    if let comments = array["editPhotos"] as![[String : Any]]! {
                                        
                                        //print(comments)
                                        
                                        
                                        
                                        for array in comments {
                                            print(comments)
                                            
                                            
                                            
                                            guard let comments = Mapper<EditPhotoMapper>().map(JSON: array) else{
                                                
                                                continue
                                            }
                                            
                                            self.editPhotoArray.append(comments)
                                            
                                        }
                                        
                                    }
                                    
                                    
                                    //print(data.count)
                                    if let userName = array["poster"] as! [String:Any]!{
                                        
                                        print(userName)
                                        
                                        guard  let article1 = Mapper<InfoMapper>().map(JSON: userName) else {
                                            
                                            continue
                                            
                                            
                                        }
                                        self.ArticlesInfo.append(article1)
                                        
                                        
                                        
                                        
                                        
                                        
                                        guard  let article2 = Mapper<FeedMapper>().map(JSON: array) else {
                                            
                                            
                                            
                                            continue
                                            
                                            
                                        }
                                        self.Articles.append(article2)
                                        
                                        //                                        print(self.ListPhoto)
                                        //                                        self.ListPhoto.removeAll()
                                        self.ListPhoto.append(article2.idPhoto)
                                        
                                        //self.tableView.reloadData()
                                        
                                    }
                                    
                                    
                                    //self.Articles.append(article1)
                                    if let rating = array["ratings"] as![[String:Any]]!{
                                        
                                        // print(rating)
                                        
                                        
                                        
                                        if let comments = array["comments"] as![[String : Any]]! {
                                            
                                            self.commentCount.append(comments.count)
                                            
                                            
                                            for array in comments {
                                                //print(comments)
                                                
                                                
                                                
                                                //print(self.commentArray)
                                                
                                                guard let comments = Mapper<CommentMapper>().map(JSON: array) else{
                                                    
                                                    continue
                                                }
                                                
                                                self.commentInfo.append(comments)
                                                
                                            }
                                            
                                        }
                               
                                        
                                        
                                    }
                          
                                    
                                    
                                    
                                    
                                }
                                
                                
                                
                                
                            }
                        }
                        
                        //                        self.pageNumber = self.pageNumber + 1
                        
                        
                    }
                    else {
                        
                        print("Network error")
                        if self.fromPublic == "true"{
                            self.getRequesPublic()
                            
                        }
                        else{
                            self.getRequest()
                            
                        }
                        
                        
                        
                    }
                    //print(jsonRoot)
                    
                    
                    
                }
                
                
            }
            else {
                
                print("Internet Error")
                if self.fromPublic == "true"{
                    self.getRequesPublic()
                    
                }
                else{
                    self.getRequest()
                    
                }
                
                
            }
            
            self.tableView.reloadData()
            for subViews in self.tableView.visibleCells {
                let bottomAnimation = AnimationType.from(direction: .bottom, offSet: 30.0)
                subViews.contentView.animateAll(withType: [bottomAnimation])
            }
            
        }
        
        
        
    }
    
    @IBAction func notificationAction(_ sender: Any) {
        
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        notification = ""
        upload = ""
        
        
        self.navigationController?.pushViewController(passData, animated: true)
        
    }
    
    
    
    func stopRequest(){
        
        let sessionManager = Alamofire.SessionManager.default
        sessionManager.session.getTasksWithCompletionHandler {
            dataTasks, uploadTasks, downloadTasks in dataTasks.forEach {
                $0.cancel()
            }
            uploadTasks.forEach { $0.cancel() }
            downloadTasks.forEach { $0.cancel() } }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.tableViewComment{
            
            
            return CommentsPic.count
            
        }
        else{
            
            if self.commentInfo.count < 1 {
                
                
                
            }
            
            // print(commentArray.underestimatedCount)
            if fromPublic == "true"{
                return self.PublicInfo.count

            }
            else{
            return self.ArticlesInfo.count
            }
            
            
            
            
            // return Articles.count + 1
            
        }
    }

    
    func getImageInfo (){
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/picture/\(getID)"
        let headers: HTTPHeaders = [
            
            "Authorization": tokenType + accessToken ,
            "Content-Type": "application/json",
            
            ]
        
        Alamofire.request(url, method:.get,encoding:JSONEncoding.default,headers:headers).responseJSON { response in
            
            
            if response.result.value != nil {
                
                if let jsonRoot = response.result.value as? [String:Any]!{
                    
                    print(jsonRoot)
                    
                    
                    if let ResultPayload = jsonRoot!["resultPayload"] as? [String:Any]!{
                        
                        //print(ResultPayload)

                        if let comments = ResultPayload!["comments"] as! [[String: Any]]!{
                            
                            //print(comments)
                            
                            
                            
                            for array in comments{
                                
                                print(array)
                                
                                guard let allcomments = Mapper<DetailComMapper>().map(JSON: array) else{
                                    
                                    continue
                                }
                                
                                self.CommentsPic.append(allcomments)
                                print(self.CommentsPic)
                                self.CommentsPic.reverse()

                            }
                            
                            
                        }
                        
                        
                        if let poster = ResultPayload!["poster"] as? [String:Any]!{
                            

                        }
                        
                    }
                    
                    
                }
                self.tableViewComment.reloadData()
            }
            else {
                
                print("Internet error")
                
            }
            
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.tableViewComment{
            
            
        }
        else{
            
        //print(editPhotoArray[indexPath.row].id)

//        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        let passData = MainStoryboard.instantiateViewController(withIdentifier: "EditPhotoVC") as! EditPhotoVC
//        passData.editInfo = [editPhotoArray[indexPath.row]]
//        self.present(passData, animated: true, completion: nil)
            
        }
        func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
            let more = UITableViewRowAction(style: .normal, title: "More") { action, index in
                print("more button tapped")
            }
            more.backgroundColor = .lightGray
            
            let favorite = UITableViewRowAction(style: .normal, title: "Favorite") { action, index in
                print("favorite button tapped")
            }
            favorite.backgroundColor = .orange
            
            let share = UITableViewRowAction(style: .normal, title: "Share") { action, index in
                print("share button tapped")
            }
            share.backgroundColor = .blue
            
            return [share, favorite, more]
        }
        func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
            return true
        }
        
        
    }
    
    // this method will be called by tableview for as many cells are there in the beginning and max times is rows count
    // this method creates the cell object and configures the cell object with data and then returns the cell object
    // get the cell for all rows in every section
    
    var  imageValue = ""
    func DeleteComment(_ title:String,message:String){
        
        let alertController = UIAlertController(title: title, message: message , preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            print("OK Pressed")
            
            let DeleteComment = "https://testglostarsdevelopers.azurewebsites.net/api/images/DeleteComment?commentId=\(self.commentIdCell)"
            //print(allComments.commentId)
            
            Alamofire.request(DeleteComment, method: .get).responseJSON { response in
                
                switch response.result {
                    
                case .success:
                    print(response)
                    self.CommentsPic.removeAll()
                    self.getImageInfo()
                    // self.performSegue(withIdentifier: "News", sender: NewsVCViewController.self)
                    
                    
                case .failure(let error):
                    print(error)
                }
                
            }
            
            
            
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
            print("Cancel Pressed")
            //self.dismiss(animated: true, completion: nil)
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if tableView == self.tableViewComment{
            let Cell = tableViewComment.dequeueReusableCell(withIdentifier: "commentCell", for: indexPath) as! CommentCell
            
            Cell.selectionStyle = .none
            
            Cell.commentMsg.isEditable = false
            
            Cell.commentMsg.translatesAutoresizingMaskIntoConstraints = false
            Cell.commentMsg.isScrollEnabled = false
            tableViewComment.separatorColor = UIColor.clear
            
            let allComments = self.CommentsPic[indexPath.row]
            
            let urlImage = NSURL(string: allComments.userproPic )
            Cell.backView.layer.cornerRadius = 10.0;
            Cell.fontView.roundCorners([.topRight, .bottomRight], radius: 10)
            
            Cell.commentMsg.text = allComments.comments
            Cell.userImage.sd_setImage(with: urlImage! as URL, placeholderImage: nil , options: .scaleDownLargeImages, completed: nil)
            Cell.username.text = allComments.userName + " " + allComments.lastName
            

            
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            if let date = dateFormatter.date(from: allComments.uploadTime ){
                
                Cell.uploadTime.text = timeAgoSinceDate(date, currentDate: Date(), numericDates: true)
            }
            
            //Cell.userLastName.text = allComments.lastName
            
            Cell.onProfileTapped = {
                
                let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                passData.getid = allComments.commenterId
                self.navigationController?.pushViewController(passData, animated: true)
                
                
            }
            Cell.onProfileTapped1 = {
                
                self.commentIdCell = String(allComments.commentId)
                self.DeleteComment("Warning", message: "Do you want to Delete your Comment")
                
            }
            
        
            
            return Cell
        }
        else{
            
            let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FeedTableViewCell

            
            if fromPublic == "true"{
                
                // create the table view cell design from the prototype cell we have created in storyboard. Reuseidentifier will have same string as in storyboard

                //tableViewCell.contentView.superview?.clipsToBounds = false
                //tableView.rowHeight = UITableViewAutomaticDimension
                //            let article2 = self.Articles[indexPath.row]
                //            let article1 = self.ArticlesInfo[indexPath.row]
                //            //idArray = [article2.idPhoto]
                //            tableViewCell.deleteButton.isHidden = true
                
                
                //print(idArray)
                // let article3 = self.commentInfo[indexPath.row]
                
                let allinfo = self.PublicInfo[indexPath.row]
                let userinfo = self.UserInfoArray[indexPath.row]
                
                //let UserInfo = self.UserInfoArray[indexPath.row]
                
                
                
                let url = NSURL(string: allinfo.picUrl)
                let url2 = NSURL(string: userinfo.profilePicURL)
                print(url2!)
                
                // let urlUser = NSURL(string: UserInfo.profilePicURL)
                
                tableViewCell.startCount.text =  allinfo.starcount.description
                
                if  userinfo.profilePicURL == "/Content/Profile/Thumbs/   Male.jpg"{
                    
                    tableViewCell.proPic.image = UIImage(named: "male")
                    
                    
                }
                else if  userinfo.profilePicURL == "/Content/Profile/Thumbs/   Female  .jpg"{
                    
                    tableViewCell.proPic.image = UIImage(named: "female")
                    
                }
                else{
                    
                    tableViewCell.proPic.sd_setImage(with: url2! as URL, placeholderImage: nil , options: .scaleDownLargeImages, completed: nil)
                }
                let dateFormatter = DateFormatter()
                
                
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                
                dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                
                if let  date = dateFormatter.date(from: allinfo.uploadedTime) {
                    tableViewCell.uploadedTime.text = timeAgoSinceDate(date, currentDate: Date(), numericDates: true)
                }
                tableViewCell.deleteButton.isHidden = true
                
                if self.PublicInfo[indexPath.row].featuredPicture == true {
                    
                    print("fretured")
                    tableViewCell.heightConstraintFeature.constant = 0.0
                    tableViewCell.featureTextOutlet.alpha = 0
                    tableViewCell.featuredImageOutlet.alpha = 0
                    tableViewCell.heightConstraintsFooterFeatureView.constant = 0.0
                    
                    tableViewCell.seeAllPicOutlet.alpha = 0
                    
                    
                }
                else {
                    
                    tableViewCell.heightConstraintFeature.constant = 00.00
                    tableViewCell.featureTextOutlet.alpha = 0
                    tableViewCell.featuredImageOutlet.alpha = 0
                    tableViewCell.heightConstraintsFooterFeatureView.constant = 00.00
                    tableViewCell.seeAllPicOutlet.alpha = 0
                    
                    
                    
                    
                }
                
                if self.PublicInfo[indexPath.row].isCompeting == true  {
                    
                    tableViewCell.privacyPic.image = UIImage(named: "privacy_competition_photo")
                    //tableViewCell.start1Outlet.isHidden = true
                    tableViewCell.startCount.isHidden = true
                    tableViewCell.commentCount.isHidden = true
                    tableViewCell.descriptionLabel.text = "See All Competition Pictures"
                    tableViewCell.commentButtonOutlet.isHidden = true
                    tableViewCell.proPic.isHidden = true
                    tableViewCell.nameUser.isHidden = true
                    tableViewCell.editButton.isHidden = false
                    tableViewCell.paintCount.isHidden = true
                    
                    
                    
                    
                }
                
                
                if self.PublicInfo[indexPath.row].privacyPublic == "public" && self.PublicInfo[indexPath.row].isCompeting == false {
                    
                    tableViewCell.privacyPic.image = UIImage(named: "Public w-shadow")
                    
                    tableViewCell.start1Outlet.isHidden = false
                    tableViewCell.startCount.isHidden = false
                    tableViewCell.commentCount.isHidden = false
                    tableViewCell.descriptionLabel.text = PublicInfo[indexPath.row].description
                    tableViewCell.commentButtonOutlet.isHidden = false
                    tableViewCell.proPic.isHidden = false
                    tableViewCell.nameUser.isHidden = false
                    tableViewCell.editButton.isHidden = false
                    tableViewCell.paintCount.isHidden = false
                    
                    
                    
                }
                
                if self.PublicInfo[indexPath.row].privacyFriend == "friends"  {
                    
                    tableViewCell.privacyPic.image = UIImage(named: "Mutual w-shadow")
                    
                    tableViewCell.start1Outlet.isHidden = false
                    tableViewCell.startCount.isHidden = false
                    tableViewCell.commentCount.isHidden = false
                    tableViewCell.descriptionLabel.text = Articles[indexPath.row].description
                    tableViewCell.commentButtonOutlet.isHidden = false
                    tableViewCell.proPic.isHidden = false
                    tableViewCell.nameUser.isHidden = false
                    tableViewCell.paintCount.isHidden = false
                    
                    
                }
                
                if userID == self.UserInfoArray[indexPath.row].userid{
                    
                    
                    tableViewCell.deleteButton.isHidden = true
                    tableViewCell.editButton.isHidden = true
                    
                }
                if  self.PublicInfo[indexPath.row].isCompeting == true {
                    
                    
                }
                if allinfo.myStarCount == 0 {
                    
                    let image = UIImage(named: "Star small 2") as UIImage?
                    
                    
                    tableViewCell.start1Outlet.setImage(image, for: .normal)
                    self.imageValue = ""
                    
                    
                }
                
                if allinfo.myStarCount == 5 {
                    
                    let image = UIImage(named: "Star filled w-shadow") as UIImage?
                    self.imageValue = "Checked"
                    
                    
                    tableViewCell.start1Outlet.setImage(image, for: .normal)
                    
                }
                if allinfo.myStarCount == 4 {
                    
                    let image = UIImage(named: "Star filled w-shadow") as UIImage?
                    self.imageValue = "Checked"
                    
                    
                    tableViewCell.start1Outlet.setImage(image, for: .normal)
                    
                }
                if allinfo.myStarCount == 3 {
                    
                    let image = UIImage(named: "Star filled w-shadow") as UIImage?
                    self.imageValue = "Checked"
                    
                    
                    tableViewCell.start1Outlet.setImage(image, for: .normal)
                    
                }
                if allinfo.myStarCount == 2 {
                    
                    let image = UIImage(named: "Star filled w-shadow") as UIImage?
                    self.imageValue = "Checked"
                    
                    
                    tableViewCell.start1Outlet.setImage(image, for: .normal)
                }
                if allinfo.myStarCount == 1 {
                    
                    let image = UIImage(named: "Star filled w-shadow") as UIImage?
                    self.imageValue = "Checked"
                    
                    
                    tableViewCell.start1Outlet.setImage(image, for: .normal)
                }
                
                
                //cell.imgImage.af_imageDownloader?.download([url as! URLRequestConvertible])
                
                tableViewCell.cellPhoto.sd_setShowActivityIndicatorView(true)
                tableViewCell.cellPhoto.sd_setIndicatorStyle(.gray)
                tableViewCell.cellPhoto.sd_setImage(with: url! as URL, placeholderImage: nil, options: .scaleDownLargeImages, completed: nil)
                
                //tableViewCell.descriptionLabel.text = allinfo.description
                //tableViewCell.proPic.af_setImage(withURL: url2! as URL)
                tableViewCell.nameUser.text = userinfo.name
                tableViewCell.paintCount.text = String(allinfo.totalEditPhoto)
                tableViewCell.commentCount.text = commentCount[indexPath.row].description

                tableViewCell.onDeleteTap = {
                    
                    self.photoId = allinfo.idPhoto
                    self.BackButtonClick("Warning", message: "Are you sure you want to delete this Photo")
                    print("Delete")
                    
                }
                tableViewCell.onReport = {
                    
                    
                    
                    let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                    if userID == userinfo.userid{
                        self.TitleShow = "Delete"
                    }
                    else{
                        self.TitleShow = "Report"
                    }
                    
                    
                    // create an action
                    let firstAction: UIAlertAction = UIAlertAction(title: self.TitleShow, style: .default) { action -> Void in
                        
                        
                        if userID == userinfo.userid{
                            
                            self.photoId = allinfo.idPhoto
                            self.BackButtonClick("Warning", message: "Are you sure you want to delete this photo?")
                            print("Delete")
                        }
                        else{
                            
                            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ReportPhoto") as! ReportPhoto
                            
                            self.navigationController?.pushViewController(passData, animated: true)
                        }
                        
                        
                        
                    }
                    
                    let secondAction: UIAlertAction = UIAlertAction(title: "Block this user", style: .default) { action -> Void in
                        
                        print("Second Action pressed")
                    }
                   
                    let thirdAction: UIAlertAction = UIAlertAction(title: "Share on Facebook", style: .default) { action -> Void in
                        
                        print("Share Photo")
                        // image to share
                        let text = allinfo.description
                        let image = tableViewCell.cellPhoto.image
                        let myWebsite = NSURL(string:"https://testglostarsdevelopers.azurewebsites.net/Home/ViewPicture/\(allinfo.idPhoto)")
                        let shareAll = [text , image!,myWebsite] as [Any]
                        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
                        activityViewController.popoverPresentationController?.sourceView = self.view
                        self.present(activityViewController, animated: true, completion: nil)
                        
                        
                    }
                    
                    
                    let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
                    
                    // add actions
                    //actionSheetController.addAction(secondAction)
                    actionSheetController.addAction(thirdAction)
                    actionSheetController.addAction(firstAction)
                    actionSheetController.addAction(cancelAction)
                    
                    // present an actionSheet...
                    self.present(actionSheetController, animated: true, completion: nil)
                }
                
                
                tableViewCell.onEditTap = {
                    let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let passData = MainStoryboard.instantiateViewController(withIdentifier: "EditImageVC") as! EditImageVC
                    passData.imagePic = tableViewCell.cellPhoto.image!
                    passData.photoID = allinfo.idPhoto
                    self.navigationController?.pushViewController(passData, animated: true)
//                    self.photoId = allinfo.idPhoto
//
//                    let photoEditor = PhotoEditorViewController(nibName:"PhotoEditorViewController",bundle: Bundle(for: PhotoEditorViewController.self))
//
//                    //PhotoEditorDelegate
//                    photoEditor.photoEditorDelegate = self
//                    //The image to be edited
//                    photoEditor.image = tableViewCell.cellPhoto.image
//
//                    //Stickers that the user will choose from to add on the image
//                    for i in 0...17 {
//                        photoEditor.stickers.append(UIImage(named: i.description )!)
//                    }
//                    //Optional: To hide controls - array of enum control
//                    photoEditor.hiddenControls = [.crop, .share]
//
//                    //Optional: Colors for drawing and Text, If not set default values will be used
//                    //            photoEditor.colors = [.r]
//
//                    //Present the View Controller
//                    self.present(photoEditor, animated: true, completion: nil)
                    
                    
                }
                
                
                
                
                
                
                
                tableViewCell.onStarTapped1 = {
                    
                    let imagevanish = UIImage(named: "Star filled w-shadow") as UIImage?
                    
                    
                    
                    
                    
                    
                    if self.imageValue == "Checked" {
                        
                        
                        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/removerate/\(self.PublicInfo[indexPath.row].idPhoto)"
                        //            let headers: HTTPHeaders = [
                        //                "Content-Type": "application/json"
                        //            ]
                        Alamofire.request(url, method:.post,encoding: URLEncoding.default).responseJSON { response in
                            switch response.result {
                            case .success:
                                
                                let imagevanish = UIImage(named: "Star small 2") as UIImage?
                                
                                
                                
                                
                                
                                tableViewCell.start1Outlet.setImage(imagevanish, for: .normal)
                                
                                print(response)
                                self.imageValue = ""
                                // self.ArticlesInfo.removeAll()
                                
                                // self.getRequest()
                                
                                
                                // self.tableView.reloadData()
                                tableViewCell.startCount.text =  allinfo.starcount.description
                                
                            case .failure(let error):
                                print(error)
                            }
                            
                            
                        }
                        
                        
                        
                        
                        
                    }
                    else{
                        
                        
                        let parameters: [String : Any]? = [
                            "NumOfStars": 1 ,
                            "photoId": self.PublicInfo[indexPath.row].idPhoto ,
                            ]
                        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/rating"
                        let headers: HTTPHeaders = [
                            "Content-Type": "application/json"
                        ]
                        Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
                            
                            if let jsonRoot = response.result.value as? [String:Any]!{
                                
                                //print(jsonRoot)
                                
                                if let resultPayload = jsonRoot!["resultPayload"] as? [String: Any]!{
                                    
                                    print(resultPayload)
                                    
                                    tableViewCell.startCount.text =  String(describing: resultPayload!["totalRating"] as! Int)
                                    print(tableViewCell.startCount.text!)
                                    
    
                                    self.imageValue = "Checked"
                                    
                                }
                                
                                let image = UIImage(named: "Star filled w-shadow") as UIImage?
                                
                                
                                
                                
                                tableViewCell.start1Outlet.setImage(image, for: .normal)
                                
                            }
                            
                            
                            
                            
                            
                            
                        }
                        //self.getRequest()
                    }
                    
                    
                }
                
                tableViewCell.onProfileTapped = {
                    //self.stopRequest()
                    
                    let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                    
                    
                    passData.getid = self.UserInfoArray[indexPath.row].userid
                    print(self.UserInfoArray[indexPath.row].userid)
                    
                    
                    
                    
                    self.navigationController?.pushViewController(passData, animated: true)
                    
                }
                
                
                //tableViewCell.nameUser.text = UserInfo
                // .name
                //tableViewCell.proPic.sd_setImage(with: urlUser! as URL, placeholderImage: nil, options: .scaleDownLargeImages, completed: nil)
                
                tableViewCell.onCommentsTapped = {
                    //                self.photoId = allinfo.idPhoto
                    //                self.commentView.isHidden = false
                    //                self.CommentsPicArray.removeAll()
                    //                self.getImageInfo()
                    //
                    //                self.tableViewComment.isHidden = false
                    //self.commentText.becomeFirstResponder()
                    let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let passData = MainStoryboard.instantiateViewController(withIdentifier: "CommentPostVC") as! CommentPostVC
                    passData.getID = allinfo.idPhoto
                    passData.getImage = allinfo.picUrl
                    passData.photoId = allinfo.idPhoto
                    
                    self.navigationController?.pushViewController(passData, animated: true)
                    
                    
                }
                
                //print(ListPhoto)
                if indexPath.row == PublicInfo.count - 1 {
                    // last cell
                    
                    if totalItems > ArticlesInfo.count {
                        pageNumber = pageNumber + 1
                        print(pageNumber)
                        self.scrollEnable = ""

                        // more items to fetch
                        getRequesPublic() // increment `fromIndex` by 20 before server call
                    }
                }
                
                
                
                return tableViewCell
                
            }
            
            else{
            // create the table view cell design from the prototype cell we have created in storyboard. Reuseidentifier will have same string as in storyboard
//            let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FeedTableViewCell
            
            //tableViewCell.contentView.superview?.clipsToBounds = false
            //tableView.rowHeight = UITableViewAutomaticDimension
            let article2 = self.Articles[indexPath.row]
            let article1 = self.ArticlesInfo[indexPath.row]
            //idArray = [article2.idPhoto]
            tableViewCell.deleteButton.isHidden = true
            tableViewCell.paintCount.text = String(article2.totalEditPhoto)
            
            
            //print(idArray)
            // let article3 = self.commentInfo[indexPath.row]
            
            
            if self.Articles[indexPath.row].featuredPicture == true {
                
                print("fretured")
                tableViewCell.heightConstraintFeature.constant = 40.0
                tableViewCell.featureTextOutlet.alpha = 1
                tableViewCell.featuredImageOutlet.alpha = 1
                tableViewCell.heightConstraintsFooterFeatureView.constant = 0.0
                
                tableViewCell.seeAllPicOutlet.alpha = 1
                //tableViewCell.elementBackgroundWidthConstrains.constant = 50.0
                
                
            }
            else {
                
                tableViewCell.heightConstraintFeature.constant = 00.00
                tableViewCell.featureTextOutlet.alpha = 0
                tableViewCell.featuredImageOutlet.alpha = 0
                tableViewCell.heightConstraintsFooterFeatureView.constant = 00.00
                tableViewCell.seeAllPicOutlet.alpha = 0
                //tableViewCell.elementBackgroundWidthConstrains.constant = 150.0
                

            }
            
            if self.Articles[indexPath.row].isCompeting == true  {
                
                tableViewCell.privacyPic.image = UIImage(named: "privacy_competition_photo")
                //tableViewCell.start1Outlet.isHidden = true
                tableViewCell.startCount.isHidden = true
                tableViewCell.commentCount.isHidden = true
                tableViewCell.descriptionLabel.text = "See All Competition Pictures"
                tableViewCell.commentButtonOutlet.isHidden = true
                tableViewCell.proPic.isHidden = true
                tableViewCell.nameUser.isHidden = true
                tableViewCell.editButton.isHidden = true
                tableViewCell.paintCount.isHidden = true
                
                //tableViewCell.elementBackgroundWidthConstrains.constant = 50.0
                
                
                
            }
            
            
            if self.Articles[indexPath.row].privacyPublic == "public" && self.Articles[indexPath.row].isCompeting == false {
                
                tableViewCell.privacyPic.image = UIImage(named: "Public w-shadow")
                
                tableViewCell.start1Outlet.isHidden = false
                tableViewCell.startCount.isHidden = false
                tableViewCell.commentCount.isHidden = false
                tableViewCell.descriptionLabel.text = Articles[indexPath.row].description
                tableViewCell.commentButtonOutlet.isHidden = false
                tableViewCell.proPic.isHidden = false
                tableViewCell.nameUser.isHidden = false
                tableViewCell.editButton.isHidden = false
                tableViewCell.paintCount.isHidden = false
               // tableViewCell.elementBackgroundWidthConstrains.constant = 150.0
                
                
                
            }
            
            if self.Articles[indexPath.row].privacyFriend == "friends"  {
                
                tableViewCell.privacyPic.image = UIImage(named: "Mutual w-shadow")
                
                tableViewCell.start1Outlet.isHidden = false
                tableViewCell.startCount.isHidden = false
                tableViewCell.commentCount.isHidden = false
                tableViewCell.descriptionLabel.text = Articles[indexPath.row].description
                tableViewCell.commentButtonOutlet.isHidden = false
                tableViewCell.proPic.isHidden = false
                tableViewCell.nameUser.isHidden = false
                tableViewCell.paintCount.isHidden = false
                //tableViewCell.elementBackgroundWidthConstrains.constant = 150.0
                
                
            }
            
            if userID == self.ArticlesInfo[indexPath.row].id{
                
                
                tableViewCell.deleteButton.isHidden = true
                tableViewCell.editButton.isHidden = false
                tableViewCell.paintCount.isHidden = false
                
            }
            if  self.Articles[indexPath.row].isCompeting == true {
                
                
                
            }
            

            PicURL = self.Articles[indexPath.row].picUrl
            tableViewCell.nameUser.text = article1.name
            tableViewCell.commentCount.text = commentCount[indexPath.row].description
            starCount = commentCount[indexPath.row].description
            
            
            self.starCountName = article2.myStarCount
            
            
            
            if article2.myStarCount == 0 {
                
                let image = UIImage(named: "Star small 2") as UIImage?
                
                
                tableViewCell.start1Outlet.setImage(image, for: .normal)
                self.imageValue = ""
                
                
            }
            
            if article2.myStarCount == 5 {
                
                let image = UIImage(named: "Star filled w-shadow") as UIImage?
                self.imageValue = "Checked"
                
                
                tableViewCell.start1Outlet.setImage(image, for: .normal)
                
            }
            if article2.myStarCount == 4 {
                
                let image = UIImage(named: "Star filled w-shadow") as UIImage?
                self.imageValue = "Checked"
                
                
                tableViewCell.start1Outlet.setImage(image, for: .normal)
                
            }
            if article2.myStarCount == 3 {
                
                let image = UIImage(named: "Star filled w-shadow") as UIImage?
                self.imageValue = "Checked"
                
                
                tableViewCell.start1Outlet.setImage(image, for: .normal)
                
            }
            if article2.myStarCount == 2 {
                
                let image = UIImage(named: "Star filled w-shadow") as UIImage?
                self.imageValue = "Checked"
                
                
                tableViewCell.start1Outlet.setImage(image, for: .normal)
            }
            if article2.myStarCount == 1 {
                
                let image = UIImage(named: "Star filled w-shadow") as UIImage?
                self.imageValue = "Checked"
                
                
                tableViewCell.start1Outlet.setImage(image, for: .normal)
            }
            
            
            let dateFormatter = DateFormatter()
            
            
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            
            if let  date = dateFormatter.date(from: article2.uploadedTime) {
                tableViewCell.uploadedTime.text = timeAgoSinceDate(date, currentDate: Date(), numericDates: true)
                
            }
            
            
            tableViewCell.startCount.text =  article2.starcount.description
            
            // UserDefaults.setValue(article2.starcount.description, forKey: "star")
            
            
            
            let url = NSURL(string: article2.picUrl)
            let purl = NSURL(string: article1.profilePicURL)

            
            
            //tableViewCell.cellPhoto.af_imageDownloader?.download([url as! URLRequestConvertible])
            //tableViewCell.cellPhoto.af_
            tableViewCell.cellPhoto.sd_setShowActivityIndicatorView(true)
            tableViewCell.cellPhoto.sd_setIndicatorStyle(.gray)
            tableViewCell.cellPhoto.sd_setImage(with: url! as URL, placeholderImage: nil , options: .highPriority, completed: nil)
            //tableViewCell.proPic.af_imageDownloader?.download([purl as! URLRequestConvertible])
            tableViewCell.proPic.layer.borderColor = UIColor.white.cgColor
            tableViewCell.proPic.layer.borderWidth = 1.0
            
            if  article1.profilePicURL == "/Content/Profile/Thumbs/   Male.jpg"{
                
                tableViewCell.proPic.image = UIImage(named: "male")
                
                
            }
            else if  article1.profilePicURL == "/Content/Profile/Thumbs/   Female  .jpg"{
                
                tableViewCell.proPic.image = UIImage(named: "female")
                
            }
            else{
                
                tableViewCell.proPic.sd_setImage(with: purl! as URL, placeholderImage: nil , options: .scaleDownLargeImages, completed: nil)
            }
            
            
            
            
            //hiddenFloatingButtonsScrool()
            
            
            
            
            tableViewCell.onDeleteTap = {
                
                self.photoId = self.Articles[indexPath.row].idPhoto
                self.BackButtonClick("Warning", message: "Are you sure you want to delete this Photo")
                print("Delete")
                
            }
            
            tableViewCell.onseeAllPic = {
                
                if self.Articles[indexPath.row].isCompeting == true {
                    
                    let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let passData = MainStoryboard.instantiateViewController(withIdentifier: "GaleryViewController") as! GaleryViewController
                    
                    self.navigationController?.pushViewController(passData, animated: true)
                }
                    
                else{
                    
                    print("Nothing")
                    
                }
                
                
            }
            
            tableViewCell.onReport = {
                
                
                
                let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                
                var title = ""
                
                if userID == self.ArticlesInfo[indexPath.row].id {
                    
                    title = "Delete"
                }
                else{
                    
                  title =  "Report"
                }
                
                // create an action
                let firstAction: UIAlertAction = UIAlertAction(title: title , style: .default) { action -> Void in
                    
                    
                    if userID == self.ArticlesInfo[indexPath.row].id {
                        
                        self.photoId = self.Articles[indexPath.row].idPhoto
                        self.BackButtonClick("Warning", message: "Are you sure you want to delete this photo?")
                        print("Delete")
                        
                    }

                    else{
                    let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let passData = MainStoryboard.instantiateViewController(withIdentifier: "ReportPhoto") as! ReportPhoto
                    
                    self.navigationController?.pushViewController(passData, animated: true)
                    }
                    
                    
                }
                
                let secondAction: UIAlertAction = UIAlertAction(title: "Block this user", style: .default) { action -> Void in
                    
                    print("Second Action pressed")
                }
                let thirdAction: UIAlertAction = UIAlertAction(title: "Share on Facebook", style: .default) { action -> Void in
                    
                    print("Second Action pressed")
                }
                
                
                let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
                
                // add actions
                
                //actionSheetController.addAction(secondAction)
                actionSheetController.addAction(thirdAction)
                actionSheetController.addAction(firstAction)
                actionSheetController.addAction(cancelAction)
                
                // present an actionSheet...
                self.present(actionSheetController, animated: true, completion: nil)
                
            }
            
            
            tableViewCell.onEditTap = {
                let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let passData = MainStoryboard.instantiateViewController(withIdentifier: "EditImageVC") as! EditImageVC
                passData.imagePic = tableViewCell.cellPhoto.image!
                passData.photoID = article2.idPhoto
                self.navigationController?.pushViewController(passData, animated: true)
//                self.photoId = article2.idPhoto
//
//                let photoEditor = PhotoEditorViewController(nibName:"PhotoEditorViewController",bundle: Bundle(for: PhotoEditorViewController.self))
//
//                //PhotoEditorDelegate
//                photoEditor.photoEditorDelegate = self
//                //The image to be edited
//                photoEditor.image = tableViewCell.cellPhoto.image
//
//                //Stickers that the user will choose from to add on the image
//                for i in 0...17 {
//                    photoEditor.stickers.append(UIImage(named: i.description )!)
//                }
//                //Optional: To hide controls - array of enum control
//                photoEditor.hiddenControls = [.crop, .share]
//
//                //Optional: Colors for drawing and Text, If not set default values will be used
//                //            photoEditor.colors = [.r]
//
//                //Present the View Controller
//                self.present(photoEditor, animated: true, completion: nil)
                
                
            }
            
            tableViewCell.onStarTapped1 = {
                
                let imagevanish = UIImage(named: "Star filled w-shadow") as UIImage?
                
                
                if self.imageValue == "Checked" {
                    
                    
                    let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/removerate/\(self.Articles[indexPath.row].idPhoto)"
                    //            let headers: HTTPHeaders = [
                    //                "Content-Type": "application/json"
                    //            ]
                    Alamofire.request(url, method:.post,encoding: URLEncoding.default).responseJSON { response in
                        switch response.result {
                        case .success:
                            
                            let imagevanish = UIImage(named: "Star small 2") as UIImage?
                            
                            
                            
                            
                            
                            tableViewCell.start1Outlet.setImage(imagevanish, for: .normal)
                            tableViewCell.startCount.text =  article2.starcount.description
                            print(article2.starcount.description)
                            
                            
                            print(response)
                            self.imageValue = ""
                            // self.ArticlesInfo.removeAll()
                            
                            
                            
                            // self.tableView.reloadData()
                            
                            
                            
                        case .failure(let error):
                            print(error)
                        }
                        
                        
                    }
                    
                }
                else{
                    
                    //                (0...10).forEach { (_) in
                    //                    self.generateAnimatedViews()
                    //                }
                    
//                    self.emitter.emitterPosition = CGPoint(x: self.view.frame.size.width / 2, y: -10)
//                    self.emitter.emitterShape = kCAEmitterLayerLine
//                    self.emitter.emitterSize = CGSize(width: self.view.frame.size.width, height: 10.0)
//                    self.emitter.emitterCells = self.generateEmitterCells()
//                    self.view.layer.addSublayer(self.emitter)
//                    
//                    Timer.scheduledTimer(withTimeInterval: 2, repeats: false, block: { (Timer) in
//                        
//                        self.emitter.removeFromSuperlayer()
//                        
//                    })
//                    
                    
                    
                    
                    let parameters: [String : Any]? = [
                        "NumOfStars": 1 ,
                        "photoId": self.Articles[indexPath.row].idPhoto ,
                        ]
                    let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/rating"
                    let headers: HTTPHeaders = [
                        "Content-Type": "application/json"
                    ]
                    Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
                        
                        if let jsonRoot = response.result.value as? [String:Any]!{
                            
                            //print(jsonRoot)
                            
                            if let resultPayload = jsonRoot!["resultPayload"] as? [String: Any]!{
                                
                                print(resultPayload)
                                
                                tableViewCell.startCount.text =  String(describing: resultPayload!["totalRating"] as! Int)
                                print(tableViewCell.startCount.text!)
                                
                                
                                self.imageValue = "Checked"
                                
                            }
                            
                            let image = UIImage(named: "Star filled w-shadow") as UIImage?
                            
                            
                            
                            
                            tableViewCell.start1Outlet.setImage(image, for: .normal)
                            
                        }
                        
                        
                        
                        
                        
                        
                    }
                }
                
                
            }
            
            
            tableViewCell.onCrossTapped = {
                
                //self.stopRequest()
                
                //            let parameters: [String : Any]? = [
                //                "NumOfStars": 2 ,
                //                "photoId": self.Articles[indexPath.row].idPhoto ,
                //                ]
                let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/removerate/\(self.Articles[indexPath.row].idPhoto)"
                //            let headers: HTTPHeaders = [
                //                "Content-Type": "application/json"
                //            ]
                Alamofire.request(url, method:.post,encoding: URLEncoding.default).responseJSON { response in
                    switch response.result {
                    case .success:
                        
                        let imagevanish = UIImage(named: "Star small 2") as UIImage?
                        
                        
                        
                        tableViewCell.star5Outlet.setImage(imagevanish, for: .normal)
                        
                        tableViewCell.star4Outlet.setImage(imagevanish, for: .normal)
                        tableViewCell.star3Outlet.setImage(imagevanish, for: .normal)
                        tableViewCell.star2Outlet.setImage(imagevanish, for: .normal)
                        
                        tableViewCell.start1Outlet.setImage(imagevanish, for: .normal)
                        
                        print(response)
                        if self.fromPublic == "true"{
                            self.getRequesPublic()
                            
                        }
                        else{
                            self.getRequest()
                            
                        }
                        
                        //self.tableView.reloadData()
                        tableViewCell.startCount.text =  article2.starcount.description
                        
                        
                        
                    case .failure(let error):
                        print(error)
                    }
                    
                    
                }

            }
            
            
            tableViewCell.onStarTapped2 = {
                self.stopRequest()
                
                let parameters: [String : Any]? = [
                    "NumOfStars": 2 ,
                    "photoId": self.Articles[indexPath.row].idPhoto ,
                    ]
                let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/rating"
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json"
                ]
                Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
                    if let jsonRoot = response.result.value as? [String:Any]!{
                        
                        //print(jsonRoot)
                        
                        if let resultPayload = jsonRoot!["resultPayload"] as? [String: Any]!{
                            
                            print(resultPayload)
                            
                            tableViewCell.startCount.text =  String(describing: resultPayload!["totalRating"] as! Int)
                            
                            
                        }
                        
                        let image = UIImage(named: "star1") as UIImage?
                        
                        
                        let imagevanish = UIImage(named: "Star small 2") as UIImage?
                        
                        
                        
                        tableViewCell.star5Outlet.setImage(imagevanish, for: .normal)
                        
                        tableViewCell.star4Outlet.setImage(imagevanish, for: .normal)
                        tableViewCell.star3Outlet.setImage(imagevanish, for: .normal)
                        tableViewCell.star2Outlet.setImage(image, for: .normal)
                        tableViewCell.start1Outlet.setImage(image, for: .normal)
                        
                    }
                    
                }
                
                
            }
            
            tableViewCell.onStarTapped3 = {
                self.stopRequest()
                
                let parameters: [String : Any]? = [
                    "NumOfStars": 3 ,
                    "photoId": self.Articles[indexPath.row].idPhoto ,
                    ]
                let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/rating"
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json"
                ]
                Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
                    if let jsonRoot = response.result.value as? [String:Any]!{
                        
                        //print(jsonRoot)
                        
                        if let resultPayload = jsonRoot!["resultPayload"] as? [String: Any]!{
                            
                            print(resultPayload)
                            
                            tableViewCell.startCount.text =  String(describing: resultPayload!["totalRating"] as! Int)
                            
                            
                        }
                        
                        let image = UIImage(named: "star1") as UIImage?
                        
                        
                        let imagevanish = UIImage(named: "Star small 2") as UIImage?
                        
                        
                        
                        tableViewCell.star5Outlet.setImage(imagevanish, for: .normal)
                        
                        tableViewCell.star4Outlet.setImage(imagevanish, for: .normal)
                        tableViewCell.star3Outlet.setImage(image, for: .normal)
                        tableViewCell.star2Outlet.setImage(image, for: .normal)
                        tableViewCell.start1Outlet.setImage(image, for: .normal)
                        
                    }
                    
                    
                }
                
                
                
            }
            
            tableViewCell.onStarTapped4 = {
                self.stopRequest()
                
                let parameters: [String : Any]? = [
                    "NumOfStars": 4 ,
                    "photoId": self.Articles[indexPath.row].idPhoto ,
                    ]
                let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/rating"
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json"
                ]
                Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
                    if let jsonRoot = response.result.value as? [String:Any]!{
                        
                        //print(jsonRoot)
                        
                        if let resultPayload = jsonRoot!["resultPayload"] as? [String: Any]!{
                            
                            print(resultPayload)
                            
                            tableViewCell.startCount.text =  String(describing: resultPayload!["totalRating"] as! Int)
                            
                            
                        }
                        
                        
                        let image = UIImage(named: "star1") as UIImage?
                        let imagevanish = UIImage(named: "Star small 2") as UIImage?
                        
                        
                        
                        tableViewCell.star5Outlet.setImage(imagevanish, for: .normal)
                        
                        tableViewCell.star4Outlet.setImage(image, for: .normal)
                        tableViewCell.star3Outlet.setImage(image, for: .normal)
                        tableViewCell.star2Outlet.setImage(image, for: .normal)
                        tableViewCell.start1Outlet.setImage(image, for: .normal)
                        
                    }
                    
                }
                
            }
            
            tableViewCell.onStarTapped5 = {
                self.stopRequest()
                
                let parameters: [String : Any]? = [
                    "NumOfStars": 5 ,
                    "photoId": self.Articles[indexPath.row].idPhoto ,
                    ]
                let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/rating"
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json"
                ]
                Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
                    if let jsonRoot = response.result.value as? [String:Any]!{
                        
                        print(jsonRoot)
                        
                        if let resultPayload = jsonRoot!["resultPayload"] as? [String: Any]!{
                            
                            print(resultPayload)
                            
                            tableViewCell.startCount.text =  String(describing: resultPayload!["totalRating"] as! Int)
                            
                            
                        }
                        let image = UIImage(named: "star1") as UIImage?
                        
                        
                        tableViewCell.star5Outlet.setImage(image, for: .normal)
                        
                        tableViewCell.star4Outlet.setImage(image, for: .normal)
                        tableViewCell.star3Outlet.setImage(image, for: .normal)
                        tableViewCell.star2Outlet.setImage(image, for: .normal)
                        tableViewCell.start1Outlet.setImage(image, for: .normal)
                        
                    }
                    
                    
                }
                
                
                
            }
            
            
            
            
            tableViewCell.onProfileTapped = {
                self.stopRequest()
                
                let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                
                
                passData.getid = self.ArticlesInfo[indexPath.row].id
                print(self.ArticlesInfo[indexPath.row].id)
                
                
                
                
                self.navigationController?.pushViewController(passData, animated: true)
                
            }
            
            //        DispatchQueue.main.async {
            //            self.tableView.reloadData()
            //        }
            
           tableViewCell.onCommentsTapped = {
            
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "CommentPostVC") as! CommentPostVC
            passData.getID = self.Articles[indexPath.row].idPhoto
                passData.getImage = self.Articles[indexPath.row].picUrl
            passData.photoId = self.Articles[indexPath.row].idPhoto

            self.navigationController?.pushViewController(passData, animated: true)
            
//                let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
//                let blurEffectView = UIVisualEffectView(effect: blurEffect)
//                blurEffectView.frame = self.view.bounds
//                blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//                self.view.addSubview(blurEffectView)
//                self.view.bringSubview(toFront: self.commentView)
//                self.commentViewTopConstarints.constant = 20.00
//                UIView.animate(withDuration: 1, animations: {
//
//                    self.view.layoutIfNeeded()
//
//                }, completion: { (true) in
//
//
//
//                })
//                self.commentView.isHidden = false
//                self.commentView.layer.cornerRadius = 20
//                self.CommentsPic.removeAll()
//                self.getID = self.Articles[indexPath.row].idPhoto
//                self.getImage = self.Articles[indexPath.row].picUrl
//                let imageUrl = NSURL(string: self.getImage)
//                self.commentsImageView.sd_setImage(with: imageUrl! as URL, completed: nil)
//                self.getImageInfo()
//
//                self.tableViewComment.isHidden = false
//                //self.commentText.becomeFirstResponder()
//
//
            }
            
            print(indexPath.row)
            print(ArticlesInfo.count)
            //print(ListPhoto)
            if indexPath.row == ArticlesInfo.count - 1 {
                // last cell
                
                if totalItems > ArticlesInfo.count {
                    pageNumber = pageNumber + 1
                    print(pageNumber)
                    // more items to fetch
                    if self.fromPublic == "true"{
                        self.getRequesPublic()
                        
                    }
                    else{
                        self.getRequest()
                        
                    }
                    
                }
            }
            
            
            
        }
            return tableViewCell

            

        }

    }
    
    // Do any additional setup after loading the view, typically from a nib
    
    fileprivate func generateAnimatedViews() {
        let image = drand48() > 0.5 ? #imageLiteral(resourceName: "Star filled w-shadow") : #imageLiteral(resourceName: "Star filled w-shadow")
        let imageView = UIImageView(image: image)
        let dimension = 20 + drand48() * 10
        imageView.frame = CGRect(x: -20, y: -20, width: dimension, height: dimension)
        
        let animation = CAKeyframeAnimation(keyPath: "position")
        
        animation.path = customPath().cgPath
        animation.duration = 2 + drand48() * 3
        animation.fillMode = kCAFillModeForwards
        animation.isRemovedOnCompletion = false
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        
        imageView.layer.add(animation, forKey: nil)
        view.addSubview(imageView)
    }
    fileprivate func generateAnimatedViewsComments() {
        let image = drand48() > 0.5 ? #imageLiteral(resourceName: "Balloons-PNG-Pic") : #imageLiteral(resourceName: "Balloons-PNG-Pic")
        let imageView = UIImageView(image: image)
        let dimension = 20 + drand48() * 10
        imageView.frame = CGRect(x: 0, y: 0, width: dimension, height: dimension)
        
        let animation = CAKeyframeAnimation(keyPath: "position")
        
        animation.path = customPath().cgPath
        animation.duration = 2 + drand48() * 3
        animation.fillMode = kCAFillModeForwards
        animation.isRemovedOnCompletion = false
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        
        imageView.layer.add(animation, forKey: nil)
        view.addSubview(imageView)
    }
    
    
    
    
    func hiddenFloatingButtons(){
        uploadButton.isHidden = !uploadButton.isHidden
        achivementButton.isHidden =  !achivementButton.isHidden
        profileButton.isHidden =  !profileButton.isHidden
        notificationButton.isHidden = !notificationButton.isHidden
        homeButton.isHidden =  !homeButton.isHidden
        searchButton.isHidden = !searchButton.isHidden
        
    }
    func hiddenFloatingButtonsScrool(){
        uploadButton.isHidden = true
        achivementButton.isHidden =  true
        profileButton.isHidden =  true
        notificationButton.isHidden = true
        homeButton.isHidden =  true
        searchButton.isHidden = true
        
    }
    
    
    
    func animatingPopUp()
    {
        UIView.animate(withDuration: 1, animations: {
            self.uploadButton.isHidden =  !self.uploadButton.isHidden
            
        }) { (true) in
            
            UIView.animate(withDuration: 1, animations: {
                self.achivementButton.isHidden = !self.achivementButton.isHidden
                
            }, completion: { (true) in
                UIView.animate(withDuration: 1, animations: {
                    
                    self.profileButton.isHidden = !self.profileButton.isHidden
                    
                }, completion: { (true) in
                    self.notificationButton.isHidden = !self.notificationButton.isHidden
                    //                    let image1 = UIImage(named: "Circleopen")
                    //                    self.floatingMainButton.setBackgroundImage(image1, for: UIControlState.normal)
                    
                    
                    
                    
                })
            })
        }
        
        
        
    }
    
    @IBAction func mainButtonAction(_ sender: UIButton) {
        print("shaikat")
        hiddenFloatingButtons()
        
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        // self.navigationController?.isNavigationBarHidden = false
        self.fromPublic = ""
        
        
        let sessionManager = Alamofire.SessionManager.default
        sessionManager.session.getTasksWithCompletionHandler {
            dataTasks, uploadTasks, downloadTasks in dataTasks.forEach {
                $0.cancel()
            }
            uploadTasks.forEach { $0.cancel() }
            downloadTasks.forEach { $0.cancel() } }
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
      //  if emojiButtonPressed == false {
            commentText.becomeFirstResponder()

            if commentText.textColor! == UIColor.lightGray {
                commentText.text! = ""
                commentText.textColor = UIColor.black
            }
            
      //  }
        
        //commentText.text! = ""
       
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if commentText.text == "" {
            commentText.text! = "Write a comment"
            commentText.textColor = UIColor.lightGray
            commentText.resignFirstResponder()
        }
        
        else {
            commentText.text = "Write a comment"
            commentText.textColor = UIColor.lightGray
            commentText.resignFirstResponder()
        }
        
        
       // emojiButtonPressed = false
    }
    
    
   
    
    @IBAction func emoButtonPressed(_ sender: Any) {
        
        if commentText.text! == "Write a comment"{
            commentText.text! = ""
        }
            let keyboardSettings = KeyboardSettings(bottomType: BottomType(rawValue: bottomType.hashValue) ?? BottomType(rawValue: 1)!)
            //keyboardSettings.customEmojis = emojis
            keyboardSettings.needToShowAbcButton = true
            
            let emojiView = EmojiView(keyboardSettings: keyboardSettings)
            emojiView.translatesAutoresizingMaskIntoConstraints = false
            emojiView.delegate = self
            commentText.inputView = emojiView
            commentText.becomeFirstResponder()
}
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!)years"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!)month"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!)w"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1w"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!)d"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1d"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!)h"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1h"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 8) {
            return "\(components.minute!)m"
        } else if (components.minute! >= 8){
            if (numericDates){
                return "1m"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 0) {
            return "Just now"
        } else {
            return "Just now"
        }
        
    }
    
    private func generateEmitterCells() -> [CAEmitterCell] {
        var cells:[CAEmitterCell] = [CAEmitterCell]()
        for index in 0..<16 {
            
            let cell = CAEmitterCell()
            
            cell.birthRate = 4.0
            cell.lifetime = 14.0
            cell.lifetimeRange = 0
            cell.velocity = CGFloat(getRandomVelocity())
            cell.velocityRange = 0
            cell.emissionLongitude = CGFloat(Double.pi)
            cell.emissionRange = 0.5
            cell.spin = 3.5
            cell.spinRange = 0
            cell.color = getNextColor(i: index)
            cell.contents = getNextImage(i: index)
            cell.scaleRange = 0.25
            cell.scale = 0.1
            
            cells.append(cell)
            
        }
        
        return cells
        
    }
    
    private func getRandomVelocity() -> Int {
        return velocities[getRandomNumber()]
    }
    
    private func getRandomNumber() -> Int {
        return Int(arc4random_uniform(4))
    }
    
    private func getNextColor(i:Int) -> CGColor {
        if i <= 4 {
            return colors[0].cgColor
        } else if i <= 8 {
            return colors[1].cgColor
        } else if i <= 12 {
            return colors[2].cgColor
        } else {
            return colors[3].cgColor
        }
    }
    private func getNextImage(i:Int) -> CGImage {
        return images[i % 4].cgImage!
    }
    
}


func customPath() -> UIBezierPath {
    let path = UIBezierPath()
    
    path.move(to: CGPoint(x: 0, y: 200))
    
    let endPoint = CGPoint(x: 400, y: 200)
    
    let randomYShift = 200 + drand48() * 300
    let cp1 = CGPoint(x: 100, y: 100 - randomYShift)
    let cp2 = CGPoint(x: 200, y: 300 + randomYShift)
    
    path.addCurve(to: endPoint, controlPoint1: cp1, controlPoint2: cp2)
    return path
}


class CurvedView: UIView {
    
    override func draw(_ rect: CGRect) {
        //do some fancy curve drawing
        let path = customPath()
        path.lineWidth = 3
        path.stroke()
    }
    
}

import UIKit

extension UIImageView {
    
    func setRounded() {
        self.layer.cornerRadius = (self.frame.width / 2) //instead of let radius = CGRectGetWidth(self.frame) / 2
        self.layer.masksToBounds = true
    }
}
extension UIButton {
    
    func setRounded() {
        self.layer.cornerRadius = (self.frame.width / 2) //instead of let radius = CGRectGetWidth(self.frame) / 2
        self.layer.masksToBounds = true
    }
    
}
extension UIView {
    
    /// Remove UIBlurEffect from UIView
    func removeBlurEffect() {
        let blurredEffectViews = self.subviews.filter{$0 is UIVisualEffectView}
        blurredEffectViews.forEach{ blurView in
            blurView.removeFromSuperview()
        }
    }
}

extension UIView {
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}

