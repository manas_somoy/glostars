
import UIKit
import Foundation

public class LoadingOverlay{
    
    var overlayView = UIView()
    class var shared: LoadingOverlay {
        struct Static {
            static let instance: LoadingOverlay = LoadingOverlay()
        }
        return Static.instance
    }

    public func showOverlay(view: UIView!) {
        overlayView = UIView(frame: UIScreen.main.bounds)
        overlayView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        view.addSubview(overlayView)
    }

    public func hideOverlayView() {
        overlayView.removeFromSuperview()
    }
}


