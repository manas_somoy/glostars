//
//  ViewController.swift
//  File 2
//
//  Created by Sanzid Ashan on 5/14/16.
//  Copyright © 2016 Sanzid Ashan. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit

class LoginViewController: UIViewController,FBSDKLoginButtonDelegate {
    
    
    var dict : [String : AnyObject]!
    
    public func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        
        
        print("Logged in..")
        
        //self.performSegue(withIdentifier: "aa", sender: NewViewController.self)
        
        
    }
    
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        
        
        // TODO: Change where the log in button is positioned in your view
        //logInButton.frame = CGRectMake(39, 455, 238, 43)
        
        // self.view.addSubview(logInButton)
        
        
        
        if (FBSDKAccessToken.current() == nil)
        {
            print("Not logged in..")
            if UserDefaults.standard.value(forKey: "id") != nil {
                
                let nextViewController =                                  self.storyboard!.instantiateViewController(withIdentifier:"NewsVCViewController")
                
                let nvc = UINavigationController(rootViewController: nextViewController)
                self.navigationController?.pushViewController(nextViewController, animated: true)
                
            }
        }
        else
        {
            print("Logged in..")
            //self.performSegue(withIdentifier: "aa", sender: NewViewController.self)
        }
        
        let loginButton = FBSDKLoginButton()
        loginButton.readPermissions = ["public_profile", "email", "user_friends"]
        //loginButton.frame = CGRectMake(39, 391, 238, 43)
        loginButton.delegate = self
        //self.view.addSubview(loginButton)
        
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email,gender"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    self.dict = result as! [String : AnyObject]
                    print(result!)
                    print(self.dict)
                }
            })
        }
        
        
        
      
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: NSError!)
    {
        if error == nil
        {
            print("Login complete.")
        }
        else
        {
            print(error.localizedDescription)
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!)
    {
        print("User logged out...")
    }
    
    
   
  
    
    
    
}




