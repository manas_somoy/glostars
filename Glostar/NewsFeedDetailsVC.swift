//
//  NewsFeedDetailsVC.swift
//  Glostars
//
//  Created by Sanzid Ashan on 4/22/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
import ObjectMapper
import Social
import SDWebImage


class NewsFeedDetailsVC: UIViewController,UITableViewDataSource,UITableViewDelegate,DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    var commentIdCell = ""

   // @IBOutlet var widthConstraintsDelete: NSLayoutConstraint!
    var  imageValue = ""
    
    @IBOutlet var commentText: UITextView!

    @IBOutlet var tableViewComment: UITableView!
    var CommentsPic : Array<SingleCommentsMap> = []
    @IBOutlet var commentView: UIView!

    @IBOutlet var curveButton: UIButton!

    //@IBOutlet var deleteWidthConstraints: NSLayoutConstraint!
    @IBOutlet weak var star1: UIButton!
    
    @IBOutlet var deleteButtonOutlet: UIButton!
   // @IBOutlet var toConstraints: NSLayoutConstraint!
 
    //@IBOutlet var violeatBoxHeight: NSLayoutConstraint!
    //@IBOutlet var vanishedConstrains4: NSLayoutConstraint!
   // @IBOutlet var vanishedConstraints2: NSLayoutConstraint!
    
    //@IBOutlet var vanishedContraints1: NSLayoutConstraint!
    
   // @IBOutlet var vanishedConstraints3: NSLayoutConstraint!
    
   // @IBOutlet var bottomConstraintsComment: NSLayoutConstraint!
    
   // @IBOutlet weak var star2: UIButton!
    
   // @IBOutlet var sendView: UIView!
    
    ///@IBOutlet weak var star3: UIButton!
    
    
   // @IBOutlet weak var star4: UIButton!
    
   // @IBOutlet weak var star5: UIButton!
    
    
   // @IBOutlet var starFollowingHConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var followingButtonOutlet: DesignableButton!
    
    //@IBOutlet var commentsTextField: UITextView!
    
   // @IBOutlet var childView: UIView!
    
   // @IBOutlet var ScrollView: UIScrollView!
  
    var pageNumber = 1
    
    var Articles : Array<FeedMapper> = []
    var ArticlesInfo : Array<InfoMapper> = []
    @IBOutlet var postUserImage: UIImageView!

    
    //var commentInfo : Array< CommentMapper > = []
    
    var commentInfo = [[String : Any]]()
    var commentId = ""


    @IBOutlet weak var userTime: UILabel!
    

    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var descriptionOutlet: UILabel!
    
    @IBOutlet weak var imageUser: UIImageView!

    @IBOutlet weak var imageView: UIImageView!
    
    //@IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var numberOfStarCounts: UILabel!

    
    @IBOutlet var imagePrivacyOutlet: UIImageView!
    //@IBOutlet weak var emojTextButton: UITextField!
    @IBOutlet weak var emojButton: UIButton!
    @IBOutlet weak var numberOfComments: UILabel!
    
    //@IBOutlet weak var heightConstraintsStar2: NSLayoutConstraint!
    
    
    
    //@IBOutlet weak var tableViewHeightConstraints: NSLayoutConstraint!
    
    
    var photoId = 0
    var getImageUrl = String()
    var getDescription = String()
    var getUserProfile = String()
    var getUserName = String()
    var getTheTime = String()
    var getcomments = [String : Any]()
    var getID = Int()
    
    
    var getUserID = ""
    var getprofilePicUrl = " "
    var getfirstName = " "
    var getlastName = " "
    
    var publicReating = ""
    var friendsRatig = ""
    var isComprting = Bool()
    var is_mutual = Bool()
    var me_follow = Bool()
    var he_follow = Bool()
    var userOrginatedId = ""
    
    @IBOutlet var commentButtonOutlet: UIButton!
    var imageStar4 = UIImage()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        commentView.isHidden = true
        commentView.layer.cornerRadius = 16
        postUserImage.setRounded()
        tableViewComment.emptyDataSetSource = self
        //print(getID)
        deleteButtonOutlet.isHidden = true
        getImageInfo()
        let tap = UITapGestureRecognizer(target: self, action: #selector(NewsFeedDetailsVC.tapFunction))
        descriptionOutlet.isUserInteractionEnabled = true
        descriptionOutlet.addGestureRecognizer(tap)
        
        
        //self.tableView.addSubview(self.refreshControl)

        //deleteWidthConstraints.constant = 0
        
      // violeatBoxHeight.constant = 220
        
       let heightOfScroll : CGFloat = 1000.0
        
       // self.ScrollView.contentSize = CGSize(width: self.view.frame.width , height: heightOfScroll)
        
       // self.childView.frame = CGRect(x: 0, y: 0, width:self.view.frame.width , height: heightOfScroll)
        
      //  ScrollView.contentSize.height = heightOfScroll
        

        //descriptionOutlet.layer.cornerRadius = 15
       // commentsTextField.layer.cornerRadius = 15
        curveButton.layer.cornerRadius = 16.0
        commentText.layer.borderWidth = 1.0
        
        commentText.layer.borderColor = UIColor.black.cgColor
        commentText.layer.cornerRadius = 8.0
        commentText.textContainer.lineBreakMode = .byTruncatingTail
        
//        let DownSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe:)))
//        DownSwipe.direction = UISwipeGestureRecognizerDirection.down
//        self.view.addGestureRecognizer(DownSwipe)
        imageView.layer.borderWidth = 0
        
        let url = NSURL(string: getImageUrl)
        //print(url!)
        imageView.sd_setImage(with: url! as URL, placeholderImage: nil , options: .scaleDownLargeImages, completed: nil)
        //descriptionOutlet.text = getDescription
        //userName.text = getUserName
        
        //userTime.text = getTheTime
        imageUser.setRounded()
        //let url2 = NSURL(string: getprofilePicUrl)
        //print(url2!)
        //imageUser.af_setImage(withURL: url2! as URL)
        
      //  tableView.layer.cornerRadius = 8
        if userPic == "/Content/Profile/Thumbs/   Male.jpg"{
            
            postUserImage.image = UIImage(named: "male")
            
            
        }
        else if userPic == "/Content/Profile/Thumbs/   Female  .jpg"{
            
            postUserImage.image = UIImage(named: "female")
            
        }
            
            
        else{
            let urlString = URL(string:String(describing: UserDefaults.standard.value(forKey: "pic")!))
            postUserImage.af_setImage(withURL: urlString!)
            
            
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from: getTheTime)
        
        //userTime.text = timeAgoSinceDate(date!, currentDate: Date(), numericDates: true)

        
      //   sendView.layer.cornerRadius = 15

           print(getUserID)
           print(userID)

        // Do any additional setup after loading the view.
        
        if userID == self.getUserID {
            
        //    self.deleteWidthConstraints.constant = 20
            //self.widthConstraintsDelete.constant = 25.0
            
            
        }

        
        
        
            }
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        
        print(UIDevice.current.orientation)
    }
    
    @IBAction func commentBackAction(_ sender: Any) {
        
        
        commentView.isHidden = true
        
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        
        let text = NSAttributedString(string: "No Comments")
        tableViewComment.separatorColor = UIColor.clear
        return text
        
    }
    
    @IBAction func sendCommentAction(_ sender: Any) {
        
        
        
        let parameters: [String : Any]? = [
            "CommentText": commentText.text!,
            "photoId": getID ,
            ]
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/comment"
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                print(response)
                self.CommentsPic.removeAll()
                self.getImageInfoComment()
            case .failure(let error):
                print(error)
            }
        }
        commentText.text = ""
        
    }

 func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    return CommentsPic.count

    
    }

    
    func tapFunction(sender:UITapGestureRecognizer) {

       if descriptionOutlet.text == "See All Competition Pictures"{
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "GaleryViewController") as! GaleryViewController
        
        
        
        
        
        
        self.navigationController?.pushViewController(passData, animated: true)
        
        
        }
       else{
        
        
        
        }
    
    
    
    }
    
    func swipeAction(swipe:UISwipeGestureRecognizer)
    {
        //performSegue(withIdentifier: "Dismiss", sender: self)
        self.navigationController?.popViewController(animated: true)
        //dismiss(animated: true, completion: nil)
        
    }
    
//    override var shouldAutorotate: Bool {
//        return true
//    }
//    
//    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
//        return .landscape
//    }
//    
//    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
//        return .portrait
//    }
    
//    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
//        if (toInterfaceOrientation.isLandscape) {
//            childView.frame.size.width = 670.0
//            imageView.frame.size.height = 100.0
//            childView.frame.size.height = 1200.0
//            ScrollView.frame.size.width = 1200.0
//            ScrollView.frame.size.height = 500.0
//
//            
//        }
//        else {
//            print("Portrait");
//            
//        
//        }
//    }
    
    func BackButtonClick(_ title:String,message:String){
        
        let alertController = UIAlertController(title: title, message: message , preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            print("OK Pressed")
            
            let DeletePicUrl = "https://testglostarsdevelopers.azurewebsites.net/api/images/delete/\(self.getID)"
            print(self.getID)
            Alamofire.request(DeletePicUrl, method: .post).responseJSON { response in
                
                switch response.result {
                    
                case .success:
                    print(response)
                    self.navigationController?.popViewController(animated: true)
                    
                case .failure(let error):
                    print(error)
                }
                
            }

            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
            print("Cancel Pressed")
            //self.dismiss(animated: true, completion: nil)
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    func UnfollowClick(_ title:String,message:String){
        
        let alertController = UIAlertController(title: title, message: message , preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            print("OK Pressed")
            
            let unfollowUrl = "https://testglostarsdevelopers.azurewebsites.net/api/Follower/Unfollowing?id=\(self.getUserID)"
            Alamofire.request(unfollowUrl, method: .post).responseJSON { response in
                
                print(response)
                self.CommentsPic.removeAll()
                self.getImageInfo()
            }
            
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
            print("Cancel Pressed")
            //self.dismiss(animated: true, completion: nil)
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    func DeleteComment(_ title:String,message:String){
        
        let alertController = UIAlertController(title: title, message: message , preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            print("OK Pressed")
            
            let DeleteComment = "https://testglostarsdevelopers.azurewebsites.net/api/images/DeleteComment?commentId=\(self.commentIdCell)"
            //print(allComments.commentId)
            
            Alamofire.request(DeleteComment, method: .get).responseJSON { response in
                
                switch response.result {
                    
                case .success:
                    print(response)
                    self.CommentsPic.removeAll()
                    self.getImageInfo()
                    // self.performSegue(withIdentifier: "News", sender: NewsVCViewController.self)
                    
                    
                case .failure(let error):
                    print(error)
                }
                
            }
            

            
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
            print("Cancel Pressed")
            //self.dismiss(animated: true, completion: nil)
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    @IBAction func commentButtonAction(_ sender: Any) {
        
        UIView.animate(withDuration: 0.5, delay: 0.2, options: .curveEaseIn, animations: {
            
            
            
        }) { (true) in
            
            self.commentView.isHidden = false
            self.CommentsPic.removeAll()
            self.getImageInfoComment()
        }
            
        
        
    }
    
        
        
    
    

    
    @IBAction func deletePictureAction(_ sender: Any) {
        
       
        BackButtonClick("Warning", message: "Do you want to delete your photo ")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
                
        
    }
    
    override func viewDidLayoutSubviews() {
        
        
//        self.view.translatesAutoresizingMaskIntoConstraints = true
//        self.ScrollView.translatesAutoresizingMaskIntoConstraints = true
//        self.childView.translatesAutoresizingMaskIntoConstraints = true
//        self.view.needsUpdateConstraints()
//        self.ScrollView.needsUpdateConstraints()
//        self.childView.needsUpdateConstraints()
//        
//        tableView.frame = CGRect(x: tableView.frame.origin.x, y: tableView.frame.origin.y, width: tableView.frame.size.width, height: tableView.contentSize.height)
        
        
    }
    
    @IBAction func startOneAction(_ sender: Any) {
        
        
        if self.imageValue == "Checked"{
            //            let parameters: [String : Any]? = [
            //                "NumOfStars": 2 ,
            //                "photoId": self.Articles[indexPath.row].idPhoto ,
            //                ]
            let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/removerate/\(self.getID)"
            //            let headers: HTTPHeaders = [
            //                "Content-Type": "application/json"
            //            ]
            Alamofire.request(url, method:.post,encoding: URLEncoding.default).responseJSON { response in
                switch response.result {
                case .success:
                    print(response)
                    let url2 = "https://testglostarsdevelopers.azurewebsites.net/api/images/picture/\(self.getID)"
                    let headers: HTTPHeaders = [
                        
                        "Authorization": tokenType + accessToken ,
                        "Content-Type": "application/json",
                        
                        ]
                    
                    Alamofire.request(url2, method:.get,encoding:JSONEncoding.default,headers:headers).responseJSON { response in
                        
                        
                        let image = UIImage(named: "Star empty w-shadow") as UIImage?
                        
                        
                        self.star1.setImage(image, for: .normal)
                        //   self.star2.setImage(image, for: .normal)
                        //   self.star3.setImage(image, for: .normal)
                        //   self.star4.setImage(image, for: .normal)
                        //  self.star5.setImage(image, for: .normal)
                        
                        
                        if let jsonRoot = response.result.value as? [String:Any]!{
                            
                            print(jsonRoot)
                            
                            self.imageValue = ""

                            if let ResultPayload = jsonRoot!["resultPayload"] as? [String:Any]!{
                                
                                //print(ResultPayload)
                                
                                self.descriptionOutlet.text = ResultPayload!["description"] as? String
                                
                                let dateFormatter = DateFormatter()
                                dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                                
                                self.numberOfStarCounts.text = String(describing: ResultPayload!["starsCount"] as! Int)
                            }
                        }
                    }
                    
                    
                case .failure(let error):
                    print(error)
                }
                
                
            }

            
        }
        else
        {
        
        let image = UIImage(named: "Star filled w-shadow") as UIImage?
        
        
        self.star1.setImage(image, for: .normal)
        
        let parameters: [String : Any]? = [
            "NumOfStars": 1 ,
            "photoId":  getID ,
            ]
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/rating"
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                
                //print(response)
                self.getImageInfo()
                let url2 = "https://testglostarsdevelopers.azurewebsites.net/api/images/picture/\(self.getID)"
                let headers: HTTPHeaders = [
                    
                    "Authorization": tokenType + accessToken ,
                    "Content-Type": "application/json",
                    
                    ]
                
                Alamofire.request(url2, method:.get,encoding:JSONEncoding.default,headers:headers).responseJSON { response in
                    
                    if let jsonRoot = response.result.value as? [String:Any]!{
                        
                        print(jsonRoot)
                        
                        
                        if let ResultPayload = jsonRoot!["resultPayload"] as? [String:Any]!{
                            
                            //print(ResultPayload)
                            
                            self.descriptionOutlet.text = ResultPayload!["description"] as? String
                            
                            let dateFormatter = DateFormatter()
                            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                            
                            self.numberOfStarCounts.text = String(describing: ResultPayload!["starsCount"] as! Int)
                            
                           
                        }
                    }
                }

                
            case .failure(let error):
                print(error)
            }
    
            
        }
        }

    }
    
   
    @IBAction func starTwoAction(_ sender: Any) {
        
        let image = UIImage(named: "star1") as UIImage?
        self.star1.setImage(image, for: .normal)
       // self.star2.setImage(image, for: .normal)
        
        
        let parameters: [String : Any]? = [
            "NumOfStars": 2 ,
            "photoId":  getID,
            ]
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/rating"
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                
                print(response)
                //self.getImageInfo()
                let url2 = "https://testglostarsdevelopers.azurewebsites.net/api/images/picture/\(self.getID)"
                let headers: HTTPHeaders = [
                    
                    "Authorization": tokenType + accessToken ,
                    "Content-Type": "application/json",
                    
                    ]
                
                Alamofire.request(url2, method:.get,encoding:JSONEncoding.default,headers:headers).responseJSON { response in
                    
                    if let jsonRoot = response.result.value as? [String:Any]!{
                        
                        print(jsonRoot)
                        
                        
                        if let ResultPayload = jsonRoot!["resultPayload"] as? [String:Any]!{
                            
                            //print(ResultPayload)
                            
                            self.descriptionOutlet.text = ResultPayload!["description"] as? String
                            
                           
                            print(self.commentId)
                            let dateFormatter = DateFormatter()
                            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                            
                            self.numberOfStarCounts.text = String(describing: ResultPayload!["starsCount"] as! Int)
                            
                            
                          
                            
                        }
                    }
                }

                
                
            case .failure(let error):
                print(error)
            }
            

            
        }
        

    }

    
    
    @IBAction func starThree(_ sender: Any) {
        
        let image = UIImage(named: "star1") as UIImage?
        self.star1.setImage(image, for: .normal)
       // self.star2.setImage(image, for: .normal)
        //self.star3.setImage(image, for: .normal)

        
        let parameters: [String : Any]? = [
            "NumOfStars": 3 ,
            "photoId": getID ,
            ]
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/rating"
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                
                //print(response)
                //self.getImageInfo()
                let url2 = "https://testglostarsdevelopers.azurewebsites.net/api/images/picture/\(self.getID)"
                let headers: HTTPHeaders = [
                    
                    "Authorization": tokenType + accessToken ,
                    "Content-Type": "application/json",
                    
                    ]
                
                Alamofire.request(url2, method:.get,encoding:JSONEncoding.default,headers:headers).responseJSON { response in
                    
                    if let jsonRoot = response.result.value as? [String:Any]!{
                        
                       // print(jsonRoot)
                        
                        
                        if let ResultPayload = jsonRoot!["resultPayload"] as? [String:Any]!{
                            
                            print(ResultPayload)
                            
                            self.descriptionOutlet.text = ResultPayload!["description"] as? String

                            
                            
                            
                            let dateFormatter = DateFormatter()
                            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                            
                            self.numberOfStarCounts.text = String(describing: ResultPayload!["starsCount"] as! Int)
                            
                            
                        }
                    }
                }

                
                
            case .failure(let error):
                print(error)
            }
            
        }
        

    }
    
    @IBAction func starFourAction(_ sender: Any) {
        
        let image = UIImage(named: "star1") as UIImage?
        
        self.star1.setImage(image, for: .normal)
       // self.star2.setImage(image, for: .normal)
       // self.star3.setImage(image, for: .normal)
      //  self.star4.setImage(image, for: .normal)
        
        let parameters: [String : Any]? = [
            "NumOfStars": 4 ,
            "photoId": getID ,
            ]
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/rating"
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                
                print(response)
                //self.getImageInfo()
                let url2 = "https://testglostarsdevelopers.azurewebsites.net/api/images/picture/\(self.getID)"
                let headers: HTTPHeaders = [
                    
                    "Authorization": tokenType + accessToken ,
                    "Content-Type": "application/json",
                    
                    ]
                
                Alamofire.request(url2, method:.get,encoding:JSONEncoding.default,headers:headers).responseJSON { response in
                    
                    
                    
                    if let jsonRoot = response.result.value as? [String:Any]!{
                        
                        print(jsonRoot)
                        
                       
                        
                        if let ResultPayload = jsonRoot!["resultPayload"] as? [String:Any]!{
                            
                            //print(ResultPayload)
                            
                            self.descriptionOutlet.text = ResultPayload!["description"] as? String
                            
                            let dateFormatter = DateFormatter()
                            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                            
                            self.numberOfStarCounts.text = String(describing: ResultPayload!["starsCount"] as! Int)
                            
                            self.numberOfStarCounts.text = String(describing: ResultPayload!["starsCount"] as! Int)
                            
                        
                      

                        }
                    }
                }

                
                
            case .failure(let error):
                print(error)
            }
           // UserDefaults().set(image, forKey: "star4")


        }
        

    }
    
    
    
    func displayShareSheet(ImageShare:UIImage) {
        
        let activityViewController = UIActivityViewController(activityItems: [ImageShare as UIImage], applicationActivities: nil)
        present(activityViewController, animated: true, completion: {})
        
    }
    
    
    
    
    @IBAction func shareImage(_ sender: Any) {
        
        let vc = SLComposeViewController(forServiceType:SLServiceTypeFacebook)
        vc?.add(imageView.image)
        
        vc?.add(URL(string: "https://testglostarsdevelopers.azurewebsites.net/Home/ViewPicture/\(getID)"))
        
       // vc?.setInitialText("Initial text here.")
        
        self.present(vc!, animated: true, completion: nil)
         //displayShareSheet(ImageShare: imageView.image!)
//        let textToShare = ""
//        
//        let myWebsite = NSURL(string: "https://testglostarsdevelopers.azurewebsites.net")
//        if let image = imageView.image
//        
//        {
//            let objectsToShare = [textToShare, myWebsite! , image] as [Any]
//            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
//            
//            //New Excluded Activities Code
//            activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
//            //
//            
//            activityVC.popoverPresentationController?.sourceView = sender as? UIView
//            self.present(activityVC, animated: true, completion: nil)
//        }
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        let imageViewSize = imageView.frame.size
        let scrollViewSize = scrollView.bounds.size
        
        let verticalPadding = imageViewSize.height < scrollViewSize.height ? (scrollViewSize.height - imageViewSize.height) / 2 : 0
        let horizontalPadding = imageViewSize.width < scrollViewSize.width ? (scrollViewSize.width - imageViewSize.width) / 2 : 0
        
        scrollView.contentInset = UIEdgeInsets(top: verticalPadding, left: horizontalPadding, bottom: verticalPadding, right: horizontalPadding)
    }

    
    @IBAction func crossReatingAction(_ sender: Any) {
        
        
        
       
        
    }

    @IBAction func starFiveAction(_ sender: Any) {
        
        
        let image = UIImage(named: "star1") as UIImage?
        
        
        self.star1.setImage(image, for: .normal)
       // self.star2.setImage(image, for: .normal)
      //  self.star3.setImage(image, for: .normal)
      //  self.star4.setImage(image, for: .normal)
      //  self.star5.setImage(image, for: .normal)
        
        let parameters: [String : Any]? = [
            "NumOfStars": 5 ,
            "photoId": getID ,
            ]
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/rating"
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                
                print(response)
                
                let url2 = "https://testglostarsdevelopers.azurewebsites.net/api/images/picture/\(self.getID)"
                let headers: HTTPHeaders = [
                    
                    "Authorization": tokenType + accessToken ,
                    "Content-Type": "application/json",
                    
                    ]
                
                Alamofire.request(url2, method:.get,encoding:JSONEncoding.default,headers:headers).responseJSON { response in
                    
                    if let jsonRoot = response.result.value as? [String:Any]!{
                        
                       // print(jsonRoot)
                        
                        
                        if let ResultPayload = jsonRoot!["resultPayload"] as? [String:Any]!{
                            
                            //print(ResultPayload)
                            
                            self.descriptionOutlet.text = ResultPayload!["description"] as? String
                            
                            let dateFormatter = DateFormatter()
                            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                            
                            self.numberOfStarCounts.text = String(describing: ResultPayload!["starsCount"] as! Int)
                           
                            
                           // self.imageStar4 = image!

                        }
                    }
                }
                
            case .failure(let error):
                print(error)
            }

            
        }
        

    }
 
  
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
 
    @IBAction func profileSendAction(_ sender: Any) {
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        
        
        passData.getid = getUserID
        
        
        
        
        
        self.navigationController?.pushViewController(passData, animated: true)
        
        
    }
    
    
    
//    @IBAction func postComments(_ sender: Any) {
//        
//     
//        
//       
//        let parameters: [String : Any]? = [
//            "CommentText": commentsTextField.text!,
//            "photoId": getID ,
//   ]
//        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/comment"
//        let headers: HTTPHeaders = [
//            "Content-Type": "application/json"
//        ]
//        Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
//            switch response.result {
//            case .success:
//                print(response)
//                self.CommentsPic.removeAll()
//                self.getImageInfo()
//            case .failure(let error):
//                print(error)
//            }
//        }
//        commentsTextField.text = ""
//
//     
//        
//    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        
//        
//        print("Row Selected")
//        
//    }

    func getImageInfoComment(){
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/picture/\(getID)"
        let headers: HTTPHeaders = [
            
            "Authorization": tokenType + accessToken ,
            "Content-Type": "application/json",
            
            ]
        
        Alamofire.request(url, method:.get,encoding:JSONEncoding.default,headers:headers).responseJSON { response in
            
            
            if response.result.value != nil {
                
                if let jsonRoot = response.result.value as? [String:Any]!{
                    
                    print(jsonRoot)
                    
                    
                    if let ResultPayload = jsonRoot!["resultPayload"] as? [String:Any]!{
                        
                        //print(ResultPayload)
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        if let comments = ResultPayload!["comments"] as! [[String: Any]]!{
                            
                            //print(comments)
                            
                            
                            
                            for array in comments{
                                
                                print(array)
                                
                                guard let allcomments = Mapper<SingleCommentsMap>().map(JSON: array) else{
                                    
                                    continue
                                }
                                
                                self.CommentsPic.append(allcomments)
                                print(self.CommentsPic)
                                self.CommentsPic.reverse()
                                
                                
                                
                                
                                
                                
                                
                            }
                            
                            
                        }
                        
                        
                        if let poster = ResultPayload!["poster"] as? [String:Any]!{
                            
                            //print(poster)
                            
                            
                            //                        self.imageUser.sd_setImage(with: PicUrl! as URL, placeholderImage: nil , options: .scaleDownLargeImages, completed: nil)
                        }
                        
                    }
                    
                    
                }
                self.tableViewComment.reloadData()
            }
            else {
                
                print("Internet error")
                
            }
            
            
        }
        
    }

    func DeleteCommentinfo(_ title:String,message:String){
        
        let alertController = UIAlertController(title: title, message: message , preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            print("OK Pressed")
            
            let DeleteComment = "https://testglostarsdevelopers.azurewebsites.net/api/images/DeleteComment?commentId=\(self.commentIdCell)"
            //print(allComments.commentId)
            
            Alamofire.request(DeleteComment, method: .get).responseJSON { response in
                
                switch response.result {
                    
                case .success:
                    print(response)
                    self.CommentsPic.removeAll()
                    self.getImageInfoComment()
                    // self.performSegue(withIdentifier: "News", sender: NewsVCViewController.self)
                    
                    
                case .failure(let error):
                    print(error)
                }
                
            }
            
            
            
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
            print("Cancel Pressed")
            //self.dismiss(animated: true, completion: nil)
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let Cell = tableViewComment.dequeueReusableCell(withIdentifier: "SingleCommentCell", for: indexPath) as! SingleCommentCell
        
        Cell.commentMsg.isEditable = false
        tableViewComment.separatorColor = UIColor.brown
        
        
        let allComments = self.CommentsPic[indexPath.row]
        
        let urlImage = NSURL(string: allComments.userproPic )
        
        Cell.commentMsg.text = allComments.comments
        Cell.userImage.sd_setImage(with: urlImage! as URL, placeholderImage: nil , options: .scaleDownLargeImages, completed: nil)
        Cell.username.text = allComments.userName + allComments.lastName
        
        Cell.deleteButtonOutlet.isHidden = true
        Cell.editCommentOutlet.isHidden = true
        
        if userID == allComments.commenterId {
            
            Cell.deleteButtonOutlet.isHidden = false
            
        }
        if userID == allComments.commenterId {
            
            Cell.editCommentOutlet.isHidden = false
            
        }
        
        //Cell.deleteButtonOutlet.alpha = 0
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let date = dateFormatter.date(from: allComments.uploadTime ){
            
            Cell.uploadTime.text = timeAgoSinceDate(date, currentDate: Date(), numericDates: true)
        }
        
        
        //Cell.userLastName.text = allComments.lastName
        
        
        Cell.onProfileTapped = {
            
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            
            
            passData.getid = allComments.commenterId
            
            
            
            
            
            self.navigationController?.pushViewController(passData, animated: true)
            
            
        }
        Cell.onProfileTapped1 = {
            
            self.commentIdCell = String(allComments.commentId)
            self.DeleteComment("Warning", message: "Do you want to Delete your Comment")
            
        }
        Cell.onProfileTapped2 = {
            
            
            if Cell.editCommentOutlet.currentTitle == "Edit"{
                
                Cell.editCommentOutlet.setTitle("Done", for: .normal)
                Cell.commentMsg.isEditable = true
                Cell.commentMsg.becomeFirstResponder()
                
            }
                
            else {
                
                let parameter = [
                    
                    "PhotoId" : self.getID,
                    "CommentId" : allComments.commentId,
                    "NewCommentText" : Cell.commentMsg.text
                    
                    
                    ] as [String : Any]
                
                let EditComment = "https://testglostarsdevelopers.azurewebsites.net/api/images/comment"
                
                //         Alamofire.request(EditComment, method: .put, parameters: parameter, encoding: .URLEncoding.https, headers: nil).response
                Alamofire.request(EditComment, method: .put, parameters: parameter, encoding: URLEncoding.httpBody, headers: nil).responseJSON { response in
                    
                    switch response.result {
                        
                    case .success:
                        print(response)
                        Cell.editCommentOutlet.setTitle("Edit", for: .normal)
                        print(self.commentText.text)
                        self.CommentsPic.removeAll()
                        self.getImageInfoComment()
                        //self.tableViewComment.reloadData()
                        
                    case .failure(let error):
                        print(error)
                    }
                    
                }
                
                
                
                
            }
            
            
            
            
        }
        
        
        
        
        
        
        
        return Cell

        
    }
    
    func getImageInfo (){
        
//        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/picture/\(getID)"
//        let headers: HTTPHeaders = [
//            
//            "Authorization": tokenType + accessToken ,
//            "Content-Type": "application/json",
//            
//            ]
//        
//       Alamofire.request(url, method:.get,encoding:JSONEncoding.default,headers:headers).responseJSON { response in
//        
//        
//        if response.result.value != nil {
//        
//            if let jsonRoot = response.result.value as? [String:Any]!{
//                
//                print(jsonRoot)
//                
//                
//                if let ResultPayload = jsonRoot!["resultPayload"] as? [String:Any]!{
//                    
//                    //print(ResultPayload)
//                   
//                    self.descriptionOutlet.text = ResultPayload!["description"]as? String
//                    
//                    if ResultPayload!["myStarCount"] as! Int == 5 {
//                        
//                        let image = UIImage(named: "Star filled w-shadow") as UIImage?
//                        
//                        self.imageValue = "Checked"
//
//                        self.star1.setImage(image, for: .normal)
////                        self.star2.setImage(image, for: .normal)
////                        self.star3.setImage(image, for: .normal)
////                        self.star4.setImage(image, for: .normal)
////                        self.star5.setImage(image, for: .normal)
//
//                        
//                    }
//                    if ResultPayload!["myStarCount"] as! Int == 4 {
//                        
//                        let image = UIImage(named: "Star filled w-shadow") as UIImage?
//                        self.imageValue = "Checked"
//
//                        
//                        self.star1.setImage(image, for: .normal)
////                        self.star2.setImage(image, for: .normal)
////                        self.star3.setImage(image, for: .normal)
////                        self.star4.setImage(image, for: .normal)
////                        
//                    }
//                    if ResultPayload!["myStarCount"] as! Int == 3 {
//                        
//                        let image = UIImage(named: "Star filled w-shadow") as UIImage?
//                        
//                        self.imageValue = "Checked"
//
//                        self.star1.setImage(image, for: .normal)
////                        self.star2.setImage(image, for: .normal)
////                        self.star3.setImage(image, for: .normal)
//                        
//                    }
//                    if ResultPayload!["myStarCount"] as! Int == 2 {
//                        
//                        let image = UIImage(named: "Star filled w-shadow") as UIImage?
//                        self.imageValue = "Checked"
//
//                        
//                        self.star1.setImage(image, for: .normal)
//                       // self.star2.setImage(image, for: .normal)
//                        
//                    }
//                    if ResultPayload!["myStarCount"] as! Int == 1 {
//                        
//                        let image = UIImage(named: "Star filled w-shadow") as UIImage?
//                        self.imageValue = "Checked"
//
//                        
//                        self.star1.setImage(image, for: .normal)
//                        
//                    }
//                    if ResultPayload!["myStarCount"] as! Int == 0 {
//                        
//                        let image = UIImage(named: "Star empty w-shadow") as UIImage?
//                        self.imageValue = ""
//                        
//                        
//                        self.star1.setImage(image, for: .normal)
//                        
//                    }
//
//                    self.photoId = (ResultPayload!["id"] as? Int!)!
//                    print(self.photoId)
//
//                    
//                    self.isComprting = (ResultPayload!["isCompeting"] as? Bool)!
//                print(self.isComprting)
//                    
//           if  self.isComprting == true {
//                    
//                    
//         // self.heightConstraintsStar2.constant = 20.0
//            
//            self.imagePrivacyOutlet.image = UIImage(named: "privacy_competition_photo")
//                    
//                    
//            
//                    }
//                    
//                    if  self.isComprting == false {
//                        
////                        self.heightConstraintsStar2.constant = 0.0
////                        self.vanishedContraints1.constant = 0
////                        self.vanishedConstraints2.constant = 0
////                        self.vanishedConstraints3.constant = 0
////                        self.vanishedConstrains4.constant = 0
//                        
//                        self.imagePrivacyOutlet.image = UIImage(named: "Public_Icon")
//                        
//                    }
//
//                    
//                    
//                    self.is_mutual = (ResultPayload!["is_mutual"] as? Bool)!
//                    self.me_follow = (ResultPayload!["me_follow"] as? Bool)!
//                    self.he_follow = (ResultPayload!["he_follow"] as? Bool)!
//                    
//                    
//                    
//                    
//                    if self.is_mutual == true {
//                        
//                        self.followingButtonOutlet.setTitle("Mutual", for: .normal)
//                        self.followingButtonOutlet.backgroundColor = UIColor.clear
//                        
//                    }
//                    
//                    
//                    if self.is_mutual == false && self.me_follow == false && self.he_follow == false {
//                        
//                        self.followingButtonOutlet.setTitle("Follow", for: .normal)
//                        
//                        self.followingButtonOutlet.backgroundColor = UIColor.clear
//                    }
//                    
//                    if self.is_mutual == false && self.me_follow == true && self.he_follow == false {
//                        
//                        self.followingButtonOutlet.setTitle("Following", for: .normal)
//                        self.followingButtonOutlet.backgroundColor = UIColor.clear
//                        
//                    }
//                    if self.is_mutual == false && self.me_follow == false && self.he_follow == true {
//                        
//                        self.followingButtonOutlet.setTitle("Follow Back", for: .normal)
//                        self.followingButtonOutlet.backgroundColor = UIColor.clear
//                        
//                    }
//
//                    
//                    let dateFormatter = DateFormatter()
//                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
//                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
//                    if let date = dateFormatter.date(from: (ResultPayload!["uploaded"] as? String)!){
//
//                      self.userTime.text = self.timeAgoSinceDate(date, currentDate: Date(), numericDates: true)
//                    }
//                    
//                    self.numberOfStarCounts.text = String(describing: ResultPayload!["starsCount"] as! Int)
//                    
//                   
//                    
//                    if let comments = ResultPayload!["comments"] as! [[String: Any]]!{
//                        
//                      //print(comments)
//                        self.numberOfComments.text = String(comments.count)
//                        
//                        
//                        for array in comments{
//                            
//                            print(array)
//                            
//                            guard let allcomments = Mapper<SingleCommentsMap>().map(JSON: array) else{
//                                
//                                continue
//                            }
//                            
//                           self.CommentsPic.append(allcomments)
//                            print(self.CommentsPic)
//                            
//                            
//                            
//                            
//                            
//                            
//
//                        }
//
//                        
//                    }
//                    
//                   
//                    if let poster = ResultPayload!["poster"] as? [String:Any]!{
//                        
//                        //print(poster)
//                        self.userName.text = (poster!["name"] as? String)! + "  |  "
//                        
//                        self.getUserID = (poster!["userId"] as? String)!
//                        
//                        if userID == self.getUserID {
//                            
//                            self.followingButtonOutlet.isHidden = true
//                            self.deleteButtonOutlet.isHidden = false
//                            //self.starFollowingHConstraints.constant = 0
//                            //self.widthConstraintsDelete.constant = 25.0
//                        }
//                        
//                        self.getprofilePicUrl = (poster!["profilePicURL"] as? String)!
//                        let url2 = NSURL(string: self.getprofilePicUrl)
//
//                        self.imageUser.af_setImage(withURL: url2! as URL)
//                        self.getfirstName = (poster!["name"] as? String)!
//
//                        
////                        let PicUrl = NSURL(string: (poster["profilePicURL"] as? String)! )
//                        
////                        self.imageUser.sd_setImage(with: PicUrl! as URL, placeholderImage: nil , options: .scaleDownLargeImages, completed: nil)
//                    }
//                    
//                }
//                
//                
//            }
//        //self.tableView.reloadData()
//        }
//        else {
//            
//            print("Internet error")
//            
//        }
//
//        
//    }
        
    }
    
    func RealTimeFollow(){
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/picture/\(getID)"
        let headers: HTTPHeaders = [
            
            "Authorization": tokenType + accessToken ,
            "Content-Type": "application/json",
            
            ]
        
        Alamofire.request(url, method:.get,encoding:JSONEncoding.default,headers:headers).responseJSON { response in
            
            if response.result.value != nil {
            
            if let jsonRoot = response.result.value as? [String:Any]!{
                
                print(jsonRoot)
                
                
                if let ResultPayload = jsonRoot!["resultPayload"] as? [String:Any]!{
                    
                    //print(ResultPayload)
                    
                    self.descriptionOutlet.text = ResultPayload!["description"] as? String
                    
                    
                    self.isComprting = (ResultPayload!["isCompeting"] as? Bool)!
                    print(self.isComprting)
                    
                    if  self.isComprting == true {
                        
                        
                      //  self.heightConstraintsStar2.constant = 20.0
                        
                        
                        
                    }
                    
                    if  self.isComprting == false {
                        
                        
                     //   self.heightConstraintsStar2.constant = 0.0
                        
                        
                    }
                    
                    
                    
                    self.is_mutual = (ResultPayload!["is_mutual"] as? Bool)!
                    self.me_follow = (ResultPayload!["me_follow"] as? Bool)!
                    self.he_follow = (ResultPayload!["he_follow"] as? Bool)!
                    
                    if self.is_mutual == true {
                        
                        self.followingButtonOutlet.setTitle("Mutual", for: .normal)
                        self.followingButtonOutlet.backgroundColor = UIColor.clear
                        
                    }
                    
                    
                    if self.is_mutual == false && self.me_follow == false && self.he_follow == false {
                        
                        self.followingButtonOutlet.setTitle("Follow", for: .normal)
                        self.followingButtonOutlet.backgroundColor = UIColor.clear
                        
                        
                    }
                    
                    if self.is_mutual == false && self.me_follow == true && self.he_follow == false {
                        
                        self.followingButtonOutlet.setTitle("Following", for: .normal)
                        self.followingButtonOutlet.backgroundColor = UIColor.clear
                    }
                    
                    if self.is_mutual == false && self.me_follow == false && self.he_follow == true {
                        
                        self.followingButtonOutlet.setTitle("Follow Back", for: .normal)
                        self.followingButtonOutlet.backgroundColor = UIColor.clear
                    }
                    
                    
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                    if let date = dateFormatter.date(from: (ResultPayload!["uploaded"] as? String)!){
                        
                        self.userTime.text = self.timeAgoSinceDate(date, currentDate: Date(), numericDates: true)
                    }
                    
                    self.numberOfStarCounts.text = String(describing: ResultPayload!["starsCount"] as! Int)
                    
                    
                    
                    if let comments = ResultPayload!["comments"] as! [[String: Any]]!{
                        
                        //print(comments)
                        self.numberOfComments.text = String(comments.count)
                        
                        
                        for array in comments{
                            
                            print(array)
                            
                            guard let allcomments = Mapper<DetailComMapper>().map(JSON: array) else {
                                
                                continue
                            }
                            
                            //self.CommentsPic.append(allcomments)
                            
                        }
                        
                        
                    }
                    
                    
                    if let poster = ResultPayload!["poster"] as? [String:Any]!{
                        
                        //print(poster)
                        self.userName.text = poster!["name"] as? String
                        
                        self.getUserID = (poster!["userId"] as? String)!
                        
                        if userID == self.getUserID {
                            
                      //      self.deleteWidthConstraints.constant = 20
                            //self.widthConstraintsDelete.constant = 25.0

                            
                        }
                        
                        
                        self.getprofilePicUrl = (poster!["profilePicURL"] as? String)!
                        self.getfirstName = (poster!["name"] as? String)!
                        
                        
                        let PicUrl = NSURL(string: (poster!["profilePicURL"] as? String)! )
                        print(PicUrl)
                        
                        self.imageUser.sd_setImage(with: PicUrl! as URL, placeholderImage: nil , options: .scaleDownLargeImages, completed: nil)
                    }
                    
                }
                
                
            }
           // self.tableView.reloadData()
            
            }
            else {
                
                
                print("Internet Error")
                
            }
            
        }
        
    }
    
    
   
    @IBAction func followingButtonAction(_ sender: Any) {
        
       
        if is_mutual == true  {
            
            let unfollowUrl = "https://testglostarsdevelopers.azurewebsites.net/api/Follower/Unfollowing?id=\(getUserID)"
            Alamofire.request(unfollowUrl, method: .post).responseJSON { response in
                
                print(response)
                self.CommentsPic.removeAll()
                self.getImageInfo()
                
            }
        }
        else if me_follow == true {
            
          UnfollowClick(" Warning ", message: "Do You Want to Unfollow")
            
        }
        else {
            
            let url = "https://testglostarsdevelopers.azurewebsites.net/api/Follower/Following?id=\(getUserID)"
            
            print(getUserID)
            
            Alamofire.request(url, method: .post).responseJSON { response in
                
                
                
                
                print(response)
                self.CommentsPic.removeAll()
                self.getImageInfo()
                
                
                
                
                
                
                
            }
        }
        
   
                

        
        
    }
    

    
   // "https://testglostarsdevelopers.azurewebsites.net/Following/usrId"
   
  
   
    
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
//    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
    
    func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!)y"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1y"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!)m"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1m"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!)w"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1w"
            } else {
                return "Last Week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!)d"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1d"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!)h"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1h"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 8) {
            return "\(components.minute!) min"
        } else if (components.minute! >= 8){
            if (numericDates){
                return "1min"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 0) {
            return "Just now"
        } else {
            return "Just now"
        }
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        moveTextView(textView, moveDistance: -100, up: true)

    }
    func textViewDidEndEditing(_ textView: UITextView) {
        moveTextView(textView, moveDistance: -100, up: false)

    }
    
    // Start Editing The Text Field
//    func textFieldDidBeginEditing(_ textField: UITextView) {
//        moveTextField(textView, moveDistance: -100, up: true)
//    }
//    
//    // Finish Editing The Text Field
//    func textFieldDidEndEditing(_ textField: UITextView) {
//        moveTextField(textView, moveDistance: -100, up: false)
//    }
    
    // Hide the keyboard when the return key pressed
    func textViewShouldReturn(_ textView: UITextView) -> Bool {
        textView.resignFirstResponder()
        return true
    }

    func textView(_ textView: UITextView, shouldChangeTextIn _: NSRange, replacementText text: String) -> Bool {
        let resultRange = text.rangeOfCharacter(from: CharacterSet.newlines, options: .backwards)
        if text.characters.count == 1 && resultRange != nil {
            textView.resignFirstResponder()
            // Do any additional stuff here
            return false
        }
        return true
    }
    // Move the text field in a pretty animation!
    func moveTextView(_ textView: UITextView, moveDistance: Int, up: Bool) {
        let moveDuration = 0.3
        let movement: CGFloat = CGFloat(up ? moveDistance : -moveDistance)
        
        UIView.beginAnimations("animateTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(moveDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }

 
    
//    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
//        
//        return self.imageView
//    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
