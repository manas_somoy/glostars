//
//  FollowingMapper.swift
//  Glostars
//
//  Created by Sanzid iOS on 6/9/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import Foundation
import ObjectMapper

class FBMapper : Mappable {
    

    dynamic var Url : String = ""

    required convenience init?(map: Map) {
        
        
        self.init()
        
        
    }
    
    func mapping(map: Map) {
        
        Url <- map["url"]
    }
}

