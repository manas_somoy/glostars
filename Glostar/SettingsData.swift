//
//  SettingsData.swift
//  Glostars
//
//  Created by Sanzid iOS on 11/16/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit

class SettingsData: UIViewController {

    @IBOutlet var webView: UIWebView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = false
        self.title = getData
        print(getData)
        if getData == "AboutUS" {
            if let url = Bundle.main.url(forResource: "About us", withExtension: "html") {
                webView.loadRequest(URLRequest(url: url))
            }
 
        }
        if getData == "Terms Of Use" {
            if let url = Bundle.main.url(forResource: "Terms of Use", withExtension: "html") {
                webView.loadRequest(URLRequest(url: url))
            }
            
        }
        if getData == "Data Privacy" {
            if let url = Bundle.main.url(forResource: "Data Privacy", withExtension: "html") {
                webView.loadRequest(URLRequest(url: url))
            }
            
        }
        if getData == "faq" {
            if let url = Bundle.main.url(forResource: "Frequently Asked Questions", withExtension: "html") {
                webView.loadRequest(URLRequest(url: url))
            }
            
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
