//
//  DetailGaleryCollectionViewCell.swift
//  Glostars
//
//  Created by Sanzid Ashan on 4/7/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit

class DetailGaleryViewController: UIViewController {
    
    @IBOutlet weak var imgImage: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    var getAllImag = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgImage.layer.borderWidth = 0
        
        // Do any additional setup after loading the view.
        
        let url = NSURL(string: getAllImag)
        imgImage.af_setImage(withURL: url! as URL)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
