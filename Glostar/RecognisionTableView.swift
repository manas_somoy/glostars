//
//  RecognisionTableView.swift
//  Glostars
//
//  Created by Sanzid Ashan on 4/7/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit

class RecognisionTableView: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet var tableView: UITableView!

    
    var imageArray = [UIImage(named:"1-1"),UIImage(named:"2-1"),UIImage(named:"3-1"),UIImage(named:"4-1"),UIImage(named:"5-1"),UIImage(named:"W-1"),UIImage(named:"M-1")]
    var ImageText = ["1st Prize","2nd Prize","3rd Prize","4th Prize","5th Prize","Monthly Prize","Weekly Prize"]
    var PrizeNames = ["PRINTOMATIC Instant Camera","Smart Alarm Clock","Magnetic Tripod ","LED Bulb Speaker","Smart wristband","Mobile Camera Lense","Mini power bank",]

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.contentInset = UIEdgeInsetsMake(40.0, 0.0, 60.0, 0.0)

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return imageArray.count
    }

    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RecognisitionCell
        
        print(indexPath.row)
        if (indexPath.row % 2 == 0){
            cell.backgroundColor = UIColor.init(hex: "e1c8ff")
        }
        else {
            cell.backgroundColor = UIColor.init(hex: "ffffff")
        }
        cell.recogImage.image = imageArray[indexPath.row]
        cell.prizeText.text = ImageText[indexPath.row]
        cell.prizeNames.text = PrizeNames[indexPath.row]

        // Configure the cell...
        return cell
    }
    
 
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.panGestureRecognizer.translation(in: scrollView.superview).y > 0 {
            //scrolling downwards
            
            navigationController?.isNavigationBarHidden = false
            
            
            if scrollView.contentOffset.y < 0 {
                //this means we are at top of the scrollView
                navigationController?.isNavigationBarHidden = false
                
                
            }
        }
        else {
            //we are scrolling upward
            
            navigationController?.isNavigationBarHidden = true
            
            
        }
    }

  
 
  
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
