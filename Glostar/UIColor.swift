//
//  UIColor+HexColor.swift
//  Demo_app_R&D
//
//  Created by Shamiul on 12/7/17.
//  Copyright © 2017 Shamiul. All rights reserved.
//


import UIKit

extension UIColor {
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
    

    
    convenience init (hex:String) {
        //let newScanner = Scanner(string: hex)
       // newScanner.scanLocation = 0
        
//        var RGBValue: UInt64 = 0
//        
//        newScanner.scanHexInt64(&RGBValue)
//        
//        let r = (RGBValue & 0xff0000) >> 16
//        let g = (RGBValue & 0xff00) >> 8
//        let b = RGBValue & 0xff
//        
//        self.init(
//            red: CGFloat(r) / 0xff,
//            green: CGFloat(g) / 0xff,
//            blue: CGFloat(b) / 0xff, alpha: 1
//        )
//        
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            let index = cString.index(cString.startIndex, offsetBy: 1)
            cString = String(cString[index...])
        }
        
        if ((cString.count) != 6) {
            fatalError("invalid hex color")
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        
        self.init(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                  green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                  blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                  alpha: CGFloat(1.0)
        )
        
        
    }
    
    func lighter(amount : CGFloat = 0.25) -> UIColor {
        return hueColorWithBrightnessAmount(amount: 1 + amount)
    }
    
    func darker(amount : CGFloat = 0.25) -> UIColor {
        return hueColorWithBrightnessAmount(amount: 1 - amount)
    }
    
    private func hueColorWithBrightnessAmount(amount: CGFloat) -> UIColor {
        var hue         : CGFloat = 0
        var saturation  : CGFloat = 0
        var brightness  : CGFloat = 0
        var alpha       : CGFloat = 0
        
        
        if getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha) {
            return UIColor( hue: hue,
                            saturation: saturation,
                            brightness: brightness * amount,
                            alpha: alpha )
        } else {
            return self
        }
    }
    
    
}


//MARK:- CGFloat Extensions
//Divided by 0 fix
infix operator ~/
extension CGFloat {
    public static func ~/(lhs: CGFloat, rhs: CGFloat) -> CGFloat {
        if rhs == 0 {
            return 0
        }
        return lhs/rhs
    }
}

//Limit rhs (expl: 4 !> 3 ,result will be 3 )
infix operator !>
extension CGFloat {
    public static func !>(lhs: CGFloat, rhs: CGFloat) -> CGFloat {
        if lhs > rhs {
            return rhs
        }
        return lhs
    }
}
//Limit lhs (expl: 3 !< 4 ,result will be 3 )
infix operator !<
extension CGFloat {
    public static func !<(lhs: CGFloat, rhs: CGFloat) -> CGFloat {
        if lhs > rhs {
            return lhs
        }
        return rhs
    }
}

//MARK:- UIColor Extensions
extension UIColor {
    struct ColorComponents {
        var r:CGFloat, g:CGFloat, b:CGFloat, a:CGFloat
    }
    var getComponents:ColorComponents? {
        if (self.cgColor.numberOfComponents == 2) {
            guard let cc = self.cgColor.components else { return nil }
            return ColorComponents(r:cc[0], g:cc[0], b:cc[0], a:cc[1])
        }
        else {
            guard let cc = self.cgColor.components else { return nil }
            return ColorComponents(r:cc[0], g:cc[1], b:cc[2], a:cc[3])
        }
    }
    
    //Tested
    func interpolateRGBColorTo(end: UIColor, fraction: CGFloat) -> UIColor? {
        var f = max(0, fraction)
        f = min(1, fraction)
        
        guard let c1 = self.getComponents,let c2 = end.getComponents else { return nil }
        
        
        let r = c1.r + (c2.r - c1.r) * f
        let g = c1.g + (c2.g - c1.g) * f
        let b = c1.b + (c2.b - c1.b) * f
        let a = c1.a + (c2.a - c1.a) * f
        
        return UIColor.init(red: r, green: g, blue: b, alpha: a)
    }
    
    
    //May not work
    func interpolateHSVColorFrom(end: UIColor, fraction: CGFloat) -> UIColor {
        var f = max(0, fraction)
        f = min(1, fraction)
        var h1: CGFloat = 0, s1: CGFloat = 0, b1: CGFloat = 0, a1: CGFloat = 0
        self.getHue(&h1, saturation: &s1, brightness: &b1, alpha: &a1)
        var h2: CGFloat = 0, s2: CGFloat = 0, b2: CGFloat = 0, a2: CGFloat = 0
        end.getHue(&h2, saturation: &s2, brightness: &b2, alpha: &a2)
        let h = h1 + (h2 - h1) * f
        let s = s1 + (s2 - b1) * f
        let b = b1 + (b2 - b1) * f
        let a = a1 + (a2 - a1) * f
        return UIColor(hue: h, saturation: s, brightness: b, alpha: a)
    }
    
}



