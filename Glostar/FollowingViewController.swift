//
//  FollowingViewController.swift
//  Glostars
//
//  Created by Sanzid Ashan on 3/30/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import ObjectMapper
import SwiftMessages

class FollowingViewController: UIViewController, UITableViewDataSource,UITableViewDelegate,DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    
    @IBOutlet weak var followerTableViewTopConstraints: NSLayoutConstraint!
    
    var GetFollowerID = ""
    
    var FollowingNotifyArray : Array< FollowingMapper > = []
    var FollowingstatusArray : Array <FollowingStatusMapper> = []
    var is_mutual = Bool()
    var me_follow = Bool()
    var he_follow = Bool()
    @IBOutlet weak var achivementButton: UIButton!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var uploadButton: UIButton!

    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        

       tableView.tableFooterView = UIView(frame: .zero)

        print(userID)
       

//        tableView.emptyDataSetSource = self

        //cintainerView.isHidden = true
        //tableView2.isHidden = true
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tableView.emptyDataSetSource = self;
        self.tableView.emptyDataSetDelegate = self;
        gettingFollowers()

        tableView.contentInset = UIEdgeInsetsMake(40.0, 0.0, 150.0, 0.0)

        //tableView.reloadData()
    
    

        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        followClick = ""
        
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        
        let text = NSAttributedString(string: "No Notifications")
        tableView.separatorColor = UIColor.clear
        return text
        
    }
    
//    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
//        
//        if(velocity.y>0) {
//            UIView.animate(withDuration: 0.1, delay: 0, options: UIViewAnimationOptions(), animations: {
//                self.navigationController?.setNavigationBarHidden(true, animated: true)
//                print("Hide")
//            }, completion: nil)
//            
//        } else {
//            UIView.animate(withDuration: 2.5, delay: 0, options: UIViewAnimationOptions(), animations: {
//                self.navigationController?.setNavigationBarHidden(false, animated: true)
//                print("Unhide")
//            }, completion: nil)
//        }
//    }

    
    func followerGeting(){
        
        
    }
    
    
    func gettingFollowers () {
        
        FollowingNotifyArray.removeAll()

        let url = "https://testglostarsdevelopers.azurewebsites.net/api/notifications/user/\(userID)"
        print(url)
        Alamofire.request(url, method: .get , encoding: JSONEncoding.default)
            
            
            .responseJSON{ response in
                
                print(response)
                
                if let jsonRoot = response.result.value as? [String:Any]!{
                    
                    if let resultPayload = jsonRoot?["resultPayload"] as? [String : Any]{
                        
                        if let actNotify = resultPayload["followerNotifications"] as? [[String : Any]]! {
                            
                            for data in actNotify!{
                                
                                guard let Notify = Mapper<FollowingMapper>().map(JSON: data) else{
                                    
                                    continue
                                }
                                
                            self.FollowingNotifyArray.append(Notify)
                                
                                if self.FollowingNotifyArray.count == 0{
                                    self.tableView.emptyDataSetSource = self
                                }
                                //print(data)
                                
                            }
                            
                            
                            
                        }
                        
                        
                        
                    }
                    
                }
                self.tableView.reloadData()
//                for subViews in self.tableView.visibleCells {
//                    let bottomAnimation = AnimationType.from(direction: .bottom, offSet: 30.0)
//                    subViews.contentView.animateAll(withType: [bottomAnimation])
//                }

        }

        
        
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        //        navigationController?.isNavigationBarHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //        navigationController?.isNavigationBarHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
    }

    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//
//        if scrollView.panGestureRecognizer.translation(in: scrollView.superview).y > 0 {
//            //scrolling downwards
//
//            navigationController?.isNavigationBarHidden = false
//            navigationController?.navigationBar.isHidden = false
//
//
//            if scrollView.contentOffset.y < 0 {
//                //this means we are at top of the scrollView
//                navigationController?.isNavigationBarHidden = false
//                navigationController?.navigationBar.isHidden = false
//
//
//
//            }
//        }
//        else {
//            //we are scrolling upward
//
//            navigationController?.isNavigationBarHidden = true
//            navigationController?.navigationBar.isHidden = true
//
//
//        }
//    }
    
    
    func UnfollowClick(_ title:String,message:String){
        
        let alertController = UIAlertController(title: title, message: message , preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            print("OK Pressed")
            let unfollowUrl = "https://testglostarsdevelopers.azurewebsites.net/api/Follower/Unfollowing?id=\(self.GetFollowerID)"
            Alamofire.request(unfollowUrl, method: .post).responseJSON { response in
            print(response)
            self.gettingFollowers()
                
                
            }
            
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
            print("Cancel Pressed")
            //self.dismiss(animated: true, completion: nil)
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }

    
    // this method will be called by the table view to know the count of rows for each section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return FollowingNotifyArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
       
        
        // create the table view cell design from the prototype cell we have created in storyboard. Reuseidentifier will have same string as in storyboard
        let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellF", for: indexPath) as! FollowingNotifyCell

        
      
      
        
        if self.FollowingNotifyArray[indexPath.row].is_mutual == true  {
            
            tableViewCell.followingButton.setTitle("Mutual", for: .normal)
            tableViewCell.followingButton.backgroundColor = UIColor(hex: 0x660066, alpha: 1)
            
        }
        
        
         else if self.FollowingNotifyArray[indexPath.row].is_mutual == false && self.FollowingNotifyArray[indexPath.row].me_follow == false && self.FollowingNotifyArray[indexPath.row].he_follow == false {
            
            tableViewCell.followingButton.setTitle("Follow", for: .normal)
            tableViewCell.followingButton.backgroundColor = UIColor(hex: 0x4d94ff, alpha: 1)
            
            
        }
        
        else if self.FollowingNotifyArray[indexPath.row].is_mutual == false && self.FollowingNotifyArray[indexPath.row].me_follow == true && self.FollowingNotifyArray[indexPath.row].he_follow == false {
            
            tableViewCell.followingButton.setTitle("Following", for: .normal)
            tableViewCell.followingButton.backgroundColor = UIColor(hex: 0xcc99ff, alpha: 1)
            
        }
        
        else if self.FollowingNotifyArray[indexPath.row].is_mutual == false && self.FollowingNotifyArray[indexPath.row].me_follow == false && self.FollowingNotifyArray[indexPath.row].he_follow == true {
            
            tableViewCell.followingButton.setTitle("Follow Back", for: .normal)
            tableViewCell.followingButton.backgroundColor = UIColor.darkGray
            
        }

       
        let dateFormatter = DateFormatter()
        
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        
        if let  date = dateFormatter.date(from: FollowingNotifyArray[indexPath.row].uploadTime) {
            tableViewCell.uploadTime.text = timeAgoSinceDate(date, currentDate: Date(), numericDates: true)
            
        }
        
        
//        let url = NSURL(string: FollowingNotifyArray[indexPath.row].profilePicURL)
        
        let url = FollowingNotifyArray[indexPath.row].profilePicURL
        
        if url == "/Content/Profile/Thumbs/   Male.jpg"{
            
           tableViewCell.proPicImage.image = UIImage(named: "male")
            
        }
        else if url == "/Content/Profile/Thumbs/   Female  .jpg"{
            
            tableViewCell.proPicImage.image = UIImage(named: "female")

        }
            
            
        else{
           
            let nsUrl = NSURL(string: FollowingNotifyArray[indexPath.row].profilePicURL)
            


            
            tableViewCell.proPicImage.af_setImage(withURL: nsUrl! as URL, placeholderImage: #imageLiteral(resourceName: "male"), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .crossDissolve(0.5), runImageTransitionIfCached: true, completion: nil)
            
            
        }
        
      
        
        tableViewCell.nameProfile.text = FollowingNotifyArray[indexPath.row].name
        
        tableViewCell.onPicTapped = {
            
                    let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            
            
                    passData.getid = self.FollowingNotifyArray[indexPath.row].originatedById
            
            
                     self.navigationController?.pushViewController(passData, animated: true)
            
            
        }
        tableViewCell.onUserPermissonTapped = {
            let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
          
            // create an action
            let firstAction: UIAlertAction = UIAlertAction(title: "Network", style: .default) { action -> Void in
                
                
                let params : Parameters = [
                    "PermissionByUserId" : userID,
                    "PermissionWhomUserId" : self.FollowingNotifyArray[indexPath.row].originatedById,
                    "Permission" : "Network",
                    
                    ]
                
                
                let url = "https://testglostarsdevelopers.azurewebsites.net/api/Userprivacy/UserPermission"
                Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.httpBody)
                    .responseJSON{ response in
                        
                        if response.result.value != nil{
                            
                            if let jsonRoot = response.result.value as? [String:Any]!{
                                
                                if response.result.isSuccess == true{
                                    let title = jsonRoot!["msg"] as! String

                                    self.successMessage(title: "Network Selected", subtitle: title)
                                   // tableView.reloadData()
                                    
                                }
                                    
                                else {
                                    
                                    DispatchQueue.main.async{
                                        
                                        self.warningMessage(title: "Warning", subtitle: "Something Wrong")
                                    }
                                    
                                }
                                
                            }
                            else {
                                
                                self.warningMessage(title: "Warning", subtitle: "Please Check Your Internet")
                                
                            }
                        }
                        
                        
                }
                
                
                
            }
            
            let secondAction: UIAlertAction = UIAlertAction(title: "Insider", style: .default) { action -> Void in
                let params : Parameters = [
                    "PermissionByUserId" : userID,
                    "PermissionWhomUserId" : self.FollowingNotifyArray[indexPath.row].originatedById,
                    "Permission" : "Insider",
                    
                    ]
                
                
                let url = "https://testglostarsdevelopers.azurewebsites.net/api/Userprivacy/UserPermission"
                Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.httpBody)
                    .responseJSON{ response in
                        
                        if response.result.value != nil{
                            
                            if let jsonRoot = response.result.value as? [String:Any]!{
                                
                                if response.result.isSuccess == true{
                                    let title = jsonRoot!["msg"] as! String

                                    self.successMessage(title: "Insider Selected", subtitle: title)
                                    //tableView.reloadData()

                                    
                                }
                                    
                                else {
                                    
                                    DispatchQueue.main.async{
                                        
                                        self.warningMessage(title: "Warning", subtitle: "Something Wrong")
                                    }
                                    
                                }
                                
                            }
                            else {
                                
                                self.warningMessage(title: "Warning", subtitle: "Please Check Your Internet")
                                
                            }
                        }
                        
                        
                }
                
                
            }
            let thirdAction: UIAlertAction = UIAlertAction(title: "Block user", style: .default) { action -> Void in
                
                print("Block User")
                let params : Parameters = [
                    "BlockedByUserId" : userID,
                    "BlockedWhomUserId" : self.FollowingNotifyArray[indexPath.row].originatedById,
                    
                    ]
                
                
                let url = "https://testglostarsdevelopers.azurewebsites.net/api/Block/BlockUser"
                Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.httpBody)
                    .responseJSON{ response in
                        
                        if response.result.value != nil{
                            
                            if let jsonRoot = response.result.value as? [String:Any]!{
                                
                                if response.result.isSuccess == true{
                                    
                                    let title = jsonRoot!["msg"] as! String

                                    
                                    self.successMessage(title: "Block User", subtitle: title)
//                                    tableView.reloadData()
                                    self.gettingFollowers()

                                    
                                }
                                    
                                else {
                                    
                                    DispatchQueue.main.async{
                                        
                                        self.warningMessage(title: "Warning", subtitle: "Something Wrong")
                                    }
                                    
                                }
                                
                            }
                            else {
                                
                                self.warningMessage(title: "Warning", subtitle: "Please Check Your Internet")
                                
                            }
                        }
                        
                        
                }

                
            }
            
            
            let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
            
            // add actions
            actionSheetController.addAction(firstAction)
            actionSheetController.addAction(secondAction)
            actionSheetController.addAction(thirdAction)
            actionSheetController.addAction(cancelAction)
            
            // present an actionSheet...
            self.present(actionSheetController, animated: true, completion: nil)

            
            
        }

        tableViewCell.onNameTapped = {
            
                    let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            
            
                    passData.getid = self.FollowingNotifyArray[indexPath.row].originatedById
            
                     self.navigationController?.pushViewController(passData, animated: true)
            
            
            
        }
        
        tableViewCell.onFollowTapped = {
            
            if self.FollowingNotifyArray[indexPath.row].is_mutual == true {
                let unfollowUrl = "https://testglostarsdevelopers.azurewebsites.net/api/Follower/Unfollowing?id=\(self.FollowingNotifyArray[indexPath.row].originatedById)"
                Alamofire.request(unfollowUrl, method: .post).responseJSON { response in
                    
                    print(response)

                    self.gettingFollowers()
                    
                    
                }
            }
                
            else if self.FollowingNotifyArray[indexPath.row].me_follow == true {
                
                self.GetFollowerID = self.FollowingNotifyArray[indexPath.row].originatedById
                self.UnfollowClick("Unfollow User ", message: "Do you want to unfollow user")
                
            }
            else {
                
                let url = "https://testglostarsdevelopers.azurewebsites.net/api/Follower/Following?id=\(self.FollowingNotifyArray[indexPath.row].originatedById)"
                
                Alamofire.request(url, method: .post).responseJSON { response in
                    
                    print(response)
                   
                    self.gettingFollowers()

                }
            }
            


                        
                }
                
                    
                    

                    
        
            
                
                
                
        
                
//            if self.FollowingNotifyArray[indexPath.row].he_follow == true {
//                
//                
//                tableViewCell.followingButton.setTitle("Mutual", for: .normal)
//                tableViewCell.followingButton.backgroundColor = UIColor(hex: 0x660066, alpha: 1)
            
//                let url = "https://testglostarsdevelopers.azurewebsites.net/api/Follower/Following/\(self.FollowingNotifyArray[indexPath.row].originatedById))"
//                
//                
//                Alamofire.request(url, method: .post).responseJSON { response in
//                    
//                    
//                    if let jsonRoot = response.result.value as? [String:Any]!{
//                        
//                        print(jsonRoot)
//                        
//                        if let resultPayload = jsonRoot["resultPayload"] as? [String : Any]{
//                            
//                            self.FollowingNotifyArray[indexPath.row].is_mutual = (resultPayload["is_mutual"] as? Bool)!
//                            self.FollowingNotifyArray[indexPath.row].me_follow = (resultPayload["me_follow"] as? Bool)!
//                            self.FollowingNotifyArray[indexPath.row].he_follow = (resultPayload["he_follow"] as? Bool)!
//                            
//                            if self.FollowingNotifyArray[indexPath.row].is_mutual == true {
//                                
//                                tableViewCell.followingButton.setTitle("Mutual", for: .normal)
//                               tableViewCell.followingButton.backgroundColor = UIColor(hex: 0x660066, alpha: 1)
//                                
//                            }
//                            
//                            
//                            if self.FollowingNotifyArray[indexPath.row].is_mutual == false && self.FollowingNotifyArray[indexPath.row].me_follow == false && self.FollowingNotifyArray[indexPath.row].he_follow == false {
//                                
//                                tableViewCell.followingButton.setTitle("Follow", for: .normal)
//                                
//                                tableViewCell.followingButton.backgroundColor = UIColor(hex: 0x4d94ff, alpha: 1)
//                            }
//                            
//                            if self.FollowingNotifyArray[indexPath.row].is_mutual == false && self.FollowingNotifyArray[indexPath.row].me_follow == true && self.FollowingNotifyArray[indexPath.row].he_follow == false {
//                                
//                                tableViewCell.followingButton.setTitle("Following", for: .normal)
//                                tableViewCell.followingButton.backgroundColor = UIColor(hex: 0xcc99ff, alpha: 1)
//                                
//                            }
//                            if self.FollowingNotifyArray[indexPath.row].is_mutual == false && self.FollowingNotifyArray[indexPath.row].me_follow == false && self.FollowingNotifyArray[indexPath.row].he_follow == true {
//                                
//                                tableViewCell.followingButton.setTitle("Follow Back", for: .normal)
//                                tableViewCell.followingButton.backgroundColor = UIColor.gray
//                                
//                            }
//                            
//                        }
//                        
//                    }
//                    
//                    
//                    
//                    
//                    
//                    
//                }
                
                
                
                
                
           // }
            
        

        
       // tableViewCell.descriptionOutlet.text = NotifyFollowing.discription
        
        // configure the table view with data
        // textLabel is the title
        // detailTextLable is the subtitle
        
        //indexpath will have section and row
        //indexpath is passed as and argument when table view calls this method
        
        // return the tableviewcell object for the particular row
        //tableView.reloadData()
        return tableViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileSearchVC") as! ProfileSearchVC
//        
//        
//        passData.getid = FollowingNotifyArray[indexPath.row].originatedById
//        passData.getName = FollowingNotifyArray[indexPath.row].name
//        passData.getProfileUrl = FollowingNotifyArray[indexPath.row].profilePicURL
//        passData.getLastName = ""
//
//         self.navigationController?.pushViewController(passData, animated: true)
        
        
        
    }
    func warningMessage(title:String,subtitle:String){
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: .messageView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.warning)
        view.button?.isHidden = true
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        // Set message title, body, and icon. Here, we're overriding the default warning
        // image with an emoji character.
        let iconText = ["🤔", "😳", "🙄", "😶"].sm_random()!
        view.configureContent(title: title, body: subtitle, iconText: iconText)
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        
        // Show the message.
        SwiftMessages.show(view: view)
        
    }
    func successMessage(title:String,subtitle:String){
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: .messageView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.success)
        view.button?.isHidden = true
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        
        // Set message title, body, and icon. Here, we're overriding the default warning
        // image with an emoji character.
        let iconText = ["🤔", "😳", "🙄", "😶"].sm_random()!
        view.configureContent(title: title, body: subtitle, iconText: iconText)
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        
        SwiftMessages.show(view: view)
        
    }
    
  
    func userNetwork(){
        //http://localhost:3224/api/Userprivacy/UserPermission
        
    }
    func userBlock(){
        
        //https://testglostarsdevelopers.azurewebsites.net/api/Block/BlockUser
    }
    
    
    
    
    
//    func hiddenFloatingButtons(){
//        
//        uploadButton.isHidden = true
//        achivementButton.isHidden = true
//        profileButton.isHidden = true
//        notificationButton.isHidden = true
//        homeButton.isHidden = true
//        
//        
//    }
    
  
  
    
   

//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//    if scrollView.panGestureRecognizer.translation(in: scrollView.superview).y > 0 {
//    //scrolling downwards
//        
//    navigationController?.isNavigationBarHidden = false
//
//        
//    if scrollView.contentOffset.y < 0 {
//    //this means we are at top of the scrollView
//        navigationController?.isNavigationBarHidden = false
//
//        
//        }
//    }
//    else {
//    //we are scrolling upward
//        
//        navigationController?.isNavigationBarHidden = true
//
//
//        }
//    }
    
   
    func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!) years"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!)w"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1w"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!)d"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1d"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!)h"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "h"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!)m"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1m"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
        
    }

    
    @IBAction func accessTypeButtonPressed(_ sender: Any) {
        
    }
    
 
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
