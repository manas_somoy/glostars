//
//  NotificationViewController.swift
//  Glostars
//
//  Created by Sanzid Ashan on 3/30/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

import ObjectMapper
import SDWebImage


class ProfileCompititionVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    var  imageValue = ""
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var ScrollingTimer = Timer()
    
    var x = 0
    var scrollEnable = "True"

    var IndexNumber : CGFloat = 0.0
    
    var ComptitionPicFull : Array<GaleryMapperFull> = []
    var pageNumber = 1
    
    var totalItems = 1000
    
    var getID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        pageNumber = Int(ceil((IndexNumber/10)))
        print(pageNumber)


        
        if IndexNumber >= 10{
            
            IndexNumber = CGFloat(Int(IndexNumber) % 10)
            
            print(IndexNumber)
            
        }
        x = Int(IndexNumber)

        
        getRequest()
        
        
        navigationController?.navigationBar.isHidden = true
        let DownSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe:)))
        DownSwipe.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(DownSwipe)
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeActionLeft(swipeleft:)))
        leftSwipe.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(leftSwipe)
        let RightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeActionRight(swiperight:)))
        RightSwipe.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(RightSwipe)
        
        
        //        let urlString = URL(string: userPic)!
        //
        //
        //        profileButton.af_setBackgroundImage(for: [] , url: urlString )
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        //ChangeAPP

        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.landscapeRight, andRotateTo: UIInterfaceOrientation.landscapeRight)
        
        setTimer()
        
    }
    override func viewWillDisappear(_ animated : Bool) {
        super.viewWillDisappear(animated)
        //ChangeAPP

       AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
        
    }
    
    func swipeAction(swipe:UISwipeGestureRecognizer)
    {
        //performSegue(withIdentifier: "Dismiss", sender: self)
        self.navigationController?.popViewController(animated: true)
        self.navigationController?.navigationBar.isHidden = false 
        
    }
    func swipeActionLeft(swipeleft:UISwipeGestureRecognizer)
    {
        //performSegue(withIdentifier: "Dismiss", sender: self)
        
        
    }
    func swipeActionRight(swiperight:UISwipeGestureRecognizer)
    {
        //performSegue(withIdentifier: "Dismiss", sender: self)
        
        
    }
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if scrollView.panGestureRecognizer.translation(in: scrollView).x < 0 {
            ScrollingTimer.invalidate()
            setTimer()
            print("left")
            
            
        } else {
            
            ScrollingTimer.invalidate()
            print("right")
            
        }
    }
    
    
    
    func getRequest(){
        
        
        
        
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/user/\(getID)/\(pageNumber)"
        Alamofire.request(url, method: .get,encoding: URLEncoding.default)
            
            
            .responseJSON{ response in
                
                
                
                if let jsonRoot = response.result.value as? [String:Any]{
                    
                    print(jsonRoot)
                    if let resultPaylaod = jsonRoot["resultPayload"] as? [String:Any]{
                        
                        //                       let allPictureCount = resultPaylaod["resultPaylaod"] as!
                        
                        
                        
                        if let model = resultPaylaod["model"] as? [String: Any]{
                            
                            if let mutPic = model["mutualFollowerPictures"] as! [[String: Any]]! {
                                
                                for data in mutPic{
                                    
                                    guard let mutpics = Mapper<ProfilePublicFeedMapper>().map(JSON: data) else{
                                        
                                        continue
                                    }
                                    
                                }
                                for dic in mutPic {
                                    
                                    print(dic)
                                    if let poster = dic["poster"] as? [String:Any]!{
                                        
                                        guard  let userinfo = Mapper<PublicUserInfoMapper>().map(JSON: poster!) else {
                                            
                                            
                                            continue
                                            
                                        }
                                        
                                    }
                                    
                                    
                                }
                                
                            }
                            
                            if let pubPic = model["publicPictures"] as! [[String: Any]]! {
                                
                                for data in pubPic{
                                    
                                    guard let pubpics = Mapper<ProfilePublicFeedMapper>().map(JSON: data) else{
                                        
                                        continue
                                    }
                                    
                                }
                                
                                
                                
                                
                                
                                
                                for dic in pubPic {
                                    
                                    print(dic)
                                    if let poster = dic["poster"] as? [String:Any]!{
                                        
                                        guard  let userinfo = Mapper<PublicUserInfoMapper>().map(JSON: poster!) else {
                                            
                                            
                                            continue
                                            
                                        }
                                        
                                    }
                                    
                                    
                                }
                                
                                
                                
                            }
                            
                            if let comPic = model["competitionPictures"] as! [[String: Any]]! {
                                
                                
                                for data in comPic{
                                    
                                    guard let pics = Mapper<GaleryMapperFull>().map(JSON: data) else{
                                        
                                        continue
                                    }
                                    self.ComptitionPicFull.append(pics)
                                    
                                }
                                
                                
                                
                                for dic in comPic {
                                    
                                    print(dic)
                                    if let poster = dic["poster"] as? [String:Any]!{
                                        
                                        guard  let userinfo = Mapper<PublicUserInfoMapper>().map(JSON: poster!) else {
                                            
                                            
                                            continue
                                            
                                        }
                                        
                                    }
                                    
                                    
                                }
                                
                                
                                
                                
                            }
                        }
                    }
                    self.collectionView.reloadData()
                    if  self.scrollEnable == "True"{
                        
                        self.collectionView.scrollToItem(at: IndexPath.init(row: Int(self.IndexNumber), section: 0), at: .centeredHorizontally, animated: true)
                    }

                }
                
                
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ComptitionPicFull.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PublicComCellTableViewCell", for: indexPath) as! PublicComCellTableViewCell
        
        
        
        if ComptitionPicFull.count > 0 {
            
            let allphoto = ComptitionPicFull[indexPath.row]
            
            
            let url = NSURL(string: allphoto.picUrl)
            
            // cell.imgImage.af_imageDownloader?.download([url as! URLRequestConvertible])
            cell.imgImage.sd_setShowActivityIndicatorView(true)
            cell.imgImage.sd_setIndicatorStyle(.gray)
            cell.imgImage.sd_setImage(with: url! as URL, placeholderImage: nil, options: .scaleDownLargeImages, completed: nil)
            
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ProfileCompititionVC.connected(_:)))
            
            cell.imgImage.isUserInteractionEnabled = true
            cell.imgImage.tag = indexPath.row
            cell.imgImage.addGestureRecognizer(tapGestureRecognizer)
            cell.starCount.text = "Total Star:  " + allphoto.starcount.description
            
            if allphoto.myStarCount == 0 {
                
                let image = UIImage(named: "Star empty w-shadow") as UIImage?
                
                
                cell.starCellButton.setImage(image, for: .normal)
                self.imageValue = ""
                
                
            }
            
            if allphoto.myStarCount == 5 {
                
                let image = UIImage(named: "Star filled w-shadow") as UIImage?
                self.imageValue = "Checked"
                
                
                cell.starCellButton.setImage(image, for: .normal)
                
            }
            if allphoto.myStarCount == 4 {
                
                let image = UIImage(named: "Star filled w-shadow") as UIImage?
                self.imageValue = "Checked"
                
                
                cell.starCellButton.setImage(image, for: .normal)
                
            }
            if allphoto.myStarCount == 3 {
                
                let image = UIImage(named: "Star filled w-shadow") as UIImage?
                self.imageValue = "Checked"
                
                
                cell.starCellButton.setImage(image, for: .normal)
                
            }
            if allphoto.myStarCount == 2 {
                
                let image = UIImage(named: "Star filled w-shadow") as UIImage?
                self.imageValue = "Checked"
                
                
                cell.starCellButton.setImage(image, for: .normal)
            }
            if allphoto.myStarCount == 1 {
                
                let image = UIImage(named: "Star filled w-shadow") as UIImage?
                self.imageValue = "Checked"
                
                
                cell.starCellButton.setImage(image, for: .normal)
            }
            
            
            
            cell.starCellButtonTap = {
                
                
                
                if self.imageValue == "Checked" {
                    
                    
                    let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/removerate/\(allphoto.photoId)"
                    //            let headers: HTTPHeaders = [
                    //                "Content-Type": "application/json"
                    //            ]
                    Alamofire.request(url, method:.post,encoding: URLEncoding.default).responseJSON { response in
                        switch response.result {
                        case .success:
                            
                            let imagevanish = UIImage(named: "Star empty w-shadow") as UIImage?
                            
                            
                            
                            
                            
                            cell.starCellButton.setImage(imagevanish, for: .normal)
                            
                            print(response)
                            
                            self.imageValue = ""
                            // self.ArticlesInfo.removeAll()
                            
                            // self.getRequest()
                            
                            
                            // self.tableView.reloadData()
                            cell.starCount.text = "Total Star:  " + allphoto.starcount.description
                            
                            
                            
                        case .failure(let error):
                            print(error)
                        }
                        
                        
                    }
                    
                    
                    
                    
                    
                }
                else{
                    
                    
                    let parameters: [String : Any]? = [
                        "NumOfStars": 1 ,
                        "photoId": allphoto.photoId ,
                        ]
                    let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/rating"
                    let headers: HTTPHeaders = [
                        "Content-Type": "application/json"
                    ]
                    Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
                        
                        if let jsonRoot = response.result.value as? [String:Any]{
                            
                            //print(jsonRoot)
                            
                            if let resultPayload = jsonRoot["resultPayload"] as? [String: Any]{
                                
                                print(resultPayload)
                                let rating = resultPayload["totalRating"]as! Int
                                
                                cell.starCount.text = "Total Star:  " + String(rating)
                                
                                self.imageValue = "Checked"
                                
                            }
                            
                            let image = UIImage(named: "Star filled w-shadow") as UIImage?
                            
                            
                            
                            
                            cell.starCellButton.setImage(image, for: .normal)
                            
                        }
                        
                        
                        
                        
                        
                        
                    }
                    //self.getRequest()
                }
                
                
            }
            //            var rowIndex = indexPath.row
            //            let numberOfRecords : Int = self.ComptitionPicFull.count - 1
            //            if (rowIndex < numberOfRecords){
            //
            //
            //                rowIndex = (rowIndex + 1 )
            //            }
            //            else{
            //
            //                rowIndex = 0
            //            }
            
            //ScrollingTimer = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(CompetitionPicFullVC.starTimer(theTimer:)), userInfo: rowIndex, repeats: true)
            
        }
        else {
            
            print("Error")
        }
        if indexPath.row == ComptitionPicFull.count - 1 {
            // last cell
            
            if totalItems > ComptitionPicFull.count {
                pageNumber = pageNumber + 1
                print(pageNumber)
                self.scrollEnable = ""

                // more items to fetch
                getRequest() // increment `fromIndex` by 20 before server call
            }
        }
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width
        let height = collectionView.frame.height
        
        return CGSize(width: width, height: height )
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileCompititionVC") as! ProfileCompititionVC
        
        passData.IndexNumber = CGFloat(indexPath.row)
        
        
    }

    
    func connected(_ sender:AnyObject){
        
        ScrollingTimer.invalidate()
        
        //print("you tap image number : \(sender.view.tag)")
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        ScrollingTimer.invalidate()
        
    }
    
    
    func setTimer() {
        ScrollingTimer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(ProfileCompititionVC.autoScroll), userInfo: nil, repeats: true)
    }
    
    
    func autoScroll() {
        if self.x < ComptitionPicFull.count {
            let indexPath = IndexPath(item: x, section: 0)
            self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            self.x = self.x + 1
        } else {
            self.x = 0
            self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
 
}






