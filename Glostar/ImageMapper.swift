//
//  ImageMapper.swift
//  Glostars
//
//  Created by Sanzid Ashan on 5/17/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import Foundation


import Foundation
import ObjectMapper

class ImageMapper : Mappable {
    dynamic var picUrl : String = ""
    dynamic var id : Int = 0
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        picUrl <- map["picUrl"]
        id <- map["id"]
    }
}

