//
//  RegisterVC.swift
//  Glostar
//
//  Created by Sanzid Ashan on 3/20/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit

import Alamofire
import SwiftMessages

var userRegisterID = ""
var userRegisterName = ""

var userFName = ""
var userLName = ""


class RegisterVC: UIViewController,UITextFieldDelegate {

    @IBOutlet var uncheckWithoutBack: UIButton!
    @IBOutlet var uncheckButton: UIButton!
    
    @IBOutlet var popUpYConstrants: NSLayoutConstraint!
    @IBOutlet var popUpView: UIView!
    @IBOutlet weak var ddLabel: UILabel!
    @IBOutlet weak var yyLabel: UILabel!
    @IBOutlet weak var mmLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var yearDropDown: DropMenuButton!
    @IBOutlet weak var dateDropDown: DropMenuButton!
    @IBOutlet weak var genderDropDown: DropMenuButton!
    @IBOutlet weak var monthDropDown: DropMenuButton!
    @IBOutlet var popUpInputTextFieldd: UITextField!
    
    let URL_USER_REGISTER = "https://testglostarsdevelopers.azurewebsites.net/api/account/register"
    
    var result = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe:)))
        rightSwipe.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(rightSwipe)
        
//        let backButton : UIBarButtonItem = UIBarButtonItem(image: UIImage(named:"left-arrow"), style: .plain, target: self, action:  #selector(back))
////                let backButton: UIBarButtonItem = UIBarButtonItem(title: "Create Account", style: .plain, target: self, action: #selector(back))
//        self.navigationItem.leftBarButtonItem = backButton
        
        
        
        self.navigationItem.title = ""
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.white,
             NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 20)!]

      //  hidingPOPView()
        
        
        
        genderDropDown.layer.cornerRadius = 11
        yearDropDown.layer.cornerRadius = 11
        dateDropDown.layer.cornerRadius = 11
        monthDropDown.layer.cornerRadius = 11
        let leftGesture = UISwipeGestureRecognizer(target: self, action: Selector(("swipeToLogin:")))
        leftGesture.direction = .left
        self.view.addGestureRecognizer(leftGesture)
        //popUpYConstrants.constant = 0

    genderDropDown.initMenu(["   Female  ", "   Male"], actions: [({ () -> (Void) in
        
        self.dateDropDown.table.alpha = 0
        self.monthDropDown.table.alpha = 0
        self.yearDropDown.table.alpha = 0

        })])
        
        dateDropDown.initMenu([" 1  ", " 2 ", " 3 "," 4"," 5"," 6"," 7"," 8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"], actions: [({ () -> (Void) in
            
            self.genderDropDown.table.isHidden = true
            self.monthDropDown.table.isHidden = true
            self.yearDropDown.table.isHidden = true

       })])
        
        
        monthDropDown.initMenu([" Jan  ", " Feb ", " Mar "," Apr"," May"," Jun"," Jul"," Aug"," Sep"," Oct"," Nov"," Dec"], actions: [({ () -> (Void) in
            self.genderDropDown.table.isHidden = true
            self.dateDropDown.table.isHidden = true
            self.yearDropDown.table.isHidden = true
        })])
        
        yearDropDown.initMenu([" 1950  ", " 1951 ", " 1952 "," 1953","  1954"," 1955"," 1956"," 1957"," 1958"," 1959"," 1960"," 1961", "1962", " 1963 ", " 1964 "," 1965","  1966"," 1967"," 1968"," 1969","1970"," 1971", "1972", " 1973 ", " 1974 "," 1975","  1976"," 1977"," 1978"," 1979"," 1980  ", " 1981 ", " 1982 "," 1983","  1984"," 1985"," 1986"," 1987"," 1988"," 1989"," 1990"," 1991", "1992", " 1993 ", " 1994 "," 1995","  1996"," 1997"," 1998"," 1999"," 2000"], actions: [({ () -> (Void) in
            
            self.genderDropDown.table.isHidden = true
            self.dateDropDown.table.isHidden = true
            self.monthDropDown.table.isHidden = true
        })])

    }
    
    
   
    
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true

        firstNameTextField.keyboardAppearance = .dark
        lastNameTextField.keyboardAppearance = .dark
        emailTextField.keyboardAppearance = .dark
        passwordTextField.keyboardAppearance = .dark



        
    }
    override func viewWillDisappear(_ animated: Bool) {
        

    }
    
    func warningMessage(title:String,subtitle:String){
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: .messageView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.warning)
        view.button?.isHidden = true
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        // Set message title, body, and icon. Here, we're overriding the default warning
        // image with an emoji character.
        let iconText = ["🤔", "😳", "🙄", "😶"].sm_random()!
        view.configureContent(title: title, body: subtitle, iconText: iconText)
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        
        // Show the message.
        SwiftMessages.show(view: view)
        
    }
    func successMessage(title:String,subtitle:String){
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: .messageView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.success)
        view.button?.isHidden = true
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        
        // Set message title, body, and icon. Here, we're overriding the default warning
        // image with an emoji character.
        let iconText = ["🤔", "😳", "🙄", "😶"].sm_random()!
        view.configureContent(title: title, body: subtitle, iconText: iconText)
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        
        SwiftMessages.show(view: view)
        
    }
    
    func swipeAction(swipe:UISwipeGestureRecognizer)
    {
        
        //dismiss(animated: true, completion: nil)
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionPush
       // transition.subtype = kCATransitionFromRight
        transition.subtype = kCATransitionFromLeft
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        
        let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "LoginViewController2")
        
        let nvc = UINavigationController(rootViewController: nextViewController)
        present(nvc, animated: false, completion: nil)
    }
    
    

    
    @IBAction func TermsOfUse(_ sender: Any) {
        
        getData = "Terms Of Use"

        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SettingsData") as? SettingsData {
            //iewController.newsObj = newsObj
            
            
            
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
        
        
        
    }
    
    
    
    
    func back() {
        self.dismiss(animated: true, completion: nil)
    }
    func hidingPOPView(){
        for view in self.view.subviews {
            
            popUpView.removeFromSuperview()
            
        }
    }
    
    func displayMyAlertMessage(_ userMessage:String)
    {
        
        let myAlert = UIAlertController(title:"Warning", message:userMessage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title:"Ok", style:UIAlertActionStyle.default, handler:nil);
        
        myAlert.addAction(okAction);
        self.present(myAlert, animated: true, completion: nil);
        
        
    }
    
    
    @IBAction func backToLogin(_ sender: Any) {
        

        let resultController = storyboard!.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
        let navController = UINavigationController(rootViewController: resultController!)
        self.present(navController, animated:true, completion: nil)

        
        
    }
    
    
    @IBAction func uncheckButtonAction(_ sender: Any) {
        
        uncheckButton.isHidden = false
        
    }
    
    @IBAction func uncheck(_ sender: Any) {
        
    uncheckButton.isHidden = true
        
    }
    
    @IBAction func submitPopUpAction(_ sender: Any) {
        let url = "https://testglostarsdevelopers.azurewebsites.net/Account/Confirmcode?userId=\(userRegisterID)&confirmCode=\(popUpInputTextFieldd.text!)"

        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: nil).responseJSON { response in
        
            switch response.result {
                
            case .success:
            //print(response)
             if let jsonRoot = response.result.value as? [String:Any]!{
                
                print(jsonRoot)
                
                if jsonRoot!["Msg"] as? String == "Wrong code"{
                    
                    print("Wrong Code ")
                    
                    //self.displayMyAlertMessage("Wrong Code")
                    //SwiftMessageService.WarningMessage("Warning", description: "Wrong Code")

                    
                    
                    
                }else{
                    
                    self.performSegue(withIdentifier: "Login", sender: NewsFeedDetailsVC.self)

                    
                }
                
                
            }
            
        
         
                
                
            case .failure(let error):
            print(error)
                
                
            }
            
            //print(response)
 
        }
        
            
//        Alamofire.request(url, method:.get, parameters:parameter!,encoding: URLEncoding.httpBody).responseJSON { response in
//            
//            if response.result.value != nil {
//                switch response.result {
//                    
//                case .success:
//                    print(response)
//                    
//                    
//                case .failure(let error):
//                    print(error)
//                }
//                
//            
//            }
        
        
        
        
        
    }
    
    @IBAction func cancelPopUpAction(_ sender: Any) {
        
        hidingPOPView()
        
    }
    func RegisterUser (){
        
        self.showLoader("")
        userFName = firstNameTextField.text!
        userLName = lastNameTextField.text!

        let parameters: [String : Any]? = [
            "Name":firstNameTextField.text!,
            "LastName":lastNameTextField.text!,
            "Gender": genderName,
            "Email": emailTextField.text!,
            "BirthdayYear":1993,
            "BirthdayMonth":12,
            "BirthdayDay": 12,
            "Password": passwordTextField.text!,
            ]
        
                Alamofire.request(URL_USER_REGISTER, method: .post, parameters: parameters).responseJSON
                    {
                        response in
                        //printing response
                        print(response)
                        UserDefaults.standard.set(self.emailTextField.text, forKey: "E")
                        UserDefaults.standard.set(self.passwordTextField.text, forKey: "P")

        
                        let alertController = UIAlertController(title: "REGISTRATION SUCCESSFUL!", message: nil, preferredStyle: .alert);
                        
        
                        alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
        
  
                }
        
                Alamofire.request(URL_USER_REGISTER, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
        
                    switch(response.result) {
                    case .success(_):
                        if response.result.value != nil{

                            print(response.result.value!)
                        }
                        break
        
                    case .failure(_):
                        print(response.result.error!)
                        break
        
                    }
                }
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/Account/Signup"
        
        
        
        
        Alamofire.request(url, method:.post, parameters:parameters,encoding: URLEncoding.httpBody, headers:nil).responseJSON { response in
            
            if response.result.value != nil {
                switch response.result {
                    
                case .success:
                print(response)
                    
                    //self.performSegue(withIdentifier: "aa", sender: LoginVC.self)
                    if let jsonRoot = response.result.value as? [String:Any]!{
                        
                        
                        //print(jsonRoot)
                        
                        
                       
                        
                        if response.result.value != nil {
                            
                            if jsonRoot!["user"] as? [String: Any]! != nil{
                                
                            
                                if let userinfo = jsonRoot!["user"] as? [String: Any]!{
                            
                            if userinfo == nil{
                                
                                self.warningMessage(title: "Warning", subtitle: "Email already taken")
                                self.hideLoader()

                           // self.displayMyAlertMessage("Email already taken")
                             //   SwiftMessageService.WarningMessage("Warning", description: "Email already taken")
                                
                                // create the alert
                                
                                //                                let alert = UIAlertController(title: "self.Email already taken", message: "The Email is already taken. Please provide a new email.", preferredStyle: UIAlertController.Style.alert)
//
//                                // add an action (button)
//                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//
//                                // show the alert
//                                self.present(alert, animated: true, completion: nil)
                                
                                
                            }
                            
                            
                            
                            if userinfo != nil{
                                
                                userRegisterID = userinfo!["Id"] as! String
                                userRegisterName = userinfo!["UserName"] as! String
                                
                                
                                UserDefaults.standard.set(self.emailTextField.text, forKey: "EmailSave")
                                
                                
                                UserDefaults.standard.set(self.passwordTextField.text, forKey: "PassSave")

                                print(userRegisterID)
                                UserDefaults.standard.set(self.emailTextField.text, forKey: "E")
                                UserDefaults.standard.set(self.passwordTextField.text, forKey: "P")

                                let transition = CATransition()
                                transition.duration = 0.3
                                transition.type = kCATransitionPush
                                transition.subtype = kCATransitionFromRight
                                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                                self.view.window!.layer.add(transition, forKey: kCATransition)
                                
                                let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "ActivateAccountVC")

                                let nvc = UINavigationController(rootViewController: nextViewController)
                                self.hideLoader()
                                self.present(nvc, animated: false, completion: nil)
                  
                                
                            }
                            
                            
                          
                            //self.view.addSubview(self.popUpView)
                            
                            //self.performSegue(withIdentifier: "a", sender: NewsFeedDetailsVC.self)
                            
                            
                            //print(self.userInfo.userName = "email")
                            
                        }
                                
                            }else {
                                
                                print("Found Crash")
                                self.hideLoader()

                            }
                        }
                        else {
                            
                            print("Error Found")
                            self.hideLoader()

                        }
 
                    
                    }
                case .failure(let error):
                    print(error)
                    self.hideLoader()

                }
                
            }
            else {
                
                
                // create the alert
                let alert = UIAlertController(title: "No Internet", message: "Please Check Your Internet Connection.", preferredStyle: UIAlertController.Style.alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                self.hideLoader()

                
               // self.displayMyAlertMessage("Check Your Internet Connection")
                //SwiftMessageService.WarningMessage("Warning", description: "Check Your Internet Connection")

            }

        }

        
    }
   
    
    @IBAction func alreadySignedAction(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.present(newViewController, animated: true, completion: nil)
   
    }

    @IBAction func createAccount(_ sender: Any) {
        
        if self.validateForm(firstName: firstNameTextField, lastName: lastNameTextField, email: emailTextField, pass: passwordTextField){
            
            if (passwordTextField.text?.count)! >= 8 {
                 RegisterUser()

            }
            else{
                warningMessage(title: "Password Range", subtitle: "Please Enter Atleast 8 Characters")
                self.showLoader("")
                self.hideLoader()

            }
            
        }
        
//        if firstNameTextField.text?.isEmpty == true {
//            // create the alert
//            let alert = UIAlertController(title: "First name", message: "Please enter your first name", preferredStyle: UIAlertController.Style.alert)
//
//            // add an action (button)
//            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//
//            // show the alert
//            self.present(alert, animated: true, completion: nil)
//            //displayMyAlertMessage("Please enter your first name")
//            //SwiftMessageService.WarningMessage("Warning", description: "Please enter your first name")
//
//        }
//       else if lastNameTextField.text?.isEmpty == true {
//            // create the alert
//            let alert = UIAlertController(title: "Last name", message: "Please enter your last name.", preferredStyle: UIAlertController.Style.alert)
//
//            // add an action (button)
//            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//
//            // show the alert
//            self.present(alert, animated: true, completion: nil)
//
//            //displayMyAlertMessage("Please enter your last name")
//            //SwiftMessageService.WarningMessage("Warning", description: "Please enter your last name")
//
//
//        }
//        else if emailTextField.text?.isEmpty == true {
//            // create the alert
//            let alert = UIAlertController(title: "Email address", message: "Please enter your valid email address.", preferredStyle: UIAlertController.Style.alert)
//
//            // add an action (button)
//            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//
//            // show the alert
//            self.present(alert, animated: true, completion: nil)
//
//            //displayMyAlertMessage("Please enter your email address")
//            //SwiftMessageService.WarningMessage("Warning", description: "Please enter your email address")
//
//
//        }
//       else if passwordTextField.text?.isEmpty == true {
//            // create the alert
//            let alert = UIAlertController(title: "Password", message: "Please enter your Password.", preferredStyle: UIAlertController.Style.alert)
//
//            // add an action (button)
//            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//
//            // show the alert
//            self.present(alert, animated: true, completion: nil)
//
//            //displayMyAlertMessage("Please enter your Password")
//            //SwiftMessageService.WarningMessage("Warning", description: "Please enter your Password")
//
//        }
//       else if (passwordTextField.text?.characters.count)! < 8 {
//            // create the alert
//            let alert = UIAlertController(title: "Password too easy to guess", message: "Please enter atleast 8 characters password.", preferredStyle: UIAlertController.Style.alert)
//
//            // add an action (button)
//            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//
//            // show the alert
//            self.present(alert, animated: true, completion: nil)
//
//            //displayMyAlertMessage("Please enter atleast 8 characters password")
//           // SwiftMessageService.WarningMessage("Warning", description: "Please enter atleast 8 characters password")
//
//        }
    
      //  else{
          
            //SwiftMessageService.SuccessMessage("Please Wait ", description: "Preparing Glostars for you")
        
      //  }
        
        
   // }
        
        //performSegue(withIdentifier: "Login", sender: LoginVC.self)

        


        
        
//         let email = self.emailTextField.text ?? ""
//         let password = self.passwordTextField.text ?? ""
//         let lastName = self.lastNameTextField.text ?? ""
//         let firstName = self.firstNameTextField.text ?? ""
//
//        // Check for empty fields
//        if(email.isEmpty || password.isEmpty || lastName.isEmpty || firstName.isEmpty )
//        {
//
//            // Display alert message
//
//            // create the alert
//            let alert = UIAlertController(title: "Password too easy to guess", message: "Please enter atleast 8 characters password.", preferredStyle: UIAlertController.Style.alert)
//
//            // add an action (button)
//            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//
//            // show the alert
//            self.present(alert, animated: true, completion: nil)
//
//           // displayMyAlertMessage("All fields are required");
//
//            return;
//        }

}
    
    @IBAction func genderButtonAction(_ sender: Any) {
        
        genderDropDown.initMenu(["   Female  ", "   Male"], actions: [({ () -> (Void) in

        })])
        self.dateDropDown.table.removeFromSuperview()
        self.monthDropDown.table.removeFromSuperview()
        self.yearDropDown.table.removeFromSuperview()

    }
    
    @IBAction func dateDropAction(_ sender: Any) {
        
        dateDropDown.initMenu([" 1  ", " 2 ", " 3 "," 4"," 5"," 6"," 7"," 8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"], actions: [({ () -> (Void) in
            
            
        })])
        self.genderDropDown.table.removeFromSuperview()
        self.monthDropDown.table.removeFromSuperview()
        self.yearDropDown.table.removeFromSuperview()
    }
    
    
    @IBAction func yearDropAction(_ sender: Any) {
        yearDropDown.initMenu([" 1950  ", " 1951 ", " 1952 "," 1953","  1954"," 1955"," 1956"," 1957"," 1958"," 1959"," 1960"," 1961", "1962", " 1963 ", " 1964 "," 1965","  1966"," 1967"," 1968"," 1969","1970"," 1971", "1972", " 1973 ", " 1974 "," 1975","  1976"," 1977"," 1978"," 1979"," 1980  ", " 1981 ", " 1982 "," 1983","  1984"," 1985"," 1986"," 1987"," 1988"," 1989"," 1990"," 1991", "1992", " 1993 ", " 1994 "," 1995","  1996"," 1997"," 1998"," 1999"," 2000"], actions: [({ () -> (Void) in
            
            
        })])
        self.genderDropDown.table.removeFromSuperview()
        self.dateDropDown.table.removeFromSuperview()
        self.monthDropDown.table.removeFromSuperview()
    }
    
    @IBAction func monthDropAction(_ sender: Any) {
        monthDropDown.initMenu([" Jan  ", " Feb ", " Mar "," Apr"," May"," Jun"," Jul"," Aug"," Sep"," Oct"," Nov"," Dec"], actions: [({ () -> (Void) in
            
        })])
        self.genderDropDown.table.removeFromSuperview()
        self.dateDropDown.table.removeFromSuperview()
        self.yearDropDown.table.removeFromSuperview()
    }
    
    @IBAction func termsOfUseClick(_ sender: Any) {
        
        if let resultController = storyboard!.instantiateViewController(withIdentifier: "RegistrationTermVC") as? RegistrationTermVC {
            resultController.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(UIWebView.goBack))
            let navController = UINavigationController(rootViewController: resultController) // Creating a navigation controller with VC1 at the root of the navigation stack.
            
            UINavigationBar.appearance().barTintColor = UIColor.purple
            UINavigationBar.appearance().tintColor = .white
            UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
            UINavigationBar.appearance().isTranslucent = false
            UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 14)]
            
            self.present(navController, animated:true, completion: nil)

    }
    }

    func goBack(){
            dismiss(animated: true, completion: nil)
        
        }
    
    // Mark : Check if any field is empty
    
    func isAnyFieldEmpty(firstName:UITextField,
                         lastName:UITextField, email:UITextField,pass:UITextField)->Bool{
        
        if (firstNameTextField.text?.isEmpty)! {
            //setFormErrorMessage("Enter First Name")
            //displayMyAlertMessage("Please enter your first name", title: "First Name")
            warningMessage(title: "First Name", subtitle: "Please enter your first name")
            return true
        }

        else if (lastNameTextField.text?.isEmpty)!{
            //displayMyAlertMessage("Please enter your last name", title: "Last Name")
            warningMessage(title: "Last Name", subtitle: "Please enter your last name")

            return true
        }
        else if (emailTextField.text?.isEmpty)!{
           // displayMyAlertMessage("Please enter your email", title: "Email")
            warningMessage(title: "Enter Email", subtitle: "Please enter your email")

            return true
        }
        else if (passwordTextField.text?.isEmpty)!{
            //displayMyAlertMessage("Please enter your password", title: "Password")
            warningMessage(title: "Enter Password", subtitle: "Please enter your password")

            return true
        }

        return false
    }
    
    // Start Editing The Text Field
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        moveTextField(textField, moveDistance: -50, up: true)
//    }
//    
//    // Finish Editing The Text Field
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        moveTextField(textField, moveDistance: -50, up: false)
//    }
    
    // Hide the keyboard when the return key pressed
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // Move the text field in a pretty animation!
//    func moveTextField(_ textField: UITextField, moveDistance: Int, up: Bool) {
//        let moveDuration = 0.3
//        let movement: CGFloat = CGFloat(up ? moveDistance : -moveDistance)
//        
//        UIView.beginAnimations("animateTextField", context: nil)
//        UIView.setAnimationBeginsFromCurrentState(true)
//        UIView.setAnimationDuration(moveDuration)
//        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
//        UIView.commitAnimations()
//    }

        // Do any additional setup after loading the view.
    func displayMyAlertMessage(_ userMessage:String,title:String)
        
    {
        let myAlert = UIAlertController(title:title, message:userMessage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title:"Ok", style:UIAlertActionStyle.default, handler:nil);
        
        myAlert.addAction(okAction);
        self.present(myAlert, animated: true, completion: nil);
    }
    
    // email validity
    func isValidEmail(testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        let result = emailTest.evaluate(with: testStr)
        
        return result
        
    }
    
    func validateForm(firstName:UITextField, lastName:UITextField, email:UITextField, pass:UITextField)->Bool{
        
        if isAnyFieldEmpty(firstName: firstName,lastName: lastName, email: email,pass: pass) {
            return false
        }
        
        if !isValidEmail(testStr: emailTextField.text!){
            self.emailTextField.becomeFirstResponder()
            //displayMyAlertMessage("EnterValidEmail", title: "Email")
            warningMessage(title: "Enter Email", subtitle: "Please enter your Valid Email")

            return false
        }
        
        return true
    }
    


}
