//
//  ViewController.swift
//  Glostar
//
//  Created by Sanzid Ashan on 3/19/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//
var starCount1 = ""
var bottomType1: BottomType!
var emojis1: [EmojiCategory]?

var emojiButtonPressed1: Bool = false


import UIKit
import Alamofire
import ObjectMapper
import AlamofireImage
import SDWebImage
import ISEmojiView




class ProfilePublicFeed: UIViewController,UITableViewDataSource,UITableViewDelegate,DZNEmptyDataSetSource, DZNEmptyDataSetDelegate,FloatyDelegate,UITextViewDelegate,EmojiViewDelegate{
    var commentIdCell = ""
    var TypePhotos = String()
    var indexNumber :CGFloat = 0.0
    var scrollEnable = "True"
    var TitleShow = ""


    @IBOutlet var commentText: UITextView!
    
    @IBOutlet var commentTextVie: UITextView!
    
    @IBOutlet var postUserImage: UIImageView!
    
    @IBOutlet var tableViewComment: UITableView!
    
    var CommentsPicArray : Array<PublicProfileCommentMapper> = []
    
    var photoId = 0
    //var getID = ""
    var userIDFeed = ""
    var getUserName = ""
    
    @IBOutlet var mainView: UIView!
    
    var starCountName = 0
    var starShow = 0
    @IBOutlet var leftSearch: UIBarButtonItem!
    
    var commentCount = [Int]()
    var ListPhoto = [Int]()
    var fab = Floaty()

    
    @IBOutlet var searchButton: DesignableButton!
    
    
    var pageNumber = 1
    var totalItems = 1000
    
    var Articles : Array<FeedMapper> = []
    var ArticlesInfo : Array<InfoMapper> = []
    var commentInfo : Array< CommentMapper > = []
    var idArray = [Int]()
    var PublicInfo : Array< ProfilePublicFeedMapper > = []
    var UserInfoArray : Array< PublicUserInfoMapper > = []
    
    
    
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet var commentView: UIView!
    
    
    
    
    var starCountValue = 0
    
    
    
    // create the data source
    
    @IBOutlet weak var achivementButton: UIButton!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var uploadButton: UIButton!
    
    @IBOutlet weak var floatingMainButton: UIButton!
    
    
    
    
    
    //tableview data source methods
    
    // return how many sections in tableview
    //    func numberOfSections(in tableView: UITableView) -> Int {
    //        return R
    //
    // return title string for each section
    
    
    @IBAction func barLogo(_ sender: Any) {
        
        
        hiddenFloatingButtons()
        
        
    }
    @IBAction func barButtonLogo(_ sender: Any) {
        
        
        hiddenFloatingButtons()
        
        
        
    }
    
    
    @IBAction func onClinkProfile(_ sender: Any) {
        
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        
        
        passData.getid = UserDefaults.standard.value(forKey: "id") as! String
        
        self.navigationController?.pushViewController(passData, animated: true)
        
        
    }
    
    
    // this method will be called by the table view to know the count of rows for each section
    
    
    //table view delegate methods
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //getRequest()
        //tableView.reloadData()
        floatingMainButton.isHidden = true
        layoutFAB()
        //hiddenFloatingButtons()
        print(TypePhotos)
        
        tableView.delegate = self
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGesture.minimumPressDuration = 0.5
        longPressGesture.delegate = self as? UIGestureRecognizerDelegate
        self.tableView.addGestureRecognizer(longPressGesture)
        
        
        //print(UserDefaults.standard.value(forKey: "access") as! String)
        
        
        //IJProgressView.shared.showProgressView(view)
        
        //setCloseTimer()
        //self.automaticallyAdjustsScrollViewInsets = false
        
        //        if (UserDefaults.standard.value(forKey: "userPic"))as? String == nil {
        //            //
        //        }
        //        else {
        
        
        
        
        //}
        //self.tableView.addSubview(self.refreshControl)
        
        
        
        
        
        
        
        
        //        self.navigationItem.title = "glostars"
        //        self.navigationController?.navigationBar.titleTextAttributes =
        //            [NSForegroundColorAttributeName: UIColor.white,
        //             NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 20)!]
        
        
        
        //        let image : UIImage = UIImage(named: "titleLogo")!
        //        let imageView = UIImageView(frame: CGRect(x: (self.navigationController?.navigationBar.frame.size.width)!/2 + 20 , y: (self.navigationController?.navigationBar.frame.size.width)!/2 + 20, width: 30, height: 30))
        //
        //        imageView.contentMode = .scaleAspectFit
        //        imageView.image = image
        //        imageView.backgroundColor = UIColor.white
        //        self.navigationItem.titleView = imageView
        //
        
        
        
        
        //navigationItem.leftBarButtonItems = [backButton!, leftItem]
        
        
        
        //print(accessToken)
        //print(tokenType)
        
        
        commentText.delegate = self
        commentText.text! = "Write a comment"
        commentText.textColor = UIColor.lightGray
        
        
    }
    
    func emojiViewDidSelectEmoji(_ emoji: String, emojiView: EmojiView) {
        
        commentText.text = ""
        commentText.insertText(emoji)
        print(commentText.text!)
    }
    
    func emojiViewDidPressChangeKeyboardButton(_ emojiView: EmojiView) {
        commentText.inputView = nil
        commentText.keyboardType = .default
        commentText.reloadInputViews()
    }
    
    func emojiViewDidPressDeleteBackwardButton(_ emojiView: EmojiView) {
        commentText.deleteBackward()
    }
    
    func emojiViewDidPressDismissKeyboardButton(_ emojiView: EmojiView) {
        commentText.resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //tableView.reloadData()
        self.inputViewController?.dismissKeyboard()
        emojiButtonPressed = false
        pageNumber = Int(ceil((indexNumber/10)))
        
        //print(indexNumber/10)
        //print(Int(ceil((indexNumber/10))))
        print(pageNumber)
        
        if indexNumber >= 10{
            
            indexNumber = CGFloat(Int(indexNumber) % 10)
            
            print(indexNumber)
            
        }
        self.getRequest()
        commentView.isHidden = true
        
        //postUserImage.setRounded()
        tableViewComment.emptyDataSetSource = self
        
        //        if (UserDefaults.standard.value(forKey: "star"))as? String != nil{
        //
        //            UserDefaults.value(forKey: "star")
        //
        //        }
        hiddenFloatingButtonsScrool()
        self.navigationController?.isNavigationBarHidden = false
        if self.TypePhotos == "mutual"{
        self.title = getUserName + "'s" + "Mutual Posts"
        }
        if self.TypePhotos == "public"{
            self.title = getUserName + "'s" + " Public Posts"
        }
        if self.TypePhotos == "compitition"{
            self.title = getUserName + "'s" + " Compitition Posts"
            self.navigationController?.isNavigationBarHidden = false
        }

        commentText.layer.borderWidth = 1.0
        commentText.layer.borderColor = UIColor.black.cgColor
        commentText.layer.cornerRadius = 8.0
        commentText.textContainer.lineBreakMode = .byTruncatingTail
        
        if userPic == "/Content/Profile/Thumbs/   Male.jpg"{
            
            profileButton.setBackgroundImage(UIImage(named:"male"), for: .normal)
            //postUserImage.image = UIImage(named: "male")
            
            
        }
        else if userPic == "/Content/Profile/Thumbs/   Female  .jpg"{
            
            profileButton.setBackgroundImage(UIImage(named:"female"), for: .normal)
            //postUserImage.image = UIImage(named: "female")
            
        }
            
            
        else{
            let urlString = URL(string:String(describing: UserDefaults.standard.value(forKey: "pic")!))
            profileButton.af_setBackgroundImage(for: [] , url: urlString! )
            //postUserImage.af_setImage(withURL: urlString!)
            
            
        }
        
//        floatingMainButton.setRounded()
//        notificationButton.setRounded()
//        uploadButton.setRounded()
//        achivementButton.setRounded()
//        homeButton.setRounded()
//        profileButton.setRounded()
//        searchButton.setRounded()
        
        
        
        //        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        //        imageView.contentMode = .scaleAspectFit
        //
        //        let image = UIImage(named: "titleLogo")
        //        imageView.image = image
        //
        //        navigationItem.titleView = imageView
        
    }
    
    
    @IBAction func commentBackAction(_ sender: Any) {
        
        
        commentView.isHidden = true
        
    }
    
    @IBAction func SendOwnProfileAction(_ sender: Any) {
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        
        
        passData.getid = UserDefaults.standard.value(forKey: "id") as! String
        
        self.navigationController?.pushViewController(passData, animated: true)
        
        
        
    }
    
    
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        
        let text = NSAttributedString(string: "No Comments")
        tableViewComment.separatorColor = UIColor.clear
        return text
        
    }
    
    @IBAction func sendCommentAction(_ sender: Any) {
        
        if self.commentText.text != "" {
            if self.commentText.text != "Write a comment"{
                let parameters: [String : Any]? = [
                    "CommentText": commentText.text!,
                    "photoId": photoId ,
                    ]
                let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/comment"
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json"
                ]
                Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
                    switch response.result {
                    case .success:
                        print(response)
                        self.CommentsPicArray.removeAll()
                        self.getImageInfo()
                        self.commentText.resignFirstResponder()
                    case .failure(let error):
                        print(error)
                    }
                }
            }
            //}
        }
        
        
        commentText.text = "Write a comment"
        commentText.textColor = UIColor.lightGray
        
        
//        let parameters: [String : Any]? = [
//            "CommentText": commentText.text!,
//            "photoId": photoId ,
//            ]
//        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/comment"
//        let headers: HTTPHeaders = [
//            "Content-Type": "application/json"
//        ]
//        Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
//            switch response.result {
//            case .success:
//                print(response)
//                self.CommentsPicArray.removeAll()
//                self.getImageInfo()
//            case .failure(let error):
//                print(error)
//            }
//        }
//        commentText.text = ""
//
//    }
    }
    
    
    //override func viewWillAppear(_ animated: Bool) {
    
    //self.getRequest()
    //self.tableView.reloadData()
    
    // IJProgressView.shared.showProgressView(view)
    
    // setCloseTimer()
    
    //}
    
    
    
    
    //    func close() {
    //        IJProgressView.shared.hideProgressView()
    //    }
    //
    //    func setCloseTimer() {
    //        _ = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(close), userInfo: nil, repeats: false)
    //    }
    
    func BackButtonClick(_ title:String,message:String){
        
        let alertController = UIAlertController(title: title, message: message , preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            print("OK Pressed")
            
            let DeletePicUrl = "https://testglostarsdevelopers.azurewebsites.net/api/images/delete/\(self.photoId)"
            print(self.photoId)
            Alamofire.request(DeletePicUrl, method: .post).responseJSON { response in
                
                switch response.result {
                    
                case .success:
                    print(response)
                    
                    let nextViewController =                                  self.storyboard!.instantiateViewController(withIdentifier:"NewsVCViewController")
                    
                    let nvc = UINavigationController(rootViewController: nextViewController)
                    self.present(nvc, animated: false, completion: nil)
                    
                case .failure(let error):
                    print(error)
                }
                
            }
            
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
            print("Cancel Pressed")
            //self.dismiss(animated: true, completion: nil)
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    func handleLongPress(longPressGesture:UILongPressGestureRecognizer) {
        
        
        
        
        
        
    }
    
    func getRequest(){
        
        
        
        
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/user/\(userIDFeed)/\(pageNumber)"
        Alamofire.request(url, method: .get,encoding: URLEncoding.default)
            
            
            .responseJSON{ response in
                
                
                
                if let jsonRoot = response.result.value as? [String:Any]!{
                    
                    print(jsonRoot)
                    if let resultPaylaod = jsonRoot!["resultPayload"] as? [String:Any]!{
                        
//                       let allPictureCount = resultPaylaod["resultPaylaod"] as! 
                        
                        
                        
                        if let model = resultPaylaod!["model"] as? [String: Any]{
                            
                            if let mutPic = model["mutualFollowerPictures"] as! [[String: Any]]! {
                                
                                for data in mutPic{
                                    
                                    guard let mutpics = Mapper<ProfilePublicFeedMapper>().map(JSON: data) else{
                                        
                                        continue
                                    }
                                    if self.TypePhotos == "mutual"{
                                    self.PublicInfo.append(mutpics)
                                    //print(data)
                                    self.totalItems = self.PublicInfo.count
                                    }
                                    
                                }
                            for dic in mutPic {
                                    
                                       print(dic)
                            if let poster = dic["poster"] as? [String:Any]!{
                                    
                                guard  let userinfo = Mapper<PublicUserInfoMapper>().map(JSON: poster!) else {
                                    
                                    
                                continue
                                    
                            }
                                if self.TypePhotos == "mutual"{

                        self.UserInfoArray.append(userinfo)
                                    self.totalItems = self.PublicInfo.count

                                }
                                
                            }
                                    
                                                                    
                            }

                            }
                            
                            if let pubPic = model["publicPictures"] as! [[String: Any]]! {
                                
                                for data in pubPic{
                                    
                                    guard let pubpics = Mapper<ProfilePublicFeedMapper>().map(JSON: data) else{
                                        
                                        continue
                                    }
                                    if self.TypePhotos == "public"{
                                    self.PublicInfo.append(pubpics)
                                    self.totalItems = self.PublicInfo.count

                                    }
                                    
                                }
                             

                                    
                                
                                
                                
                                for dic in pubPic {
                                    
                                    print(dic)
                                    if let poster = dic["poster"] as? [String:Any]!{
                                        
                                        guard  let userinfo = Mapper<PublicUserInfoMapper>().map(JSON: poster!) else {
                                            
                                            
                                            continue
                                            
                                        }
                                        if self.TypePhotos == "public"{

                                        self.UserInfoArray.append(userinfo)
                                        }
                                        
                                    }
                                    
                                    
                                }
                                

                                
                            }
                            
                            if let comPic = model["competitionPictures"] as! [[String: Any]]! {
                                
                                
                                for data in comPic{
                                    
                                    guard let pics = Mapper<ProfilePublicFeedMapper>().map(JSON: data) else{
                                        
                                        continue
                                    }
                                    if self.TypePhotos == "compitition"{
                                    self.PublicInfo.append(pics)
                                        self.totalItems = self.PublicInfo.count

                                    }
                                    
                                }
                                
                                
                                
                                for dic in comPic {
                                    
                                    print(dic)
                                    if let poster = dic["poster"] as? [String:Any]!{
                                        
                                        guard  let userinfo = Mapper<PublicUserInfoMapper>().map(JSON: poster!) else {
                                            
                                            
                                            continue
                                            
                                        }
                                        if self.TypePhotos == "compitition"{

                                        self.UserInfoArray.append(userinfo)
                                        }
                                        
                                    }
                                    
                                    
                                }
 
                                
                                
                                
                            }
                        }
                    }
                    self.tableView.reloadData()
                    
                    if  self.scrollEnable == "True"{
                        
                        self.tableView.scrollToRow(at: IndexPath.init(row: Int(self.indexNumber), section: 0), at: .top, animated: true)
                    }
                }
                
                
        }
        
        
        
        
    }

//    func getRequest(){
//        
//        
//        
//        
//        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/public/\(pageNumber)"
//        
//        Alamofire.request(url).responseJSON { response in
//            
//            if let jsonRoot = response.result.value as? [String:Any]!{
//                //print(jsonRoot)
//                if response.result.value != nil {
//                    
//                    if let resultPayload = jsonRoot["resultPayload"] as? [String:Any]!{
//                        
//                        print(resultPayload)
//                        
//                        if let picReturn = resultPayload["picsToReturn"] as? [[String:Any]]!{
//                            
//                            for data in picReturn {
//                                print(data)
//                                
//                                guard  let imageInfo = Mapper<ProfilePublicFeedMapper>().map(JSON: data) else {
//                                    
//                                    
//                                    continue
//                                    
//                                }
//                                
//                                self.PublicInfo.append(imageInfo)
//                            }
//                            
//                            for dic in picReturn {
//                                
//                                print(dic)
//                                if let poster = dic["poster"] as? [String:Any]!{
//                                    
//                                    guard  let userinfo = Mapper<PublicUserInfoMapper>().map(JSON: poster) else {
//                                        
//                                        
//                                        continue
//                                        
//                                    }
//                                    self.UserInfoArray.append(userinfo)
//                                    
//                                }
//                                
//                                
//                                
//                                
//                                
//                                
//                            }
//                            
//                            
//                            //print(picReturn)
//                            
//                        }
//                        
//                        //print(resultPayload)
//                        
//                        
//                    }
//                    
//                    
//                    
//                    self.tableView.reloadData()
//                    //self.collectionView.
//                    
//                    
//                }
//                
//            }
//                
//            else {
//                
//                print("Network Error ")
//                
//            }
//            
//            
//            
//            
//        }
//        
//    }
    
    
    
    func stopRequest(){
        
        let sessionManager = Alamofire.SessionManager.default
        sessionManager.session.getTasksWithCompletionHandler {
            dataTasks, uploadTasks, downloadTasks in dataTasks.forEach {
                $0.cancel()
            }
            uploadTasks.forEach { $0.cancel() }
            downloadTasks.forEach { $0.cancel() } }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if tableView == self.tableViewComment{
            
            
            return self.CommentsPicArray.count
            
        }
        else{
            
            if self.commentInfo.count < 1 {
                
                
                
            }
            
            // print(commentArray.underestimatedCount)
            return self.PublicInfo.count
            
            
            
            
            // return Articles.count + 1
            
        }
        
        
    }
    
    //    lazy var refreshControl: UIRefreshControl = {
    //        let refreshControl = UIRefreshControl()
    //        refreshControl.addTarget(self, action: #selector(NewsVCViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
    //
    //        return refreshControl
    //    }()
    //
    //    func handleRefresh(_ refreshControl: UIRefreshControl) {
    //
    //        getRequest()
    //        //refreshControl.endRefreshing()
    //    }
    
    
    func getImageInfo (){
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/picture/\(photoId)"
        let headers: HTTPHeaders = [
            
            "Authorization": tokenType + accessToken ,
            "Content-Type": "application/json",
            
            ]
        
        Alamofire.request(url, method:.get,encoding:JSONEncoding.default,headers:headers).responseJSON { response in
            
            
            if response.result.value != nil {
                
                if let jsonRoot = response.result.value as? [String:Any]!{
                    
                    print(jsonRoot)
                    
                    
                    if let ResultPayload = jsonRoot!["resultPayload"] as? [String:Any]!{
                        
                        //print(ResultPayload)
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        if let comments = ResultPayload!["comments"] as! [[String: Any]]!{
                            
                            //print(comments)
                            
                            
                            
                            for array in comments{
                                
                                print(array)
                                
                                guard let allcomments = Mapper<PublicProfileCommentMapper>().map(JSON: array) else{
                                    
                                    continue
                                }
                                
                                self.CommentsPicArray.append(allcomments)
                                
                                
                                
                                
                                
                                
                                
                            }
                            
                            
                        }
                        
                        
                        if let poster = ResultPayload!["poster"] as? [String:Any]!{
                            
                            //print(poster)
                            
                            
                            //                        self.imageUser.sd_setImage(with: PicUrl! as URL, placeholderImage: nil , options: .scaleDownLargeImages, completed: nil)
                        }
                        
                    }
                    
                    
                }
                self.tableViewComment.reloadData()
            }
            else {
                
                print("Internet error")
                
            }
            
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // hiddenFloatingButtons()
        if tableView == self.tableViewComment{
            
            
            
        }
        else{
            
            tableViewComment.isHidden = true
            PicURL = self.PublicInfo[indexPath.row].picUrl
            
            
            if self.PublicInfo[indexPath.row].isCompeting == true {
                
                let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let passData = MainStoryboard.instantiateViewController(withIdentifier: "GaleryViewController") as! GaleryViewController
                
                
                
                
                
                //passData.getcomments = commentArray
                
                self.navigationController?.pushViewController(passData, animated: true)
            }
            else{
                
//                let photoEditor = PhotoEditorViewController(nibName:"PhotoEditorViewController",bundle: Bundle(for: PhotoEditorViewController.self))
//
//                photoEditor.photoEditorDelegate = self as? PhotoEditorDelegate
//
//                photoEditor.getImageUrl = PicURL
//                print(PicURL)
//                photoEditor.colors = [.red,.blue,.green]
//
//
//
//
//
//                //                    for i in 0...10 {
//                //                        photoEditor.stickers.append(UIImage(named: i.description )!)
//                //                    }
//
//
//                self.present(photoEditor, animated: true, completion: nil)
                
                
            }
            
        }
        
        
    }
    
    // this method will be called by tableview for as many cells are there in the beginning and max times is rows count
    // this method creates the cell object and configures the cell object with data and then returns the cell object
    // get the cell for all rows in every section
    
    var  imageValue = ""
    func DeleteComment(_ title:String,message:String){
        
        let alertController = UIAlertController(title: title, message: message , preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            print("OK Pressed")
            
            let DeleteComment = "https://testglostarsdevelopers.azurewebsites.net/api/images/DeleteComment?commentId=\(self.commentIdCell)"
            //print(allComments.commentId)
            
            Alamofire.request(DeleteComment, method: .get).responseJSON { response in
                
                switch response.result {
                    
                case .success:
                    print(response)
                    self.CommentsPicArray.removeAll()
                    self.getImageInfo()
                    // self.performSegue(withIdentifier: "News", sender: NewsVCViewController.self)
                    
                    
                case .failure(let error):
                    print(error)
                }
                
            }
            
            
            
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
            print("Cancel Pressed")
            //self.dismiss(animated: true, completion: nil)
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if tableView == self.tableViewComment{
            let Cell = tableViewComment.dequeueReusableCell(withIdentifier: "PublicCommentCell", for: indexPath) as! PublicCommentCell
            
            Cell.commentMsg.isEditable = false
            tableViewComment.separatorColor = UIColor.brown
            
            
            let allComments = self.CommentsPicArray[indexPath.row]
            
            let urlImage = NSURL(string: allComments.userproPic )
            
            Cell.commentMsg.text = allComments.comments
            Cell.userImage.sd_setImage(with: urlImage! as URL, placeholderImage: nil , options: .scaleDownLargeImages, completed: nil)
            Cell.username.text = allComments.userName + " " + allComments.lastName
            
            //Cell.deleteButtonOutlet.isHidden = true
            //Cell.editCommentOutlet.isHidden = true
            
//            if userID == allComments.commenterId {
//
//                Cell.deleteButtonOutlet.isHidden = false
//
//            }
//            if userID == allComments.commenterId {
//
//                Cell.editCommentOutlet.isHidden = false
//
//            }
            
            //Cell.deleteButtonOutlet.alpha = 0
            
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            if let date = dateFormatter.date(from: allComments.uploadTime ){
                
                Cell.uploadTime.text = timeAgoSinceDate(date, currentDate: Date(), numericDates: true)
            }
            
            
            //Cell.userLastName.text = allComments.lastName
            
            
            Cell.onProfileTapped = {
                
                let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                
                
                passData.getid = allComments.commenterId
                
                
                
                
                
                self.navigationController?.pushViewController(passData, animated: true)
                
                
            }
            Cell.onProfileTapped1 = {
                
                self.commentIdCell = String(allComments.commentId)
                self.DeleteComment("Warning", message: "Do you want to Delete your Comment")
                
            }
            Cell.onProfileTapped2 = {
                
                
                if Cell.editCommentOutlet.currentTitle == "Edit"{
                    
                    Cell.editCommentOutlet.setTitle("Done", for: .normal)
                    Cell.commentMsg.isEditable = true
                    Cell.commentMsg.becomeFirstResponder()
                    
                }
                    
                else {
                    
                    let parameter = [
                        
                        "PhotoId" : self.photoId,
                        "CommentId" : allComments.commentId,
                        "NewCommentText" : Cell.commentMsg.text
                        
                        
                        ] as [String : Any]
                    
                    let EditComment = "https://testglostarsdevelopers.azurewebsites.net/api/images/comment"
                    
                    //         Alamofire.request(EditComment, method: .put, parameters: parameter, encoding: .URLEncoding.https, headers: nil).response
                    Alamofire.request(EditComment, method: .put, parameters: parameter, encoding: URLEncoding.httpBody, headers: nil).responseJSON { response in
                        
                        switch response.result {
                            
                        case .success:
                            print(response)
                            Cell.editCommentOutlet.setTitle("Edit", for: .normal)
                            print(self.commentText.text)
                            self.CommentsPicArray.removeAll()
                            self.getImageInfo()
                            //self.tableViewComment.reloadData()
                            
                        case .failure(let error):
                            print(error)
                        }
                        
                    }
                    
                    
                    
                    
                }
                
                
                
                
            }
            
            
            
            
            
            
            
            return Cell
        }
        else{
            
            
            
            
            
            // create the table view cell design from the prototype cell we have created in storyboard. Reuseidentifier will have same string as in storyboard
            let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "PublicFeedCell", for: indexPath) as! PublicFeedCell
            
            //tableViewCell.contentView.superview?.clipsToBounds = false
            //tableView.rowHeight = UITableViewAutomaticDimension
            //            let article2 = self.Articles[indexPath.row]
            //            let article1 = self.ArticlesInfo[indexPath.row]
            //            //idArray = [article2.idPhoto]
            //            tableViewCell.deleteButton.isHidden = true
            
            
            //print(idArray)
            // let article3 = self.commentInfo[indexPath.row]
            
            let allinfo = self.PublicInfo[indexPath.row]
            let userinfo = self.UserInfoArray[indexPath.row]
            
            //let UserInfo = self.UserInfoArray[indexPath.row]
            
            
            
            let url = NSURL(string: allinfo.picUrl)
            let url2 = NSURL(string: userinfo.profilePicURL)
            print(url2!)
            
            // let urlUser = NSURL(string: UserInfo.profilePicURL)
            
            tableViewCell.startCount.text =  allinfo.starcount.description
          // tableViewCell.commentCount.text =
            if  userinfo.profilePicURL == "/Content/Profile/Thumbs/   Male.jpg"{
                
                tableViewCell.proPic.image = UIImage(named: "male")
                
                
            }
            else if  userinfo.profilePicURL == "/Content/Profile/Thumbs/   Female  .jpg"{
                
                tableViewCell.proPic.image = UIImage(named: "female")
                
            }
            else{
                
                tableViewCell.proPic.sd_setImage(with: url2! as URL, placeholderImage: nil , options: .scaleDownLargeImages, completed: nil)
            }
            let dateFormatter = DateFormatter()
            
            
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            
            if let  date = dateFormatter.date(from: allinfo.uploadedTime) {
                tableViewCell.uploadedTime.text = timeAgoSinceDate(date, currentDate: Date(), numericDates: true)
            }
            tableViewCell.deleteButton.isHidden = true
            
            if self.PublicInfo[indexPath.row].featuredPicture == true {
                
                print("fretured")
                tableViewCell.heightConstraintFeature.constant = 0.0
                tableViewCell.featureTextOutlet.alpha = 0
                tableViewCell.featuredImageOutlet.alpha = 0
                tableViewCell.heightConstraintsFooterFeatureView.constant = 0.0
                
                tableViewCell.seeAllPicOutlet.alpha = 0
                
                
            }
            else {
                
                tableViewCell.heightConstraintFeature.constant = 00.00
                tableViewCell.featureTextOutlet.alpha = 0
                tableViewCell.featuredImageOutlet.alpha = 0
                tableViewCell.heightConstraintsFooterFeatureView.constant = 00.00
                tableViewCell.seeAllPicOutlet.alpha = 0
                
                
                
                
            }
            
            if self.PublicInfo[indexPath.row].isCompeting == true  {
                
                tableViewCell.privacyPic.image = UIImage(named: "privacy_competition_photo")
                //tableViewCell.start1Outlet.isHidden = true
                tableViewCell.startCount.isHidden = true
                tableViewCell.commentCount.isHidden = true
                tableViewCell.descriptionLabel.text = "See All Competition Pictures"
                tableViewCell.commentButtonOutlet.isHidden = true
                tableViewCell.proPic.isHidden = true
                tableViewCell.nameUser.isHidden = true
                tableViewCell.editButton.isHidden = true
                tableViewCell.paintCount.isHidden = true
                
                
                
                
            }
            
            
            if self.PublicInfo[indexPath.row].privacyPublic == "public" && self.PublicInfo[indexPath.row].isCompeting == false {
                
                tableViewCell.privacyPic.image = UIImage(named: "Public w-shadow")
                
                tableViewCell.start1Outlet.isHidden = false
                tableViewCell.startCount.isHidden = false
                tableViewCell.commentCount.isHidden = false
                tableViewCell.descriptionLabel.text = PublicInfo[indexPath.row].description
                tableViewCell.commentButtonOutlet.isHidden = false
                tableViewCell.proPic.isHidden = false
                tableViewCell.nameUser.isHidden = false
                tableViewCell.editButton.isHidden = false
                tableViewCell.paintCount.isHidden = false
                
                
                
            }
            
            if self.PublicInfo[indexPath.row].privacyFriend == "friends"  {
                
                tableViewCell.privacyPic.image = UIImage(named: "Mutual w-shadow")
                
                tableViewCell.start1Outlet.isHidden = false
                tableViewCell.startCount.isHidden = false
                tableViewCell.commentCount.isHidden = false
                tableViewCell.descriptionLabel.text = PublicInfo[indexPath.row].description
                tableViewCell.commentButtonOutlet.isHidden = false
                tableViewCell.proPic.isHidden = false
                tableViewCell.nameUser.isHidden = false
                tableViewCell.paintCount.isHidden = false
                
                
            }
            
            if userID == self.UserInfoArray[indexPath.row].userid{
                
                
                tableViewCell.deleteButton.isHidden = false
                tableViewCell.editButton.isHidden = true
                tableViewCell.paintCount.isHidden = true
                
            }
            if  self.PublicInfo[indexPath.row].isCompeting == true {
                
                
                
            }
            if allinfo.myStarCount == 0 {
                
                let image = UIImage(named: "Star empty w-shadow") as UIImage?
                
                
                tableViewCell.start1Outlet.setImage(image, for: .normal)
                self.imageValue = ""
                
                
            }
            
            if allinfo.myStarCount == 5 {
                
                let image = UIImage(named: "Star filled w-shadow") as UIImage?
                self.imageValue = "Checked"
                
                
                tableViewCell.start1Outlet.setImage(image, for: .normal)
                
            }
            if allinfo.myStarCount == 4 {
                
                let image = UIImage(named: "Star filled w-shadow") as UIImage?
                self.imageValue = "Checked"
                
                
                tableViewCell.start1Outlet.setImage(image, for: .normal)
                
            }
            if allinfo.myStarCount == 3 {
                
                let image = UIImage(named: "Star filled w-shadow") as UIImage?
                self.imageValue = "Checked"
                
                
                tableViewCell.start1Outlet.setImage(image, for: .normal)
                
            }
            if allinfo.myStarCount == 2 {
                
                let image = UIImage(named: "Star filled w-shadow") as UIImage?
                self.imageValue = "Checked"
                
                
                tableViewCell.start1Outlet.setImage(image, for: .normal)
            }
            if allinfo.myStarCount == 1 {
                
                let image = UIImage(named: "Star filled w-shadow") as UIImage?
                self.imageValue = "Checked"
                
                
                tableViewCell.start1Outlet.setImage(image, for: .normal)
            }
            
            
            //cell.imgImage.af_imageDownloader?.download([url as! URLRequestConvertible])
            
            tableViewCell.cellPhoto.sd_setShowActivityIndicatorView(true)
            tableViewCell.cellPhoto.sd_setIndicatorStyle(.gray)
            tableViewCell.cellPhoto.sd_setImage(with: url! as URL, placeholderImage: nil, options: .scaleDownLargeImages, completed: nil)
            
            //tableViewCell.descriptionLabel.text = allinfo.description
            //tableViewCell.proPic.af_setImage(withURL: url2! as URL)
            tableViewCell.nameUser.text = userinfo.name
            tableViewCell.onDeleteTap = {
                
                self.photoId = self.PublicInfo[indexPath.row].idPhoto
                self.BackButtonClick("Warning", message: "Are you sure you want to delete this Photo")
                print("Delete")
                
            }
            
            
            // Report Button Action
            //-----------------------------------------------
            // Mark: changed by shaikat
            //-----------------------------------------------
            
            tableViewCell.onReport = {
                
                let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                if userID == userinfo.userid{
                    self.TitleShow = "Delete"
                }
                else{
                    self.TitleShow = "Report"
                }
                
                
                // create an action
                let firstAction: UIAlertAction = UIAlertAction(title: self.TitleShow, style: .default) { action -> Void in
                    
                    
                    if userID == userinfo.userid{
                        
                        self.photoId = allinfo.idPhoto
                        self.BackButtonClick("Warning", message: "Are you sure you want to delete this photo?")
                        print("Delete")
                    }
                    else{
                        
                        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                        let passData = MainStoryboard.instantiateViewController(withIdentifier: "ReportPhoto") as! ReportPhoto
                        
                        self.navigationController?.pushViewController(passData, animated: true)
                    }
                    
                    
                    
                }
                
                let secondAction: UIAlertAction = UIAlertAction(title: "Block this user", style: .default) { action -> Void in
                    
                    print("Second Action pressed")
                }
                
                let thirdAction: UIAlertAction = UIAlertAction(title: "Share on Facebook", style: .default) { action -> Void in
                    
                    print("Share Photo")
                    // image to share
                    let text = allinfo.description
                    let image = tableViewCell.cellPhoto.image
                    let myWebsite = NSURL(string:"https://testglostarsdevelopers.azurewebsites.net/Home/ViewPicture/\(allinfo.idPhoto)")
                    let shareAll = [text , image!,myWebsite] as [Any]
                    let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view
                    self.present(activityViewController, animated: true, completion: nil)
                    
                    
                }
                
                
                let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
                
                // add actions
                //actionSheetController.addAction(secondAction)
                actionSheetController.addAction(thirdAction)
                actionSheetController.addAction(firstAction)
                actionSheetController.addAction(cancelAction)
                
                // present an actionSheet...
                self.present(actionSheetController, animated: true, completion: nil)
            }
            
            
            tableViewCell.onEditTap = {
                
//                print("Edit")
//                let photoEditor = PhotoEditorViewController(nibName:"PhotoEditorViewController",bundle: Bundle(for: PhotoEditorViewController.self))
//                
//                photoEditor.photoEditorDelegate = self as? PhotoEditorDelegate
//                
//                photoEditor.getImageUrl = self.PublicInfo[indexPath.row].picUrl
//                
//                photoEditor.colors = [.red,.blue,.green]
//                
//                //                    for i in 0...10 {
//                //                        photoEditor.stickers.append(UIImage(named: i.description )!)
//                //                    }
//                
//                
//                self.navigationController?.pushViewController(photoEditor, animated: true)
                
                
            }
            
            
            
            
            
            
            
            tableViewCell.onStarTapped1 = {
                
                let imagevanish = UIImage(named: "Star filled w-shadow") as UIImage?
                
                
                
                
                
                
                if self.imageValue == "Checked" {
                    
                    
                    let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/removerate/\(self.PublicInfo[indexPath.row].idPhoto)"
                    //            let headers: HTTPHeaders = [
                    //                "Content-Type": "application/json"
                    //            ]
                    Alamofire.request(url, method:.post,encoding: URLEncoding.default).responseJSON { response in
                        switch response.result {
                        case .success:
                            
                            let imagevanish = UIImage(named: "Star empty w-shadow") as UIImage?
                            
                            
                            
                            
                            
                            tableViewCell.start1Outlet.setImage(imagevanish, for: .normal)
                            
                            print(response)
                            self.imageValue = ""
                            // self.ArticlesInfo.removeAll()
                            
                            // self.getRequest()
                            
                            
                            // self.tableView.reloadData()
                            tableViewCell.startCount.text =  allinfo.starcount.description
                            
                            
                            
                        case .failure(let error):
                            print(error)
                        }
                        
                        
                    }
                    
                    
                    
                    
                    
                }
                else{
                    
                    
                    let parameters: [String : Any]? = [
                        "NumOfStars": 1 ,
                        "photoId": self.PublicInfo[indexPath.row].idPhoto ,
                        ]
                    let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/rating"
                    let headers: HTTPHeaders = [
                        "Content-Type": "application/json"
                    ]
                    Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
                        
                        if let jsonRoot = response.result.value as? [String:Any]!{
                            
                            //print(jsonRoot)
                            
                            if let resultPayload = jsonRoot!["resultPayload"] as? [String: Any]!{
                                
                                print(resultPayload)
                                
                                tableViewCell.startCount.text =  String(describing: resultPayload!["totalRating"] as! Int)
                                print(tableViewCell.startCount.text!)
                                
                                
                                self.imageValue = "Checked"
                                
                            }
                            
                            let image = UIImage(named: "Star filled w-shadow") as UIImage?
                            
                            
                            
                            
                            tableViewCell.start1Outlet.setImage(image, for: .normal)
                            
                        }
                        
                        
                        
                        
                        
                        
                    }
                    //self.getRequest()
                }
                
                
            }
            
            tableViewCell.onProfileTapped = {
                //self.stopRequest()
                
                let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                
                
                passData.getid = self.UserInfoArray[indexPath.row].userid
                print(self.UserInfoArray[indexPath.row].userid)
                
                
                
                
                self.navigationController?.pushViewController(passData, animated: true)
                
            }
            
            
            //tableViewCell.nameUser.text = UserInfo
            // .name
            //tableViewCell.proPic.sd_setImage(with: urlUser! as URL, placeholderImage: nil, options: .scaleDownLargeImages, completed: nil)
            
            tableViewCell.onCommentsTapped = {
//                self.photoId = allinfo.idPhoto
//                self.commentView.isHidden = false
//                self.CommentsPicArray.removeAll()
//                self.getImageInfo()
//                self.tableViewComment.isHidden = false
                //self.commentText.becomeFirstResponder()
                let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let passData = MainStoryboard.instantiateViewController(withIdentifier: "CommentPostVC") as! CommentPostVC
                passData.getID = allinfo.idPhoto
                passData.getImage = allinfo.picUrl

                passData.photoId = allinfo.idPhoto
                
                self.navigationController?.pushViewController(passData, animated: true)
                
                
            }
            
            //print(ListPhoto)
            if indexPath.row == PublicInfo.count - 1 {
                // last cell
                
                if totalItems > ArticlesInfo.count {
                    pageNumber = pageNumber + 1
                    print(pageNumber)
                    self.scrollEnable = ""

                    // more items to fetch
                    getRequest() // increment `fromIndex` by 20 before server call
                }
            }
            
            
            
            return tableViewCell
        }
        
    }
    
    //    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    //
    //        if scrollView == tableView {
    //
    //            hiddenFloatingButtonsScrool()
    //        }
    //        hiddenFloatingButtonsScrool()
    //        let offsetY = scrollView.contentOffset.y
    //        let contentHeight = scrollView.contentSize.height
    //
    //        if offsetY > contentHeight - scrollView.frame.size.height {
    //            //ListPhoto.append(ArrayItemCount)
    //            pageNumber += 1
    //
    //            getRequest()
    //            self.tableView.reloadData()
    //
    //            //IJProgressView.shared.showProgressView(view)
    //
    //            //setCloseTimer()
    //        }
    //    }
    
    
    
    
    
    // Do any additional setup after loading the view, typically from a nib
    
    
    
    func hiddenFloatingButtons(){
        
        uploadButton.isHidden = !uploadButton.isHidden
        achivementButton.isHidden =  !achivementButton.isHidden
        profileButton.isHidden =  !profileButton.isHidden
        notificationButton.isHidden = !notificationButton.isHidden
        homeButton.isHidden =  !homeButton.isHidden
        searchButton.isHidden = !searchButton.isHidden
        
    }
    func hiddenFloatingButtonsScrool(){
        
        uploadButton.isHidden = true
        achivementButton.isHidden =  true
        profileButton.isHidden =  true
        notificationButton.isHidden = true
        homeButton.isHidden =  true
        searchButton.isHidden = true
        
    }
    
    
    
    func animatingPopUp()
    {
        UIView.animate(withDuration: 1, animations: {
            self.uploadButton.isHidden =  !self.uploadButton.isHidden
            
        }) { (true) in
            
            UIView.animate(withDuration: 1, animations: {
                self.achivementButton.isHidden = !self.achivementButton.isHidden
                
            }, completion: { (true) in
                UIView.animate(withDuration: 1, animations: {
                    
                    self.profileButton.isHidden = !self.profileButton.isHidden
                    
                }, completion: { (true) in
                    self.notificationButton.isHidden = !self.notificationButton.isHidden
                    //                    let image1 = UIImage(named: "Circleopen")
                    //                    self.floatingMainButton.setBackgroundImage(image1, for: UIControlState.normal)
                    
                    
                    
                    
                })
            })
        }
        
        
        
    }
    
    func layoutFAB() {
        let item = FloatyItem()
        item.buttonColor = UIColor(displayP3Red: 1, green: 1, blue: 1, alpha: 0.7)
        //item.circleShadowColor = UIColor.red
        //item.titleShadowColor = UIColor.blue
        
        //fab.addItem(title: "I got a title")
        //fab.addItem("I got a icon", icon: UIImage(named: "icShare"))
        fab.addItem("", icon: UIImage(named: "notif+"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "photo-camera"), titlePosition: .left) { (item) in
            
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "FAImageCropperVC") as! FAImageCropperVC
            
            self.navigationController?.pushViewController(passData, animated: true)
            
            
            
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "search-1"), titlePosition: .left) { (item) in
            
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "competiton"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "GaleryViewController") as! GaleryViewController
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "Profile_male"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            passData.getid = userID
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "home+"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "NewsVCViewController") as! NewsVCViewController
            
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        
        // For Image
        
        
        self.fab.sticky = true
        self.fab.paddingX = self.view.frame.width/20
        
        self.fab.fabDelegate = self
        
        print(tableView.frame)
        
        self.view.addSubview(self.fab)
    }
    
    @IBAction func mainButtonAction(_ sender: UIButton) {
        
        
        hiddenFloatingButtons()
        
        
        
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        
        
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        //  if emojiButtonPressed == false {
        commentText.becomeFirstResponder()
        
        if commentText.textColor! == UIColor.lightGray {
            commentText.text! = ""
            commentText.textColor = UIColor.black
        }
        
        //  }
        
        //commentText.text! = ""
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if commentText.text == "" {
            commentText.text! = "Write a comment"
            commentText.textColor = UIColor.lightGray
            commentText.resignFirstResponder()
        }
            
        else {
            commentText.text = "Write a comment"
            commentText.textColor = UIColor.lightGray
            commentText.resignFirstResponder()
        }
        
        
        // emojiButtonPressed = false
    }
    
    
    
    
    @IBAction func emoButtonPressed(_ sender: Any) {
        
        if commentText.text! == "Write a comment"{
            commentText.text! = ""
        }
        let keyboardSettings = KeyboardSettings(bottomType: BottomType(rawValue: bottomType.hashValue) ?? BottomType(rawValue: 1)!)
        //keyboardSettings.customEmojis = emojis
        keyboardSettings.needToShowAbcButton = true
        
        let emojiView = EmojiView(keyboardSettings: keyboardSettings)
        emojiView.translatesAutoresizingMaskIntoConstraints = false
        emojiView.delegate = self
        commentText.inputView = emojiView
        commentText.becomeFirstResponder()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!)years"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!)month"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!)w"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1w"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!)d"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1d"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!)h"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1h"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 8) {
            return "\(components.minute!)m"
        } else if (components.minute! >= 8){
            if (numericDates){
                return "1m"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 0) {
            return "Just now"
        } else {
            return "Just now"
        }
        
    }
    
    
    
    
}







