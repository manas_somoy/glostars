//
//  InterestVC.swift
//  Glostars
//
//  Created by Pathao on 23/7/18.
//  Copyright © 2018 Pathao. All rights reserved.
//

import UIKit

var interesrtValue = "Choose Interest"

class InterestVC: UIViewController {
    
    var fromUpload = ""
    @IBOutlet weak var interestButton: DesignableButton!
    
    @IBOutlet weak var interestCollection: UICollectionView!
    
    // interest array
    let interestArray = ["Landscape","Nature","Animal","People","Art","Portrait","Macro","Sunset","Architecture","Drinks","Sports","Event","Love","Cars","Food","Culture","Pet","Seasons","Adventure","Urban","Fitness & Exercise","Science & Technology","Vacation/Travel","Motivation","Make-up","Lifestyle","Funny","Fashion"]
    
    // interest image array
    let interstImageArray = [#imageLiteral(resourceName: "Landscape"),#imageLiteral(resourceName: "Nature 1"),#imageLiteral(resourceName: "Animal"),#imageLiteral(resourceName: "People"),#imageLiteral(resourceName: "Art"),#imageLiteral(resourceName: "Portrait"),#imageLiteral(resourceName: "Macro"),#imageLiteral(resourceName: "Sunset"),#imageLiteral(resourceName: "Architecture"),#imageLiteral(resourceName: "Drinks"),#imageLiteral(resourceName: "Sports"),#imageLiteral(resourceName: "Event"),#imageLiteral(resourceName: "Love"),#imageLiteral(resourceName: "action-american-car-asphalt-981035"),#imageLiteral(resourceName: "Food"),#imageLiteral(resourceName: "Culture"),#imageLiteral(resourceName: "Pet"),#imageLiteral(resourceName: "Seasons"),#imageLiteral(resourceName: "Adventure"),#imageLiteral(resourceName: "Urban"),#imageLiteral(resourceName: "Fitness & Exercise"),#imageLiteral(resourceName: "Science"),#imageLiteral(resourceName: "Vacation"),#imageLiteral(resourceName: "Motivation"),#imageLiteral(resourceName: "Make-up"),#imageLiteral(resourceName: "Lifestyle"),#imageLiteral(resourceName: "Funny"),#imageLiteral(resourceName: "Fashion")]
    var interestArrayElements = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    self.navigationItem.setHidesBackButton(true, animated:true);
        
        interestCollection.allowsMultipleSelection = true;


        interestButton.setTitle("Interest Selected 0 of 3 ", for: .normal)
        interestButton.alpha = 0.6
        interestButton.isUserInteractionEnabled = false
        self.title = "Choose at least 3 Interests"

        // left navigation label initialization
        if let navigationBar = self.navigationController?.navigationBar {
            //register collection view cell
            let interestCollectionNib : UINib? = UINib(nibName: "InterestCell", bundle: nil)
            interestCollection.register(interestCollectionNib, forCellWithReuseIdentifier: "InterestCell")
            
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .vertical
            interestCollection.collectionViewLayout = layout
            interestCollection!.contentInset = UIEdgeInsets(top: 0, left: 8, bottom:0, right: 8)
            
            if let layout = interestCollection.collectionViewLayout as? UICollectionViewFlowLayout {
                layout.minimumInteritemSpacing = 0
                layout.minimumLineSpacing = 5
                layout.itemSize = CGSize(width: interestCollection.frame.size.width / 4, height: 100)
                layout.invalidateLayout()
            }
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func interestAction(_ sender: Any) {
        
        if fromUpload == "true"{
        self.navigationController?.popViewController(animated: true)
            
        }
        else{
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        self.view.window!.layer.add(transition, forKey: kCATransition)
        
        let nextViewController =                                  self.storyboard!.instantiateViewController(withIdentifier:"NewsVCViewController")
        
        let nvc = UINavigationController(rootViewController: nextViewController)
        self.present(nvc, animated: false, completion: nil)
        }

    }
    
    
    @objc func rightButtonAction(){
        print("Right bar button clicked")
    }

}

extension InterestVC: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return interestArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InterestCell", for: indexPath) as! InterestCell
        
        cell.interestImageViewOutlet.image = interstImageArray[indexPath.row]
        cell.interestLabelOutlet.text = interestArray[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
      
        interestArrayElements.append(interestArray[indexPath.row])
        var  uniqueUnordered = Array(Set(interestArrayElements))

        
        if uniqueUnordered.count == 1 {
            
        interestButton.setTitle("Interest Selected 1 of 3 ", for: .normal)

        }
        if uniqueUnordered.count == 2 {
            
            interestButton.setTitle("Interest Selected 2 of 3 ", for: .normal)
            
        }
        if uniqueUnordered.count > 2 {
            
            interestButton.setTitle("Continue", for: .normal)
            interestButton.alpha = 1
            interestButton.isUserInteractionEnabled = true
            interesrtValue = uniqueUnordered.joined(separator: " , ")
            //interestArrayElements.joined(separator: " , ")
            //print(interesrtValue)
            print(uniqueUnordered)
        }
        
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.borderWidth = 5.0
        cell?.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        if let index = interestArrayElements.index(of: interestArray[indexPath.row]){
            interestArrayElements.remove(at: index)
        }
        
       
        var  uniqueUnordered = Array(Set(interestArrayElements))
        
        if uniqueUnordered.count < 1 {
            interestButton.isUserInteractionEnabled = false
            interestButton.setTitle("Interest Selected 0 of 3 ", for: .normal)
            
        }
    
        if uniqueUnordered.count == 1 {
            interestButton.isUserInteractionEnabled = false
            interestButton.setTitle("Interest Selected 1 of 3 ", for: .normal)
            
        }
        if uniqueUnordered.count == 2 {
            interestButton.isUserInteractionEnabled = false
            interestButton.setTitle("Interest Selected 2 of 3 ", for: .normal)
            
        }
        if uniqueUnordered.count > 2 {
            
            interestButton.setTitle("Continue", for: .normal)
            interestButton.alpha = 1
            interestButton.isUserInteractionEnabled = true
            interesrtValue = uniqueUnordered.joined(separator: " , ")
            print(uniqueUnordered)
        }
        
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.borderWidth = 5.0
        cell?.layer.borderColor = UIColor.clear.cgColor
    }
}
