//
//  GaleryMapper.swift
//  Glostars
//
//  Created by Sanzid Ashan on 5/19/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

//
//  ImageMapper.swift
//  Glostars
//
//  Created by Sanzid Ashan on 5/17/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import Foundation


import Foundation
import ObjectMapper

class SecondPageMapper : Mappable {
    
    
    dynamic var id : Int = 0
    dynamic var themePicUrlMedium : String = ""
    dynamic var themePicUrlMini : String = ""
    dynamic var year : String = ""
    dynamic var weekNumber : Int = 0
    dynamic var themeName : String = ""


    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    required convenience init?(map: Map) {
        
        
        self.init()
        
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        themePicUrlMedium <- map["themePicUrlMedium"]
        themePicUrlMini <- map["themePicUrlMini"]
        year <- map["year"]
        weekNumber <- map["weekNumber"]
        themeName <- map["themeName"]

        
    }
}


