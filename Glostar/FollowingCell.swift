//
//  FollowingCell.swift
//  Glostars
//
//  Created by Sanzid iOS on 7/12/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit

class FollowingCell: UITableViewCell {

    @IBOutlet weak var proPicOutlet: UIImageView!
    
    @IBOutlet weak var followDetails: UIButton!
    @IBOutlet var lastName: UILabel!
    
    @IBOutlet weak var userNameOutlet: UILabel!
    
    @IBOutlet var flowOutletButton: UIButton!

    
    var onProfileTapped: (() -> Void)? = nil
    var onImageTapped: (() -> Void)? = nil
    var onLabelTapped: (() -> Void)? = nil
    var onDetailsTapped: (() -> Void)? = nil



    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //proPicOutlet.setRounded()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        proPicOutlet.setRounded()
        
        // Configure the view for the selected state
    }
    
    
    @IBAction func proPicAction(_ sender: Any) {
        
        if let onImageTapped = self.onImageTapped {
            
            onImageTapped()
            
        }
        
        
    }
    @IBAction func NamePicAction(_ sender: Any) {
        
        if let onLabelTapped = self.onLabelTapped {
            
            onLabelTapped()
            
        }
        
    }
    
    
    
    @IBAction func ProfileClickAction(_ sender: UIButton) {
        
        if let onProfileTapped = self.onProfileTapped {
            
            onProfileTapped()
            
        }
        
    }
    @IBAction func FollowDetails(_ sender: UIButton) {
        
        if let onDetailsTapped = self.onDetailsTapped {
            
            onDetailsTapped()
            
        }
        
    }
    
}

