//
//  EditImageVC.swift
//  EditedProject
//
//  Created by sadidur rahman on 4/2/19.
//  Copyright © 2019 sadidur rahman. All rights reserved.
//

import UIKit
import iOSPhotoEditor
import Alamofire
import ObjectMapper


class EditImageVC: UIViewController,PhotoEditorDelegate {

    var PhotoArray : Array<EditPhotoMapper> = []
    var imagePic = UIImage()
    var photoID = 0
    @IBOutlet weak var imageEdit: UIImageView!
    @IBOutlet weak var editIcon: UIImageView!
    
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageEdit.image = imagePic
        self.getPhotos()
        
        editView.addTapGestureRecognizer {
                
                
                let photoEditor = PhotoEditorViewController(nibName:"PhotoEditorViewController",bundle: Bundle(for: PhotoEditorViewController.self))
                
                //PhotoEditorDelegate
            photoEditor.photoEditorDelegate = self
                //The image to be edited
            photoEditor.image = self.imagePic
                
                //Stickers that the user will choose from to add on the image
                for i in 0...17 {
                    photoEditor.stickers.append(UIImage(named: i.description )!)
                }
                //Optional: To hide controls - array of enum control
                photoEditor.hiddenControls = [.crop, .share]
                
                //Optional: Colors for drawing and Text, If not set default values will be used
                //            photoEditor.colors = [.r]
                
                //Present the View Controller
                self.present(photoEditor, animated: true, completion: nil)
                
        
            
        }
        editView.addTapGestureRecognizer {
    
    
    
    let photoEditor = PhotoEditorViewController(nibName:"PhotoEditorViewController",bundle: Bundle(for: PhotoEditorViewController.self))
    
    //PhotoEditorDelegate
    photoEditor.photoEditorDelegate = self
    //The image to be edited
    photoEditor.image = self.imagePic
    
    //Stickers that the user will choose from to add on the image
    for i in 0...17 {
    photoEditor.stickers.append(UIImage(named: i.description )!)
    }
    //Optional: To hide controls - array of enum control
    photoEditor.hiddenControls = [.crop, .share]
    
    //Optional: Colors for drawing and Text, If not set default values will be used
    //            photoEditor.colors = [.r]
    
    //Present the View Controller
    self.present(photoEditor, animated: true, completion: nil)
    
    }
            
        

        // Do any additional setup after loading the view.
    }
    
    
    
    func getPhotos(){
        
       //https://testglostarsdevelopers.azurewebsites.net/api/editimages/seeall?id=52
            let url = "https://testglostarsdevelopers.azurewebsites.net/api/editimages/seeall?id=\(photoID)"
            Alamofire.request(url, method: .get , encoding: JSONEncoding.default)
                
                
                .responseJSON{ response in
                   // print(response)
                    
                    if response.result.value != nil {
                        
                        
                        if let jsonRoot = response.result.value as? [String:Any]!{
                            
                            
                            print(jsonRoot)
                            
                            
                            if let resultPayload = jsonRoot!["resultPayload"] as? [String : Any]{
                                
                                if let data = resultPayload["data"] as? [[String : Any]]{
                                    print(data)
                                    
                                    for dic in data {
                                        
                                        guard  let picInfo = Mapper<EditPhotoMapper>().map(JSON: dic) else {
                                            
                                            
                                            continue
                                            
                                        }
                                        self.PhotoArray.append(picInfo)
                                    }
                                    
                                }
                            }
                        }
                        self.collectionView.reloadData()
                        
                    }
                    else{
                        
                        print("No Data")
                        self.getPhotos()
                        
                    }
            }
            
            
            
        }
    
    
    
    func doneEditing(image: UIImage) {
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "ImageViewController") as! ImageViewController
        passData.getImageData = imagePic
        passData.chooseCtegeory = ""
        passData.copetionPost = String(photoID)
        
        self.navigationController?.pushViewController(passData, animated: true)
    }
    
    func canceledEditing() {
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
//        self.navigationController?.navigationBar.isHidden = false
    }
    
}



extension EditImageVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        print(PhotoArray.count)
        return PhotoArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EditCell", for: indexPath) as! EditCell
        let url = URL(string: PhotoArray[indexPath.row].editPicUrl)
        let urlPic = URL(string: PhotoArray[indexPath.row].getProfilePictureThumb)
        
        
        cell.userImage.addTapGestureRecognizer {
           
        }
        
        cell.userImage.addTapGestureRecognizer {
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            passData.getid = self.PhotoArray[indexPath.row].userId
            self.navigationController?.pushViewController(passData, animated: true)
        }

        
        
      
        
            
            cell.cellPhotoImage.sd_setImage(with: url, completed: nil)
            cell.userImage.sd_setImage(with: urlPic, completed: nil)
            cell.userNameLabel.text = PhotoArray[indexPath.row].username
          
//
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width / 2 - 2, height: 250)
        //self.collectionView.frame.height / 2 - 4
    }

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "DetailedEditedImageVC") as! DetailedEditedImageVC
//        self.present(nextViewController, animated:true, completion:nil)
        
        
//        let newViewController = DetailedEditedImageVC()
//        self.navigationController?.pushViewController(newViewController, animated: true)
              let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
              let passData = MainStoryboard.instantiateViewController(withIdentifier: "DetailedEditedImageVC") as! DetailedEditedImageVC
//                passData.PhotoArrayFeed = PhotoArray
                passData.idPhoto = self.photoID
        
              self.navigationController?.pushViewController(passData, animated: true)
        
    }
    
    
    
}
