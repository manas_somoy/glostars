//
//  SecondCollectinCell.swift
//  Glostars
//
//  Created by sadidur rahman on 5/1/19.
//  Copyright © 2019 Sanzid Ashan. All rights reserved.
//

import UIKit

class SecondCollectinCell: UICollectionViewCell {
    @IBOutlet weak var comImage: UIImageView!
    @IBOutlet weak var week: UILabel!
    @IBOutlet weak var title: UILabel!
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

}
