//
//  NotificationViewController.swift
//  Glostars
//
//  Created by Sanzid Ashan on 3/30/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

import ObjectMapper
import SDWebImage

var ComptitionPic : Array<GaleryMapper> = []
var IndexCount = 0

class GaleryViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,FloatyDelegate{
    
    
    @IBOutlet weak var topView: UIView!
    
    
    @IBOutlet weak var comUpImage: UIImageView!
    @IBOutlet weak var weekLabel: UILabel!
    @IBOutlet weak var ThemeBeginLabel: UILabel!
    
    @IBOutlet var searchButton: UIButton!
    
    @IBOutlet var hidingView: UIView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    @IBOutlet weak var galerySegment: UISegmentedControl!
    @IBOutlet weak var recognisionView: UIView!
    
    @IBOutlet weak var tipsView: UIView!
    @IBOutlet weak var termsView: UIView!
    
    
    @IBOutlet weak var leftFabButton: UIButton!
    @IBOutlet weak var achivementButton: UIButton!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var uploadButton: UIButton!
    
    @IBOutlet weak var floatingMainButton: UIButton!
    
    @IBOutlet var segmentTopConstraints: NSLayoutConstraint!
    var pageNumber = 1

    var totalItems = 100000000
    var fab = Floaty()
    //var des = ""
    @IBOutlet weak var weekStackView: UIStackView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.contentInset = UIEdgeInsetsMake(40.0, 0.0, 60.0, 0.0)
    
        layoutFAB()

        self.navigationController?.setNavigationBarHidden(false, animated: true)


//        self.navigationController?.isNavigationBarHidden = false
       // var timer = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(self.update), userInfo: nil, repeats: false)


        galerySegment.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white,
                                                  NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 14)!], for: .normal)
        
        galerySegment.setTitleTextAttributes([
            NSForegroundColorAttributeName: UIColor.white,
            NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 14)!], for: .selected)
        
        hiddenFloatingButtons()
        
        self.navigationItem.title = "Competition Photos"
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.white,
             NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 18)!]

        recognisionView.isHidden = true
        tipsView.isHidden = true
        termsView.isHidden = true

   
        
//        let urlString = URL(string: userPic)!
//
//
//        profileButton.af_setBackgroundImage(for: [] , url: urlString )
        
        
        // Do any additional setup after loading the view, typically from a nib.
        
        
        

    }
    
    
    
    
    
    @IBAction func priviousComAction(_ sender: Any) {
        
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "SeconndGalleryVC") as! SeconndGalleryVC
        self.navigationController?.pushViewController(passData, animated: true)
        
        
    }
    
    
    
    
    @IBAction func sumbitClickAction(_ sender: Any) {
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "FAImageCropperVC") as! FAImageCropperVC
        passData.uploadCom = "true"
        
        self.navigationController?.pushViewController(passData, animated: true)
        
    }
    
    
    func layoutFAB() {
        let item = FloatyItem()
        item.buttonColor = UIColor(displayP3Red: 1, green: 1, blue: 1, alpha: 0.7)
        //item.circleShadowColor = UIColor.red
        //item.titleShadowColor = UIColor.blue
        
        //fab.addItem(title: "I got a title")
        //fab.addItem("I got a icon", icon: UIImage(named: "icShare"))
        fab.addItem("", icon: UIImage(named: "notif+"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "photo-camera"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "FAImageCropperVC") as! FAImageCropperVC
            
            self.navigationController?.pushViewController(passData, animated: true)
            
            
            
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "search-1"), titlePosition: .left) { (item) in
            
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "competiton"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "GaleryViewController") as! GaleryViewController
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "Profile_male"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            passData.getid = userID
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "home+"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "NewsVCViewController") as! NewsVCViewController
            
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        
        self.fab.sticky = true
        self.fab.paddingX = self.view.frame.width/20
        
        self.fab.fabDelegate = self
        
       // print(tableView.frame)
        
        self.view.addSubview(self.fab)
    }
    

    
    override func viewWillAppear(_ animated: Bool) {
        
       // print(pageNumber)
        self.navigationController?.navigationBar.isHidden = false
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        ComptitionPic.removeAll()
        pageNumber = 1
        getRequest()
        //ChangeAPP
       AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)

        leftFabButton.setRounded()
        notificationButton.setRounded()
        uploadButton.setRounded()
        achivementButton.setRounded()
        homeButton.setRounded()
        profileButton.setRounded()
        searchButton.setRounded()

        

        if userPic == "/Content/Profile/Thumbs/   Male.jpg"{
            
            profileButton.setBackgroundImage(UIImage(named:"male"), for: .normal)
            
            
        }
        else if userPic == "/Content/Profile/Thumbs/   Female  .jpg"{
            
            profileButton.setBackgroundImage(UIImage(named:"female"), for: .normal)
        }  
            
        else{
            let urlString = URL(string:String(describing: UserDefaults.standard.value(forKey: "pic")!))
            profileButton.af_setBackgroundImage(for: [] , url: urlString! )
            
        }
        
        if #available(iOS 11.0, *) {
            weekStackView.setCustomSpacing(30.0, after: ThemeBeginLabel)
        } else {
            // Fallback on earlier versions
        }
        
        
    }
    


    
    @IBAction func onClinkProfile(_ sender: Any) {
        
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        
        
        passData.getid = userID
        
        self.navigationController?.pushViewController(passData, animated: true)
        
        
    }
  
    func getRequest(){
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/weeklycompetition/\(pageNumber)"
        Alamofire.request(url).responseJSON { response in
            
            
            if response.result.value != nil{
            
            if let jsonRoot = response.result.value as? [String:Any]{
            //print(jsonRoot)
                
            if let resultPayload = jsonRoot["resultPayload"] as? [String:Any]{
                
            print(resultPayload)
                
        if let result = resultPayload["recentWeek"] as? [String:Any]{
            
            print(result)
            
        let weekNumber =  result["weekNumber"] as! Int
        self.weekLabel.text = "W E E K " + String(weekNumber)
        let themeName =  result["themeName"] as! String
        self.ThemeBeginLabel.text = themeName
        
        let path =  result["path"] as! String
        let url = URL(string: path)
            
        self.comUpImage!.af_setImage(withURL: url!)

        if let resultallPost = resultPayload["allpost"] as?[ [String:Any]]{
            
            
             print(resultallPost)
            
                
            for data in resultallPost {

                guard  let CompititionImage = Mapper<GaleryMapper>().map(JSON: data) else {
                continue

                        }

              ComptitionPic.append(CompititionImage)


                }
            

            
//                    if let picReturn = resultPayload["picsToReturn"] as? [[String:Any]]!{
//
//                        for data in picReturn {
//
//                            guard  let CompititionImage = Mapper<GaleryMapper>().map(JSON: data) else {
//                                continue}
//
//
//                            self.ComptitionPic.append(CompititionImage)
//                        }
                    
                        //print(picReturn)
                    
                    //}
                    
                    //print(resultPayload)
                    
                    
                }
                
            }
                }
             self.collectionView.reloadData()
                
        }
            }
            
            else {
                
                print("Internet Error")
                self.getRequest()
                
            }
        }

    }

    @IBAction func galeryActionSegment(_ sender: Any) {
        
        if galerySegment.selectedSegmentIndex == 0{
            topView.isHidden = false
            recognisionView.isHidden = true
            tipsView.isHidden = true
            termsView.isHidden = true

            recognisionView.isHidden = true

            tipsView.isHidden = true
            termsView.isHidden = true
            self.navigationItem.title = "Competition Photos"

            self.navigationController?.navigationBar.titleTextAttributes =
                [NSForegroundColorAttributeName: UIColor.white,
                 NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 18)!]
        }
        if galerySegment.selectedSegmentIndex == 1 {
            recognisionView.isHidden = false
            topView.isHidden = true

            tipsView.isHidden = true
            termsView.isHidden = true

            self.navigationItem.title = "Recognition"
            
            self.navigationController?.navigationBar.titleTextAttributes =
                [NSForegroundColorAttributeName: UIColor.white,
                 NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 18)!]
        }
        if galerySegment.selectedSegmentIndex == 2 {
            recognisionView.isHidden = true
            tipsView.isHidden = false
            topView.isHidden = true

            termsView.isHidden = true

            self.navigationItem.title = "Tips"


            self.navigationController?.navigationBar.titleTextAttributes =
                [NSForegroundColorAttributeName: UIColor.white,
                 NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 18)!]
        }
        if galerySegment.selectedSegmentIndex == 3 {
            recognisionView.isHidden = true
            tipsView.isHidden = true
            termsView.isHidden = false
            topView.isHidden = true

            self.navigationItem.title = "Terms"

            self.navigationController?.navigationBar.titleTextAttributes =
                [NSForegroundColorAttributeName: UIColor.white,
                 NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 18)!]
        }
        
    }
    
    func hiddenFloatingButtons(){
        
        uploadButton.isHidden = true
        achivementButton.isHidden = true
        profileButton.isHidden = true
        notificationButton.isHidden = true
        homeButton.isHidden = true
        searchButton.isHidden = true
        
        
    }
    
    @IBAction func segmentControlAction(_ sender: Any) {
        
        
        switch galerySegment.selectedSegmentIndex {
        
        case 0:
            
            recognisionView.isHidden = true
            tipsView.isHidden = true
            termsView.isHidden = true
            hiddenFloatingButtons()
        case 1:
            recognisionView.isHidden = false
            tipsView.isHidden = true
            termsView.isHidden = true
            hiddenFloatingButtons()
        case 2:
            tipsView.isHidden = true
            recognisionView.isHidden = true
            termsView.isHidden = false
            hiddenFloatingButtons()


        case 3:
            termsView.isHidden = true
            tipsView.isHidden = false
            recognisionView.isHidden = true
            hiddenFloatingButtons()
            
        default:
        ()
        
    }
        
        
        
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        //        navigationController?.isNavigationBarHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //        navigationController?.isNavigationBarHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    

    func animatingPopUp()
    {
        UIView.animate(withDuration: 1, animations: {
            self.uploadButton.isHidden =  !self.uploadButton.isHidden
            
        }) { (true) in
            
            UIView.animate(withDuration: 1, animations: {
                self.achivementButton.isHidden = !self.achivementButton.isHidden
            }, completion: { (true) in
                UIView.animate(withDuration: 1, animations: {
                    
                    self.profileButton.isHidden = !self.profileButton.isHidden
                    self.searchButton.isHidden = !self.searchButton.isHidden

                }, completion: { (true) in
                    self.notificationButton.isHidden = !self.notificationButton.isHidden
                    
                    self.homeButton.isHidden = !self.homeButton.isHidden
                    
                    //                    let image1 = UIImage(named: "Circleopen")
                    //                    self.floatingMainButton.setBackgroundImage(image1, for: UIControlState.normal)
                    
                    
                    
                    
                })
            })
        }
        
        
        
    }
    

    
    
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return ComptitionPic.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GaleryCollectionViewCell", for: indexPath) as! GaleryCollectionViewCell
        
        
        if ComptitionPic.count > 0 {
        
        let allphoto = ComptitionPic[indexPath.row]
        
        
        let url = NSURL(string: allphoto.picUrl)
        
       // cell.imgImage.af_imageDownloader?.download([url as! URLRequestConvertible])
        cell.imgImage.sd_setShowActivityIndicatorView(true)
        cell.imgImage.sd_setIndicatorStyle(.gray)
        cell.imgImage.sd_setImage(with: url! as URL, placeholderImage: nil, options: .scaleDownLargeImages, completed: nil)

        hiddenFloatingButtons()
        }
        else {
            
            print("Error")
        }
//        if indexPath.row == ComptitionPic.count - 1 {
//            // last cell
//
////            if totalItems > ComptitionPic.count {
////                pageNumber = pageNumber + 1
////                print(pageNumber)
////                // more items to fetch
////                getRequest() // increment `fromIndex` by 20 before server call
////            }
//        }
        
        
        return cell
    }
  
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 3 - 1
        
        return CGSize(width: width, height: width)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
//    func close() {
//
//
//
//        IJProgressView.shared.hideProgressView()
//        hidingView.isHidden = true
//
//    }
//
////    func setCloseTimer() {
////
////
////        _ = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(close), userInfo: nil, repeats: false)
////    }
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let offsetY = scrollView.contentOffset.y
//        let contentHeight = scrollView.contentSize.height
//        
//        if offsetY > contentHeight - scrollView.frame.size.height {
//            hiddenFloatingButtons()
//            
//            pageNumber += 1
//            getRequest()
//            self.collectionView.reloadData()
////            hidingView.isHidden = false
////
////           IJProgressView.shared.showProgressView(view)
////
////           setCloseTimer()
//        }
//    }
    
//    func close() {
//        IJProgressView.shared.hideProgressView()
//    }
//
//    func setCloseTimer() {
//        _ = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(close), userInfo: nil, repeats: false)
//    }
    
    @IBAction func leftFabButtonAction(_ sender: Any) {
        
        
        animatingPopUp()
        //self.navigationController?.isNavigationBarHidden = false

        
        
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GaleryCollectionViewCell", for: indexPath) as! GaleryCollectionViewCell
        
        
        if ComptitionPic.count > 0 {
            
            let allphoto = ComptitionPic[indexPath.row]
            //des = allphoto.description
        }
        
        
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "CompetitionPicFullVC") as! CompetitionPicFullVC
        

        passData.indexNumber = CGFloat(indexPath.row)
       // passData.seeDescription = des
        
        self.navigationController?.pushViewController(passData, animated: true)
        
        
        
    }
    

    
    
}

