//
//  CommentPostVC.swift
//  Glostars
//
//  Created by Sanzid iOS on 9/1/19.
//  Copyright © 2019 Sanzid Ashan. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireImage
import SDWebImage
import YPImagePicker
import Foundation
import ISEmojiView


class CommentPostVC: UIViewController,UITableViewDataSource,UITableViewDelegate ,UITextViewDelegate, EmojiViewDelegate{
    var commentIdCell = ""
    
    @IBOutlet var commentsImageView: UIImageView!
    @IBOutlet var commentText: UITextView!
    
    @IBOutlet weak var commentView: UIView!
    
    @IBOutlet var commentTextVie: UITextView!
    
    @IBOutlet var tableViewComment: UITableView!
    
    var CommentsPic : Array<DetailComMapper> = []
    
    var photoId = 0
    var getID = 0
    var getImage = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
    self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        getImageInfo()
        
        let imageUrl = NSURL(string: self.getImage)
        self.commentsImageView.sd_setImage(with: imageUrl! as URL, completed: nil)

        // Do any additional setup after loading the view.
        self.title = "Comments"
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.white,
             NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 18)!]
        
        commentView.layer.borderWidth = 1.0
        commentView.layer.borderColor = UIColor.black.cgColor
        commentView.layer.cornerRadius = 8.0
        
        commentTextVie.delegate = self
        commentTextVie.text! = "Write a comment"
        commentTextVie.textColor = UIColor.lightGray
        
    }
    
    
    @IBOutlet private weak var emojiView: EmojiView! {
        didSet {
            emojiView.delegate = self
        }
    }
    func getImageInfo (){
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/picture/\(getID)"
        let headers: HTTPHeaders = [
            
            "Authorization": tokenType + accessToken ,
            "Content-Type": "application/json",
            
            ]
        
        Alamofire.request(url, method:.get,encoding:JSONEncoding.default,headers:headers).responseJSON { response in
            
            
            if response.result.value != nil {
                
                if let jsonRoot = response.result.value as? [String:Any]!{
                    
                    print(jsonRoot)
                    
                    
                    if let ResultPayload = jsonRoot!["resultPayload"] as? [String:Any]!{
                        
                        //print(ResultPayload)
                        
                        if let comments = ResultPayload!["comments"] as! [[String: Any]]!{
                            
                            //print(comments)
                            
                            
                            
                            for array in comments{
                                
                                print(array)
                                
                                guard let allcomments = Mapper<DetailComMapper>().map(JSON: array) else{
                                    
                                    continue
                                }
                                
                            self.CommentsPic.append(allcomments)
                                print(self.CommentsPic)
                                self.CommentsPic.reverse()
                                
                            }
                            
                            
                        }
                        
                        
                        if let poster = ResultPayload!["poster"] as? [String:Any]!{
                            
                            
                        }
                        
                    }
                    
                    
                }
                self.tableViewComment.reloadData()
            }
            else {
                
                print("Internet error")
                self.getImageInfo()
                
            }
            
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let Cell = tableViewComment.dequeueReusableCell(withIdentifier: "commentCell", for: indexPath) as! CommentCell
            
            Cell.selectionStyle = .none
        
            
            Cell.commentMsg.isEditable = false
            
            Cell.commentMsg.translatesAutoresizingMaskIntoConstraints = false
            Cell.commentMsg.isScrollEnabled = false
            tableViewComment.separatorColor = UIColor.clear
            
            let allComments = self.CommentsPic[indexPath.row]
            
            let urlImage = NSURL(string: allComments.userproPic )
            Cell.backView.layer.cornerRadius = 10.0;
        Cell.fontView.roundCorners([.topRight, .bottomRight], radius: 10)
            
            Cell.commentMsg.text = allComments.comments
            Cell.userImage.sd_setImage(with: urlImage! as URL, placeholderImage: nil , options: .scaleDownLargeImages, completed: nil)
            Cell.username.text = " " + allComments.userName + " " + allComments.lastName
            
            
            
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            if let date = dateFormatter.date(from: allComments.uploadTime ){
                
                Cell.uploadTime.text = timeAgoSinceDate(date, currentDate: Date(), numericDates: true)
            }
            
            //Cell.userLastName.text = allComments.lastName
            
            Cell.onProfileTapped = {
                
                let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                passData.getid = allComments.commenterId
                self.navigationController?.pushViewController(passData, animated: true)
                
                
            }
            Cell.onProfileTapped1 = {
                
                self.commentIdCell = String(allComments.commentId)
                
            }
            
            
            
            return Cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        self.BackButtonClick("Delete Comment", message: "Are you sure want to delete this comment?")
        

        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CommentsPic.count
    }
    
    
    
    @IBAction func emoButtonPressed(_ sender: Any) {
        
        if commentTextVie.text! == "Write a comment"{
            commentTextVie.text! = ""
        }
        let keyboardSettings = KeyboardSettings(bottomType: BottomType(rawValue: bottomType.hashValue) ?? BottomType(rawValue: 1)!)
        //keyboardSettings.customEmojis = emojis
        keyboardSettings.needToShowAbcButton = true
        
        let emojiView = EmojiView(keyboardSettings: keyboardSettings)
        emojiView.translatesAutoresizingMaskIntoConstraints = false
        emojiView.delegate = self
        commentTextVie.inputView = emojiView
        commentTextVie.becomeFirstResponder()
    }

    
    func BackButtonClick(_ title:String,message:String){
        
        let alertController = UIAlertController(title: title, message: message , preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            print("OK Pressed")
            
            let DeletePicUrl = "https://testglostarsdevelopers.azurewebsites.net/api/images/delete/81"
            print(self.photoId)
            Alamofire.request(DeletePicUrl, method: .post).responseJSON { response in
                
                switch response.result {
                    
                case .success:
                    print(response)
                    self.CommentsPic.removeAll()
                    self.getImageInfo()
                case .failure(let error):
                    print(error)
                }
                
            }
            
            
        }
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
            print("Cancel Pressed")
            //self.dismiss(animated: true, completion: nil)
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }

    @IBAction func sendCommentAction(_ sender: Any) {
        if self.commentTextVie.text != "" {
            if self.commentTextVie.text != "Write a comment"{
        
                let parameters: [String : Any]? = [
                    "CommentText": commentTextVie.text!,
                    "photoId": getID ,
                    ]
                let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/comment"
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json"
                ]
                Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
                    switch response.result {
                    case .success:
                        print(response)
                        self.CommentsPic.removeAll()
                        self.getImageInfo()
                    self.commentTextVie.resignFirstResponder()
                    case .failure(let error):
                        print(error)
                }
            }
        }
    }
}
//    func textViewDidBeginEditing(_ textView: UITextView) {
//
//        commentTextVie.text = ""
//        commentText.textColor = UIColor.black
//    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        commentTextVie.becomeFirstResponder()
        
        if commentTextVie.textColor! == UIColor.lightGray {
            commentTextVie.text! = ""
            commentTextVie.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if commentTextVie.text == "" {
            commentTextVie.text! = "Write a comment"
            commentTextVie.textColor = UIColor.lightGray
            commentTextVie.resignFirstResponder()
        }
            
        else {
            commentTextVie.text = "Write a comment"
            commentTextVie.textColor = UIColor.lightGray
            commentTextVie.resignFirstResponder()
        }
    }
    


    func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!)years"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!)month"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!)w"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1w"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!)d"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1d"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!)h"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1h"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 8) {
            return "\(components.minute!)m"
        } else if (components.minute! >= 8){
            if (numericDates){
                return "1m"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 0) {
            return "Just now"
        } else {
            return "Just now"
        }
        
    }

    
    func emojiViewDidSelectEmoji(_ emoji: String, emojiView: EmojiView) {
        commentTextVie.text = ""
        commentTextVie.insertText(emoji)
        print(commentTextVie.text!)
    }
    
    func emojiViewDidPressChangeKeyboardButton(_ emojiView: EmojiView) {
        commentTextVie.inputView = nil
        commentTextVie.keyboardType = .default
        commentTextVie.reloadInputViews()
    }
    
    func emojiViewDidPressDeleteBackwardButton(_ emojiView: EmojiView) {
        commentTextVie.deleteBackward()
    }
    
    func emojiViewDidPressDismissKeyboardButton(_ emojiView: EmojiView) {
        commentTextVie.resignFirstResponder()
    }
}
