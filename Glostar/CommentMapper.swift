//
//  CommentMapper.swift
//  Glostars
//
//  Created by Sanzid iOS on 5/22/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

//
//  InfoMapper.swift
//  Glostars
//
//  Created by Sanzid Ashan on 5/14/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//


import Foundation
import ObjectMapper

class CommentMapper : Mappable {
    
    
    
    dynamic var commentMessage : [String] = []
  
    dynamic var commentId : [Int] = [0]
    
    
    
    
    
    
    
    
    
    
    required convenience init?(map: Map) {
        
        
        self.init()
        
        
    }
    
    func mapping(map: Map) {
       
        commentMessage <- map["commentMessage"]
        
        commentId <- map["commentId"]

        
        
        
        
        
    }
}
