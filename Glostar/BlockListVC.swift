//
//  BlockListVC.swift
//  Glostars
//
//  Created by sadidur rahman on 23/1/19.
//  Copyright © 2019 Sanzid Ashan. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class BlockListVC: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var blockArray : Array< BlockMapper > = []
    var userIdB = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView(frame: .zero)
        
        self.navigationItem.title = "Block list"
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.white,
             NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 18)!]
        tableView.estimatedRowHeight = 75.0;
        tableView.rowHeight = UITableViewAutomaticDimension;
        gettingBlockList()
    }
    
   // https://testglostarsdevelopers.azurewebsites.net/api/Block/UnBlockUser?userId=6ff5815a-62a6-4f65-b7cc-b4916260a4b1
    
    func unblockUser(){
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/Block/UnBlockUser?userId=\(userIdB)"
        Alamofire.request(url, method: .get , encoding: JSONEncoding.default)
            
            
            .responseJSON{ response in
                print(response)
                
                if response.result.value != nil {
                    self.blockArray.removeAll()
                    self.gettingBlockList()

                    
                }
                else{
                    
                    print("No Data")
                    
                }
        }
        

    }
    
    
    func gettingBlockList () {
       // https://testglostarsdevelopers.azurewebsites.net/api/Block/BlockUserListById?userId=363ec52f-1739-4b9a-837b-79adcc90ba7b&page=0
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/Block/BlockUserListById?userId=\(userID)&page=0"
        Alamofire.request(url, method: .get , encoding: JSONEncoding.default)
            
            
            .responseJSON{ response in
                print(response)
                
                if response.result.value != nil {
                    
                    
                    if let jsonRoot = response.result.value as? [String:Any]!{
                        
                       // print(jsonRoot)
                        
                        
                        if let resultPayload = jsonRoot!["resultPayload"] as? [String : Any]{
                            
                            if let userBlockLish = resultPayload["userBlockList"] as? [[String : Any]]{
                                
                                print(userBlockLish)
                                for data in userBlockLish{
                                    
                                    guard let Notify = Mapper<BlockMapper>().map(JSON: data) else{
                                        
                                        continue
                                    }
                                    
                                    self.blockArray.append(Notify)
                                    
                                }
                                
                            }

                            
                            
                        }
                        
                    }
                    self.tableView.reloadData()
                }
                else{
                    
                    print("No Data")
                    self.gettingBlockList()
                    
                }
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return blockArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "BlockListCell", for: indexPath) as! BlockListCell
        let url = URL(string: blockArray[indexPath.row].profilemediumPath)
        tableViewCell.userImageView.sd_setImage(with: url, completed: nil)
        tableViewCell.userNameLabel.text = blockArray[indexPath.row].name
        
        tableViewCell.onUnFollowTapped = {
            self.userIdB = self.blockArray[indexPath.row].id
            self.unblockUser()
        }
        tableViewCell.onImageTapped = {
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            passData.getid = self.blockArray[indexPath.row].id
            self.navigationController?.pushViewController(passData, animated: true)
            
        }
        tableViewCell.onLabelTapped = {
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            passData.getid = self.blockArray[indexPath.row].id
            self.navigationController?.pushViewController(passData, animated: true)

        }
        
        
        return tableViewCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 75.0
    }
}
