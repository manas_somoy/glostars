//
//  CollectionViewCell.swift
//  MultiScrolling
//
//  Created by vikram singh rajpoot on 19/03/17.
//  Copyright © 2017 vikram singh rajpoot. All rights reserved.
//

import UIKit

class NewsFeedCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cellPhoto: UIImageView!
    @IBOutlet weak var proPicture: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var privacyPicture: UIImageView!
    @IBOutlet weak var uploadedTime: UILabel!
    @IBOutlet weak var elementsBackgroundButton: UIButton!
    @IBOutlet weak var starCount: UILabel!
    
    @IBOutlet weak var star1Outlet: UIButton!
    
    @IBOutlet weak var commentCount: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var commentButton: UIButton!
}
