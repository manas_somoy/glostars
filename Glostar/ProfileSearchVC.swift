//
//  ProfileVC.swift
//  Glostars
//
//  Created by Sanzid Ashan on 4/11/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
import ObjectMapper

class ProfileSearchVC: UIViewController, UIScrollViewDelegate, UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
//    @IBOutlet var collectionView1: UIView!
//    
//    @IBOutlet var collectionView2: UIView!
//    
//    @IBOutlet var collectionView3: UIView!
    
    @IBOutlet var mutualPicLabel: UILabel!
    @IBOutlet weak var achivementButton: UIButton!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var uploadButton: UIButton!
    
    @IBOutlet weak var floatingMainButton: UIButton!
    
    @IBOutlet var publicViewHight: NSLayoutConstraint!
    
    @IBOutlet var competitioViewHight: NSLayoutConstraint!
    
//    @IBOutlet var mutualViewHeight: NSLayoutConstraint!
    
    
    @IBOutlet var settingOutlet: UIButton!
    
    
    @IBOutlet var editButtonOutlet: UIButton!
    
    
    
    
    @IBOutlet weak var seeAllPub: UIButton!
    
    @IBOutlet weak var seeAllCom: UIButton!
    
    
    @IBOutlet weak var scrollView: UIView!
    @IBOutlet weak var recW: UILabel!
    @IBOutlet weak var collectionView2: UICollectionView!
    @IBOutlet weak var interest: UILabel!
    @IBOutlet weak var recM: UILabel!
    @IBOutlet weak var recE: UILabel!
    @IBOutlet weak var recG: UILabel!
    @IBOutlet weak var aboutMe: UILabel!
    @IBOutlet weak var background: UIScrollView!
    @IBOutlet weak var foreground: UIScrollView!
    
    
    @IBOutlet var relationButtonOutlet: UIButton!
    
    @IBOutlet weak var numFollowers: UILabel!
    @IBOutlet weak var numFlowing: UILabel!
    @IBOutlet weak var numPhoto: UILabel!
    @IBOutlet weak var userLocation: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var imageProfile: UIImageView!
    
    @IBOutlet var topConstraintsPublicPhotoLabel: NSLayoutConstraint!
    
    @IBOutlet var topConstraintsMutualPhotoLabel: NSLayoutConstraint!
    
    @IBOutlet weak var collectionView3: UICollectionView!
    @IBOutlet weak var collectionView1: UICollectionView!
    
    @IBOutlet weak var compititionViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var publicViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var mutualViewHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var lastName: UILabel!
    
    
    @IBOutlet weak var followingOutlet: UILabel!
    
    
    @IBOutlet weak var seeAllMut: UIButton!
    
    @IBOutlet weak var firstNameEditOutlet: UITextField!
    
    
    @IBOutlet weak var lastNameEditOutlet: UITextField!
    
    
    @IBOutlet weak var descriptionEditNameOutlet: UITextField!
    
    @IBOutlet weak var popUpViewEdit: UIView!
    
    @IBOutlet var editProfileOutlet: DesignableButton!
    
    @IBOutlet weak var interestEditOutlet: UITextField!
    
    @IBOutlet weak var changePassEditOutlet: UITextField!
    
    @IBOutlet var countryChoose: DropMenuButton!
    
    @IBOutlet weak var confirmChangepassEditOulet: DesignableButton!
    
    var is_mutual = Bool()
    var me_follow = Bool()
    var he_follow = Bool()
    
    var Compitition : Array<CompititionMapper> = []
    var PublicArray : Array<PublicMapper> = []
    var MutualArray : Array<MutualMapper> = []
    
    
    var getid = String()
    var getName = String()
    var getLastName = String()
    var getProfileUrl = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hiddenFloatingButtons()
        //settingOutlet.isHidden = true
        editProfileOutlet.isHidden = true

       // let urlString2 = URL(string: userPic)!
        
        if userPic == "/Content/Profile/Thumbs/ 1951 .jpg"{
            
            profileButton.setBackgroundImage(UIImage(named:"male"), for: .normal)
            
            
        }
            
            
        else{
            let urlString = URL(string:String(describing: UserDefaults.standard.value(forKey: "pic")!))
            profileButton.af_setBackgroundImage(for: [] , url: urlString! )
            
        }
        
        if userID == getid{
            
            relationButtonOutlet.isHidden = true
            
            editProfileOutlet.isHidden = false
            
            //settingOutlet.isHidden = false
        }
        popUpViewEdit.isHidden = true
        
        //profileButton.af_setBackgroundImage(for: [] , url: urlString2 )
        foreground.delegate = self
        
        //let urlString = URL(string: getProfileUrl)!
        
        print(getid)
        
        getUserInfo()
        
        userPicInfo()
        
        gettingNotification()
        
        
       // imageProfile.af_setImage(withURL: urlString, placeholderImage: #imageLiteral(resourceName: "ImagePlace"), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .crossDissolve(0.5), runImageTransitionIfCached: true, completion: nil)
        
         imageProfile.setRounded()
        
     

        
  

        
        //userName.text = getName
        //lastName.text = getLastName
        
        
        // Do any additional setup after loading the view.
    }
    
    
    func hiddenFloatingButtons(){
        
        uploadButton.isHidden = true
        achivementButton.isHidden = true
        profileButton.isHidden = true
        notificationButton.isHidden = true
        homeButton.isHidden = true
        
        
    }
    
    @IBAction func cancellButtonAction(_ sender: Any) {
        
        
        popUpViewEdit.isHidden = true
        
        
        
    }
    @IBAction func profileEditOnClick(_ sender: Any) {
        
        popUpViewEdit.alpha = 1
        if popUpViewEdit.isHidden {
            popUpViewEdit.isHidden = false
        } else {
            popUpViewEdit.isHidden = true
        }
        
    }
    func animatingPopUp()
    {
        UIView.animate(withDuration: 1, animations: {
            self.uploadButton.isHidden =  !self.uploadButton.isHidden
            
        }) { (true) in
            
            UIView.animate(withDuration: 1, animations: {
                self.achivementButton.isHidden = !self.achivementButton.isHidden
                
            }, completion: { (true) in
                UIView.animate(withDuration: 1, animations: {
                    
                    self.profileButton.isHidden = !self.profileButton.isHidden
                    self.homeButton.isHidden = !self.homeButton.isHidden
                    
                }, completion: { (true) in
                    self.notificationButton.isHidden = !self.notificationButton.isHidden
                    //                    let image1 = UIImage(named: "Circleopen")
                    //                    self.floatingMainButton.setBackgroundImage(image1, for: UIControlState.normal)
                    
                    
                    
                    
                })
            })
        }
        
        
        
    }
    

    @IBAction func sendUserId(_ sender: Any) {
        
        
//        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        let passData = MainStoryboard.instantiateViewController(withIdentifier: "FollowerNewVC") as! FollowerNewVC
//        
//        
//        passData.getNewUserId = getid
//        passData.getUserName = getName
//        
//        
//        self.navigationController?.pushViewController(passData, animated: true)
        
        
    }
    
    @IBAction func mainButtonAction(_ sender: Any) {
        
        
        animatingPopUp()
        
    }
    
    func gettingNotification () {
        
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/account/GetUserFollowById?userId=\(getid)"
        Alamofire.request(url, method: .get , encoding: JSONEncoding.default)
            
            
            .responseJSON{ response in
                
                
                
                if let jsonRoot = response.result.value as? [String:Any]!{
                    
                    if let resultPayload = jsonRoot!["resultPayload"] as? [String : Any]{
                        
                        if let actNotify = resultPayload["followerList"] as? [[String : Any]] {
                            
                            self.followingOutlet.text = String(actNotify.count) + " Follower"
                            
                        }
                        
                       
                        
                        
                        
                    }
                    
                }
        }
        
        
        
    }

    
    @IBAction func onFollowClick(_ sender: Any) {
        
        if is_mutual == true  {
        
        let unfollowUrl = "https://testglostarsdevelopers.azurewebsites.net/api/Follower/Unfollowing?id=\(getid)"
        Alamofire.request(unfollowUrl, method: .post).responseJSON { response in
            
            print(response)
            
            self.getUserInfo()
            
            }
        }
        else if me_follow == true {
            
            let unfollowUrl = "https://testglostarsdevelopers.azurewebsites.net/api/Follower/Unfollowing?id=\(getid)"
            Alamofire.request(unfollowUrl, method: .post).responseJSON { response in
                
                print(response)
                
                self.getUserInfo()
                
            }
            
        }
        else {
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/Follower/Following?id=\(getid)"
        
        print(getid)
        
        Alamofire.request(url, method: .post).responseJSON { response in
            
            
            
            
            print(response)
            
            self.getUserInfo()
            
            
            
            
            
            
            
        }
        }
        

        
        
        
        
    }
    
    func getUserInfo(){
        
        
        let params : Parameters = [
            "userId" : getid,
            
            
            
            ]
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/account/GetUserDetails?"
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default)
            
            
            .responseJSON{ response in
                
                
                
                if let jsonRoot = response.result.value as? [String:Any]!{
                    
                    if let userinfo = jsonRoot!["resultPayload"] as? [String: Any]!{
                        print(userinfo)
                        if userinfo!["location"] as? String  == nil{
                            
                            self.userLocation.text! = ""
                        }
                        
                        
                        if userinfo!["location"] as? String  != nil && userinfo!["country"] as? String  != nil {
                            self.userLocation.text! = ((userinfo!["location"] as? String)! + "," + (userinfo!["country"] as? String)!)
                            self.aboutMe.text = userinfo!["aboutMe"] as? String
                            self.interest.text = userinfo!["interests"] as? String
                        }
                        
                        else {
                            
                            self.userLocation.text! = ""
                            
                        }
                      
                        
                        let urlString = URL(string: (userinfo!["profilePicURL"] as? String)! )!

                        if let recogprofile = userinfo!["recogprofile"] as? [String: Any]!{
                            
                            print(recogprofile)
                            self.recW.text = String(describing: recogprofile!["grand"] as? Int)
                            self.recM.text = String(describing: recogprofile!["exhibition"] as? Int)
                            self.recG.text = String(describing: recogprofile!["monthly"] as? Int)
                            self.recE.text = String(describing: recogprofile!["weekly"] as? Int)
                            
                            //print(recogprofile)
                            
                        
                         self.imageProfile.af_setImage(withURL: urlString, placeholderImage: #imageLiteral(resourceName: "ImagePlace"), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .crossDissolve(0.5), runImageTransitionIfCached: true, completion: nil)
                            self.userName.text = userinfo!["name"] as? String
                            self.lastName.text = userinfo!["lastName"] as? String
                            self.navigationItem.title = (userinfo!["name"] as? String)! + "s Profile"
                        self.navigationController?.navigationBar.titleTextAttributes =
                            [NSForegroundColorAttributeName: UIColor.white,
                             NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 20)!]
                        
                        
                            self.is_mutual = (userinfo!["isMutual"] as? Bool)!
                            self.me_follow = (userinfo!["meFollow"] as? Bool)!
                            self.he_follow = (userinfo!["heFollow"] as? Bool)!
                        
                        
                        if self.is_mutual == true {
                            
                            
                            self.collectionView3.isHidden = false
                            
                        }
                        else{
                            
                            
                            self.collectionView3.isHidden = true
                            self.mutualPicLabel.isHidden = true
                            
                            
                        }
                        
                        
                        if self.is_mutual == true {
                            
                            self.relationButtonOutlet.setTitle("Mutual", for: .normal)
                            self.relationButtonOutlet.backgroundColor = UIColor(hex: 0x660066, alpha: 1)
                            
                        }
                        
                        
                        if self.is_mutual == false && self.me_follow == false && self.he_follow == false {
                            
                            self.relationButtonOutlet.setTitle("Follow", for: .normal)
                            
                             self.relationButtonOutlet.backgroundColor = UIColor(hex: 0x4d94ff, alpha: 1)
                        }
                        
                        if self.is_mutual == false && self.me_follow == true && self.he_follow == false {
                            
                            self.relationButtonOutlet.setTitle("Following", for: .normal)
                            self.relationButtonOutlet.backgroundColor = UIColor(hex: 0xcc99ff, alpha: 1)
                            
                        }
                        if self.is_mutual == false && self.me_follow == false && self.he_follow == true {
                            
                            self.relationButtonOutlet.setTitle("Follow Back", for: .normal)
                            self.relationButtonOutlet.backgroundColor = UIColor.gray
                            
                        }
                        
                        print(userinfo)
                        
                            self.followingOutlet.text! = String(describing: userinfo!["followersCount"] as! Int) + " Follower"
                        
                       
                    }
                    
                    
                }
                }
        }
        
        
    }
    
    
    @IBAction func compititionSeeAll(_ sender: Any) {
        
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileSearchCompitition") as! ProfileSearchCompitition
        
        
        passData.getSearchUserid = getid
      
        passData.getTheName = getName
        
        self.navigationController?.pushViewController(passData, animated: true)
        
        
        
        
    }
    
    @IBAction func publicImageAll(_ sender: Any) {
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileSearchPublic") as! ProfileSearchPublic
        
        
        passData.getSearchUserid = getid
        
        passData.getTheName = getName
        
        self.navigationController?.pushViewController(passData, animated: true)
        
        
    }
    
    @IBAction func mutualImageAll(_ sender: Any) {
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileSearchMutual") as! ProfileSearchMutual
        
        
        passData.getSearchUserid = getid
        
        passData.getTheName = getName
        
        self.navigationController?.pushViewController(passData, animated: true)
        
        
        
    }
    
    
    
    func userPicInfo(){
        
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/user/\(getid)/1"
        Alamofire.request(url, method: .get,encoding: URLEncoding.default)
            
            
            .responseJSON{ response in
                
                
                
                if let jsonRoot = response.result.value as? [String:Any]!{
                    print(jsonRoot)
                    
                    if let resultPaylaod = jsonRoot!["resultPayload"] as? [String:Any]!{
                        
                        print(resultPaylaod)
                        self.numPhoto.text = String(describing: resultPaylaod!["allPictureCount"] as! Int)
                        
                        
                        if let model = resultPaylaod!["model"] as? [String: Any]{
                            
                            if let mutPic = model["mutualFollowerPictures"] as! [[String: Any]]! {
                                
                                for data in mutPic{
                                    
                                    guard let mutpics = Mapper<MutualMapper>().map(JSON: data) else{
                                        
                                        continue
                                    }
                                    
                                    self.MutualArray.append(mutpics)
                                    //print(data)
                                    self.collectionView3.reloadData()
                                    
                                }
                                
                                
                                
                            }
                            
                            if let pubPic = model["publicPictures"] as! [[String: Any]]! {
                                
                                for data in pubPic{
                                    
                                    guard let pubpics = Mapper<PublicMapper>().map(JSON: data) else{
                                        
                                        continue
                                    }
                                    
                                    self.PublicArray.append(pubpics)
                                    self.collectionView2.reloadData()
                                    
                                }
                                
                            }
                            
                            if let comPic = model["competitionPictures"] as! [[String: Any]]! {
                                
                                
                                for data in comPic{
                                    
                                    guard let pics = Mapper<CompititionMapper>().map(JSON: data) else{
                                        
                                        continue
                                    }
                                    
                                    self.Compitition.append(pics)
                                    self.collectionView1.reloadData()
                                    
                                }
                                
                                
                                
                                
                                
                                
                            }
                        }
                    }
                }
                
                
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let foregroundHeight = foreground.contentSize.height - foreground.bounds.height
        let percentageScroll = foreground.contentOffset.y / foregroundHeight
        let backgroundHeight = background.contentSize.height - background.bounds.height
        
        background.contentOffset = CGPoint(x: 0, y: backgroundHeight * percentageScroll)
        hiddenFloatingButtons()

    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.collectionView1 {
            
         
            
            if Compitition.count <= 9{
                
                seeAllCom.isHidden = true
                
                
            }
            
            if Compitition.count > 9 {
                
                seeAllCom.isHidden = false
            }
            if Compitition.count <= 6{
                
//                collectionView1.frame.size.height = 150.0
//                topConstraintsPublicPhotoLabel.constant = -90
//                topConstraintsMutualPhotoLabel.constant = -230
                
            }
            if Compitition.count <= 3{
                
//                collectionView1.frame.size.height = 100.0
//                topConstraintsPublicPhotoLabel.constant = -150
//                topConstraintsMutualPhotoLabel.constant = -290
                
                
            }
            if Compitition.count > 9{
                
//                collectionView1.frame.size.height = 195.0
//                topConstraintsPublicPhotoLabel.constant = 0
//                topConstraintsMutualPhotoLabel.constant = 0

                
            }
            return Compitition.count
            
            
            
        }
        else if collectionView == self.collectionView3{
            if MutualArray.count <= 9{
                
                seeAllMut.isHidden = true
                
                
            }
            
            if MutualArray.count > 9 {
                
                seeAllMut.isHidden = false
            }
            if MutualArray.count <= 6{
                
//                collectionView.frame.size.height = 100.0
//                topConstraintsPublicPhotoLabel.constant = -90
//                topConstraintsMutualPhotoLabel.constant = -230
                
            }
            if MutualArray.count <= 3{
                
//                collectionView.frame.size.height = 100.0
//                topConstraintsPublicPhotoLabel.constant = -130
//                topConstraintsMutualPhotoLabel.constant = -230
                
                
            }
            return MutualArray.count
        }
        else {
            
            if PublicArray.count <= 9{
                
                seeAllPub.isHidden = true
                
                
            }
            
            if PublicArray.count > 9 {
                
                seeAllPub.isHidden = false
            }
            if PublicArray.count <= 6{
                
//                collectionView.frame.size.height = 150.0
//                topConstraintsMutualPhotoLabel.constant = -230
                
            }
            if PublicArray.count <= 3{
                
//                collectionView.frame.size.height = 100.0
//                topConstraintsMutualPhotoLabel.constant = -305
                
                
            }
            if PublicArray.count <= 9{
                
//                collectionView.frame.size.height = 180.0
//                topConstraintsMutualPhotoLabel.constant = 0.0
                
                
            }

            
            return PublicArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.collectionView1 {
            
            let cell : CompititionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell1", for: indexPath) as! CompititionCell
            
            let comInfomation = self.Compitition[indexPath.row]
            
            let url = NSURL(string: comInfomation.picUrl)
            
            cell.compitiotionPic.af_setImage(withURL: url! as URL, placeholderImage: UIImage(named:"loadingdots"), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .crossDissolve(0.5), runImageTransitionIfCached: true, completion: nil)
            hiddenFloatingButtons()
            
            if Compitition.count == 0 && MutualArray.count == 0 && PublicArray.count == 0 {
                
                cell.compitiotionPic.image = UIImage(named: "Icon_blank")
                
                
            }

            return cell
        }
        else if collectionView == self.collectionView3 {
            
            let cell : MutualCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell3", for: indexPath) as! MutualCell
            
            let mutInfomation = self.MutualArray[indexPath.row]
            
            let url2 = NSURL(string: mutInfomation.picUrl)
            
            cell.mutImage.af_setImage(withURL: url2! as URL, placeholderImage: UIImage(named:"loadingdots"), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .crossDissolve(0.5), runImageTransitionIfCached: true, completion: nil)
            hiddenFloatingButtons()

            return cell
        }
        else{
            
            let cell : PublicCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell2", for: indexPath) as! PublicCell
            let PublicInfo = self.PublicArray[indexPath.row]
            let url3 = NSURL(string: PublicInfo.picUrl)
            cell.publicImg.af_setImage(withURL: url3! as URL, placeholderImage: UIImage(named:"loadingdots"), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .crossDissolve(0.5), runImageTransitionIfCached: true, completion: nil)
            hiddenFloatingButtons()

            return cell
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 3 - 1
        let height = CGFloat(201.0)
        return CGSize(width: width, height: height / 3  )
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
       
        if collectionView == self.collectionView1{
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "NewsFeedDetailsVC") as! NewsFeedDetailsVC
        
        
        passData.getImageUrl = Compitition[indexPath.row].picUrl
        passData.getID = Compitition[indexPath.row].photoId
        
        
        self.navigationController?.pushViewController(passData, animated: true)
        }
        
        else if collectionView == self.collectionView3{
            
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "NewsFeedDetailsVC") as! NewsFeedDetailsVC
            
            
            passData.getImageUrl = MutualArray[indexPath.row].picUrl
            passData.getID = MutualArray[indexPath.row].PhotoId
            
            
            self.navigationController?.pushViewController(passData, animated: true)
            
        }
        else{
            
            
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "NewsFeedDetailsVC") as! NewsFeedDetailsVC
            
            
            passData.getImageUrl = PublicArray[indexPath.row].picUrl
            passData.getID = PublicArray[indexPath.row].photoId
            
            
            self.navigationController?.pushViewController(passData, animated: true)
            
        }
        //hiddenFloatingButtons()
        
        
        
    }
   
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
