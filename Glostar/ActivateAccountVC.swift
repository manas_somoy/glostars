//
//  ActivateAccountVC.swift
//  Glostars
//
//  Created by Sanzid iOS on 12/24/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftMessages

class ActivateAccountVC: UIViewController {

    @IBOutlet var popUpInputTextField: DesignableTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        self.successMessage(title: "Please Wait", subtitle: "Sending You The Activation Token")

        // Do any additional setup after loading the view.
        
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe:)))
        rightSwipe.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(rightSwipe)
        
    }
 
    func warningMessage(title:String,subtitle:String){
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: .messageView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.warning)
        view.button?.isHidden = true
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        // Set message title, body, and icon. Here, we're overriding the default warning
        // image with an emoji character.
        let iconText = ["🤔", "😳", "🙄", "😶"].sm_random()!
        view.configureContent(title: title, body: subtitle, iconText: iconText)
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        
        // Show the message.
        SwiftMessages.show(view: view)
        
    }
    func successMessage(title:String,subtitle:String){
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: .messageView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.success)
        view.button?.isHidden = true
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        
        // Set message title, body, and icon. Here, we're overriding the default warning
        // image with an emoji character.
        let iconText = ["🤔", "😳", "🙄", "😶"].sm_random()!
        view.configureContent(title: title, body: subtitle, iconText: iconText)
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        
        SwiftMessages.show(view: view)
        
    }

    func swipeAction(swipe:UISwipeGestureRecognizer)
    {
        
        //dismiss(animated: true, completion: nil)
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        
        let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "RegisterVC")
        
        let nvc = UINavigationController(rootViewController: nextViewController)
        present(nvc, animated: false, completion: nil)
    }
    
    @IBAction func submitPopUpAction(_ sender: Any) {
        
        let interestProfileVC = self.storyboard!.instantiateViewController(withIdentifier: "InterestProfileVC")
        
        let nvc = UINavigationController(rootViewController: interestProfileVC)
        self.hideLoader()
        self.present(nvc, animated: false, completion: nil)
        
    
        if popUpInputTextField.text! == ""{
            
            warningMessage(title: "Empty Code", subtitle: "Please Enter Your Code")
        }
        else{
    
    let url = "https://testglostarsdevelopers.azurewebsites.net/Account/Confirmcode?userId=\(userRegisterID)&confirmCode=\(popUpInputTextField.text!)"
    
    Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: nil).responseJSON { response in
    
    switch response.result {
    
    case .success:
    //print(response)
    if let jsonRoot = response.result.value as? [String:Any]!{
    
    print(jsonRoot)
    
    if jsonRoot!["Msg"] as? String == "Wrong code"{
    
    print("Wrong Code ")
    //self.displayMyAlertMessage("Wrong Code")
   // SwiftMessageService.WarningMessage("Warning", description: "Wrong Code")
        self.warningMessage(title: "Please Wait", subtitle: "Preparing Glostars for you")

    
    
    }else{
        
        //SwiftMessageService.SuccessMessage("Please Wait ", description: "Preparing Glostars for you")
        self.successMessage(title: "Please Wait", subtitle: "Preparing Glostars for you")
        self.getRequest()
    
    
    
    }
    
    
    }

    
    
    
    
    case .failure(let error):
    print(error)
    
    
    }
    
    //print(response)
    
    }
    
    
    //        Alamofire.request(url, method:.get, parameters:parameter!,encoding: URLEncoding.httpBody).responseJSON { response in
    //
    //            if response.result.value != nil {
    //                switch response.result {
    //
    //                case .success:
    //                    print(response)
    //
    //
    //                case .failure(let error):
    //                    print(error)
    //                }
    //
    //
    //            }
    
    
    
        }
    
    }
    func displayMyAlertMessage(_ userMessage:String)
    {
        
        let myAlert = UIAlertController(title:"Warning", message:userMessage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title:"Ok", style:UIAlertActionStyle.default, handler:nil);
        
        myAlert.addAction(okAction);
        self.present(myAlert, animated: true, completion: nil);
        
        
    }
    
    func  getRequest(){
        
        print(UserDefaults.standard.value(forKey: "EmailSave")!)
        print(UserDefaults.standard.value(forKey: "PassSave")!)
        

        
        let params : Parameters = [
            "grant_type" : "password",
            "password" : UserDefaults.standard.value(forKey: "PassSave")!,
            "username" : UserDefaults.standard.value(forKey: "EmailSave")!,
            
            
            ]
        
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/Token"
        Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.httpBody)
            .responseJSON{ response in
                
                if response.result.value != nil{
                    
                    if let jsonRoot = response.result.value as? [String:Any]!{
                        
                        print(jsonRoot)
                        
                        if jsonRoot!["access_token"] != nil  && jsonRoot!["token_type"] != nil {
                            accessToken = jsonRoot!["access_token"] as! String!
                            UserDefaults.standard.set(accessToken, forKey: "access")
                            
                            tokenType = jsonRoot!["token_type"] as! String!
                            UserDefaults.standard.set(tokenType, forKey: "token")
                            self.getData()
                            //self.localStorage()
                            
                            
                        }
                        else {
                            
                            //self.displayMyAlertMessage("Wrong User Name or password ")
                           // SwiftMessageService.WarningMessage("Warning", description: "Wrong User Name or password ")

                        }
                        
                        
                        // self.localStorage()
                        
                        
                        
                    }
                    else {
                        
                       // self.displayMyAlertMessage(" Please Check Your Email and Password ")
                       // SwiftMessageService.WarningMessage("Warning", description: "Please Check Your Email and Password")

                    }
                }
                
                
        }
        
    }
    
    
    func  getData() {
        
        
        
        
        let params2 : Parameters = [
            
            "userEmail" : UserDefaults.standard.value(forKey: "EmailSave")!,
            
            ]
        
        let url2 = "https://testglostarsdevelopers.azurewebsites.net/api/account/GetUserInfo"
        
        Alamofire.request(url2, method: .get, parameters: params2, encoding: URLEncoding.default)
            
            
            .responseJSON{ response2 in
                
                if response2.result.value != nil {
                    
                    if let jsonRoot = response2.result.value as? [String:Any]!{
                        print(jsonRoot)
                        
                        if let userinfo = jsonRoot!["resultPayload"] as? [String: Any]!{
                            
                            
                            //gender = userinfo!["gender"] as! String
                           // print(gender)
                            
                            userID = userinfo!["userId"] as! String
                            print(userID)
                            UserDefaults.standard.set(userID, forKey: "id")
                            userPic = userinfo!["profilePicURL"] as! String
                            print(userPic)
                            UserDefaults.standard.set(userPic, forKey: "pic")
                            
                            userNameR = userinfo!["name"] as! String
                            
                            
                            
                            
                            
                            
                            
                            let transition = CATransition()
                            transition.duration = 0.3
                            transition.type = kCATransitionPush
                            transition.subtype = kCATransitionFromRight
                            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                            self.view.window!.layer.add(transition, forKey: kCATransition)
                            
                            
                            let interestProfileVC = self.storyboard!.instantiateViewController(withIdentifier: "InterestProfileVC")
                            self.navigationController?.pushViewController(interestProfileVC, animated: true)
                            
//                            let nvc = UINavigationController(rootViewController: interestProfileVC)
//                            self.hideLoader()
//                            self.present(nvc, animated: false, completion: nil)
                            
                            
//                            let nextViewController =                                  self.storyboard!.instantiateViewController(withIdentifier:"NewsVCViewController")
//
//                            let nvc = UINavigationController(rootViewController: nextViewController)
//                            self.present(nvc, animated: false, completion: nil)
//
                            
                            
                           
                            
                        }
                        
                        
                    }
                    else{
                        
                        //self.displayMyAlertMessage("Please Check Your Email and Password")
                        //SwiftMessageService.WarningMessage("Warning", description: "Please Check Your Email and Password")

                    }
                }
                
                
                
                
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
