//
//  InterestCell.swift
//  Glostars
//
//  Created by Pathao on 23/7/18.
//  Copyright © 2018 Pathao. All rights reserved.
//

import UIKit

class InterestCell: UICollectionViewCell {
    @IBOutlet weak var interestImageViewOutlet: UIImageView!
    @IBOutlet weak var interestLabelOutlet: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
