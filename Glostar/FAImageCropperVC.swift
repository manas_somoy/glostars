//
//  FAImageCropperVC.swift
//  FAImageCropper
//
//  Created by Fahid Attique on 11/02/2017.
//  Copyright © 2017 Fahid Attique. All rights reserved.
//

import UIKit
import Photos
import PhotosUI
import iOSPhotoEditor
import YPImagePicker

class FAImageCropperVC: UIViewController,PhotoEditorDelegate , UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    @IBOutlet weak var introView: UIView!
    @IBOutlet weak var galleryButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
     var imagePicker: UIImagePickerController!
    
    var uploadType = ""
    var uploadCom = ""
    func doneEditing(image: UIImage) {
        
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "ImageViewController") as! ImageViewController
        passData.getImageData = image
        passData.chooseCtegeory = uploadType
        passData.copetionPost = "false"
        
        self.navigationController?.pushViewController(passData, animated: true)
        
    }
    
    func canceledEditing() {
        
    }
    
    
    // MARK: IBOutlets
    
    @IBOutlet weak var scrollContainerView: UIView!
    @IBOutlet weak var scrollView: FAScrollView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnZoom: UIButton!
    //@IBOutlet weak var btnCrop: UIButton!
    @IBAction func zoom(_ sender: Any) {
        scrollView.zoom()
    }
    @IBAction func crop(_ sender: Any) {
        
        if uploadCom != "true"{
        let actionSheetController: UIAlertController = UIAlertController(title: "Choose Category", message: "Please Select Your Category for Uploading Photo", preferredStyle: .actionSheet)
        
        // create an action
        let firstAction: UIAlertAction = UIAlertAction(title: "Public", style: .default) { action -> Void in
            self.croppedImage = self.captureVisibleRect()
            
            //performSegue(withIdentifier: "FADetailViewSegue", sender: nil)
            let photoEditor = PhotoEditorViewController(nibName:"PhotoEditorViewController",bundle: Bundle(for: PhotoEditorViewController.self))
            
            //PhotoEditorDelegate
            photoEditor.photoEditorDelegate = self
            //The image to be edited
            photoEditor.image = self.croppedImage
            
            //Stickers that the user will choose from to add on the image
            for i in 0...17 {
                photoEditor.stickers.append(UIImage(named: i.description )!)
            }
            //Optional: To hide controls - array of enum control
            photoEditor.hiddenControls = [.crop, .share]
            
            //Optional: Colors for drawing and Text, If not set default values will be used
            //photoEditor.colors = [.red,.blue,.green]
            self.uploadType = "public"

            //Present the View Controller
            self.present(photoEditor, animated: true, completion: nil)
            
        }
        
        let secondAction: UIAlertAction = UIAlertAction(title: "Insider", style: .default) { action -> Void in
            self.croppedImage = self.captureVisibleRect()
            
            //performSegue(withIdentifier: "FADetailViewSegue", sender: nil)
            let photoEditor = PhotoEditorViewController(nibName:"PhotoEditorViewController",bundle: Bundle(for: PhotoEditorViewController.self))
            
            //PhotoEditorDelegate
            photoEditor.photoEditorDelegate = self
            //The image to be edited
            photoEditor.image = self.croppedImage
            
            //Stickers that the user will choose from to add on the image
            for i in 0...17 {
                photoEditor.stickers.append(UIImage(named: i.description )!)
            }
            //Optional: To hide controls - array of enum control
            photoEditor.hiddenControls = [.crop, .share]
            
            //Optional: Colors for drawing and Text, If not set default values will be used
            //photoEditor.colors = [.red,.blue,.green]
            self.uploadType = "friends"
            
            //Present the View Controller
            self.present(photoEditor, animated: true, completion: nil)
            
          
            
        }
        let thirdAction: UIAlertAction = UIAlertAction(title: "Competition", style: .default) { action -> Void in
            self.croppedImage = self.captureVisibleRect()
            
            //            print("Block User")
            let picker = YPImagePicker()
            //        //picker.onlySquareImages = true
            //         picker.showsFilters = false
            //        // picker.usesFrontCamera = true
            //        picker.showsVideo = false
            picker.didSelectImage = { img in
                
                let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let passData = MainStoryboard.instantiateViewController(withIdentifier: "ImageViewController") as! ImageViewController
                passData.getImageData = img
                passData.chooseCtegeory = "Competition"
                passData.copetionPost = "true"
                picker.dismiss(animated: true, completion: nil)
                self.navigationController?.pushViewController(passData, animated: true)
                
                
                
                
            }
            //        picker.didSelectVideo = { videoData in
            //            // video picked
            //        }
            picker.pickedImage = self.croppedImage!
            self.present(picker, animated: true, completion: nil)
   
            
        }
        
        
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
        
        // add actions
        actionSheetController.addAction(firstAction)
        actionSheetController.addAction(secondAction)
        //actionSheetController.addAction(thirdAction)
        actionSheetController.addAction(cancelAction)
        
        // present an actionSheet...
        self.present(actionSheetController, animated: true, completion: nil)
        }
        else {
            self.croppedImage = self.captureVisibleRect()
            let picker = YPImagePicker()
            //        //picker.onlySquareImages = true
            //         picker.showsFilters = false
            //        // picker.usesFrontCamera = true
            //        picker.showsVideo = false
            picker.didSelectImage = { img in
                
                let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let passData = MainStoryboard.instantiateViewController(withIdentifier: "ImageViewController") as! ImageViewController
                passData.newConpitition = "true"
                passData.getImageData = img
                passData.chooseCtegeory = "Competition"
                passData.copetionPost = "true"
                picker.dismiss(animated: true, completion: nil)
                self.navigationController?.pushViewController(passData, animated: true)
                
                
                
                
            }
            //        picker.didSelectVideo = { videoData in
            //            // video picked
            //        }
            picker.pickedImage = self.croppedImage!
            self.present(picker, animated: true, completion: nil)
            
        }
        
    }
    
    
    
    // MARK: Public Properties
    
    var photos:[PHAsset]!
    var imageViewToDrag: UIImageView!
    var indexPathOfImageViewToDrag: IndexPath!
    
    let cellWidth = ((UIScreen.main.bounds.size.width)/3)-1
    
    
    // MARK: Private Properties
    
    private let imageLoader = FAImageLoader()
    private var croppedImage: UIImage? = nil
    

    
    // MARK: LifeCycle
    
//    override func viewWillAppear(_ animated: Bool) {
//       // func viewDidAppear(animated: Bool) {
//            if UserDefaults.standard.bool(forKey: "firstLaunch") {
//                // Terms have been accepted, proceed as normal
//                introView.isHidden = false
//
//                let gesture = UITapGestureRecognizer(target: self, action: #selector(introViewTapped))
//                introView.isUserInteractionEnabled = true
//                introView.addGestureRecognizer(gesture)
//            } else {
//                // Terms have not been accepted. Show terms (perhaps using performSegueWithIdentifier)
//                introView.isHidden = true
//            }
//        }
//  //  }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
            if !UserDefaults.standard.bool(forKey: "firstLaunch") {
                UserDefaults.standard.set(false, forKey: "firstLaunch")
                 introView.isHidden = false
            }
        
        
        // Do any additional setup after loading the view, typically from a nib.\
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.title  = "Choose Image"
        viewConfigurations()
        checkForPhotosPermission()
        cameraButton.addLeftBorder(color: .gray, width: 2)
        
      //  let firstLaunch = FirstLaunch()
        
       // if firstLaunch.isFirstLaunch{
//            introView.isHidden = false
        
            let gesture = UITapGestureRecognizer(target: self, action: #selector(introViewTapped))
            introView.isUserInteractionEnabled = true
            introView.addGestureRecognizer(gesture)
     //   }
        
    }
    
    @objc func introViewTapped(_ sender: UITapGestureRecognizer){
        //-----------------------------------------------
        // Mark: second approach
        //-----------------------------------------------
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.introView.isHidden = true
           // UserDefaults.standard.set(true, forKey: "firstLaunch")
            UserDefaults.standard.set(true, forKey: "firstLaunch")

        })
        
    }
   
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.scrollView.imageToDisplay = image
        }
        picker.dismiss(animated: true, completion: nil);
    }
    
    @IBAction func galleryButtonPressed(_ sender: Any) {
       
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
//            let imagePicker = UIImagePickerController()
//            imagePicker.delegate = self
//            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
//            imagePicker.allowsEditing = true
           // self.present(imagePicker, animated: true, completion: nil)
            
            imagePicker =  UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            present(imagePicker, animated: true, completion: nil)

        }
    }
    
    
    @IBAction func cameraButtonPressed(_ sender: Any) {
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        present(imagePicker, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "FADetailViewSegue" {
            
            let detailVC = segue.destination as? FADetailVC
            detailVC?.croppedImage = croppedImage
        }
    }
    
    // MARK: Private Functions
    
    private func checkForPhotosPermission(){
        
        // Get the current authorization state.
        let status = PHPhotoLibrary.authorizationStatus()
        
        if (status == PHAuthorizationStatus.authorized) {
            // Access has been granted.
            loadPhotos()
        }
        else if (status == PHAuthorizationStatus.denied) {
            // Access has been denied.
        }
        else if (status == PHAuthorizationStatus.notDetermined) {
            
            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                
                if (newStatus == PHAuthorizationStatus.authorized) {
                    
                    DispatchQueue.main.async {
                        self.loadPhotos()
                    }
                }
                else {
                    // Access has been denied.
                }
            })
        }
            
        else if (status == PHAuthorizationStatus.restricted) {
            // Restricted access - normally won't happen.
        }
    }
    
    private func viewConfigurations() {
        
        navigationBarConfigurations()
        //btnCrop.layer.cornerRadius = btnCrop.frame.size.width/2
        btnZoom.layer.cornerRadius = btnZoom.frame.size.width/2
    }
    
    private func navigationBarConfigurations() {
        
        navigationController?.navigationBar.backgroundColor = UIColor.init(hex: "9a9a9a")
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.setBackgroundImage(UIImage(color: .gray), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage(color: .clear)
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
    }
    
    
    private func loadPhotos(){
        
        imageLoader.loadPhotos { (assets) in
            self.configureImageCropper(assets: assets)
        }
    }
    
    private func configureImageCropper(assets:[PHAsset]){
        
        if assets.count != 0{
            photos = assets
            collectionView.delegate = self
            collectionView.dataSource = self
            collectionView.reloadData()
            selectDefaultImage()
        }
    }
    
    private func selectDefaultImage(){
        collectionView.selectItem(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: .top)
        selectImageFromAssetAtIndex(index: 0)
    }
    
    
    private func captureVisibleRect() -> UIImage{
        
        var croprect = CGRect.zero
        let xOffset = (scrollView.imageToDisplay?.size.width)! / scrollView.contentSize.width;
        let yOffset = (scrollView.imageToDisplay?.size.height)! / scrollView.contentSize.height;
        
        croprect.origin.x = scrollView.contentOffset.x * xOffset;
        croprect.origin.y = scrollView.contentOffset.y * yOffset;
        
        let normalizedWidth = (scrollView?.frame.width)! / (scrollView?.contentSize.width)!
        let normalizedHeight = (scrollView?.frame.height)! / (scrollView?.contentSize.height)!
        
        croprect.size.width = scrollView.imageToDisplay!.size.width * normalizedWidth
        croprect.size.height = scrollView.imageToDisplay!.size.height * normalizedHeight
        
        let toCropImage = scrollView.imageView.image?.fixImageOrientation()
        let cr: CGImage? = toCropImage?.cgImage?.cropping(to: croprect)
        let cropped = UIImage(cgImage: cr!)
        
        return cropped
        
    }
    private func isSquareImage() -> Bool{
        let image = scrollView.imageToDisplay
        if image?.size.width == image?.size.height { return true }
        else { return false }
    }
    
    
    // MARK: Public Functions
    
    func selectImageFromAssetAtIndex(index:NSInteger){
        
        FAImageLoader.imageFrom(asset: photos[index], size: PHImageManagerMaximumSize) { (image) in
            DispatchQueue.main.async {
                self.displayImageInScrollView(image: image)
            }
        }
    }
    
    func displayImageInScrollView(image:UIImage){
        self.scrollView.imageToDisplay = image
        if isSquareImage() { btnZoom.isHidden = true }
        else { btnZoom.isHidden = false }
    }
    
    func replicate(_ image:UIImage) -> UIImage? {
        
        guard let cgImage = image.cgImage?.copy() else {
            return nil
        }
        
        return UIImage(cgImage: cgImage,
                       scale: image.scale,
                       orientation: image.imageOrientation)
    }
    
    
    func handleLongPressGesture(recognizer: UILongPressGestureRecognizer) {
        
        let location = recognizer.location(in: view)
        
        if recognizer.state == .began {
            
            let cell: FAImageCell = recognizer.view as! FAImageCell
            indexPathOfImageViewToDrag = collectionView.indexPath(for: cell)
            imageViewToDrag = UIImageView(image: replicate(cell.imageView.image!))
            imageViewToDrag.frame = CGRect(x: location.x - cellWidth/2, y: location.y - cellWidth/2, width: cellWidth, height: cellWidth)
            view.addSubview(imageViewToDrag!)
            view.bringSubview(toFront: imageViewToDrag!)
        }
        else if recognizer.state == .ended {
            
            if scrollView.frame.contains(location) {
                collectionView.selectItem(at: indexPathOfImageViewToDrag, animated: true, scrollPosition: UICollectionViewScrollPosition.centeredVertically)
                selectImageFromAssetAtIndex(index: indexPathOfImageViewToDrag.item)
            }
            
            imageViewToDrag.removeFromSuperview()
            imageViewToDrag = nil
            indexPathOfImageViewToDrag = nil
        }
        else{
            imageViewToDrag.center = location
        }
    }
}





extension FAImageCropperVC:UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell:FAImageCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FAImageCell", for: indexPath) as! FAImageCell
        cell.populateDataWith(asset: photos[indexPath.item])
        cell.configureGestureWithTarget(target: self, action: #selector(FAImageCropperVC.handleLongPressGesture))
        
        return cell
    }
}


extension FAImageCropperVC:UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell:FAImageCell = collectionView.cellForItem(at: indexPath) as! FAImageCell
        cell.isSelected = true
        selectImageFromAssetAtIndex(index: indexPath.item)
    }
}


extension FAImageCropperVC:UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellWidth, height: cellWidth)
    }
}



extension UIButton {
    
    func addRightBorder(borderColor: UIColor, borderWidth: CGFloat) {
        let border = CALayer()
        border.backgroundColor = borderColor.cgColor
        border.frame = CGRect(x: self.frame.size.width - borderWidth,y: 0, width:borderWidth, height:self.frame.size.height)
        self.layer.addSublayer(border)
    }
    
    func addLeftBorder(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x:0, y:0, width:width, height:self.frame.size.height)
        self.layer.addSublayer(border)
    }
}
