//
//  FollowingMapper.swift
//  Glostars
//
//  Created by Sanzid iOS on 6/9/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import Foundation
import ObjectMapper

class FollowingMapper : Mappable {
    
    
    
    dynamic var name : String = ""
    dynamic var profilePicURL : String = ""
    dynamic var originatedById : String = ""
    dynamic var userId : String = ""
    dynamic var uploadTime : String = ""
    dynamic var discription : String = ""
    dynamic var he_follow = Bool()
    dynamic var is_mutual = Bool()
    dynamic var me_follow = Bool()
    dynamic var permissionmeGives : String = ""


    
  
    
    
    
    
    required convenience init?(map: Map) {
        
        
        self.init()
        
        
    }
    
    func mapping(map: Map) {
        
        name <- map["name"]
        profilePicURL <- map["profilePicURL"]
        originatedById <- map["originatedById"]
        userId <- map["userId"]
        uploadTime <- map["date"]
        discription <- map["description"]
        he_follow <- map["he_follow"]
        is_mutual <- map["is_mutual"]
        me_follow <- map["me_follow"]
        permissionmeGives <- map["permissionmeGives"]


        

        
        
    }
}

