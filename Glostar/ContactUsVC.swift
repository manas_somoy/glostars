//
//  ContactUsVC.swift
//  Glostars
//
//  Created by sadidur rahman on 24/1/19.
//  Copyright © 2019 Sanzid Ashan. All rights reserved.
//

import UIKit
import DropDown
import MessageUI


class ContactUsVC: UIViewController , UITextViewDelegate, UITextFieldDelegate,MFMailComposeViewControllerDelegate{
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var submitButton: UIButton!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var phoneTextField: UITextField!
    
    @IBOutlet weak var messageTextView: UITextView!
    
    var selectedUser = "User"
    
    @IBOutlet weak var dropDownButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        messageTextView.delegate = self
        emailTextField.delegate = self
        phoneTextField.delegate = self
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        dropDownButton.layer.cornerRadius = 10.0
        cancelButton.layer.cornerRadius = 10.0
        submitButton.layer.cornerRadius = 10.0
        
        emailTextField.layer.cornerRadius = 10.0
        emailTextField.layer.borderWidth = 1.0
        emailTextField.layer.borderColor = UIColor.init(hex: "640064").cgColor
        
        phoneTextField.layer.cornerRadius = 10.0
        phoneTextField.layer.borderWidth = 1.0
        phoneTextField.layer.borderColor = UIColor.init(hex: "640064").cgColor
        
        messageTextView.layer.cornerRadius = 10.0
        messageTextView.layer.borderWidth = 1.0
        messageTextView.layer.borderColor = UIColor.init(hex: "640064").cgColor
        
       
        messageTextView.text = "Write your query"
        messageTextView.textColor = UIColor.lightGray
        
        self.navigationItem.title = "Contact us"
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.white,
             NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 18)!]
        
        emailTextField.keyboardAppearance = .dark
        phoneTextField.keyboardAppearance = .dark
        messageTextView.keyboardAppearance = .dark
    }
    
    // Hide the keyboard when the return key pressed
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        
        
        
    }
    
    @IBAction func submitButtonPressed(_ sender: Any) {
        
        if emailTextField.text == ""{
            
        }
        else if phoneTextField.text == ""{
            
        }
        else if  messageTextView.text == "Write your query"{
            
        }
        else if  messageTextView.text == ""{
            
        }
        else{
            if !MFMailComposeViewController.canSendMail() {
                print("Mail services are not available")
                return
            }
            sendEmail()
        }
        
    }
    func sendEmail() {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        // Configure the fields of the interface.
        composeVC.setToRecipients(["care.glostars@gmail.com"])
        composeVC.setSubject("Message to Glostars")
        composeVC.setMessageBody("\(selectedUser)" + " \(messageTextView.text!)" + " \(emailTextField.text!)" + "\(phoneTextField.text!)", isHTML: false)
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
    }
    
    @nonobjc func mailComposeController(controller: MFMailComposeViewController,
                               didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        // Check the result or perform other tasks.
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if messageTextView.textColor == UIColor.lightGray {
            messageTextView.text = nil
            messageTextView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if messageTextView.text.isEmpty {
            messageTextView.text = "Write your query"
            messageTextView.textColor = UIColor.lightGray
        }
    }
    
    
    @IBAction func dropDownButtonPressed(_ sender: Any) {
        let dropDown = DropDown()
        
        // The view to which the drop down will appear on
        dropDown.anchorView = dropDownButton // UIView or UIBarButtonItem
        dropDown.cornerRadius = 10.0
        
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = ["User", "Investor", "Sponsor/Advertiser"]
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.dropDownButton.setTitle(item, for: .normal)
            self.selectedUser = self.dropDownButton.currentTitle!
        }
        
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        DropDown.appearance().backgroundColor = UIColor.init(hex: "E1C8FF")
        dropDown.show()
    }
    
}

