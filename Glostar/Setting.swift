//
//  Setting.swift
//  Glostars
//
//  Created by Sanzid Ashan on 4/18/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit
import Alamofire
import FirebaseInstanceID
import AlamofireImage

var getData = ""

class Setting: UIViewController,FloatyDelegate {
    
    @IBOutlet weak var logoutTitle: UILabel!
    
    @IBOutlet weak var contactUsOutlet: UILabel!
    
    @IBOutlet var aboutUsOutlet: UILabel!
    
    @IBOutlet var termsOutlet: UILabel!
    
    @IBOutlet var dataPrivacyOutLet: UILabel!
    
    @IBOutlet var searchButton: UIButton!
    @IBOutlet var faqOutlet: UILabel!
    
    @IBOutlet weak var blockListLabelOutlet: UILabel!
    
    @IBOutlet weak var achivementButton: UIButton!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var uploadButton: UIButton!
    
    @IBOutlet weak var floatingMainButton: UIButton!
    var fab = Floaty()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hiddenFloatingButtons()
        
        logoutTitle.addTapGestureRecognizer {
            
            self.unregisterFcmToken()
            
        }
        
        layoutFAB()
        
        floatingMainButton.setRounded()
        notificationButton.setRounded()
        uploadButton.setRounded()
        achivementButton.setRounded()
        homeButton.setRounded()
        profileButton.setRounded()
        searchButton.setRounded()
        
        aboutUsOutlet.isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(Setting.tapFunction))
        aboutUsOutlet.addGestureRecognizer(tap)
        termsOutlet.isUserInteractionEnabled = true
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(Setting.tapFunction2))
        termsOutlet.addGestureRecognizer(tap2)
        dataPrivacyOutLet.isUserInteractionEnabled = true
        
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(Setting.tapFunction3))
        dataPrivacyOutLet.addGestureRecognizer(tap3)
        faqOutlet.isUserInteractionEnabled = true
        
        let tap4 = UITapGestureRecognizer(target: self, action: #selector(Setting.tapFunction4))
        faqOutlet.addGestureRecognizer(tap4)
        
        let blockListTap = UITapGestureRecognizer(target: self, action: #selector(Setting.blockList))
        blockListLabelOutlet.addGestureRecognizer(blockListTap)
        blockListLabelOutlet.isUserInteractionEnabled = true
        
        let contactUsTap = UITapGestureRecognizer(target: self, action: #selector(Setting.contactUs))
        contactUsOutlet.addGestureRecognizer(contactUsTap)
        contactUsOutlet.isUserInteractionEnabled = true
        
        
        if userPic == "/Content/Profile/Thumbs/   Male.jpg"{
            
            profileButton.setBackgroundImage(UIImage(named:"male"), for: .normal)
            
            
        }
        else if userPic == "/Content/Profile/Thumbs/   Female  .jpg"{
            
            profileButton.setBackgroundImage(UIImage(named:"female"), for: .normal)
        }
            
            
        else{
            let urlString = URL(string:String(describing: UserDefaults.standard.value(forKey: "pic")!))
            profileButton.af_setBackgroundImage(for: [] , url: urlString! )
            
        }
        
        self.navigationItem.title = "Settings"
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.white,
             NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 20)!]
        
        // Do any additional setup after loading the view.
    }
    
    // UnRegister FCM Token
    fileprivate func unregisterFcmToken() {
        
        InstanceID.instanceID().instanceID(handler: { (result, error) in
            if let error = error {
                print("FCM UnRegister Failed. Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                let fcmToken = result.token
                
                let url = "https://testglostarsdevelopers.azurewebsites.net/api/fcmtokens/removetokens"
                let params : Parameters = [
                    "Email" : UserDefaults.standard.string(forKey: "E") ?? "",
                    "UserId" : UserDefaults.standard.string(forKey: "id") ?? "",
                    "FcmToken" : fcmToken]
                
                let headers: HTTPHeaders = [
                    "Authorization": "Bearer \(String(describing: UserDefaults.standard.value(forKey: "id")!) + String(describing: UserDefaults.standard.value(forKey: "access")!))"/* + (UserDefaults.standard.string(forKey: "access") ?? "")*/,
                    "Content-Type": "application/x-www-form-urlencoded"
                ]
                
                Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: headers).responseJSON { response in
                    switch response.result {
                    case .success(let value):
                        print(value)
                        
                        UserDefaults.standard.removeObject(forKey: "E")
                        UserDefaults.standard.removeObject(forKey: "P")
                        self.navigationController?.setNavigationBarHidden(true, animated: true)
                        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                        let passData = MainStoryboard.instantiateViewController(withIdentifier: "LoginViewController2") as! LoginViewController2
                        
                        self.navigationController?.pushViewController(passData, animated: true)
                    case .failure(let error):
                        print(response)
                        print(error)
                    }
                }
            }
        })
    }
    
    
    func layoutFAB() {
        let item = FloatyItem()
        item.buttonColor = UIColor(displayP3Red: 1, green: 1, blue: 1, alpha: 0.7)
        //item.circleShadowColor = UIColor.red
        //item.titleShadowColor = UIColor.blue
        
        //fab.addItem(title: "I got a title")
        //fab.addItem("I got a icon", icon: UIImage(named: "icShare"))
        fab.addItem("", icon: UIImage(named: "notif+"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "photo-camera"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "FAImageCropperVC") as! FAImageCropperVC
            
            self.navigationController?.pushViewController(passData, animated: true)
            
            
            
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "search-1"), titlePosition: .left) { (item) in
            
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "competiton"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "GaleryViewController") as! GaleryViewController
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "Profile_male"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            passData.getid = userID
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "home+"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "NewsVCViewController") as! NewsVCViewController
            
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        
        self.fab.sticky = true
        self.fab.paddingX = self.view.frame.width/20
        
        self.fab.fabDelegate = self
        
        // print(tableView.frame)
        
        self.view.addSubview(self.fab)
    }
    
    func tapFunction(sender:UITapGestureRecognizer) {
        print("tap working")
        
        getData = "AboutUS"
        
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SettingsData") as? SettingsData {
            //iewController.newsObj = newsObj
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
    }
    func tapFunction2(sender:UITapGestureRecognizer) {
        print("tap working")
        
        getData = "Terms Of Use"
        
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SettingsData") as? SettingsData {
            //iewController.newsObj = newsObj
            
            
            
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
    }
    func tapFunction3(sender:UITapGestureRecognizer) {
        print("tap working")
        
        getData = "Data Privacy"
        
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SettingsData") as? SettingsData {
            //iewController.newsObj = newsObj
            
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
    }
    
    
    func tapFunction4(sender:UITapGestureRecognizer) {
        print("tap working")
        
        getData = "faq"
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SettingsData") as? SettingsData {
            //iewController.newsObj = newsObj
            
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
    }
    
    func blockList(sender:UITapGestureRecognizer){
        print("tap working")
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "BlockListVC") as! BlockListVC
        
        self.navigationController?.pushViewController(passData, animated: true)
        
    }
    
    func contactUs(sender:UITapGestureRecognizer){
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
        
        self.navigationController?.pushViewController(passData, animated: true)
    }
    
    
    
    func hiddenFloatingButtons(){
        
        uploadButton.isHidden = true
        achivementButton.isHidden = true
        profileButton.isHidden = true
        notificationButton.isHidden = true
        homeButton.isHidden = true
        searchButton.isHidden = true
        
    }
    
    func animatingPopUp()
    {
        UIView.animate(withDuration: 1, animations: {
            self.uploadButton.isHidden =  !self.uploadButton.isHidden
            self.searchButton.isHidden = !self.searchButton.isHidden
            
        }) { (true) in
            
            UIView.animate(withDuration: 1, animations: {
                self.achivementButton.isHidden = !self.achivementButton.isHidden
                
            }, completion: { (true) in
                UIView.animate(withDuration: 1, animations: {
                    
                    self.profileButton.isHidden = !self.profileButton.isHidden
                    self.homeButton.isHidden = !self.homeButton.isHidden
                    
                }, completion: { (true) in
                    self.notificationButton.isHidden = !self.notificationButton.isHidden
                    //                    let image1 = UIImage(named: "Circleopen")
                    //                    self.floatingMainButton.setBackgroundImage(image1, for: UIControlState.normal)
                    
                    
                    
                    
                })
            })
        }
        
        
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func mainButtonAction(_ sender: Any) {
        
        
        animatingPopUp()
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
