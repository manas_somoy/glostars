//
//  FollowingVC.swift
//  Glostars
//
//  Created by Sanzid iOS on 7/12/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import ObjectMapper
import SwiftMessages


class FollowingVC: UIViewController , UITableViewDelegate,UITableViewDataSource,FloatyDelegate {
    var fab = Floaty()
    var is_mutual = Bool()
    var me_follow = Bool()
    var he_follow = Bool()
    @IBOutlet weak var tableView2: UITableView!
    
    @IBOutlet var searchButton: UIButton!
    @IBOutlet var segmentOutlet: UISegmentedControl!
    @IBOutlet weak var achivementButton: UIButton!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var uploadButton: UIButton!
    
    @IBOutlet weak var floatingMainButton: UIButton!
    
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var cintainerView: UIView!
    var pageNumber = 1
    
    var totalItems = 1000
    
    var FollowingArray : Array<FollowingUserMapper> = []

    var ActiveNotifyArray : Array< ActiveNotifyMapper > = []
    
    var getid = String()
    
    @IBOutlet var segmentHeight: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        floatingMainButton.isHidden = true
        layoutFAB()
        
        if getid != userID {
            
            segmentController.removeSegment(at:0 , animated: true)
            segmentController.isHidden = true
            segmentHeight.constant = 0.0

            self.navigationItem.title = "Follower's"
            self.navigationController?.navigationBar.titleTextAttributes =
                [NSForegroundColorAttributeName: UIColor.white,
                 NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 18)!]
            
        }
        
        
        
        
        
        if userPic == "/Content/Profile/Thumbs/   Male.jpg"{
            
            profileButton.setBackgroundImage(UIImage(named:"male"), for: .normal)
            
            
        }
        else if userPic == "/Content/Profile/Thumbs/   Female  .jpg"{
            
            profileButton.setBackgroundImage(UIImage(named:"female"), for: .normal)
        }
            
            
        else{
            let urlString = URL(string:String(describing: UserDefaults.standard.value(forKey: "pic")!))
            profileButton.af_setBackgroundImage(for: [] , url: urlString! )
            
        }
        
        
        cintainerView.isHidden = true
        hiddenFloatingButtons()
        
        tableView2.tableFooterView = UIView(frame: .zero)
        if segmentOutlet.selectedSegmentIndex == 0 {
            self.navigationItem.title = "Follower's"
            self.navigationController?.navigationBar.titleTextAttributes =
                [NSForegroundColorAttributeName: UIColor.white,
                 NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 18)!]
            
        }
        else{
            
            
            if getid != userID {
                
                self.navigationItem.title = "Follower's"
                self.navigationController?.navigationBar.titleTextAttributes =
                    [NSForegroundColorAttributeName: UIColor.white,
                     NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 18)!]
                
            }
            else{

        self.navigationItem.title = "Following"
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.white,
             NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 18)!]
            }
        }
        segmentOutlet.tintColor = UIColor.darkGray
        segmentOutlet.backgroundColor = UIColor.darkGray
        
        
        segmentOutlet.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white,
                                                  NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 14)!], for: .normal)
        
        segmentOutlet.setTitleTextAttributes([
            NSForegroundColorAttributeName: UIColor.white,
            NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 14)!], for: .selected)
        
        //gettingFollowingUser()
//        gettingFollowingUser()
//        pageNumber = 2
//        gettingFollowingUser()


        
        //tableView2.isHidden = true
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {

        gettingFollowingUser()
        tableView2.contentInset = UIEdgeInsetsMake(40, 0, 0, 0)
//        floatingMainButton.setRounded()
//        notificationButton.setRounded()
//        uploadButton.setRounded()
//        achivementButton.setRounded()
//        homeButton.setRounded()
//        profileButton.setRounded()
//        searchButton.setRounded()
        
        if getid != userID {
            
            self.navigationItem.title = "Follower's"
            navigationController?.isNavigationBarHidden = false

            self.navigationController?.navigationBar.titleTextAttributes =
                [NSForegroundColorAttributeName: UIColor.white,
                 NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 18)!]
            
        }
        else{
        

        if segmentOutlet.selectedSegmentIndex == 1 {
            navigationController?.isNavigationBarHidden = false

            self.navigationItem.title = "Follower's"
            self.navigationController?.navigationBar.titleTextAttributes =
                [NSForegroundColorAttributeName: UIColor.white,
                 NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 18)!]
            
        }
        
        self.navigationItem.title = "Following"
        navigationController?.isNavigationBarHidden = false

        self.navigationController?.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.white,
             NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 18)!]
        
        }
        
    }
  
    

    
    @IBAction func segmentAction(_ sender: Any) {
        if segmentOutlet.selectedSegmentIndex == 0 {
            navigationController?.isNavigationBarHidden = false

        self.navigationItem.title = "Following"
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.white,
             NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 18)!]
        }
        if segmentOutlet.selectedSegmentIndex == 1 {
            navigationController?.isNavigationBarHidden = false

            self.navigationItem.title = "Followers"
            self.navigationController?.navigationBar.titleTextAttributes =
                [NSForegroundColorAttributeName: UIColor.white,
                 NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 18)!]
        }

        
    }

    
    func hiddenFloatingButtons(){
        
        uploadButton.isHidden = true
        achivementButton.isHidden = true
        profileButton.isHidden = true
        notificationButton.isHidden = true
        homeButton.isHidden = true
        searchButton.isHidden = true
        
        
    }
    
    func animatingPopUp()
    {
        UIView.animate(withDuration: 1, animations: {
            self.uploadButton.isHidden =  !self.uploadButton.isHidden
            
        }) { (true) in
            
            UIView.animate(withDuration: 1, animations: {
                self.achivementButton.isHidden = !self.achivementButton.isHidden
                
            }, completion: { (true) in
                UIView.animate(withDuration: 1, animations: {
                    
                    self.profileButton.isHidden = !self.profileButton.isHidden
                    self.homeButton.isHidden = !self.homeButton.isHidden
                    
                }, completion: { (true) in
                    self.notificationButton.isHidden = !self.notificationButton.isHidden
                     self.searchButton.isHidden = !self.searchButton.isHidden

                    //                    let image1 = UIImage(named: "Circleopen")
                    //                    self.floatingMainButton.setBackgroundImage(image1, for: UIControlState.normal)
                    
                    
                    
                    
                })
            })
        }
        
        
        
    }
    
    
    
    
    
    func gettingFollowingUser() {
        
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/account/GetUserFollowById?userId=\(getid)&page=\(pageNumber)"
        Alamofire.request(url, method: .get , encoding: JSONEncoding.default)
            
            
            .responseJSON{ response in
                
                
                if let jsonRoot = response.result.value as? [String:Any]!{
                    print(jsonRoot)
                    if let resultPayload = jsonRoot!["resultPayload"] as? [String : Any]{
                        print(resultPayload)
                        if let followList = resultPayload["followerList"] as? [[String : Any]] {
                            
                         
                            
                        }
                        
                        if let followingList = resultPayload["followingList"] as? [[String : Any]] {
                            
                            for data in followingList{
                                
                                guard let followinglist = Mapper<FollowingUserMapper>().map(JSON: data) else{
                                    
                                    continue
                                }
                                
                                self.FollowingArray.append(followinglist)
                                self.tableView2.reloadData()
                                
                            }
                            
                            
                        }
                        
                        
                        
                    }
                    
                }
        }
        
        
        
    }
    
    func warningMessage(title:String,subtitle:String){
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: .messageView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.warning)
        view.button?.isHidden = true
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        // Set message title, body, and icon. Here, we're overriding the default warning
        // image with an emoji character.
        let iconText = ["🤔", "😳", "🙄", "😶"].sm_random()!
        view.configureContent(title: title, body: subtitle, iconText: iconText)
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        
        // Show the message.
        SwiftMessages.show(view: view)
        
    }
    func successMessage(title:String,subtitle:String){
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: .messageView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.success)
        view.button?.isHidden = true
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        
        // Set message title, body, and icon. Here, we're overriding the default warning
        // image with an emoji character.
        let iconText = ["🤔", "😳", "🙄", "😶"].sm_random()!
        view.configureContent(title: title, body: subtitle, iconText: iconText)
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        
        SwiftMessages.show(view: view)
        
    }
    

    func gettingFollowingUserWithout() {
        
        
    let url = "https://testglostarsdevelopers.azurewebsites.net/api/account/GetUserFollowById?userId=\(getid)&page=\(pageNumber)"
        Alamofire.request(url, method: .get , encoding: JSONEncoding.default)
            
            
            .responseJSON{ response in
                
                
                if let jsonRoot = response.result.value as? [String:Any]!{
                    
                    if let resultPayload = jsonRoot!["resultPayload"] as? [String : Any]{
                        
                        if let followList = resultPayload["followerList"] as? [[String : Any]] {
                            
                            
                            
                        }
                        
                        if let followingList = resultPayload["followingList"] as? [[String : Any]] {
                            
                            for data in followingList{
                                
                                guard let followinglist = Mapper<FollowingUserMapper>().map(JSON: data) else{
                                    
                                    continue
                                }
                                
                                //self.FollowingArray.append(followinglist)
                                self.tableView2.reloadData()
                                
                            }
                            
                            
                        }
                        
                        
                        
                    }
                    
                }
        }
        
        
        
    }
    
    @IBAction func flotingAction(_ sender: Any) {
        
        
        animatingPopUp()
    }
    
    
    
    @IBAction func segmentControllerAction(_ sender: UISegmentedControl) {
        
        if segmentController.selectedSegmentIndex == 0 {
            
            cintainerView.isHidden = true
        }
            
        else{
            
            cintainerView.isHidden = false
            //gettingFollowingUser()
            
        }
        
    }
    
    
    
    
    // return title string for each section
    
    
    // this method will be called by the table view to know the count of rows for each section
    func UnfollowClick(_ title:String,message:String){
        
        let alertController = UIAlertController(title: title, message: message , preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            print("OK Pressed")
            
            
                
                
            
            
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
            print("Cancel Pressed")
            //self.dismiss(animated: true, completion: nil)
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(FollowingArray.count)
        return FollowingArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView2.dequeueReusableCell(withIdentifier: "cellOne", for: indexPath) as! FollowingCell
        
      
        cell.userNameOutlet.text = FollowingArray[indexPath.row].name + " " + FollowingArray[indexPath.row].lastName
        
        let url = FollowingArray[indexPath.row].proPic

        
        if url == "/Content/Profile/Thumbs/   Male.jpg"{
            
            cell.proPicOutlet.image = UIImage(named: "male")
            
        }
        else if url == "/Content/Profile/Thumbs/   Female  .jpg"{
            
            cell.proPicOutlet.image = UIImage(named: "female")
            
        }
            
            
        else{
            
            
            
            let nsurl = NSURL(string: FollowingArray[indexPath.row].proPic)

            cell.proPicOutlet.af_setImage(withURL: nsurl! as URL, placeholderImage: #imageLiteral(resourceName: "male"), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .crossDissolve(0.5), runImageTransitionIfCached: true, completion: nil)
            
        }
        
        
        

        
        
        
//        let url = NSURL(string: FollowingArray[indexPath.row].proPic)
//
//        
//        cell.proPicOutlet.af_setImage(withURL: url as! URL, placeholderImage: #imageLiteral(resourceName: "male"), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .crossDissolve(0.5), runImageTransitionIfCached: true, completion: nil)
        
        
        cell.onImageTapped = {
            
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            
            passData.getid = self.FollowingArray[indexPath.row].id
//            passData.getName = self.FollowingArray[indexPath.row].name
//            passData.getProfileUrl = self.FollowingArray[indexPath.row].proPic
//            passData.getLastName = self.FollowingArray[indexPath.row].lastName
            
            
            self.navigationController?.pushViewController(passData, animated: true)
            
            
        }
        cell.onDetailsTapped = {
            
            let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            // create an action
            let firstAction: UIAlertAction = UIAlertAction(title: "Network", style: .default) { action -> Void in
                
                
                let params : Parameters = [
                    "PermissionByUserId" : userID,
                    "PermissionWhomUserId" : self.FollowingArray[indexPath.row].id,
                    "Permission" : "Network",
                    
                    ]
                
                
                let url = "https://testglostarsdevelopers.azurewebsites.net/api/Userprivacy/UserPermission"
                Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.httpBody)
                    .responseJSON{ response in
                        
                        if response.result.value != nil{
                            
                            if let jsonRoot = response.result.value as? [String:Any]!{
                                
                                if response.result.isSuccess == true{
                                    let title = jsonRoot!["msg"] as! String
                                    
                                    self.successMessage(title: "Network Selected", subtitle: title)
                                    // tableView.reloadData()
                                    
                                }
                                    
                                else {
                                    
                                    DispatchQueue.main.async{
                                        
                                        self.warningMessage(title: "Warning", subtitle: "Something Wrong")
                                    }
                                    
                                }
                                
                            }
                            else {
                                
                                self.warningMessage(title: "Warning", subtitle: "Please Check Your Internet")
                                
                            }
                        }
                        
                        
                }
                
                
                
            }
            
            let secondAction: UIAlertAction = UIAlertAction(title: "Insider", style: .default) { action -> Void in
                let params : Parameters = [
                    "PermissionByUserId" : userID,
                    "PermissionWhomUserId" : self.FollowingArray[indexPath.row].id,
                    "Permission" : "Insider",
                    
                    ]
                
                
                let url = "https://testglostarsdevelopers.azurewebsites.net/api/Userprivacy/UserPermission"
                Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.httpBody)
                    .responseJSON{ response in
                        
                        if response.result.value != nil{
                            
                            if let jsonRoot = response.result.value as? [String:Any]!{
                                
                                if response.result.isSuccess == true{
                                    let title = jsonRoot!["msg"] as! String
                                    
                                    self.successMessage(title: "Insider Selected", subtitle: title)
                                    //tableView.reloadData()
                                    
                                    
                                }
                                    
                                else {
                                    
                                    DispatchQueue.main.async{
                                        
                                        self.warningMessage(title: "Warning", subtitle: "Something Wrong")
                                    }
                                    
                                }
                                
                            }
                            else {
                                
                                self.warningMessage(title: "Warning", subtitle: "Please Check Your Internet")
                                
                            }
                        }
                        
                        
                }
                
                
            }
            let thirdAction: UIAlertAction = UIAlertAction(title: "Block User", style: .default) { action -> Void in
                
                print("Block User")
                let params : Parameters = [
                    "BlockedByUserId" : userID,
                    "BlockedWhomUserId" : self.FollowingArray[indexPath.row].id,
                    
                    ]
                
                
                let url = "https://testglostarsdevelopers.azurewebsites.net/api/Block/BlockUser"
                Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.httpBody)
                    .responseJSON{ response in
                        
                        if response.result.value != nil{
                            
                            if let jsonRoot = response.result.value as? [String:Any]!{
                                
                                if response.result.isSuccess == true{
                                    
                                    let title = jsonRoot!["msg"] as! String
                                    
                                    
                                    self.successMessage(title: "Block User", subtitle: title)
                                    //                                    tableView.reloadData()
                                    
                                    
                                }
                                    
                                else {
                                    
                                    DispatchQueue.main.async{
                                        
                                        self.warningMessage(title: "Warning", subtitle: "Something Wrong")
                                    }
                                    
                                }
                                
                            }
                            else {
                                
                                self.warningMessage(title: "Warning", subtitle: "Please Check Your Internet")
                                
                            }
                        }
                        
                        
                }
                
                
            }
            
            
            let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
            
            // add actions
            if self.FollowingArray[indexPath.row].is_mutual == true {
                
                actionSheetController.addAction(firstAction)
                actionSheetController.addAction(secondAction)
                actionSheetController.addAction(thirdAction)
                actionSheetController.addAction(cancelAction)

                
            }
            else{
                actionSheetController.addAction(thirdAction)
                actionSheetController.addAction(cancelAction)
            }
            
            // present an actionSheet...
            self.present(actionSheetController, animated: true, completion: nil)
            
            
        }
        
        cell.onLabelTapped = {
            
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            
            passData.getid = self.FollowingArray[indexPath.row].id
            
            
            
            self.navigationController?.pushViewController(passData, animated: true)
            
            
        }
    

        
        if self.FollowingArray[indexPath.row].is_mutual == true {
            
            cell.flowOutletButton.setTitle("Mutual", for: .normal)
            cell.flowOutletButton.backgroundColor = UIColor(hex: 0x660066, alpha: 1)
            
        }
        
        
       else if self.FollowingArray[indexPath.row].is_mutual == false && self.FollowingArray[indexPath.row].me_follow == false && self.FollowingArray[indexPath.row].he_follow == false {
            
            cell.flowOutletButton.setTitle("Follow", for: .normal)
            cell.flowOutletButton.backgroundColor = UIColor(hex: 0x4d94ff, alpha: 1)
            
        }
        
       else if self.FollowingArray[indexPath.row].is_mutual == false && self.FollowingArray[indexPath.row].me_follow == true && self.FollowingArray[indexPath.row].he_follow == false {
            
            cell.flowOutletButton.setTitle("Following", for: .normal)
            cell.flowOutletButton.backgroundColor = UIColor(hex: 0xcc99ff, alpha: 1)
            
        }
       else  {
            
            cell.flowOutletButton.setTitle("Follow Back", for: .normal)
            cell.flowOutletButton.backgroundColor = UIColor.darkGray
        }
        
        //cell.lastName.text = FollowingArray[indexPath.row].lastName
        
        
        cell.onProfileTapped = {
            
            
            

            if cell.flowOutletButton.title(for: .normal) == "Following" {
                
                let unfollowUrl = "https://testglostarsdevelopers.azurewebsites.net/api/Follower/Unfollowing?id=\(self.FollowingArray[indexPath.row].id)"
                Alamofire.request(unfollowUrl, method: .post).responseJSON { response in
                    
                    if let jsonRoot = response.result.value as? [String:Any]!{
                        
                        print(jsonRoot)
                        if let resultPayload = jsonRoot!["resultPayload"] as? [String : Any]{
                            
                            self.is_mutual = (resultPayload["is_mutual"] as? Bool)!
                            self.me_follow = (resultPayload["me_follow"] as? Bool)!
                            self.he_follow = (resultPayload["he_follow"] as? Bool)!
                            

                            
                            
                            if self.is_mutual == false && self.me_follow == false && self.he_follow == false {
                                
                                cell.flowOutletButton.setTitle("Follow", for: .normal)
                                
                                cell.flowOutletButton.backgroundColor = UIColor(hex: 0x4d94ff, alpha: 1)
                            }
                            
                            if self.is_mutual == false && self.me_follow == true && self.he_follow == false {
                                
                                cell.flowOutletButton.setTitle("Following", for: .normal)
                                cell.flowOutletButton.backgroundColor = UIColor(hex: 0xcc99ff, alpha: 1)
                                
                            }
                            if self.is_mutual == false && self.me_follow == false && self.he_follow == true {
                                
                                cell.flowOutletButton.setTitle("Follow Back", for: .normal)
                                cell.flowOutletButton.backgroundColor = UIColor.gray
                                
                            }
                            
                        }
                        
                        
                    }
                    
                    
                    
                }

                
                
            }
              
             else if cell.flowOutletButton.title(for: .normal) == "Follow" {
                let url = "https://testglostarsdevelopers.azurewebsites.net/api/Follower/Following?id=\(self.FollowingArray[indexPath.row].id)"
                
                
                Alamofire.request(url, method: .post).responseJSON { response in
                    
                    
                    if let jsonRoot = response.result.value as? [String:Any]!{
                        print(jsonRoot)
                        
                        
                        if let resultPayload = jsonRoot!["resultPayload"] as? [String : Any]{
                            
                            self.is_mutual = (resultPayload["is_mutual"] as? Bool)!
                            self.me_follow = (resultPayload["me_follow"] as? Bool)!
                            self.he_follow = (resultPayload["he_follow"] as? Bool)!
                            
                            if self.is_mutual == true {
                                
                                cell.flowOutletButton.setTitle("Mutual", for: .normal)
                                cell.flowOutletButton.backgroundColor = UIColor(hex: 0x660066, alpha: 1)
                                
                            }
                            
                            
                            if self.is_mutual == false && self.me_follow == false && self.he_follow == false {
                                
                                cell.flowOutletButton.setTitle("Follow", for: .normal)
                                
                                cell.flowOutletButton.backgroundColor = UIColor(hex: 0x4d94ff, alpha: 1)
                            }
                            
                            if self.is_mutual == false && self.me_follow == true && self.he_follow == false {
                                
                                cell.flowOutletButton.setTitle("Following", for: .normal)
                                cell.flowOutletButton.backgroundColor = UIColor(hex: 0xcc99ff, alpha: 1)
                                
                            }
                            if self.is_mutual == false && self.me_follow == false && self.he_follow == true {
                                
                                cell.flowOutletButton.setTitle("Follow Back", for: .normal)
                                cell.flowOutletButton.backgroundColor = UIColor.gray
                                
                            }
                            
                        }
                        
                    }
                    
                    
                    
                    
                    
                    
                }
                
                
                
                
                
            }
                
                
            else {
                
                let url = "https://testglostarsdevelopers.azurewebsites.net/api/Follower/Unfollowing?id=\(self.FollowingArray[indexPath.row].id)"
                
                
                Alamofire.request(url, method: .post).responseJSON { response in
                    
                    
                    if let jsonRoot = response.result.value as? [String:Any]!{
                        print(jsonRoot)

                        
                        if let resultPayload = jsonRoot!["resultPayload"] as? [String : Any]{
                            
                            self.is_mutual = (resultPayload["is_mutual"] as? Bool)!
                            self.me_follow = (resultPayload["me_follow"] as? Bool)!
                            self.he_follow = (resultPayload["he_follow"] as? Bool)!
                            
                            if self.is_mutual == true {
                                
                            cell.flowOutletButton.setTitle("Mutual", for: .normal)
                            cell.flowOutletButton.backgroundColor = UIColor(hex: 0x660066, alpha: 1)
                                
                            }
                            
                            
                            if self.is_mutual == false && self.me_follow == false && self.he_follow == false {
                                
                                cell.flowOutletButton.setTitle("Follow", for: .normal)
                                
                                cell.flowOutletButton.backgroundColor = UIColor(hex: 0x4d94ff, alpha: 1)
                            }
                            
                            if self.is_mutual == false && self.me_follow == true && self.he_follow == false {
                                
                            cell.flowOutletButton.setTitle("Following", for: .normal)
                            cell.flowOutletButton.backgroundColor = UIColor(hex: 0xcc99ff, alpha: 1)
                                
                            }
                            if self.is_mutual == false && self.me_follow == false && self.he_follow == true {
                                
                                cell.flowOutletButton.setTitle("Follow Back", for: .normal)
                                cell.flowOutletButton.backgroundColor = UIColor.gray
                                
                            }
                            
                        }
                        
                    }
                    
                    
                    
                    
                    
                    
                }
                
                
                
                
                
            }            
            
        }
        if indexPath.row == FollowingArray.count - 1 {
            // last cell
            
            if totalItems > FollowingArray.count {
                pageNumber = pageNumber + 1
                print(pageNumber)
                // more items to fetch
                gettingFollowingUser()
            }
        }
        
        return cell
        
    }
    
    func layoutFAB() {
        let item = FloatyItem()
        item.buttonColor = UIColor(displayP3Red: 1, green: 1, blue: 1, alpha: 0.7)
        //item.circleShadowColor = UIColor.red
        //item.titleShadowColor = UIColor.blue
        
        //fab.addItem(title: "I got a title")
        //fab.addItem("I got a icon", icon: UIImage(named: "icShare"))
        fab.addItem("", icon: UIImage(named: "notif+"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "photo-camera"), titlePosition: .left) { (item) in
            
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "FAImageCropperVC") as! FAImageCropperVC
            
            self.navigationController?.pushViewController(passData, animated: true)
            
            
            
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "search-1"), titlePosition: .left) { (item) in
            
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "competiton"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "GaleryViewController") as! GaleryViewController
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "Profile_male"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            passData.getid = userID
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "home+"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "NewsVCViewController") as! NewsVCViewController
            
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        
        // For Image
        
        
        self.fab.sticky = true
        self.fab.paddingX = self.view.frame.width/20
        
        self.fab.fabDelegate = self
        
//        print(tableView.frame)
        
        self.view.addSubview(self.fab)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        navigationController?.isNavigationBarHidden = false
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        let passData = MainStoryboard.instantiateViewController(withIdentifier: "NewsFeedDetailsVC") as! NewsFeedDetailsVC
//        
//        passData.getID = ActiveNotifyArray[indexPath.row].pictureID
//        passData.getImageUrl = ActiveNotifyArray[indexPath.row].PictureUrl
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileSearchVC") as! ProfileSearchVC
        
        passData.getid = self.FollowingArray[indexPath.row].id
        passData.getName = self.FollowingArray[indexPath.row].name
        passData.getProfileUrl = self.FollowingArray[indexPath.row].proPic
        passData.getLastName = self.FollowingArray[indexPath.row].lastName
        
        
        self.navigationController?.pushViewController(passData, animated: true)
        
        
        
    }
  
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.panGestureRecognizer.translation(in: scrollView.superview).y > 0 {
            //scrolling downwards
            
            navigationController?.isNavigationBarHidden = false
            
            
            if scrollView.contentOffset.y < 0 {
                //this means we are at top of the scrollView
                navigationController?.isNavigationBarHidden = false
                
                
            }
        }
        else {
            //we are scrolling upward
            
            navigationController?.isNavigationBarHidden = true
            
            
        }
//        let offsetY = scrollView.contentOffset.y
//        let contentHeight = scrollView.contentSize.height
//        
//        if offsetY > contentHeight - scrollView.frame.size.height {
//            gettingFollowingUser()
//            pageNumber += 1
//            self.tableView2.reloadData()
//            
//        }
    }
    
    @IBAction func accessTypeButtonPressed(_ sender: Any) {
        print("Pressed")
    }
    
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
