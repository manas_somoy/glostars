//
//  CountryCell.swift
//  Glostars
//
//  Created by Sanzid iOS on 12/18/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit

class CountryCell: UITableViewCell {
    
    
    @IBOutlet var countryName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
