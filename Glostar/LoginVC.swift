//
//  ViewController.swift
//  Glostar
//
//  Created by Sanzid Ashan on 3/19/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit
import Alamofire
import FBSDKLoginKit
import SwiftMessages



var userID = String()
var accessToken = String()
var tokenType = String()
var userPic = String()
var userNameR = String()
var gender = String()

class LoginVC: UIViewController {
    
    @IBOutlet weak var glostarsOutlets: UILabel!
    @IBOutlet weak var forgotPasswordOutlet: UILabel!
    @IBOutlet weak var signupOutlet: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    var userInfo = (accessToken: String(), token_Type : String(),userName:String())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //getRequest()
        
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe:)))
        rightSwipe.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(rightSwipe)
//        
//        if  accessToken = FBSDKAccessToken.current(){
//            getFBUserData()
//        }
        
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func swipeAction(swipe:UISwipeGestureRecognizer)
    {
        
        //dismiss(animated: true, completion: nil)
                let transition = CATransition()
                transition.duration = 0.3
                transition.type = kCATransitionPush
                transition.subtype = kCATransitionFromLeft
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                view.window!.layer.add(transition, forKey: kCATransition)
        
                let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "LoginViewController2")
        
                let nvc = UINavigationController(rootViewController: nextViewController)
                present(nvc, animated: false, completion: nil)
    }
    
    func warningMessage(title:String,subtitle:String){
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: .messageView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.warning)
        view.button?.isHidden = true
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        // Set message title, body, and icon. Here, we're overriding the default warning
        // image with an emoji character.
        let iconText = ["🤔", "😳", "🙄", "😶"].sm_random()!
        view.configureContent(title: title, body: subtitle, iconText: iconText)
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        
        // Show the message.
        SwiftMessages.show(view: view)
        
    }
    func successMessage(title:String,subtitle:String){
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: .messageView)

        // Theme message elements with the warning style.
        view.configureTheme(.success)
        view.button?.isHidden = true
        
        // Add a drop shadow.
        view.configureDropShadow()
    
        
        // Set message title, body, and icon. Here, we're overriding the default warning
        // image with an emoji character.
        let iconText = ["🤔", "😳", "🙄", "😶"].sm_random()!
        view.configureContent(title: title, body: subtitle, iconText: iconText)
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        
        SwiftMessages.show(view: view)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        passwordTextField.keyboardAppearance = .dark
        emailTextField.keyboardAppearance = .dark

        
        self.navigationController?.isNavigationBarHidden = true
        let accessToken = FBSDKAccessToken.current()
        print(accessToken)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        navigationController?.navigationBar.isHidden = false
        DispatchQueue.main.async{
            SwiftMessages.hideAll()
            self.hideLoader()

        }


    }
    
       
    
    @IBAction func signUpAction(_ sender: Any) {
    }
    
    
    func localStorage(){
        
        //        UserDefaults.standard.set(emailTextField.text, forKey: "name")
        //        UserDefaults.standard.set(passwordTextField.text, forKey: "password")
        UserDefaults.standard.set(accessToken + tokenType, forKey: "access")
        print(UserDefaults.standard.value(forKey: "access")!)
        UserDefaults.standard.set(userID, forKey: "ID")
        UserDefaults.standard.set(userPic, forKey: "userPic")
        //UserDefaults.standard.set(userNameR, forKey: "userPic")
        
        
    }
    
    @IBAction func loginButton(_ sender: Any) {
        
        emailTextField.text = "james@gmail.com"
        passwordTextField.text = "12345678"
        
        if emailTextField.text?.isEmpty == true{
            
            warningMessage(title: "Warning", subtitle: "Please Enter Email")
        }
       else if passwordTextField.text?.isEmpty == true {
            
            warningMessage(title: "Warning", subtitle: "Please Enter Password")

        }
            
            
        else{
            //SwiftMessageService.SuccessMessage("Please Wait ", description: "Preparing Glostars for you ")
            DispatchQueue.main.async{
                self.successMessage(title: "Please Wait", subtitle: "Preparing Glostars for you")
            }
            
            getRequest()
            //print(UserDefaults.standard.value(forKey: "access")!)
            
            
            
        }
        
        
        
        
        
        //
        //        let userDefault = UserDefaults.standard
        //        userDefault.set(true, forKey: "isLoggedIn")
        //        userDefault.synchronize()
        
    }
    
    func  getRequest(){
        
        self.showLoader("")
     
        
        let params : Parameters = [
            "grant_type" : "password",
            "password" : passwordTextField.text!,
            "username" : emailTextField.text!,
            
            ]
        
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/Token"
        Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.httpBody)
            .responseJSON{ response in
                
                if response.result.value != nil{
                    
                    if let jsonRoot = response.result.value as? [String:Any]!{
                        
                        print(jsonRoot)
                        
                        if jsonRoot!["access_token"] != nil  && jsonRoot!["token_type"] != nil {
                            accessToken = jsonRoot!["access_token"] as! String!
                            UserDefaults.standard.set(accessToken, forKey: "access")
                            print("\naccessToken: \(accessToken)\n")
                            tokenType = jsonRoot!["token_type"] as! String!
                            UserDefaults.standard.set(tokenType, forKey: "token")
                            UserDefaults.standard.set(self.emailTextField.text, forKey: "E")
                            UserDefaults.standard.set(self.passwordTextField.text, forKey: "P")
                            self.getData()
                            self.localStorage()
                            
                        }
                        else {
                            
                            DispatchQueue.main.async{
                                
                                self.warningMessage(title: "Warning", subtitle: "Wrong User Name or password")
                                self.hideLoader()

                            }

                            //self.displayMyAlertMessage("Wrong User Name or password ")
                            //SwiftMessageService.WarningMessage("Warning", description: "Wrong User Name or password ")
                        }
                        
                        
                        // self.localStorage()
                        
                        
                        
                    }
                    else {
                        
                        self.warningMessage(title: "Warning", subtitle: "Please Check Your Email and Password")
                        self.hideLoader()


                    }
                }
                
                
        }
        
    }
  
    
    func  getData() {
        
        
        
        
        let params2 : Parameters = [
            
            "userEmail" : emailTextField.text!,
            
            ]
        
        let url2 = "https://testglostarsdevelopers.azurewebsites.net/api/account/GetUserInfo"
        
        Alamofire.request(url2, method: .get, parameters: params2, encoding: URLEncoding.default)
            
            
            .responseJSON{ response2 in
                
                if response2.result.value != nil {
                    
                    if let jsonRoot = response2.result.value as? [String:Any]!{
                        print(jsonRoot)
                        
                        if let userinfo = jsonRoot!["resultPayload"] as? [String: Any]!{
                            
                        
//                            gender = userinfo["gender"] as! String
//                            print(gender)
//                            
                            userID = userinfo!["userId"] as! String
                            print("\nuserID: = \(userID)\n")
                            UserDefaults.standard.set(userID, forKey: "id")
                            userPic = userinfo!["profilePicURL"] as! String
                            print(userPic)
                            UserDefaults.standard.set(userPic, forKey: "pic")
                            
                            userNameR = userinfo!["name"] as! String
                            
                            self.setupNotification()
                                                        
                            let transition = CATransition()
                            transition.duration = 0.3
                            transition.type = kCATransitionPush
                            transition.subtype = kCATransitionFromRight
                            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                            self.view.window!.layer.add(transition, forKey: kCATransition)
                            
                            
                            let nextViewController =                                  self.storyboard!.instantiateViewController(withIdentifier:"NewsVCViewController")
                            
                            let nvc = UINavigationController(rootViewController: nextViewController)
                            self.present(nvc, animated: false, completion: nil)
                            
                        }
                        
                        
                    }
                    else{
                        
                        self.warningMessage(title: "Warning", subtitle: "Please Check Your Email and Password")
                        self.hideLoader()

                    }
                }
                
                
                
                
        }
        
        
        
    }
    
    func setupNotification() {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.registerForPushNotifications()
        }
    }
    
    
    
    
    
    
    
    //        let email = self.emailTextField.text
    //        let password = self.passwordTextField.text
    //
    //        // Check for empty fields
    //        if(email!.isEmpty || password!.isEmpty)
    //        {
    //
    //            // Display alert message
    //
    //            displayMyAlertMessage("All fields are required");
    //
    //            return;
    //
    //
    
    @IBAction func forgotPasswordClick(_ sender: Any) {
        
//        UIApplication.shared.openURL(NSURL(string: "https://testglostarsdevelopers.azurewebsites.net/Account/ForgotPassword")! as URL)
        let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "SendVerificationVC")
        
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
        
    }
    
    
    func displayMyAlertMessage(_ userMessage:String)
    {
        
        let myAlert = UIAlertController(title:"Warning", message:userMessage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title:"Ok", style:UIAlertActionStyle.default, handler:nil);
        
        myAlert.addAction(okAction);
        self.present(myAlert, animated: true, completion: nil);
        
        
    }
    
    
    
    // Hide the keyboard when the return key pressed
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // Move the text field in a pretty animation!
    
}










