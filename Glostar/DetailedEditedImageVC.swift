//
//  DetailedEditedImageVC.swift
//  EditedProject
//
//  Created by sadidur rahman on 5/2/19.
//  Copyright © 2019 sadidur rahman. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class DetailedEditedImageVC: UIViewController {
    var  imageValue = ""
    var  idPhoto = 0


    @IBOutlet weak var tableView: UITableView!
    var PhotoArrayFeed : Array<EditPhotoMapper> = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getPhotos()

        // Do any additional setup after loading the view.
    }
    func getPhotosTask(){
        
        //https://testglostarsdevelopers.azurewebsites.net/api/editimages/seeall?id=52
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/editimages/seeall?id=\(idPhoto)"
        Alamofire.request(url, method: .get , encoding: JSONEncoding.default)
            
            
            .responseJSON{ response in
                // print(response)
                
                if response.result.value != nil {
                    
                    
                    if let jsonRoot = response.result.value as? [String:Any]!{
                        
                        
                        print(jsonRoot)
                        
                        
                        if let resultPayload = jsonRoot!["resultPayload"] as? [String : Any]{
                            
                            if let data = resultPayload["data"] as? [[String : Any]]{
                                print(data)
                                
                                for dic in data {
                                    
                                    guard  let picInfo = Mapper<EditPhotoMapper>().map(JSON: dic) else {
                                        
                                        
                                        continue
                                        
                                    }
                                    //self.PhotoArrayFeed.append(picInfo)
                                }
                                
                            }
                        }
                    }
                    self.tableView.reloadData()
                    
                }
                else{
                    
                    print("No Data")
                    self.getPhotos()
                    
                }
        }
        
        
        
    }
    
    func getPhotos(){
        
        //https://testglostarsdevelopers.azurewebsites.net/api/editimages/seeall?id=52
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/editimages/seeall?id=\(idPhoto)"
        Alamofire.request(url, method: .get , encoding: JSONEncoding.default)
            
            
            .responseJSON{ response in
                // print(response)
                
                if response.result.value != nil {
                    
                    
                    if let jsonRoot = response.result.value as? [String:Any]!{
                        
                        
                        print(jsonRoot)
                        
                        
                        if let resultPayload = jsonRoot!["resultPayload"] as? [String : Any]{
                            
                            if let data = resultPayload["data"] as? [[String : Any]]{
                                print(data)
                                
                                for dic in data {
                                    
                                    guard  let picInfo = Mapper<EditPhotoMapper>().map(JSON: dic) else {
                                        
                                        
                                        continue
                                        
                                    }
                                    self.PhotoArrayFeed.append(picInfo)
                                }
                                
                            }
                        }
                    }
                    self.tableView.reloadData()
                    
                }
                else{
                    
                    print("No Data")
                    self.getPhotos()
                    
                }
        }
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    

}

extension DetailedEditedImageVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PhotoArrayFeed.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailedImageCell", for: indexPath) as! DetailedImageCell
        cell.selectionStyle = .none
        let url = URL(string: PhotoArrayFeed[indexPath.row].editPicUrl)
        let urlPic = URL(string: PhotoArrayFeed[indexPath.row].getProfilePictureThumb)

        cell.cellPhoto.sd_setImage(with: url, completed: nil)
        cell.personImage.sd_setImage(with: urlPic, completed: nil)
        cell.personName.text = PhotoArrayFeed[indexPath.row].username
        cell.thumbUPCountLabel.text = String(PhotoArrayFeed[indexPath.row].likesCount)
        cell.thumbDownCountLabel.text = String(PhotoArrayFeed[indexPath.row].disLikesCount)
        
        if PhotoArrayFeed[indexPath.row].myLikesCount == 0 {
            
            let image = UIImage(named: "t1") as UIImage?
            
            
            cell.thumbUpButton.setImage(image, for: .normal)
            self.imageValue = ""
            
            
        }
        
        if PhotoArrayFeed[indexPath.row].myLikesCount == 1 {
            
            let image = UIImage(named: "t2") as UIImage?
            self.imageValue = "Checked"
            
            
            cell.thumbUpButton.setImage(image, for: .normal)

        }
        if PhotoArrayFeed[indexPath.row].myDislikesCount == 0 {
            
            let image = UIImage(named: "t3") as UIImage?
            
            
            cell.thumbDownButton.setImage(image, for: .normal)
            self.imageValue = ""
            
            
        }
        
        if PhotoArrayFeed[indexPath.row].myDislikesCount == 1 {
            
            let image = UIImage(named: "t4") as UIImage?
            self.imageValue = "Checked"
            
            
            cell.thumbDownButton.setImage(image, for: .normal)
            
        }



        
        cell.onThumbUpTapped = {
            print("thumb up")
            
            
                if self.imageValue == "Checked" {
                    //https://testglostarsdevelopers.azurewebsites.net/api/images/removelikes
                    
                    //https://testglostarsdevelopers.azurewebsites.net/api/images/Like
                    let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/removelikes"
                    let parameters: [String : Any]? = [
                        "PhotoId": self.PhotoArrayFeed[indexPath.row].id ,
            ]
                    //            let headers: HTTPHeaders = [
                    //                "Content-Type": "application/json"
                    //            ]
                    Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default, headers:nil).responseJSON { response in
                        switch response.result {
                        case .success:
                            
                            let imagevanish = UIImage(named: "t2") as UIImage?
                            
                            
                            cell.thumbUPCountLabel.text = String(self.PhotoArrayFeed[indexPath.row].likesCount )

                            
                            
                            cell.thumbUpButton.setImage(imagevanish, for: .normal)
                            print(response)
                            self.imageValue = ""
                            // self.ArticlesInfo.removeAll()
                            
                            // self.getRequest()
                            
                            
                            // self.tableView.reloadData()

                            
                            
                        case .failure(let error):
                            print(error)
                        }
                        
                        
                    }
                    
                    
                    
                    
                    
                }
                else{
                    
                    
                    let parameters: [String : Any]? = [
                        "NumOfLikes": 1 ,
                        "PhotoId": self.PhotoArrayFeed[indexPath.row].id ,
                        ]
                   // https://testglostarsdevelopers.azurewebsites.net/api/images/Like
                    let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/Like"
                    let headers: HTTPHeaders = [
                        "Content-Type": "application/json"
                    ]
                    Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
                        
                        if let jsonRoot = response.result.value as? [String:Any]!{
                            
                            //print(jsonRoot)
                            
                            if let resultPayload = jsonRoot!["resultPayload"] as? [String: Any]!{
                                
                                print(resultPayload)
                                
                                
                                cell.thumbUPCountLabel.text = String(self.PhotoArrayFeed[indexPath.row].likesCount )
//                                self.getPhotosTask()

                                
                                
                                self.imageValue = "Checked"
                                
                            }
                            
                            let image = UIImage(named: "t1") as UIImage?
                            
                            
                            
                            
                            cell.thumbUpButton.setImage(image, for: .normal)

                        }
                        
                        
                        
                        
                        
                        
                    }
                    //self.getRequest()
                }
            
        }
        
        cell.onThumbDownTapped = {

            if self.imageValue == "Checked" {
                
                //https://testglostarsdevelopers.azurewebsites.net/api/images/Like
               // https://testglostarsdevelopers.azurewebsites.net/api/images/removedislikes
                let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/removedislikes"
                let parameters: [String : Any]? = [
                    "PhotoId": self.PhotoArrayFeed[indexPath.row].id ,
                    ]
                //            let headers: HTTPHeaders = [
                //                "Content-Type": "application/json"
                //            ]
                Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default, headers:nil).responseJSON { response in
                    
                    switch response.result {
                    case .success:
                        
                        let imagevanish = UIImage(named: "t3") as UIImage?
                        
                        
                        
                        cell.thumbDownCountLabel.text = String(self.PhotoArrayFeed[indexPath.row].disLikesCount)

                        
                        cell.thumbDownButton.setImage(imagevanish, for: .normal)
                        print(response)
                        self.imageValue = ""
                
                        
                        
                        
                    case .failure(let error):
                        print(error)
                    }
                    
                    
                }
                
                
                
                
                
            }
            else{
                
                
                let parameters: [String : Any]? = [
                    "NumOfDislikes": 1 ,
                    "PhotoId": self.PhotoArrayFeed[indexPath.row].id ,
                    ]
                // https://testglostarsdevelopers.azurewebsites.net/api/images/Like
                //https://testglostarsdevelopers.azurewebsites.net/api/images/DisLike
                let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/DisLike"
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json"
                ]
                Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
                    
                    if let jsonRoot = response.result.value as? [String:Any]!{
                        
                        //print(jsonRoot)
                        
                        if let resultPayload = jsonRoot!["resultPayload"] as? [String: Any]!{
                            
                            print(resultPayload)
                            
                            
                            cell.thumbDownCountLabel.text = String(self.PhotoArrayFeed[indexPath.row].disLikesCount)
                            //                                self.getPhotosTask()
                            
                            
                            
                            self.imageValue = "Checked"
                            
                        }
                        
                        let image = UIImage(named: "t4") as UIImage?
                        
                        
                        
                        
                        cell.thumbDownButton.setImage(image, for: .normal)
                        
                    }
                    
                    
                    
                    
                    
                    
                }
                //self.getRequest()
            }

        }
        
        cell.onDeleteTapped = {
            
            
            
            let parameters: [String : Any]? = [
                "PhotoId": self.PhotoArrayFeed[indexPath.row].id ,
                ]
            // https://testglostarsdevelopers.azurewebsites.net/api/images/Like
            //https://testglostarsdevelopers.azurewebsites.net/api/images/DisLike
            //https://testglostarsdevelopers.azurewebsites.net/api/editimages/delete/{mainid}/{editid}
            let url = "https://testglostarsdevelopers.azurewebsites.net/api/editimages/delete/\(self.idPhoto)/\(self.PhotoArrayFeed[indexPath.row].id)"
            let headers: HTTPHeaders = [
                "Content-Type": "application/json"
            ]
            Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
                
                if let jsonRoot = response.result.value as? [String:Any]!{
                    
                    //print(jsonRoot)
                    
                    if let resultPayload = jsonRoot!["resultPayload"] as? [String: Any]!{
                        
                        print(resultPayload)
                        self.getPhotos()
                  
                    }
                    
                }
                
                
                
                
                
                
            }
            
            
            
            print("delete")
        }
        cell.personName.addTapGestureRecognizer {
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            passData.getid = self.PhotoArrayFeed[indexPath.row].userId
            self.navigationController?.pushViewController(passData, animated: true)
            
        }
        cell.personImage.addTapGestureRecognizer {
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            passData.getid = self.PhotoArrayFeed[indexPath.row].userId
            self.navigationController?.pushViewController(passData, animated: true)
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 420
    }
    
    
    
}
