//
//  ProfileVC.swift
//  Glostars
//
//  Created by Sanzid Ashan on 4/11/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
import ObjectMapper
import SwiftMessages


var followClick = ""
class ProfileVC: UIViewController, UIScrollViewDelegate, UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIImagePickerControllerDelegate ,UINavigationControllerDelegate ,UITextFieldDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate,UITableViewDelegate,UITableViewDataSource,FloatyDelegate, UITextViewDelegate{
    @IBOutlet var compitetionOutlet: UILabel!
    
    @IBOutlet var competitionCollectionHeight: NSLayoutConstraint!
    
    @IBOutlet var followingButtonOutlet: DesignableButton!
    
    @IBOutlet weak var interestLabel: UILabel!
    
    var is_mutual = Bool()
    var me_follow = Bool()
    var he_follow = Bool()
    
    @IBOutlet var viewAdd: UIView!
    
    @IBOutlet var height2: NSLayoutConstraint!
    
    @IBOutlet var countryTextField: UITextField!
    
    @IBOutlet var publicPhotoTotal: UILabel!
    
    @IBOutlet var countryTableView: UITableView!
    
    @IBOutlet var competitionViewHeightConstraints: NSLayoutConstraint!
    
    @IBOutlet var topConstraintComHide: NSLayoutConstraint!
    
    @IBOutlet var buttomEditConstrainsMutual: NSLayoutConstraint!
    @IBOutlet var competitionPhotoTotal: UILabel!
    
    var fab = Floaty()

    @IBOutlet var MutualPhotoTotal: UILabel!
    
    @IBOutlet var view2Height: NSLayoutConstraint!
    @IBOutlet var profileBackgrountView: UIScrollView!
    
    @IBOutlet var height1: NSLayoutConstraint!
    
    @IBOutlet var backgroundHeightConstrants: NSLayoutConstraint!
    
    
    @IBOutlet var cityText: UILabel!
    @IBOutlet weak var achivementButton: UIButton!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var uploadButton: UIButton!
    
    
    @IBOutlet var editProfileOutlet: DesignableButton!
    
    
    @IBOutlet var settingOutlet: DesignableButton!
    
    
    
    @IBOutlet var locationTextfield: UITextField!
    @IBOutlet weak var floatingMainButton: UIButton!
    @IBOutlet weak var scrollView: UIView!
    @IBOutlet weak var recW: UILabel!
    @IBOutlet weak var collectionView2: UICollectionView!
    @IBOutlet weak var interest: UILabel!
    @IBOutlet weak var recM: UILabel!
    @IBOutlet weak var recE: UILabel!
    @IBOutlet weak var recG: UILabel!
    @IBOutlet weak var aboutMe: UILabel!
    @IBOutlet weak var background: UIScrollView!
    @IBOutlet weak var foreground: UIScrollView!
    
    
    
    

    @IBOutlet weak var numFollowers: UILabel!
    @IBOutlet weak var numFlowing: UILabel!
    
    @IBOutlet weak var photoLabel: UILabel!
    
    @IBOutlet weak var numPhoto: UILabel!
    @IBOutlet weak var userLocation: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var imageProfile: UIImageView!
    
    @IBOutlet weak var seeAllMut: UIButton!
    @IBOutlet weak var seeAllPub: UIButton!
    @IBOutlet weak var seeAllCom: UIButton!
    
    @IBOutlet var topConstraintsPublicPhotoLabel: NSLayoutConstraint!
    @IBOutlet var bottomConstraintsCompetitionPic: NSLayoutConstraint!
    
    @IBOutlet weak var userPermission: UIButton!
    @IBOutlet var bottomConstraintsPublicPic: NSLayoutConstraint!
    
    @IBOutlet var topConstraintsMutualPhotoLabel: NSLayoutConstraint!
    
    @IBOutlet var lastname: UILabel!
    @IBOutlet weak var collectionView3: UICollectionView!
    @IBOutlet weak var collectionView1: UICollectionView!
    
    @IBOutlet var locationLeadingConstrants: NSLayoutConstraint!
    var Compitition : Array<CompititionMapper> = []
    var PublicArray : Array<PublicMapper> = []
    var MutualArray : Array<MutualMapper> = []
    var CountryArray : Array<CountryMapperArray> = []


    @IBOutlet var cityLocationTextEdit: UITextField!

    @IBOutlet weak var profilePicEditOutLet: UIImageView!
    var imagePicker = UIImagePickerController()
    let picker = UIImagePickerController()
    var pickedImagePath: URL?
    var pickedImageData: Data?
    
    
    
    @IBOutlet var profileViewHeight: NSLayoutConstraint!
    @IBOutlet var usersFollowersView: UIView!

    @IBOutlet weak var firstNameEditOutlet: UITextField!
    
    
    @IBOutlet weak var lastNameEditOutlet: UITextField!
    
    
    @IBOutlet weak var descriptionEditNameOutlet: UITextView!
    
    @IBOutlet weak var popUpViewEdit: UIView!
    
    
    @IBOutlet var searchButton: UIButton!
    
    
    @IBOutlet var mutualPhotoOutlet: UILabel!
    
    @IBOutlet weak var interestEditOutlet: UITextView!
    
    @IBOutlet weak var changePassEditOutlet: UITextField!
    
    @IBOutlet var countryChoose: DropMenuButton!
    
    @IBOutlet weak var confirmChangepassEditOulet: DesignableButton!
    
    var getid = String()

    override func viewDidLoad() {
        
        super.viewDidLoad()
        layoutFAB()

        followingButtonOutlet.isHidden = true
        collectionView2.emptyDataSetSource = self
        collectionView2.emptyDataSetDelegate = self
       // collectionView3.emptyDataSetSource = self
       // collectionView3.emptyDataSetDelegate = self
        collectionView1.emptyDataSetSource = self
        collectionView1.emptyDataSetDelegate = self
        
        navigationController?.isNavigationBarHidden = false

        hiddenFloatingButtons()


        popUpViewEdit.alpha = 0
        
        
        let font = UIFont(name: "Ubuntu-Light", size: 18.0)
        
        // Back button
        UIBarButtonItem.appearance().setTitleTextAttributes([NSFontAttributeName: font!], for: UIControlState.normal)
        
        // Title in the navigation item
        let fontAttributes = [NSFontAttributeName: font]
        self.navigationController?.navigationBar.titleTextAttributes = fontAttributes as Any as? [String : Any]
       // getUserInfo()

       
        background.delegate = self
        descriptionEditNameOutlet.delegate = self
        descriptionEditNameOutlet.text! = "About Me (max. 160 characters)"
        descriptionEditNameOutlet.textColor = UIColor.lightGray
        
        interestLabel.addTapGestureRecognizer {
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "InterestVC") as! InterestVC
            passData.fromUpload = "true"
            self.navigationController?.pushViewController(passData, animated: true)
        }
        interestEditOutlet.isUserInteractionEnabled = false
        
//        let newBackButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: self, action: Selector(("back:")))
//        self.navigationItem.leftBarButtonItem = newBackButton
        
    }
    
//    func back(sender: UIBarButtonItem) {
//        // Perform your custom actions
//        // ...
//        // Go back to the previous ViewController
//        //self.navigationController?.popViewController(animated: true)
//        self.popUpViewEdit.alpha = 0
//    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        floatingMainButton.setRounded()
        notificationButton.setRounded()
        uploadButton.setRounded()
        achivementButton.setRounded()
        homeButton.setRounded()
        profileButton.setRounded()
        searchButton.setRounded()
        GetCountryName()
        getCount()
        countryTableView.isHidden = true
        if getid != String(describing: UserDefaults.standard.value(forKey: "id")!)
        {
            
            
            //collectionView3.isHidden = true
            //seeAllMut.isHidden = true
            editProfileOutlet.isHidden = true
            settingOutlet.isHidden = true
            //mutualPhotoOutlet.isHidden = true
            usersFollowersView.alpha = 0
            //MutualPhotoTotal.isHidden = true
            
            
            profileViewHeight.constant = view.frame.height/12
            viewAdd.isHidden = true
            followingButtonOutlet.isHidden = false
            // topConstraintComHide.constant = -65
            MutualPhotoTotal.isHidden = false
            seeAllMut.isHidden = false
            mutualPhotoOutlet.isHidden = false
            //collectionView3.isHidden = false
            MutualPhotoTotal.isHidden = true
            seeAllMut.isHidden = true
            mutualPhotoOutlet.isHidden = true
            //collectionView3.isHidden = true
            
            
            
        }
        else{
            
            userPermission.isHidden = true

        //
    
            
            
        }
        getUserInfo()
        
        userPicInfo()
        
        gettingNotification()
       
        descriptionEditNameOutlet.text! = "About Me (max. 160 characters)"
        descriptionEditNameOutlet.textColor = UIColor.lightGray
        
        if interestLabel.text! == "Choose Interest"{
            interestLabel.textColor = UIColor.lightGray
            interestLabel.text! = interesrtValue

        }
        else {
            interestLabel.textColor = UIColor.black
            interestLabel.text! = interesrtValue

        }
    }
    func warningMessage(title:String,subtitle:String){
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: .messageView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.warning)
        view.button?.isHidden = true
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        // Set message title, body, and icon. Here, we're overriding the default warning
        // image with an emoji character.
        let iconText = ["🤔", "😳", "🙄", "😶"].sm_random()!
        view.configureContent(title: title, body: subtitle, iconText: iconText)
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        
        // Show the message.
        SwiftMessages.show(view: view)
        
    }
    func successMessage(title:String,subtitle:String){
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: .messageView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.success)
        view.button?.isHidden = true
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        
        // Set message title, body, and icon. Here, we're overriding the default warning
        // image with an emoji character.
        let iconText = ["🤔", "😳", "🙄", "😶"].sm_random()!
        view.configureContent(title: title, body: subtitle, iconText: iconText)
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        
        SwiftMessages.show(view: view)
        
    }

    
    @IBAction func userPermissionAction(_ sender: Any) {
        
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // create an action
        let firstAction: UIAlertAction = UIAlertAction(title: "Network", style: .default) { action -> Void in
            
            
            let params : Parameters = [
                "PermissionByUserId" : userID,
                "PermissionWhomUserId" : self.getid,
                "Permission" : "Network",
                
                ]
            
            
            let url = "https://testglostarsdevelopers.azurewebsites.net/api/Userprivacy/UserPermission"
            Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.httpBody)
                .responseJSON{ response in
                    
                    if response.result.value != nil{
                        
                        if let jsonRoot = response.result.value as? [String:Any]!{
                            
                            if response.result.isSuccess == true{
                                let title = jsonRoot!["msg"] as! String
                                
                                self.successMessage(title: "Network Selected", subtitle: title)
                                // tableView.reloadData()
                                
                            }
                                
                            else {
                                
                                DispatchQueue.main.async{
                                    
                                    self.warningMessage(title: "Warning", subtitle: "Something Wrong")
                                }
                                
                            }
                            
                        }
                        else {
                            
                            self.warningMessage(title: "Warning", subtitle: "Please Check Your Internet")
                            
                        }
                    }
                    
                    
            }
            
            
            
        }
        
        let secondAction: UIAlertAction = UIAlertAction(title: "Insider", style: .default) { action -> Void in
            let params : Parameters = [
                "PermissionByUserId" : userID,
                "PermissionWhomUserId" : self.getid,
                "Permission" : "Insider",
                
                ]
            
            
            let url = "https://testglostarsdevelopers.azurewebsites.net/api/Userprivacy/UserPermission"
            Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.httpBody)
                .responseJSON{ response in
                    
                    if response.result.value != nil{
                        
                        if let jsonRoot = response.result.value as? [String:Any]!{
                            
                            if response.result.isSuccess == true{
                                let title = jsonRoot!["msg"] as! String
                                
                                self.successMessage(title: "Insider Selected", subtitle: title)
                                //tableView.reloadData()
                                
                                
                            }
                                
                            else {
                                
                                DispatchQueue.main.async{
                                    
                                    self.warningMessage(title: "Warning", subtitle: "Something Wrong")
                                }
                                
                            }
                            
                        }
                        else {
                            
                            self.warningMessage(title: "Warning", subtitle: "Please Check Your Internet")
                            
                        }
                    }
                    
                    
            }
            
            
        }
        let thirdAction: UIAlertAction = UIAlertAction(title: "Block User", style: .default) { action -> Void in
            
            print("Block User")
            let params : Parameters = [
                "BlockedByUserId" : userID,
                "BlockedWhomUserId" : self.getid,
                
                ]
            
            
            let url = "https://testglostarsdevelopers.azurewebsites.net/api/Block/BlockUser"
            Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.httpBody)
                .responseJSON{ response in
                    
                    if response.result.value != nil{
                        
                        if let jsonRoot = response.result.value as? [String:Any]!{
                            
                            if response.result.isSuccess == true{
                                
                                let title = jsonRoot!["msg"] as! String
                                
                                
                                self.successMessage(title: "Block User", subtitle: title)
                                //                                    tableView.reloadData()
                                
                                
                            }
                                
                            else {
                                
                                DispatchQueue.main.async{
                                    
                                    self.warningMessage(title: "Warning", subtitle: "Something Wrong")
                                }
                                
                            }
                            
                        }
                        else {
                            
                            self.warningMessage(title: "Warning", subtitle: "Please Check Your Internet")
                            
                        }
                    }
                    
                    
            }
            
            
        }
        
        
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
        
        // add actions
        actionSheetController.addAction(firstAction)
        actionSheetController.addAction(secondAction)
        actionSheetController.addAction(thirdAction)
        actionSheetController.addAction(cancelAction)
        
        // present an actionSheet...
        self.present(actionSheetController, animated: true, completion: nil)

        
    }
    
    
    
    func hiddenFloatingButtons(){
        
        uploadButton.isHidden = true
        achivementButton.isHidden = true
        profileButton.isHidden = true
        notificationButton.isHidden = true
        homeButton.isHidden = true
        searchButton.isHidden = true
        
    }
    
    func layoutFAB() {
        let item = FloatyItem()
        item.buttonColor = UIColor(displayP3Red: 1, green: 1, blue: 1, alpha: 0.7)
        //item.circleShadowColor = UIColor.red
        //item.titleShadowColor = UIColor.blue
        
        //fab.addItem(title: "I got a title")
        //fab.addItem("I got a icon", icon: UIImage(named: "icShare"))
        fab.addItem("", icon: UIImage(named: "notif+"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "photo-camera"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "FAImageCropperVC") as! FAImageCropperVC
            
            self.navigationController?.pushViewController(passData, animated: true)
            
            
            
            self.fab.close()
            
        }
        fab.addItem("", icon: UIImage(named: "search-1"), titlePosition: .left) { (item) in
            
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "competiton"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "GaleryViewController") as! GaleryViewController
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "Profile_male"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            passData.getid = userID
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "home+"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "NewsVCViewController") as! NewsVCViewController
            
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        
        self.fab.sticky = true
        self.fab.paddingX = self.view.frame.width/20
        
        self.fab.fabDelegate = self
        
       // print(tableView.frame)
        
        self.view.addSubview(self.fab)
    }

    @IBAction func followingAction(_ sender: Any) {
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "FollowingVC") as! FollowingVC
        
        
        passData.getid = getid
        
        
        self.navigationController?.pushViewController(passData, animated: true)
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return CountryArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CountryCell
        cell.countryName.text = CountryArray[indexPath.row].Name
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        countryChoose.setTitle(CountryArray[indexPath.row].Name, for: .normal)
        tableView.isHidden = true
        
        
    }
    
    @IBAction func chooseCountryAction(_ sender: Any) {
        
        
        countryTableView.isHidden = false
        countryTableView.reloadData()
        
        
    }
    
    
    func UnfollowClick(_ title:String,message:String){
        
        let alertController = UIAlertController(title: title, message: message , preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            print("OK Pressed")
            
            let unfollowUrl = "https://testglostarsdevelopers.azurewebsites.net/api/Follower/Unfollowing?id=\(self.getid)"
            Alamofire.request(unfollowUrl, method: .post).responseJSON { response in
                
                print(response)
                
                self.getUserInfo()
            
        
        }
            
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
            print("Cancel Pressed")
            //self.dismiss(animated: true, completion: nil)
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    @IBAction func settingProfileAction(_ sender: Any) {
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "Setting") as! Setting
        
        self.navigationController?.pushViewController(passData, animated: true)
        
    }
    @IBAction func followingButtonAction(_ sender: Any) {
        
        followClick = "Yes"
        if is_mutual == true {
            
            let unfollowUrl = "https://testglostarsdevelopers.azurewebsites.net/api/Follower/Unfollowing?id=\(self.getid)"
            Alamofire.request(unfollowUrl, method: .post).responseJSON { response in
                
                print(response)
                
                self.getUserInfo()
            
                
            }
        }
        
        else if me_follow == true {
            
            
            UnfollowClick("Unfollow User ", message: "Do you want to unfollow user")
            

                
    
            
        }
        else {
            
            let url = "https://testglostarsdevelopers.azurewebsites.net/api/Follower/Following?id=\(getid)"
            
            print(getid)
            
            Alamofire.request(url, method: .post).responseJSON { response in

                print(response)
                
                self.getUserInfo()
 
            }
        }

        
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        
        let image = UIImage(named: "Icon_blank")
    
        return image
    }
    
    
    
    @IBAction func FollowingSend(_ sender: Any) {
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "FollowingVC") as! FollowingVC
        
        
        passData.getid = getid
        
        
        self.navigationController?.pushViewController(passData, animated: true)
        
    }
    
    
    
    func animatingPopUp()
    {
        UIView.animate(withDuration: 1, animations: {
            self.uploadButton.isHidden =  !self.uploadButton.isHidden
            
        }) { (true) in
            
            UIView.animate(withDuration: 1, animations: {
                self.achivementButton.isHidden = !self.achivementButton.isHidden
                
            }, completion: { (true) in
                UIView.animate(withDuration: 1, animations: {
                    
                    self.profileButton.isHidden = !self.profileButton.isHidden
                    self.homeButton.isHidden = !self.homeButton.isHidden
                    self.searchButton.isHidden = !self.searchButton.isHidden

                }, completion: { (true) in
                    self.notificationButton.isHidden = !self.notificationButton.isHidden
                    //                    let image1 = UIImage(named: "Circleopen")
                    //                    self.floatingMainButton.setBackgroundImage(image1, for: UIControlState.normal)
                    
                    
                    
                    
                })
            })
        }
        
        
        
    }
    
    @IBAction func mainButtonAction(_ sender: Any) {
        
        getUserInfo()
        animatingPopUp()
        
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        
        self.popUpViewEdit.alpha = 0
        hiddenFloatingButtons()

    }
    
    
    
    @IBAction func editproPic(_ sender: Any) {
        
        picker.allowsEditing = false
        fbFirstName = ""
        picker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            
            imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
            
    }
    }
    func displayMyAlertMessage(_ userMessage:String,title:String)
        
    {
        
        
        //    let banner = Banner(title: title, subtitle: userMessage , image: UIImage(named: "Icon"), backgroundColor: UIColor(red:48.00/255.0, green:174.0/255.0, blue:51.5/255.0, alpha:1.000))
        //        banner.dismissesOnTap = true
        //        banner.show(duration: 3.0)
        
        
        let myAlert = UIAlertController(title:title, message:userMessage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title:"Ok", style:UIAlertActionStyle.default, handler:nil);
        
        myAlert.addAction(okAction);
        self.present(myAlert, animated: true, completion: nil);
        
        
    }
    
    @IBAction func profileEditSaveData(_ sender: Any) {
        
        
        
        let image = profilePicEditOutLet.image
        //       imageProfile = image
        let imageData = UIImageJPEGRepresentation(image!, 0.2)!

        
//        let userid = userID
//        let Name = firstNameEditOutlet.text
        
        let parameters : NSDictionary = [
            "Id": getid,
            "AboutMe": descriptionEditNameOutlet.text!,
            "Interests":interestEditOutlet.text!,
            "Name":firstNameEditOutlet.text!,
            "LastName": lastNameEditOutlet.text!,
            "Ocupation":"Student",
            "OcupationOther": "Student",
            "Country" : countryChoose.title(for: .normal)!,
            "Location": cityLocationTextEdit.text!
            ]
//
//
//        let url = "https://testglostarsdevelopers.azurewebsites.net/Home/Edit"
//
//        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default)
//            .responseJSON{ response in
//
//            switch response.result {
//            case .success:
//                print(response)
//
//            case .failure(let error):
//                print(error)
//            }
//        }
      
        
        

        
        let URL = try! URLRequest(url: "https://testglostarsdevelopers.azurewebsites.net/Home/UserEdit", method: .post)
        
        Alamofire.upload(multipartFormData: {
            multipartFormData in
            
             multipartFormData.append(imageData, withName: "file", fileName: "file.png", mimeType: "image/png")
            
            for (key, value) in parameters {
                
            multipartFormData.append((value as! String).data(using:.utf8)!, withName: key as! String)
                
            }
            
            
            
        }, with: URL, encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    debugPrint("SUCCESS RESPONSE: \(response)")
                    
                    
                    
                    self.popUpViewEdit.alpha = 0
                    self.getUserInfo()
                    
                    self.displayMyAlertMessage("Profile Updated Successfully", title: "Profile Update")
                   
                }
            case .failure(let encodingError):
                // hide progressbas here
                print("ERROR RESPONSE: \(encodingError)")
                self.popUpViewEdit.isHidden = true
                self.getUserInfo()
                
                
            }
        })

        
        
    }
    
    

    
    @IBAction func profileEditOnClick(_ sender: Any) {
        
        popUpViewEdit.alpha = 1
        self.navigationItem.title = "Edit Profile"

        
//       //popUpViewEdit.alpha = 1
//        if popUpViewEdit.isHidden {
//            popUpViewEdit.isHidden = false
//        } else {
//            popUpViewEdit.isHidden = true
//        }
        
    }
    
    

    
    func getCount(){
        
    
            let params : Parameters = [
                "userId" : getid,
                ]
            print(getid)
            
            let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/user/\(getid)/0?"
            //https://testglostarsdevelopers.azurewebsites.net/api/
            Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default)
                
                
                .responseJSON{ response in
                    
                    if response.result.value != nil{
                        if let jsonRoot = response.result.value as? [String:Any]!{
                            
                            print(jsonRoot)
                            
                            if let userinfo = jsonRoot!["resultPayload"] as? [String: Any]!{
                                
                                let country = userinfo!["country"] as? String
                                //self.userLocation.text =  (userinfo!["country"] as? String)
                                //print(self.userLocation.text!)
                                
                                if self.userLocation.text == "null"{
                                    self.userLocation.text = ""
                                }
                                
                                //self.countryChoose.setTitle(country,for: .normal)
                                
                                //self.cityText.isHidden = true
                                //self.locationLeadingConstrants.constant = -8
                                
                                
                                if  (userinfo!["location"] as? String) != nil {
                                    self.cityText.text =  (userinfo!["location"] as? String)! + "," + country!
                                    
                                }
                                else {
                                    
                                    //                            if self.cityText.text == "<null>"{
                                    //                                self.cityText.text = ""
                                    //                            }
                                    
                                    print("No address")
                                    
                                    
                                }
                                
                                //
                                self.cityLocationTextEdit.text =  userinfo!["location"] as? String
                                
                               // self.aboutMe.text = userinfo!["aboutMe"] as? String
                                self.descriptionEditNameOutlet.text = userinfo!["aboutMe"] as? String
                                
                                    let totalComPic = userinfo!["totalCompetitonPic"] as! Int
                                    print(totalComPic)
                                    self.competitionPhotoTotal.text = "Total " + String(totalComPic)
                                    let totalPubPic = userinfo!["totalpublicPictures"] as! Int
                                    self.publicPhotoTotal.text = "Total " + String(totalPubPic)
                                
                                
//                                let totalMutPic = userinfo!["totalmutualFollowerPics"] as! Int
//                                let permHeGives = userinfo!["permissionheGives"] as! String
//                                let permGives = userinfo!["permissionmeGives"] as! String
//
//                                if permHeGives == "Insider"{
//
//                                    self.MutualPhotoTotal.isHidden = false
//                                    self.seeAllMut.isHidden = false
//                                    self.mutualPhotoOutlet.isHidden = false
//                                    // self.collectionView3.isHidden = false
//                                    //                            if totalMutPic == 0 {
//                                    //
//                                    //                                self.seeAllMut.isHidden = true
//                                    //
//                                    //                            }
//                                    //                            else{
//                                    //
//                                    //                                self.seeAllMut.isHidden = false
//                                    //
//                                    //
//                                    //                            }
//                                }
                                
                                // self.MutualPhotoTotal.text = "Total " + String(totalMutPic)
                                
                                
                                
                                //let userPicInfo = (userinfo!["profilePicURL"] as? String)!
                                
                                
                         
                                    
                                    
                                    
                                    
                              //  }
                                
                                
                                
                                
//
//                                self.userName.text = userinfo!["name"] as? String
//                                userNameR = self.userName.text!
                                
//                                self.navigationItem.title = "\(String(describing: userinfo!["name"] as! String))'s Profile"
//                                self.navigationController?.navigationBar.titleTextAttributes =
//                                    [NSForegroundColorAttributeName: UIColor.white,
//                                     NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 18)!]
//                                self.firstNameEditOutlet.text = userinfo!["name"] as? String
//                                self.lastname.text! = (userinfo!["lastName"] as? String)!
//
//                                self.interest.text = userinfo!["interests"] as? String
                                
//                                if  fbFirstName != ""{
//
//
//                                    if userID == self.getid{
//
//                                        let urlStringPic =  URL(string:String(describing: UserDefaults.standard.value(forKey: "pic")!))
//                                        self.imageProfile.af_setImage(withURL: urlStringPic! as URL)
//                                        self.profilePicEditOutLet.af_setImage(withURL: urlStringPic! as URL)
//                                    }
//                                    else{
//                                        let urlString  = NSURL(string: (userinfo!["profilePicURL"] as? String)!)
//                                        self.imageProfile.af_setImage(withURL: urlString! as URL)
//
//                                        self.profilePicEditOutLet.af_setImage(withURL: urlString! as URL)
//
//
//                                    }
//
//
//                                }
//
//                                else{
//                                    let urlString  = NSURL(string: (userinfo!["profilePicURL"] as? String)!)
//                                    if  userinfo!["profilePicURL"] as? String! == "/Content/Profile/Thumbs/   Male.jpg"{
//
//                                        self.imageProfile.image = UIImage(named: "male")
//
//                                        self.profilePicEditOutLet.image = UIImage(named: "male")
//
//                                    }
//                                    else if  userinfo!["profilePicURL"] as? String! == "/Content/Profile/Thumbs/   Female  .jpg"{
//
//                                        self.imageProfile.image = UIImage(named: "female")
//
//                                        self.profilePicEditOutLet.image = UIImage(named: "female")
//                                    }
//                                    else {
//
//                                        let urlString  = NSURL(string: (userinfo!["profilePicURL"] as? String)!)
//                                        self.imageProfile.af_setImage(withURL: urlString! as URL)
//
//                                        self.profilePicEditOutLet.af_setImage(withURL: urlString! as URL)
//                                    }
//
//                                }
//
//                                self.is_mutual = (userinfo!["isMutual"] as? Bool)!
//                                self.me_follow = (userinfo!["meFollow"] as? Bool)!
//                                self.he_follow = (userinfo!["heFollow"] as? Bool)!
                                
                                
//                                if self.is_mutual == true {
//
//                                    self.followingButtonOutlet.setTitle("Mutual", for: .normal)
//                                    //                                self.collectionView3.isHidden = true
//                                    //                                //self.seeAllMut.isHidden = false
//                                    //                                self.mutualPhotoOutlet.isHidden = true
//                                    self.followingButtonOutlet.backgroundColor = UIColor(hex: 0x660066, alpha: 1)
//                                    //                                self.MutualPhotoTotal.isHidden = true
//                                    //                                self.seeAllMut.isHidden = true
//
//
//
//                                }
                                
                                
//                                if self.is_mutual == false && self.me_follow == false && self.he_follow == false {
//
//                                    self.followingButtonOutlet.setTitle("Follow", for: .normal)
//
//                                    self.followingButtonOutlet.backgroundColor = UIColor(hex: 0x4d94ff, alpha: 1)
//                                    //self.collectionView3.isHidden = true
//                                    //self.seeAllMut.isHidden = true
//                                    if self.getid != String(describing: UserDefaults.standard.value(forKey: "id")!){
//
//                                        //self.MutualPhotoTotal.isHidden = true
//
//
//                                        self.buttomEditConstrainsMutual.constant = -195.0
//
//                                    }
//                                    else{
//
//                                        if self.MutualArray.count == 11 {
//
//
//                                            self.seeAllMut.isHidden = false
//
//
//                                        }
//
//
//                                        if self.MutualArray.count == 1 {
//
//                                            self.buttomEditConstrainsMutual.constant = -120.0
//
//
//                                        }
//                                        if self.MutualArray.count == 4  {
//
//                                            self.buttomEditConstrainsMutual.constant = -60.0
//
//                                        }
//                                        if self.MutualArray.count == 7  {
//
//                                            self.buttomEditConstrainsMutual.constant = 10.0
//
//                                        }
//                                    }
//
//                                }
//
//                                if self.is_mutual == false && self.me_follow == true && self.he_follow == false {
//
//                                    self.followingButtonOutlet.setTitle("Following", for: .normal)
//                                    self.followingButtonOutlet.backgroundColor = UIColor(hex: 0xcc99ff, alpha: 1)
//                                    self.MutualPhotoTotal.isHidden = true
//                                    //self.collectionView3.isHidden = true
//                                    //self.seeAllMut.isHidden = true
//                                    self.buttomEditConstrainsMutual.constant = -195.0
//                                }
//                                if self.is_mutual == false && self.me_follow == false && self.he_follow == true {
//
//                                    self.followingButtonOutlet.setTitle("Follow Back", for: .normal)
//                                    self.followingButtonOutlet.backgroundColor = UIColor.gray
//                                    self.MutualPhotoTotal.isHidden = true
//                                    //self.collectionView3.isHidden = true
//                                    self.seeAllMut.isHidden = true
//                                    self.buttomEditConstrainsMutual.constant = -195.0
//
//                                }
                                
                                
                                
                                
//                                self.imageProfile.setRounded()
//                                self.profilePicEditOutLet.setRounded()
//
//                                self.interestEditOutlet.text =  userinfo!["interests"] as? String
//                                self.lastNameEditOutlet.text = userinfo!["lastName"] as? String
//                                //            self.locationTextfield.text = userinfo["location"] as? String
//
//                                self.photoLabel.isHidden = false
//                                self.photoLabel.text! = String(describing: userinfo!["totalPicCount"] as! Int) + " photos"
//                                self.numFollowers.text! = String(describing: userinfo!["followersCount"] as! Int) + " Follower"
//                                self.numFlowing.text! = String(describing: userinfo!["followingCount"] as! Int) + " Following"
//
//                                if self.getid != String(describing: UserDefaults.standard.value(forKey: "id")!){
//
//                                    self.numFlowing.text! = String(describing: userinfo!["followersCount"] as! Int) + " Follower"
//
//                                    //self.collectionView3.isHidden = true
//                                    //self.seeAllMut.isHidden = true
//                                    self.editProfileOutlet.isHidden = true
//                                    self.settingOutlet.isHidden = true
//                                    // self.mutualPhotoOutlet.isHidden = true
//                                    self.usersFollowersView.alpha = 0
//                                    //self.MutualPhotoTotal.isHidden = true
//
//
//
//
//
//                                    self.viewAdd.isHidden = true
//                                    self.followingButtonOutlet.isHidden = false
//
//
//
//                                }
                                
                                
//
//                                if let recogprofile = userinfo!["recogprofile"] as? [String: Any]!{
//
//                                    self.recG.text = recogprofile!["grand"] as? String
//                                    self.recE.text =  recogprofile!["exhibition"] as? String
//                                    self.recM.text =  recogprofile!["monthly"] as? String
//                                    self.recW.text =  recogprofile!["weekly"] as? String
//
//                                    print(recogprofile)
//
//                                }
                            }
                        }
                    }
                    else{
                        
                        self.getCount()
                    }
        }
        
    }
    
    func getUserInfo(){
    
        
        let params : Parameters = [
            "userId" : getid,
            ]
        print(getid)
        //https://testglostarsdevelopers.azurewebsites.net/api/account/GetUserDetails?userId=e878a01d-4cc0-4458-b743-e593925ce495
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/account/GetUserDetails?"
        //https://testglostarsdevelopers.azurewebsites.net/api/
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default)
            
            
            .responseJSON{ response in
                
                if response.result.value != nil{
                if let jsonRoot = response.result.value as? [String:Any]!{
                    
                    print(jsonRoot)
                    
                    if let userinfo = jsonRoot!["resultPayload"] as? [String: Any]!{
                            
                        let country = userinfo!["country"] as? String
                        //self.userLocation.text =  (userinfo!["country"] as? String)
                        //print(self.userLocation.text!)
                        
                        if self.userLocation.text == "null"{
                            self.userLocation.text = ""
                        }
                            
                       //self.countryChoose.setTitle(country,for: .normal)

                                //self.cityText.isHidden = true
                                //self.locationLeadingConstrants.constant = -8
                            
                           
                        if  (userinfo!["location"] as? String) != nil {
                            self.cityText.text =  (userinfo!["location"] as? String)! + "," + country!
                                
                            }
                            else {
                            
//                            if self.cityText.text == "<null>"{
//                                self.cityText.text = ""
//                            }
                            
                                print("No address")
                            
                            
                            }

//
                        self.cityLocationTextEdit.text =  userinfo!["location"] as? String

                        self.aboutMe.text = userinfo!["aboutMe"] as? String
                        self.descriptionEditNameOutlet.text = userinfo!["aboutMe"] as? String
                         
                  

                        
                        //let totalMutPic = userinfo!["totalmutualFollowerPics"] as! Int
                        let permHeGives = userinfo!["permissionheGives"] as! String
                        let permGives = userinfo!["permissionmeGives"] as! String
                        
                        if permHeGives == "Insider"{
                            
                            self.MutualPhotoTotal.isHidden = false
                            self.seeAllMut.isHidden = false
                            self.mutualPhotoOutlet.isHidden = false
                           // self.collectionView3.isHidden = false
//                            if totalMutPic == 0 {
//
//                                self.seeAllMut.isHidden = true
//
//                            }
//                            else{
//
//                                self.seeAllMut.isHidden = false
//
//
//                            }
                        }

                           // self.MutualPhotoTotal.text = "Total " + String(totalMutPic)
                            
                      
                            
                        let userPicInfo = (userinfo!["profilePicURL"] as? String)!
       
                                        
        if  fbFirstName != ""{
            let urlStringPic =  URL(string:String(describing: UserDefaults.standard.value(forKey: "pic")!))
            self.profileButton.af_setBackgroundImage(for: [] , url: urlStringPic! )
            
        }
            else{
            if userPic == "/Content/Profile/Thumbs/   Male.jpg"{
                
                self.profileButton.setBackgroundImage(UIImage(named:"male"), for: .normal)
                
                
            }
            else if userPic == "/Content/Profile/Thumbs/   Female  .jpg"{
                
                self.profileButton.setBackgroundImage(UIImage(named:"female"), for: .normal)
            }
                else{
                    
                    
                    if userID == self.getid{
                    UserDefaults.standard.set( userPicInfo, forKey: "pic")
                    }
                    let urlStringPic =  URL(string:String(describing: UserDefaults.standard.value(forKey: "pic")!))
                    self.profileButton.af_setBackgroundImage(for: [] , url: urlStringPic! )
                }

                
                
                
                            }
            
     
        
          
          
                        self.userName.text = userinfo!["name"] as? String
                        userNameR = self.userName.text!
                            
                        self.navigationItem.title = "\(String(describing: userinfo!["name"] as! String))'s Profile"
            self.navigationController?.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.white,
            NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 18)!]
                        self.firstNameEditOutlet.text = userinfo!["name"] as? String
                        self.lastname.text! = (userinfo!["lastName"] as? String)!

                        self.interest.text = userinfo!["interests"] as? String
              
            if  fbFirstName != ""{
                
                
                if userID == self.getid{
                                
            let urlStringPic =  URL(string:String(describing: UserDefaults.standard.value(forKey: "pic")!))
                self.imageProfile.af_setImage(withURL: urlStringPic! as URL)
             self.profilePicEditOutLet.af_setImage(withURL: urlStringPic! as URL)
                }
                else{
                    let urlString  = NSURL(string: (userinfo!["profilePicURL"] as? String)!)
                    self.imageProfile.af_setImage(withURL: urlString! as URL)
                    
                    self.profilePicEditOutLet.af_setImage(withURL: urlString! as URL)
                    
                    
                }
                
                                
                }
                            
            else{
                let urlString  = NSURL(string: (userinfo!["profilePicURL"] as? String)!)
                if  userinfo!["profilePicURL"] as? String! == "/Content/Profile/Thumbs/   Male.jpg"{
                    
                    self.imageProfile.image = UIImage(named: "male")
                    
                    self.profilePicEditOutLet.image = UIImage(named: "male")
                    
                }
                else if  userinfo!["profilePicURL"] as? String! == "/Content/Profile/Thumbs/   Female  .jpg"{
                    
                    self.imageProfile.image = UIImage(named: "female")
                    
                    self.profilePicEditOutLet.image = UIImage(named: "female")
                }
                else {
                            
                    let urlString  = NSURL(string: (userinfo!["profilePicURL"] as? String)!)
            self.imageProfile.af_setImage(withURL: urlString! as URL)
                            
             self.profilePicEditOutLet.af_setImage(withURL: urlString! as URL)
                            }
                            
                            }
                            
                        self.is_mutual = (userinfo!["isMutual"] as? Bool)!
                        self.me_follow = (userinfo!["meFollow"] as? Bool)!
                        self.he_follow = (userinfo!["heFollow"] as? Bool)!

                            
                            if self.is_mutual == true {
                                
                            self.followingButtonOutlet.setTitle("Mutual", for: .normal)
//                                self.collectionView3.isHidden = true
//                                //self.seeAllMut.isHidden = false
//                                self.mutualPhotoOutlet.isHidden = true
                                self.followingButtonOutlet.backgroundColor = UIColor(hex: 0x660066, alpha: 1)
//                                self.MutualPhotoTotal.isHidden = true
//                                self.seeAllMut.isHidden = true

                                
                                
                            }
                            
                            
                            if self.is_mutual == false && self.me_follow == false && self.he_follow == false {
                                
                        self.followingButtonOutlet.setTitle("Follow", for: .normal)
                                
                                self.followingButtonOutlet.backgroundColor = UIColor(hex: 0x4d94ff, alpha: 1)
                                //self.collectionView3.isHidden = true
                                //self.seeAllMut.isHidden = true
                                if self.getid != String(describing: UserDefaults.standard.value(forKey: "id")!){
                                    
                            //self.MutualPhotoTotal.isHidden = true

                                
                       // self.buttomEditConstrainsMutual.constant = -195.0
                                    
                                }
                                else{
                                    
                                    if self.MutualArray.count == 11 {
                                        
                                        
                                        //self.seeAllMut.isHidden = false
 
                                
                                    }

                                    
                                    if self.MutualArray.count == 1 {
                                        
                                   // self.buttomEditConstrainsMutual.constant = -120.0

                                        
                                    }
                                    if self.MutualArray.count == 4  {
                                        
                                    //self.buttomEditConstrainsMutual.constant = -60.0
                                        
                                    }
                                    if self.MutualArray.count == 7  {
                                        
                                      // self.buttomEditConstrainsMutual.constant = 10.0
                                        
                                    }
                                }

                            }
                            
                            if self.is_mutual == false && self.me_follow == true && self.he_follow == false {
                                
                            self.followingButtonOutlet.setTitle("Following", for: .normal)
                                self.followingButtonOutlet.backgroundColor = UIColor(hex: 0xcc99ff, alpha: 1)
                                self.MutualPhotoTotal.isHidden = true
                                //self.collectionView3.isHidden = true
                                //self.seeAllMut.isHidden = true
                                //self.buttomEditConstrainsMutual.constant = -195.0
                            }
                            if self.is_mutual == false && self.me_follow == false && self.he_follow == true {
                                
                                self.followingButtonOutlet.setTitle("Follow Back", for: .normal)
                                self.followingButtonOutlet.backgroundColor = UIColor.gray
                                self.MutualPhotoTotal.isHidden = true
                                //self.collectionView3.isHidden = true
                                self.seeAllMut.isHidden = true
                                //self.buttomEditConstrainsMutual.constant = -195.0
                                
                            }
                            
             
                            
                            
                            self.imageProfile.setRounded()
                            self.profilePicEditOutLet.setRounded()

                        self.interestEditOutlet.text =  userinfo!["interests"] as? String
                        self.lastNameEditOutlet.text = userinfo!["lastName"] as? String
//            self.locationTextfield.text = userinfo["location"] as? String
                        
                        self.photoLabel.isHidden = false
                        self.photoLabel.text! = String(describing: userinfo!["totalPicCount"] as! Int) + " photos"
                        self.numFollowers.text! = String(describing: userinfo!["followersCount"] as! Int) + " Follower"
                        self.numFlowing.text! = String(describing: userinfo!["followingCount"] as! Int) + " Following"
                            
                            if self.getid != String(describing: UserDefaults.standard.value(forKey: "id")!){
                                
                                self.numFlowing.text! = String(describing: userinfo!["followersCount"] as! Int) + " Follower"
                                
                                //self.collectionView3.isHidden = true
                                //self.seeAllMut.isHidden = true
                                self.editProfileOutlet.isHidden = true
                                self.settingOutlet.isHidden = true
                               // self.mutualPhotoOutlet.isHidden = true
                                self.usersFollowersView.alpha = 0
                                //self.MutualPhotoTotal.isHidden = true

                                
                                
                                
                                
                                self.viewAdd.isHidden = true
                                self.followingButtonOutlet.isHidden = false

                                
                                
                            }
                            

                            
                        if let recogprofile = userinfo!["recogprofile"] as? [String: Any]!{
                                
                            self.recG.text = recogprofile!["grand"] as? String
                            self.recE.text =  recogprofile!["exhibition"] as? String
                            self.recM.text =  recogprofile!["monthly"] as? String
                            self.recW.text =  recogprofile!["weekly"] as? String
                                
                                print(recogprofile)
                                
                        }
                    }
                }
            }
                else{
                    
                    self.getUserInfo()
                }
        }
    
    
    }

    
    @IBAction func competitonPicAction(_ sender: Any) {
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileSearchCompitition") as! ProfileSearchCompitition
        
        
        passData.getSearchUserid = getid
        passData.getTheName = userNameR
        
        
        self.navigationController?.pushViewController(passData, animated: true)
        
    }
    
    
    @IBAction func publicPicAction(_ sender: Any) {
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileSearchPublic") as! ProfileSearchPublic
        
        
        passData.getSearchUserid = getid
        passData.getTheName = userNameR
        self.navigationController?.pushViewController(passData, animated: true)
        
        
    }
    
    @IBAction func mutualPicAction(_ sender: Any) {
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileSearchMutual") as! ProfileSearchMutual
        
        
        passData.getSearchUserid = getid
        passData.getTheName = userNameR

        
        
        self.navigationController?.pushViewController(passData, animated: true)
        
    }
    
    
    func GetCountryName(){
    
    
        let url = "https://restcountries.eu/rest/v2/all"
        Alamofire.request(url, method: .get , encoding: URLEncoding.default)
            
            
            .responseJSON{ response in
                
                
                print(response)
                
                if let jsonRoot = response.result.value as? [[String:Any]]!{
                    
                    print(jsonRoot)
                    for data in jsonRoot!{
                        
                        guard let mutpics = Mapper<CountryMapperArray>().map(JSON: data) else{
                            
                            continue
                        }
                        
                        self.CountryArray.append(mutpics)
                        print(mutpics.Name)
                    
                    
                }
        }
        
        
        
    }
    }
    
    func gettingNotification () {
        
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/account/GetUserFollowById?userId=\(getid)"
        Alamofire.request(url, method: .get , encoding: JSONEncoding.default)
            
            
            .responseJSON{ response in
                
                
                if response.result.value != nil{
                if let jsonRoot = response.result.value as? [String:Any]!{
                    
                    if let resultPayload = jsonRoot!["resultPayload"] as? [String : Any]{
                        
                        if let actNotify = resultPayload["followerList"] as? [[String : Any]] {
                            
                          self.numFollowers.text = String(actNotify.count) + " Follower"
                        
                        }
                        
                        if let actNotify2 = resultPayload["followingList"] as? [[String : Any]] {
                            
                           self.numFlowing.text =  String(actNotify2.count) + " Following"
                            
                        }
                        
                        
                        
                    }
                    
                }
                }
                else{
                    
                    self.gettingNotification()
                    
                }
        }
        
        
        
    }
    
    
    
    func userPicInfo(){
        
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/user/\(getid)/1"
        print(url)
        Alamofire.request(url, method: .get,encoding: URLEncoding.default)
            
            
            .responseJSON{ response in
                
                if response.result.value != nil{
                if let jsonRoot = response.result.value as? [String:Any]!{
                    
                    print(jsonRoot)

                    if let resultPaylaod = jsonRoot!["resultPayload"] as? [String:Any]!{
                        
                        

                        
                        print(resultPaylaod)
                        //self.numPhoto.text = String(describing: resultPaylaod["allPictureCount"] as! Int)
                        
                        
                        if let model = resultPaylaod!["model"] as? [String: Any]{
                            
                        
                            
                            if let pubPic = model["publicPictures"] as! [[String: Any]]! {

                                for data in pubPic{
                                    
                                    guard let pubpics = Mapper<PublicMapper>().map(JSON: data) else{
                                        
                                        continue
                                    }
                                    
                                    self.PublicArray.append(pubpics)
                                    print(pubpics)
                                    if let mutPic = model["mutualFollowerPictures"] as! [[String: Any]]! {
                                        
                                        
                                        for data in mutPic{
                                            
                                            guard let mutpics = Mapper<PublicMapper>().map(JSON: data) else{
                                                
                                                continue
                                            }
                                            
                                            self.PublicArray.append(mutpics)
                                            
                                            print(mutPic)
                                            // self.collectionView3.reloadData()
                                            
                                        }
                                        
                                        
                                        
                                    }

                                    self.collectionView2.reloadData()
                                    
                                }
                                
                            }
                            
                            if let comPic = model["competitionPictures"] as! [[String: Any]]! {
                                
                              print(comPic)
                                for data in comPic{
                                    
                                    guard let pics = Mapper<CompititionMapper>().map(JSON: data) else{
                                        
                                        continue
                                    }
                                    
                                    self.Compitition.append(pics)
                                    self.collectionView1.reloadData()
                                    
                                }
                               
                            
                            

                            
                            
                        }
                    }
                }
        }
                }
                else{
                    
                    self.userPicInfo()
                }
        
        
    }
    }
    
    
   
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
       // UIApplication.shared.statusBarView?.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.3)
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//
//        let foregroundHeight = foreground.contentSize.height - foreground.bounds.height
//        let percentageScroll = foreground.contentOffset.y / foregroundHeight
//        let backgroundHeight = background.contentSize.height - background.bounds.height
//
//        background.contentOffset = CGPoint(x: 0, y: backgroundHeight * percentageScroll)
//        hiddenFloatingButtons()
//
//    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        if collectionView == self.collectionView1{
            
            return Compitition.count

            
                
            }



        
//        else if collectionView == self.collectionView3{
//
//
//            if MutualArray.count <= 9{
//
//                seeAllMut.isHidden = true
//
//
//            }
//            if MutualArray.count > 9 {
//
//                seeAllMut.isHidden = false
//            }
//
//            return MutualArray.count
//        }
        else {

            if PublicArray.count == 1 {
                
                print("9")
               // height2.constant = 80
               // view2Height.constant = 110.0
                
            }
            if PublicArray.count == 4  {
                
                print("9")
               // height2.constant = 160
               // view2Height.constant = 182.0

                
            }
            if PublicArray.count == 7  {
                
                print("9")
               // height2.constant = 205
               // view2Height.constant = 255.0

                
            }

            if PublicArray.count <= 9{
                
                seeAllPub.isHidden = true
                
                
            }
            
            if PublicArray.count > 9 {
                
                seeAllPub.isHidden = false
            }
           
            return PublicArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        
        if collectionView == self.collectionView1 {
            
            let cell : CompititionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell1", for: indexPath) as! CompititionCell
            
             let comInfomation = self.Compitition[indexPath.row]
            
             let url = NSURL(string: comInfomation.picUrl)
            
            cell.compitiotionPic.sd_setShowActivityIndicatorView(true)
            cell.compitiotionPic.sd_setIndicatorStyle(.gray)
            cell.compitiotionPic.sd_setImage(with: url! as URL, placeholderImage: nil, options: .scaleDownLargeImages, completed: nil)

            hiddenFloatingButtons()
            collectionView.isScrollEnabled = false

        return cell
        }
//        else if collectionView == self.collectionView3 {
//
//            let cell : MutualCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell3", for: indexPath) as! MutualCell
//
//            let mutInfomation = self.MutualArray[indexPath.row]
//
//            let url2 = NSURL(string: mutInfomation.picUrl)
//
//            cell.mutImage.sd_setShowActivityIndicatorView(true)
//            cell.mutImage.sd_setIndicatorStyle(.gray)
//            cell.mutImage.sd_setImage(with: url2! as URL, placeholderImage: nil, options: .scaleDownLargeImages, completed: nil)
//            hiddenFloatingButtons()
//            collectionView.isScrollEnabled = false
//
//            return cell
//        }
        else{
            
            let cell : PublicCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell2", for: indexPath) as! PublicCell
            let PublicInfo = self.PublicArray[indexPath.row]
            let url3 = NSURL(string: PublicInfo.picUrl)
            cell.publicImg.sd_setShowActivityIndicatorView(true)
            cell.publicImg.sd_setIndicatorStyle(.gray)
            cell.publicImg.sd_setImage(with: url3! as URL, placeholderImage: nil, options: .scaleDownLargeImages, completed: nil)
            hiddenFloatingButtons()
            collectionView.isScrollEnabled = false

            return cell

        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 3 - 1
        //let height = collectionView.frame.height / (3 - 1) - 36
//        let height = CGFloat(201.0)
        return CGSize(width: width, height: width )
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if collectionView == self.collectionView1{
            
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileCompititionVC") as! ProfileCompititionVC
            
            passData.getID = getid
            passData.IndexNumber = CGFloat(indexPath.row)



            ///passData.getImageUrl = Compitition[indexPath.row].picUrl
           // passData.getID = Compitition[indexPath.row].photoId
            
            
            self.navigationController?.pushViewController(passData, animated: true)
        }
            
//        else if collectionView == self.collectionView3{
//
//            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfilePublicFeed") as! ProfilePublicFeed
//
//
//            //passData.getImageUrl = MutualArray[indexPath.row].picUrl
//            //passData.getID = MutualArray[indexPath.row].PhotoId
//            passData.TypePhotos = "mutual"
//            passData.userIDFeed = getid
//            passData.getUserName = userNameR
//            passData.indexNumber = CGFloat(indexPath.row)
//
//
//
//
//            self.navigationController?.pushViewController(passData, animated: true)
//
//        }
        else{
            
            
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfilePublicFeed") as! ProfilePublicFeed
            passData.userIDFeed = getid
            passData.TypePhotos = "public"
            passData.getUserName = userNameR
            passData.indexNumber = CGFloat(indexPath.row)


            //passData.getID = PublicArray[indexPath.row].photoId
            
            
            self.navigationController?.pushViewController(passData, animated: true)
            
        }
        //hiddenFloatingButtons()
        
        
        
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            return
        }
        
        profilePicEditOutLet.image = image
        
        
        let documentDirectory: NSString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first! as NSString
        
        let imageName = "temp"
        let imagePath = documentDirectory.appendingPathComponent(imageName)
        
        if let data = UIImageJPEGRepresentation(image, 80) {
            try? data.write(to: URL(fileURLWithPath: imagePath), options: [.atomic])
        }
        
        
        dismiss(animated: true, completion: {
            
            
            
        })
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func viewDidDisappear(_ animated: Bool) {
        
        navigationController?.isNavigationBarHidden = false

    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        descriptionEditNameOutlet.becomeFirstResponder()
        
        if descriptionEditNameOutlet.textColor! == UIColor.lightGray {
            descriptionEditNameOutlet.text! = ""
            descriptionEditNameOutlet.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if descriptionEditNameOutlet.text == "" {
            descriptionEditNameOutlet.text! = "About Me (max. 160 characters)"
            descriptionEditNameOutlet.textColor = UIColor.lightGray
            descriptionEditNameOutlet.resignFirstResponder()
        }
            
        else {
           // descriptionEditNameOutlet.text = "About Me (max. 160 characters)t"
            descriptionEditNameOutlet.textColor = UIColor.black
            descriptionEditNameOutlet.resignFirstResponder()
        }
    }
    

    @IBAction func interestButtonPressed(_ sender: Any) {
        
    }
}

