//
//  RoundButton.swift
//  Glostars
//
//  Created by Sanzid Ashan on 5/18/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//


//import UIKit
//
//@IBDesignable public class RoundButton: UIButton {
//    
//    @IBInspectable var borderColor: UIColor = UIColor.clear {
//        didSet {
//            layer.borderColor = borderColor.cgColor
//        }
//    }
//    
//    @IBInspectable var borderWidth: CGFloat = 2.0 {
//        didSet {
//            layer.borderWidth = borderWidth
//        }
//    }
//    
//    override public func layoutSubviews() {
//        super.layoutSubviews()
//        layer.cornerRadius = 0.5 * bounds.size.width
//        clipsToBounds = true
//    }
//}
