//
//  DesignableView.swift
//  Glostars
//
//  Created by Sanzid iOS on 5/21/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit

@IBDesignable class DesignableView : UIView {
    
    
    @IBInspectable var borderWidth : CGFloat = 0 {
        
        didSet{
            self.layer.borderWidth = borderWidth
            
        }
    }
    @IBInspectable var borderColor : UIColor = UIColor.clear {
        
        didSet{
            self.layer.borderColor = borderColor.cgColor
            
        }
    }
    
    @IBInspectable var cornerRadious : CGFloat = 0 {
        
        didSet{
            self.layer.cornerRadius = cornerRadious
            
        }
    }
}
