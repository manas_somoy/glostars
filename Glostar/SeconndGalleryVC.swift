//
//  SeconndGalleryVC.swift
//  Glostars
//
//  Created by sadidur rahman on 5/1/19.
//  Copyright © 2019 Sanzid Ashan. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import ObjectMapper
import SDWebImage

class SeconndGalleryVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{

    @IBOutlet weak var collectionView: UICollectionView!
    var ComptitionPi : Array<SecondPageMapper> = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getRequest()

        // Do any additional setup after loading the view.
        collectionView.delegate = self
        
        let backButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = backButton
        
        self.navigationItem.title = "Previous Competitions"
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.white,
             NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 18)!]
    }
    
    
    func getRequest(){
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/weeklycompetitionadmin/1"
        Alamofire.request(url).responseJSON { response in
            
            
            if response.result.value != nil{
                
                if let jsonRoot = response.result.value as? [String:Any]{
                    //print(jsonRoot)
                    
                    if let resultPayload = jsonRoot["resultPayload"] as? [[String:Any]]{
                        print(resultPayload)
                        for data in resultPayload {
                            
                            guard  let CompititionImage = Mapper<SecondPageMapper>().map(JSON: data) else {
                                continue
                                
                            }
                            
                            
                            self.ComptitionPi.append(CompititionImage)
                            
                            
                        }
                   
                        
                    }
                    self.collectionView.reloadData()

                }
            }
                
            else {
                
                print("Internet Error")
                self.getRequest()
                
            }
        }
        
    }

    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return ComptitionPi.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SecondCollectinCell", for: indexPath) as! SecondCollectinCell
        let path =  ComptitionPi[indexPath.row].themePicUrlMedium
        let url = URL(string: path)
        
        cell.comImage.af_setImage(withURL: url!)
        cell.week.text =  ComptitionPi[indexPath.row].themeName
        cell.title.text = "WEEK " + String(ComptitionPi[indexPath.row].weekNumber)

        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "GalleryThirdVC") as! GalleryThirdVC
        passData.weekNumber = ComptitionPi[indexPath.row].weekNumber
        passData.year = ComptitionPi[indexPath.row].year
        passData.imagepath = ComptitionPi[indexPath.row].themePicUrlMedium
        passData.themeName = ComptitionPi[indexPath.row].themeName


        self.navigationController?.pushViewController(passData, animated: true)
        
        
        
    }
 func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 2 - 8
        return CGSize(width: width, height: width)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
}
