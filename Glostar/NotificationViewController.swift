//
//  NotificationViewController.swift
//  Glostars
//
//  Created by Sanzid Ashan on 3/30/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import ObjectMapper
import ISEmojiView


var pictureIdNotification = 0
var pictureUrlNotification = ""

class NotificationViewController: UIViewController , UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource, DZNEmptyDataSetDelegate,FloatyDelegate {
    
    var fab = Floaty()
    

    @IBOutlet var searchButton: UIButton!
    
    @IBOutlet var notificationView: UIView!
    
    @IBOutlet var followerView: UIView!
    @IBOutlet weak var tableView2: UITableView!
    
    @IBOutlet weak var achivementButton: UIButton!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var uploadButton: UIButton!
    
    @IBOutlet weak var floatingMainButton: UIButton!

    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var cintainerView: UIView!
    
    let buttonBar = UIView()

    @IBOutlet weak var notificationTableTopConstraints: NSLayoutConstraint!
    
    var ActiveNotifyArray : Array< ActiveNotifyMapper > = []

    override func viewDidLoad() {
        super.viewDidLoad()
        layoutFAB()
        
        cintainerView.isHidden = true
        hiddenFloatingButtons()

       

        

        tableView2.tableFooterView = UIView(frame: .zero)

       


        
    }
    
    func layoutFAB() {
        let item = FloatyItem()
        item.buttonColor = UIColor(displayP3Red: 1, green: 1, blue: 1, alpha: 0.7)
        //item.circleShadowColor = UIColor.red
        //item.titleShadowColor = UIColor.blue
        
        //fab.addItem(title: "I got a title")
        //fab.addItem("I got a icon", icon: UIImage(named: "icShare"))
        fab.addItem("", icon: UIImage(named: "notif+"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "photo-camera"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "FAImageCropperVC") as! FAImageCropperVC
            
            self.navigationController?.pushViewController(passData, animated: true)
            
            
            
            self.fab.close()
            
        }
        fab.addItem("", icon: UIImage(named: "search-1"), titlePosition: .left) { (item) in
            
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "competiton"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "GaleryViewController") as! GaleryViewController
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "Profile_male"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            passData.getid = userID
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "home+"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "NewsVCViewController") as! NewsVCViewController
            
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        
        self.fab.sticky = true
        self.fab.paddingX = self.view.frame.width/20
        
        self.fab.fabDelegate = self
        
        //print(tableView.frame)
        
        self.view.addSubview(self.fab)
    }

    
    override func viewWillAppear(_ animated: Bool) {
        self.tableView2.emptyDataSetSource = self;
        self.tableView2.emptyDataSetDelegate = self;

       // hidingNavBarManager?.viewWillAppear(animated)

        //Remove unread notification count
        UserDefaults.standard.removeObject(forKey: appDelegate.notificationCount)
        
        gettingNotification()
        _ = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(self.update), userInfo: nil, repeats: false)
        followerView.isHidden = true
        
        //tableView.contentInset = UIEdgeInsetsMake(108, 0, 0, 0)
        tableView2.contentInset = UIEdgeInsetsMake(40.0, 0.0, 150.0, 0.0)

        


        floatingMainButton.setRounded()
        notificationButton.setRounded()
        uploadButton.setRounded()
        achivementButton.setRounded()
        homeButton.setRounded()
        profileButton.setRounded()
        searchButton.setRounded()

      

        if userPic == "/Content/Profile/Thumbs/   Male.jpg"{
            
            profileButton.setBackgroundImage(UIImage(named:"male"), for: .normal)
            
            
        }
        else if userPic == "/Content/Profile/Thumbs/   Female  .jpg"{
            
            profileButton.setBackgroundImage(UIImage(named:"female"), for: .normal)
        }
            
            
        else{
            let urlString = URL(string:String(describing: UserDefaults.standard.value(forKey: "pic")!))
            profileButton.af_setBackgroundImage(for: [] , url: urlString! )
            
        }
        
        hiddenFloatingButtons()
        if segmentController.selectedSegmentIndex == 1 {
            //segmentController.addUnderlineForSelectedSegment()

            self.navigationItem.title = "Notifications "
            self.navigationController?.navigationBar.titleTextAttributes =
                [NSForegroundColorAttributeName: UIColor.white,
                 NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 17)!]
            self.navigationController?.isNavigationBarHidden = false
            followerView.isHidden = false


            
        }
            //segmentController.addUnderlineForSelectedSegment()

        self.navigationItem.title = "Notifications"
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.white,
             NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 24)!]
        notificationView.isHidden = false
        self.navigationController?.isNavigationBarHidden = false

        
        
    }
    
//override var prefersStatusBarHidden: Bool {
       // return true
   // }
    
   
    func hiddenFloatingButtons(){
        
        uploadButton.isHidden = true
        achivementButton.isHidden = true
        profileButton.isHidden = true
        notificationButton.isHidden = true
        homeButton.isHidden = true
        searchButton.isHidden = true
        
        
    }
    
    @objc func update() {
        
        self.navigationController?.isNavigationBarHidden = false

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
       // hidingNavBarManager?.viewWillDisappear(animated)
        //self.navigationController?.setNavigationBarHidden(false, animated: animated)

        
        //self.navigationController?.isNavigationBarHidden = false

        
    }
    
//    func scrollViewShouldScrollToTop(_ scrollView: UIScrollView) -> Bool {
//        
//        hidingNavBarManager?.shouldScrollToTop()
//        
//        return true
//    }
//    func hidingNavigationBarManagerDidChangeState(_ manager: HidingNavigationBarManager, toState state: HidingNavigationBarState) {
//        
//        
//    }
//    
//    func hidingNavigationBarManagerDidUpdateScrollViewInsets(_ manager: HidingNavigationBarManager) {
//        
//        
//    }
//
//    func hidingNavigationBarManagerShouldUpdateScrollViewInsets(_ manager: HidingNavigationBarManager, insets: UIEdgeInsets) -> Bool {
//        return true
//        
//    }

    
//    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
//        
//        if(velocity.y>0) {
//            UIView.animate(withDuration: 2.5, delay: 0, options: UIViewAnimationOptions(), animations: {
//                self.navigationController?.setNavigationBarHidden(true, animated: true)
//                self.navigationController?.setToolbarHidden(true, animated: true)
//                print("Hide")
//            }, completion: nil)
//            
//        } else {
//            UIView.animate(withDuration: 2.5, delay: 0, options: UIViewAnimationOptions(), animations: {
//                self.navigationController?.setNavigationBarHidden(false, animated: true)
//                self.navigationController?.setToolbarHidden(false, animated: true)
//                print("Unhide")
//            }, completion: nil)    
//        }
//    }
    
    @IBAction func segmentAction(_ sender: UISegmentedControl) {
        

//        UIView.animate(withDuration: 0.3) {
//            self.buttonBar.frame.origin.x = (self.segmentController.frame.width / CGFloat(self.segmentController.numberOfSegments)) * CGFloat(self.segmentController.selectedSegmentIndex)
//        }

        
        if segmentController.selectedSegmentIndex == 0{
            

            notificationView.isHidden = false
            followerView.isHidden = true
            self.navigationController?.isNavigationBarHidden = false


            self.navigationItem.title = "Notifications"
            self.navigationController?.navigationBar.titleTextAttributes =
                [NSForegroundColorAttributeName: UIColor.white,
                 NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 18)!]

            hiddenFloatingButtons()


        }
        if segmentController.selectedSegmentIndex == 1 {
            
            notificationView.isHidden = true
            followerView.isHidden = false
            self.navigationItem.title = "Notifications"
            self.navigationController?.navigationBar.titleTextAttributes =
                [NSForegroundColorAttributeName: UIColor.white,
                 NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 18)!]
           
            self.navigationController?.isNavigationBarHidden = false

            hiddenFloatingButtons()

        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        navigationController?.navigationBar.isHidden = false

    }
    
    func animatingPopUp()
    {
        UIView.animate(withDuration: 1, animations: {
            self.uploadButton.isHidden =  !self.uploadButton.isHidden
            
        }) { (true) in
            
            UIView.animate(withDuration: 1, animations: {
                self.achivementButton.isHidden = !self.achivementButton.isHidden
                
            }, completion: { (true) in
                UIView.animate(withDuration: 1, animations: {
                    
                    self.profileButton.isHidden = !self.profileButton.isHidden
                    self.homeButton.isHidden = !self.homeButton.isHidden
                    self.searchButton.isHidden = !self.searchButton.isHidden
                    
                }, completion: { (true) in
                    self.notificationButton.isHidden = !self.notificationButton.isHidden
                    //                    let image1 = UIImage(named: "Circleopen")
                    //                    self.floatingMainButton.setBackgroundImage(image1, for: UIControlState.normal)
                    
                    
                    
                    
                })
            })
        }
        
        
        
    }
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        
        let text = NSAttributedString(string: "No Notifications")
        tableView2.separatorColor = UIColor.clear
        return text
        
    }

    
    @IBAction func onClinkProfile(_ sender: Any) {
        
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        self.navigationController?.isNavigationBarHidden = false
        
        passData.getid = userID
        
        self.navigationController?.pushViewController(passData, animated: true)
        
        
    }
    
    
    
    func gettingNotification () {
        
        ActiveNotifyArray.removeAll()
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/notifications/user/\(userID)"
        Alamofire.request(url, method: .get , encoding: JSONEncoding.default)
            
            
            .responseJSON{ response in
                
                if response.result.value != nil {

                
                if let jsonRoot = response.result.value as? [String:Any]!{
                    
                    //print(jsonRoot)
                    
                    
                    if let resultPayload = jsonRoot!["resultPayload"] as? [String : Any]{
                        
                        if let actNotify = resultPayload["activityNotifications"] as? [[String : Any]] {
                            
                            for data in actNotify{
                                
                                guard let Notify = Mapper<ActiveNotifyMapper>().map(JSON: data) else{
                                    
                                    continue
                                }
                                
                                self.ActiveNotifyArray.append(Notify)
                                if self.ActiveNotifyArray.count == 0{
                                    self.tableView2.emptyDataSetSource = self
                                }
                                //print(data)
                                
                            }

                            
                        }
                        
                        
                        
                    }
                    
                }
                self.tableView2.reloadData()
                    for subViews in self.tableView2.visibleCells {
                        let bottomAnimation = AnimationType.from(direction: .bottom, offSet: 30.0)
                        subViews.contentView.animateAll(withType: [bottomAnimation])
                    }



        }
                else{
                    
                    print("No Data")
                    
                }
        }
        
        
        
    }

    
    @IBAction func flotingAction(_ sender: Any) {
        
        self.navigationController?.isNavigationBarHidden = false

        animatingPopUp()
    }
    


    @IBAction func segmentControllerAction(_ sender: UISegmentedControl) {
        


        
        if segmentController.selectedSegmentIndex == 0 {

            cintainerView.isHidden = true
            notificationView.isHidden = false
            followerView.isHidden = true
            self.navigationController?.isNavigationBarHidden = false


            
        }
        
        else{
         
            cintainerView.isHidden = false
            notificationView.isHidden = true
            followerView.isHidden = false
            self.navigationController?.isNavigationBarHidden = false



        }
        
        
        
    }
    
  
    
    
    // return title string for each section
    
    
    // this method will be called by the table view to know the count of rows for each section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ActiveNotifyArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let NotifyInfo = ActiveNotifyArray[indexPath.row]
       
        let cell = tableView2.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! ActiveNotifyCell
        
        cell.descriptionOutlet.text = NotifyInfo.Description
        cell.userNameOutlet.text = NotifyInfo.UserName
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let date = dateFormatter.date(from: NotifyInfo.timeUpload) {

        cell.timeUploadOutlet.text = timeAgoSinceDate(date, currentDate: Date(), numericDates: true)
            
        }
        
        let url = NotifyInfo.userPicUrl
        
        if url == "/Content/Profile/Thumbs/   Male.jpg"{
            
            cell.proPicOutlet.image = UIImage(named: "male")
            
        }
        else if url == "/Content/Profile/Thumbs/   Female  .jpg"{
            
            cell.proPicOutlet.image = UIImage(named: "female")
            
        }
            
            
        else{
            

            
            let nsurl = NSURL(string: NotifyInfo.userPicUrl)
            cell.proPicOutlet.af_setImage(withURL: nsurl! as URL, placeholderImage: #imageLiteral(resourceName: "male"), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .crossDissolve(0.5), runImageTransitionIfCached: true, completion: nil)
            
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
      
        
        
        let url2 = NSURL(string: NotifyInfo.PictureUrl)
        
         cell.imageViewOutlet.af_setImage(withURL: url2! as URL, placeholderImage: UIImage.gif(name: "load_old"), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .crossDissolve(0.5), runImageTransitionIfCached: true, completion: nil)
        cell.OnnotifyPicClick = {
            
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "CommentPostVC") as! CommentPostVC
            passData.getID = self.ActiveNotifyArray[indexPath.row].pictureID
            passData.getImage = self.ActiveNotifyArray[indexPath.row].PictureUrl
            
            passData.photoId = self.ActiveNotifyArray[indexPath.row].pictureID
            
            self.navigationController?.pushViewController(passData, animated: true)
//            pictureIdNotification = self.ActiveNotifyArray[indexPath.row].pictureID
//            pictureUrlNotification = self.ActiveNotifyArray[indexPath.row].PictureUrl
//            self.navigationController?.setNavigationBarHidden(false, animated: true)
//
//
//            let customView = PhotoDetailsView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
//            self.view.addSubview(customView)
//
//
//
//
//            customView.frame.origin.y = 1000.0
//            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
//            let blurEffectView = UIVisualEffectView(effect: blurEffect)
//            blurEffectView.frame = self.view.bounds
//            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//            //self.view.addSubview(blurEffectView)
//
//            UIView.animate(withDuration: 1, animations: {
//                customView.frame.origin.y = 0
//                self.view.bringSubview(toFront: customView)
//
//
//            }, completion: { (true) in
//
//
//
//            })
     

//            let myViewController = NotificationViewController(nibName: "PhotoDetailsView", bundle: nil)
//            self.present(myViewController, animated: true, completion: nil)
            
//                    self.navigationController?.isNavigationBarHidden = false
//                    let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//                    let passData = MainStoryboard.instantiateViewController(withIdentifier: "NewsFeedDetailsVC") as! NewsFeedDetailsVC
//
//                    passData.getImageUrl = self.ActiveNotifyArray[indexPath.row].PictureUrl
//                    let urlString = URL(string:String(describing: UserDefaults.standard.value(forKey: "pic")!))
//                    passData.modalPresentationStyle = .overFullScreen
//                    passData.modalTransitionStyle = .crossDissolve
//                    passData.getprofilePicUrl = String(describing: urlString)
//
//                    //passData.getcomments = commentArray
//                     self.present(passData, animated: true, completion: nil)

        }
        
        cell.onProfileTapped = {
            

            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            
            passData.getid = self.ActiveNotifyArray[indexPath.row].notifyID
           
            self.navigationController?.setNavigationBarHidden(false, animated: true)

            
            self.navigationController?.pushViewController(passData, animated: true)
            
            
        }
        
        
        return cell
       
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        let passData = MainStoryboard.instantiateViewController(withIdentifier: "NewsFeedDetailsVC") as! NewsFeedDetailsVC
//        
//        passData.getID = ActiveNotifyArray[indexPath.row].pictureID
//        passData.getImageUrl = ActiveNotifyArray[indexPath.row].PictureUrl
//        
//        
//        //passData.getcomments = commentArray
//        self.navigationController?.pushViewController(passData, animated: true)
        
        
        
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
//        navigationController?.isNavigationBarHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)

    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        navigationController?.isNavigationBarHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)

    }
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//
//        hiddenFloatingButtons()
//        if scrollView.panGestureRecognizer.translation(in: scrollView.superview).y > 0 {
//            //scrolling downwards
//
//            navigationController?.isNavigationBarHidden = false
//
//
//            if scrollView.contentOffset.y < 0 {
//                //this means we are at top of the scrollView
//                navigationController?.isNavigationBarHidden = false
//
//
//
//            }
//        }
//        else {
//            //we are scrolling upward
//
//            navigationController?.isNavigationBarHidden = true
//
//
//        }
//    }
    
    
    func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!) years"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!)w"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1w"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!)d"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!)h"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1h"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!)m"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1m"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UISegmentedControl{
    func removeBorder(){
        let backgroundImage = UIImage.getColoredRectImageWith(color: UIColor.white.cgColor, andSize: self.bounds.size)
        self.setBackgroundImage(backgroundImage, for: .normal, barMetrics: .default)
        self.setBackgroundImage(backgroundImage, for: .selected, barMetrics: .default)
        self.setBackgroundImage(backgroundImage, for: .highlighted, barMetrics: .default)
        
        let deviderImage = UIImage.getColoredRectImageWith(color: UIColor.white.cgColor, andSize: CGSize(width: 1.0, height: self.bounds.size.height))
        self.setDividerImage(deviderImage, forLeftSegmentState: .selected, rightSegmentState: .normal, barMetrics: .default)
        self.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.gray], for: .normal)
        self.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor(red: 67/255, green: 129/255, blue: 244/255, alpha: 1.0)], for: .selected)
    }
    
    func addUnderlineForSelectedSegment(){
        removeBorder()
        let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
        let underlineHeight: CGFloat = 2.0
        let underlineXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth))
        let underLineYPosition = self.bounds.size.height - 1.0
        let underlineFrame = CGRect(x: underlineXPosition, y: underLineYPosition, width: underlineWidth, height: underlineHeight)
        let underline = UIView(frame: underlineFrame)
        underline.backgroundColor = UIColor.red
        underline.tag = 1
        self.addSubview(underline)
    }
    
    func changeUnderlinePosition(){
        guard let underline = self.viewWithTag(1) else {return}
        let underlineFinalXPosition = (self.bounds.width / CGFloat(self.numberOfSegments)) * CGFloat(selectedSegmentIndex)
        UIView.animate(withDuration: 0.1, animations: {
            underline.frame.origin.x = underlineFinalXPosition
        })
    }
}

extension UIImage{
    
    class func getColoredRectImageWith(color: CGColor, andSize size: CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        let graphicsContext = UIGraphicsGetCurrentContext()
        graphicsContext?.setFillColor(color)
        let rectangle = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
        graphicsContext?.fill(rectangle)
        let rectangleImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return rectangleImage!
    }
}


