//
//  ViewController.swift
//  File 2
//
//  Created by Sanzid Ashan on 5/14/16.
//  Copyright © 2016 Sanzid Ashan. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import Alamofire
//import GGLSignIn
import GoogleSignIn
import SwiftMessages
var fbFirstName = ""
var fbLastName = ""
var fbEmail = ""
var fbGender = ""
var fbUrl = ""
var fbID = ""
var GmFirstName = ""
var GmLastName = ""
var GmEmail = ""
var GmGender = ""
var GmUrl = ""
var GmID = ""
class LoginViewController2: UIViewController,FBSDKLoginButtonDelegate,GIDSignInUIDelegate,GIDSignInDelegate {
    
    
    @IBOutlet var glostarsTitle: UIImageView!
    
    @IBOutlet var registerButton: DesignableButton!
    
    @IBOutlet var alreadyAccountTitle: UILabel!
    
    @IBOutlet var loginTitle: UIButton!
    
    @IBOutlet var backgroundImage: UIImageView!
    
    @IBOutlet var googleImg: UIImageView!
    
  
    @IBOutlet var facebookImage: UIImageView!
    @IBOutlet weak var continueWithGoogleLabel: UILabel!
    @IBOutlet weak var continueWithFbLabel: UILabel!
    @IBOutlet weak var signInButton: GIDSignInButton!
    
    var dict : [String : AnyObject]!

    public func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
    
        
        print("Logged in..")
        if((FBSDKAccessToken.current()) != nil){
            
            DispatchQueue.main.async(execute: {
                FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email,gender"]).start(completionHandler: { (connection, result, error) -> Void in
                    if (error == nil){
                        self.dict = result as! [String : AnyObject]
                        print(result!)
                        print(self.dict)
                        
                        let first_name = self.dict["first_name"] as! String
                        print(first_name)
                        fbFirstName = first_name
                        let last_name = self.dict["last_name"] as! String
                        print(last_name)
                        fbLastName = last_name
                        //let email = self.dict["email"] as! String
                        //print(email)
                        ////fbEmail = email
                        //let gender = self.dict["gender"] as! String
                        //print(gender)
                        //fbGender = gender
                        let id = self.dict["id"] as! String
                        print(id)
                        fbID = id
                        
                        if let picture = self.dict ["picture"] as! [String:Any]!{
                            
                            for data in picture{
                                
                                if let picData = picture["data"] as! [String:Any]!{
                                    
                                    print(picData)
                                    
                                    let url = picData["url"] as! String
                                    fbUrl = url
                                    print(fbUrl)
                                    
                                    self.RegisterUser()
                                }
                            }
                        }
                    }
                })
            })
        }
    }

    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        
        

    }
    override func viewWillDisappear(_ animated: Bool) {
        self.showLoader("")

    }
    
    override func viewDidAppear(_ animated: Bool)
    {

        
        navigationController?.navigationBar.isHidden = true
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        
        // TODO: Change where the log in button is positioned in your view
        //logInButton.frame = CGRectMake(39, 455, 238, 43)
        
        // self.view.addSubview(logInButton)
        
        signInButton.alpha = 0.1
        signInButton.style = .wide
        
        facebookImage.layer.cornerRadius = 11.0
        googleImg.layer.cornerRadius = 11.0
        
        if (FBSDKAccessToken.current() == nil)
        {
            print("Not logged in..")
            self.hideLoader()
        }
        else
        {
            print("Logged in..")
            self.hideLoader()

            

            
            
            //self.performSegue(withIdentifier: "aa", sender: NewViewController.self)
        }
        
        let loginButton = FBSDKLoginButton(type: .roundedRect)
        loginButton.readPermissions = ["public_profile", "email", "user_friends"]
        
        loginButton.frame = CGRect(x: view.bounds.width/2, y: view.bounds.height/2, width: view.bounds.width - 32 , height: 40)
        loginButton.delegate = self
        loginButton.center = view.center
        loginButton.alpha = 0.01
        let imageFb = UIImage(named: "FacebookImg")
        var mainImageView = UIImageView(image:imageFb)
        mainImageView.contentMode = .scaleAspectFit

        loginButton.setImage(imageFb, for: .normal)
        
        loginButton.setTitle("Continue  Facebook", for: .normal)
        
        self.view.addSubview(loginButton)

        if((FBSDKAccessToken.current()) != nil){
            
            DispatchQueue.main.async(execute: {

            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email,gender"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    self.dict = result as! [String : AnyObject]
                    print(result!)
                    print(self.dict)
                }
            })
            })
        }
        
        
        
    }

    
    @IBAction func LoginButtonAction(_ sender: Any) {
        
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        
        let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "LoginVC")
        
        let nvc = UINavigationController(rootViewController: nextViewController)
        present(nvc, animated: false, completion: nil)
        
        
    }
    
    @IBAction func SignUpButtonAction(_ sender: Any) {
        
        
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        
        let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "RegisterVC")
        
        let nvc = UINavigationController(rootViewController: nextViewController)
        present(nvc, animated: false, completion: nil)

    }
    func warningMessage(title:String,subtitle:String){
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: .messageView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.warning)
        view.button?.isHidden = true
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        // Set message title, body, and icon. Here, we're overriding the default warning
        // image with an emoji character.
        let iconText = ["🤔", "😳", "🙄", "😶"].sm_random()!
        view.configureContent(title: title, body: subtitle, iconText: iconText)
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        
        // Show the message.
        SwiftMessages.show(view: view)
        
    }
    func successMessage(title:String,subtitle:String){
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: .messageView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.success)
        view.button?.isHidden = true
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        
        // Set message title, body, and icon. Here, we're overriding the default warning
        // image with an emoji character.
        let iconText = ["🤔", "😳", "🙄", "😶"].sm_random()!
        view.configureContent(title: title, body: subtitle, iconText: iconText)
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        
        SwiftMessages.show(view: view)
        
    }

    
    
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        
        
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        viewController.dismiss(animated: true, completion: nil)
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            
            DispatchQueue.main.async(execute: {

            
            let userId = user.userID!
            print(userId)
            GmID = userId
            if user.profile.hasImage {
            let ProFile = user.profile.imageURL(withDimension: 200)!
            print(ProFile)
                
            }
            
            let idToken = user.authentication.idToken // Safe to send to the server
            let lastName = user.profile.familyName!
            print(lastName)
            GmLastName = lastName
            let firstName = user.profile.givenName!
            print(firstName)
            GmFirstName = firstName
            let email = user.profile.email!
            print(email)
            GmEmail = email
            fbFirstName = ""
            
            let gplusapi = "https://www.googleapis.com/oauth2/v3/userinfo?access_token=\(user.authentication.accessToken!)"
            let url = NSURL(string: gplusapi)!
            
            
            let request = NSMutableURLRequest(url: url as URL)
            request.httpMethod = "GET"
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            
            let session = URLSession.shared
            
            
            session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
                do {
                    let userData = try JSONSerialization.jsonObject(with: data!, options:[]) as? [String:AnyObject]
                    
                    if userData!["picture"] as? String != nil{
                    let picture = userData!["picture"] as! String
                    print(picture)
                    GmUrl = picture
                    }
                    if userData!["gender"] as? String != nil{
                    let gender = userData!["gender"] as! String
                    print(gender)
                    GmGender = gender
                    }
                    
                } catch {
                    NSLog("Account Information could not be loaded")
                }
                
            }).resume()
            
            
                self.RegisterUserGmail()
            
            })
        } else {
            
                // Cancelled task
                
            }
    
    }
    

    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
        //SignOut
        if error != nil {
            //handle error
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: NSError!)
    {
        if error == nil
        {
            print("Login complete.")
            //self.performSegue(withIdentifier: "aa", sender: NewViewController.self)
        }
        else
        {
            print(error.localizedDescription)
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!)
    {
        print("User logged out...")
    }
    func RegisterUserGmail (){
        self.showLoader("")

        
        print(fbFirstName)
        print(fbLastName)
        
        
        let parameters: [String : Any]? = [
            "Name":GmFirstName,
            "LastName":GmLastName,
            "Gender": GmGender,
            "Email": GmEmail,
            "BirthdayYear":1993,
            "BirthdayMonth":12,
            "BirthdayDay": 12,
            "Password": GmID ,
            ]
        
        //        Alamofire.request(URL_USER_REGISTER, method: .post, parameters: parameters).responseJSON
        //            {
        //                response in
        //                //printing response
        //                print(response)
        //
        //                let alertController = UIAlertController(title: "REGISTRATION SUCCESSFUL!", message: nil, preferredStyle: .alert);
        //
        //                alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
        //
        //                //getting the json value from the server
        ////                if let result = response.result.value {
        ////
        ////                    //converting it as NSDictionary
        ////                    let jsonData = result as! NSDictionary
        ////
        ////                    //displaying the message in label
        ////                    print(jsonData)
        ////
        ////
        ////
        ////                }
        //        }
        //
        //        Alamofire.request(URL_USER_REGISTER, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
        //
        //            switch(response.result) {
        //            case .success(_):
        //                if response.result.value != nil{
        //                    print(response.result.value!)
        //                }
        //                break
        //
        //            case .failure(_):
        //                print(response.result.error!)
        //                break
        //
        //            }
        //        }
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/Account/Signup"
        
        
        
        
        Alamofire.request(url, method:.post, parameters:parameters,encoding: URLEncoding.httpBody, headers:nil).responseJSON { response in
            
            if response.result.value != nil {
                switch response.result {
                    
                case .success:
                    print(response)
                    
                    //self.performSegue(withIdentifier: "aa", sender: LoginVC.self)
                    if let jsonRoot = response.result.value as? [String:Any]!{
                        
                        
                        //print(jsonRoot)
                        
                        
                        
                        
                        if response.result.value != nil {
                            
                            if jsonRoot!["user"] as? [String: Any]! != nil{
                                
                                
                                if let userinfo = jsonRoot!["user"] as? [String: Any]!{
                                    
                                    if userinfo == nil{
                                        
                                        DispatchQueue.main.async{
                                            self.warningMessage(title: "Warning", subtitle: "Email already taken")
                                            self.hideLoader()
                                        GIDSignIn.sharedInstance().signOut()
                                            
                                        }

                                        
                                    }
                                    
                                    
                                    
                                    if userinfo != nil{
                                        
                                        
                                        self.getRequestGmail()
                                        
                                    }
                                    
                                    
                                    
                                    //self.view.addSubview(self.popUpView)
                                    
                                    //self.performSegue(withIdentifier: "a", sender: NewsFeedDetailsVC.self)
                                    
                                    
                                    //print(self.userInfo.userName = "email")
                                    
                                }
                                
                            }else {
                                
                                print("Found Crash")
                            }
                        }
                        else {
                            
                            print("Error Found")
                        }
                        
                        
                    }
                case .failure(let error):
                    print(error)
                }
                
            }
            else {
                
                print("Check Your Internet Connection")
            }
            
            
        }
        
        
    }

    
      func RegisterUser (){
        self.showLoader("")
        
        print(fbFirstName)
        print(fbLastName)
    
    
    let parameters: [String : Any]? = [
    "Name":fbFirstName,
    "LastName":fbLastName,
    "Gender": fbGender,
    "Email": fbEmail,
    "BirthdayYear":1993,
    "BirthdayMonth":12,
    "BirthdayDay": 12,
    "Password": fbID ,
    ]
    
    //        Alamofire.request(URL_USER_REGISTER, method: .post, parameters: parameters).responseJSON
    //            {
    //                response in
    //                //printing response
    //                print(response)
    //
    //                let alertController = UIAlertController(title: "REGISTRATION SUCCESSFUL!", message: nil, preferredStyle: .alert);
    //
    //                alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
    //
    //                //getting the json value from the server
    ////                if let result = response.result.value {
    ////
    ////                    //converting it as NSDictionary
    ////                    let jsonData = result as! NSDictionary
    ////
    ////                    //displaying the message in label
    ////                    print(jsonData)
    ////
    ////
    ////
    ////                }
    //        }
    //
    //        Alamofire.request(URL_USER_REGISTER, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
    //
    //            switch(response.result) {
    //            case .success(_):
    //                if response.result.value != nil{
    //                    print(response.result.value!)
    //                }
    //                break
    //
    //            case .failure(_):
    //                print(response.result.error!)
    //                break
    //
    //            }
    //        }
    
    let url = "https://testglostarsdevelopers.azurewebsites.net/Account/Signup"
    
    
    
    
    Alamofire.request(url, method:.post, parameters:parameters,encoding: URLEncoding.httpBody, headers:nil).responseJSON { response in
    
    if response.result.value != nil {
    switch response.result {
    
    case .success:
    print(response)
    
    //self.performSegue(withIdentifier: "aa", sender: LoginVC.self)
    if let jsonRoot = response.result.value as? [String:Any]!{
    
    
    //print(jsonRoot)
    
    
    
    
    if response.result.value != nil {
    
        if jsonRoot!["user"] as? [String: Any]! != nil{
    
        
            if let userinfo = jsonRoot!["user"] as? [String: Any]!{
    
    if userinfo == nil{
    
        self.warningMessage(title: "Warning", subtitle: "Email already taken")
        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
        self.hideLoader()



    
    }
    

    
    if userinfo != nil{
    
    
        self.getRequest()
    
    }
    
    
    
    //self.view.addSubview(self.popUpView)
    
    //self.performSegue(withIdentifier: "a", sender: NewsFeedDetailsVC.self)
    
    
    //print(self.userInfo.userName = "email")
    
    }
    
    }else {
    
    print("Found Crash")
    }
    }
    else {
    
    print("Error Found")
    }
    
    
    }
    case .failure(let error):
    print(error)
    }
    
    }
    else {
    
    print("Check Your Internet Connection")
    }
    
    
    }
    
    
    }
    
    func  getRequestGmail(){
        
        
        
        let params : Parameters = [
            "grant_type" : "password",
            "password" : GmID,
            "username" : GmEmail,
            
            
            ]
        
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/Token"
        Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.httpBody)
            .responseJSON{ response in
                
                if response.result.value != nil{
                    
                    if let jsonRoot = response.result.value as? [String:Any]!{
                        
                        print(jsonRoot)
                        
                        if jsonRoot!["access_token"] != nil  && jsonRoot!["token_type"] != nil {
                            accessToken = jsonRoot!["access_token"] as! String!
                            UserDefaults.standard.set(accessToken, forKey: "access")
                            
                            tokenType = jsonRoot!["token_type"] as! String!
                            UserDefaults.standard.set(tokenType, forKey: "token")
                            UserDefaults.standard.set(GmEmail, forKey: "E")
                            UserDefaults.standard.set(GmID, forKey: "P")

                            self.getDataGmail()
                            //self.localStorage()
                            
                            
                        }
                        else {
                            self.showLoader("")
                            
                            //                            self.displayMyAlertMessage("Wrong User Name or password ")
                        }
                        
                        
                        // self.localStorage()
                        
                        
                        
                    }
                    else {
                        self.showLoader("")

                        
                        //                        self.displayMyAlertMessage(" Please Check Your Email and Password ")
                    }
                }
                
                
        }
        
    }

    
    func  getRequest(){
        
        
        
        let params : Parameters = [
            "grant_type" : "password",
            "password" : fbID,
            "username" : fbEmail,
            
            
            ]
        
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/Token"
        Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.httpBody)
            .responseJSON{ response in
                
                if response.result.value != nil{
                    
                    if let jsonRoot = response.result.value as? [String:Any]!{
                        
                        print(jsonRoot)
                        
                        if jsonRoot!["access_token"] != nil  && jsonRoot!["token_type"] != nil {
                            accessToken = jsonRoot!["access_token"] as! String!
                            UserDefaults.standard.set(accessToken, forKey: "access")
                            
                            tokenType = jsonRoot!["token_type"] as! String!
                            UserDefaults.standard.set(tokenType, forKey: "token")
                            UserDefaults.standard.set(GmEmail, forKey: "E")
                            UserDefaults.standard.set(GmID, forKey: "P")
                            self.getData()
                            //self.localStorage()
                            
                            
                        }
                        else {
                            
                            self.warningMessage(title: "Warning", subtitle: "Email already taken")
                            let loginManager = FBSDKLoginManager()
                            loginManager.logOut()
                            self.hideLoader()

                            
                        }
                        
                        
                        // self.localStorage()
                        
                        
                        
                    }
                    else {
                        
                        self.warningMessage(title: "Warning", subtitle: "Please Check Your Email and Password")
                        self.hideLoader()

                    }
                }
                
                
        }
        
    }
    
    func displayMyAlertMessage(_ userMessage:String)
    {
        
        let myAlert = UIAlertController(title:"Warning", message:userMessage, preferredStyle: UIAlertControllerStyle.alert);
        
        let okAction = UIAlertAction(title:"Ok", style:UIAlertActionStyle.default, handler:nil);
        
        myAlert.addAction(okAction);
        self.present(myAlert, animated: true, completion: nil);
        
        
    }
    func  getDataGmail() {
        
        
        
        
        let params2 : Parameters = [
            
            "userEmail" : GmEmail ,
            
            ]
        
        let url2 = "https://testglostarsdevelopers.azurewebsites.net/api/account/GetUserInfo"
        
        Alamofire.request(url2, method: .get, parameters: params2, encoding: URLEncoding.default)
            
            
            .responseJSON{ response2 in
                
                if response2.result.value != nil {
                    
                    if let jsonRoot = response2.result.value as? [String:Any]!{
                        print(jsonRoot)
                        
                        if let userinfo = jsonRoot!["resultPayload"] as? [String: Any]!{
                            
                            
                            //gender = userinfo["gender"] as! String
                            print(gender)
                            
                            userID = userinfo!["userId"] as! String
                            print(userID)
                            UserDefaults.standard.set(userID, forKey: "id")
                            UserDefaults.standard.set(GmUrl, forKey: "pic")
                            
                            
                            
                            
                            
                            
                            let transition = CATransition()
                            transition.duration = 0.3
                            transition.type = kCATransitionPush
                            transition.subtype = kCATransitionFromRight
                            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                            self.view.window!.layer.add(transition, forKey: kCATransition)
                            
                            let nextViewController =                                  self.storyboard!.instantiateViewController(withIdentifier:"NewsVCViewController")
                            
                            let nvc = UINavigationController(rootViewController: nextViewController)
                            self.present(nvc, animated: false, completion: nil)
                            
                            
                            //print(self.userInfo.userName = "email")
                            
                        }
                        
                        
                    }
                    else{
                        self.showLoader("")
                        
                        self.warningMessage(title: "Warning", subtitle: "Please Check Your Email and Password")

                    }
                }
                
                
                
                
        }
        
        
        
    }


    
    func  getData() {
        
        
        
        
        let params2 : Parameters = [
            
            "userEmail" : fbEmail,
            
            ]
        
        let url2 = "https://testglostarsdevelopers.azurewebsites.net/api/account/GetUserInfo"
        
        Alamofire.request(url2, method: .get, parameters: params2, encoding: URLEncoding.default)
            
            
            .responseJSON{ response2 in
                
                if response2.result.value != nil {
                    
                    if let jsonRoot = response2.result.value as? [String:Any]!{
                        print(jsonRoot)
                        
                        if let userinfo = jsonRoot!["resultPayload"] as? [String: Any]!{
                            
                            
                            gender = userinfo!["gender"] as! String
                            print(gender)
                            
                            userID = userinfo!["userId"] as! String
                            print(userID)
                            UserDefaults.standard.set(userID, forKey: "id")
                            UserDefaults.standard.set(fbUrl, forKey: "pic")
 
                            let transition = CATransition()
                            transition.duration = 0.3
                            transition.type = kCATransitionPush
                            transition.subtype = kCATransitionFromRight
                            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                            self.view.window!.layer.add(transition, forKey: kCATransition)
                            
                            let nextViewController =                                  self.storyboard!.instantiateViewController(withIdentifier:"NewsVCViewController")
                            
                            let nvc = UINavigationController(rootViewController: nextViewController)
                            self.present(nvc, animated: false, completion: nil)
                            
                            
                            //print(self.userInfo.userName = "email")
                            
                        }
                        
                        
                    }
                    else{
                        
                        self.warningMessage(title: "Warning", subtitle: "Please Check Your Email and Password")
                        self.hideLoader()
                }
            }
        }
    }

    
    
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    // Start Editing The Text Field
    
    // Finish Editing The Text Field
    
    // Hide the keyboard when the return key pressed
    
    // Move the text field in a pretty animation!
}




