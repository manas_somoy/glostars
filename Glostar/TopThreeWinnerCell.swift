//
//  TopThreeWinnerCell.swift
//  Glostars
//
//  Created by sadidur rahman on 23/1/19.
//  Copyright © 2019 Sanzid Ashan. All rights reserved.
//

import UIKit

class TopThreeWinnerCell: UICollectionViewCell {
    
    @IBOutlet weak var competitionImageView: UIImageView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var reportButton: UIButton!
    @IBOutlet weak var competitionPostionWithVoteNumberLabel: UILabel!
    
    @IBOutlet weak var imageViewTop: UIImageView!
    
    @IBOutlet weak var reportView: RoundUIView!
    
    var onReportTap: (() -> Void)? = nil
    var onProfileTap: (() -> Void)? = nil
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        userImageView.setRounded()
    }
    
    
    @IBAction func userProfileButtonPressed(_ sender: Any) {
        if let onProfileTap = self.onProfileTap {
            onProfileTap()
        }
    }
    
    @IBAction func reportButtonPressed(_ sender: Any) {
        if let onReportTap = self.onReportTap {
            onReportTap()
        }
    }
    
}
