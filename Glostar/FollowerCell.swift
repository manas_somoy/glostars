//
//  FollowerCell.swift
//  Glostars
//
//  Created by Sanzid iOS on 7/12/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit

class FollowerCell: UITableViewCell {
    
    @IBOutlet weak var statusButtonOutlet: DesignableButton!
    
    @IBOutlet weak var permissionButton: UIButton!
    
    @IBOutlet weak var proPicImage: UIImageView!
    
    @IBOutlet weak var nameProfile: UILabel!
    
    @IBOutlet var followButtonOutlet: DesignableButton!
    
    @IBOutlet weak var descriptionOutlet: UILabel!
    
    @IBOutlet var lastname: UILabel!
    
    @IBOutlet weak var uploadTime: UILabel!
    
    var onFollowTapped: (() -> Void)? = nil
    var onImageTapped: (() -> Void)? = nil
    var onLabelTapped: (() -> Void)? = nil

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //proPicImage.setRounded()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        proPicImage.setRounded()

        
        // Configure the view for the selected state
    }
    @IBAction func onFollowTapped(_ sender: Any){
        
        if let onFollowTapped = self.onFollowTapped {
            
            onFollowTapped()
            
        }
        

    }
    @IBAction func onImageTapped(_ sender: Any){
        
        if let onImageTapped = self.onImageTapped {
            
            onImageTapped()
            
        }
        
        
    }
    @IBAction func onLabelTapped(_ sender: Any){
        
        if let onLabelTapped = self.onLabelTapped {
            
            onLabelTapped()
            
        }
        
        
    }
    
    
}
