//
//  RecognisitionCell.swift
//  Glostars
//
//  Created by Sanzid Ashan on 4/7/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit

class RecognisitionCell: UITableViewCell {

    @IBOutlet weak var recogImage: UIImageView!
    
    @IBOutlet var prizeNames: UILabel!
    @IBOutlet var prizeText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
