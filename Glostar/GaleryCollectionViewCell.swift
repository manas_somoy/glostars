//
//  GaleryCollectionViewCell.swift
//  Glostars
//
//  Created by Sanzid Ashan on 4/7/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit

class GaleryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgImage: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    
}
