//
//  TermVC.swift
//  Glostars
//
//  Created by Sanzid Ashan on 4/18/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit

class TermVC: UIViewController {

    //@IBOutlet var termsTextView: UITextView!
    @IBOutlet weak var termsLabel: UILabel!
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = false
        getData == "Compitition Terms"
        self.title = getData
        print(getData)
        let url = Bundle.main.url(forResource: "Competition Terms", withExtension: "html")
        webView.loadRequest(URLRequest(url: url!))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
