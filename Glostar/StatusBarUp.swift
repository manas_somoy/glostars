//
//  StatusBarUp.swift
//  Glostars
//
//  Created by Sanzid iOS on 1/7/18.
//  Copyright © 2018 Sanzid Ashan. All rights reserved.
//

import UIKit


@IBDesignable
class StatusBarUp: UIViewController {
    
    @IBInspectable var LightStatusBar: Bool = false
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            if LightStatusBar {
                return UIStatusBarStyle.lightContent
            } else {
                return UIStatusBarStyle.default
            }
        }
    }
}
