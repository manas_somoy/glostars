//
//  NewFeedVC.swift
//  MultiScrolling
//
//  Created by sadidur rahman on 3/2/19.
//  Copyright © 2019 vikram singh rajpoot. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireImage
import SDWebImage
import iOSPhotoEditor
import ISEmojiView


//,DZNEmptyDataSetSource, DZNEmptyDataSetDelegate,FloatyDelegate,PhotoEditorDelegate

class NewFeedVC: UIViewController,DZNEmptyDataSetSource, DZNEmptyDataSetDelegate,FloatyDelegate, UITextViewDelegate,PhotoEditorDelegate {
   

    
    
    func doneEditing(image: UIImage) {
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "ImageViewController") as! ImageViewController
        passData.getImageData = image
        passData.chooseCtegeory = ""
        passData.copetionPost = String(photoId)
        
        self.navigationController?.pushViewController(passData, animated: true)
    }
    
    func canceledEditing() {
        
        
    }

    @IBOutlet weak var tableViewComment: UITableView!

    //var fab = Floaty()
    var commentIdCell = ""
    var scrollEnable = "True"
    var CommentsPicArray : Array<PublicCommentMapper> = []
    var photoId = 0
    var indexNumber :CGFloat = 0.0
    var TitleShow = ""
    var starCountName = 0
    var starShow = 0
    var commentCount = [Int]()
    var ListPhoto = [Int]()
    var pageNumber = 1
    var totalItems = 1000
    var fromPublic = ""

    
    var Articles : Array<FeedMapper> = []
    var ArticlesInfo : Array<InfoMapper> = []
    var commentInfo : Array< CommentMapper > = []
    var idArray = [Int]()
    var PublicInfo : Array< PublicFeeedMapper > = []
    var UserInfoArray : Array< UserInfoMapper > = []
    var editPhotoArray : Array<EditPhotoMapper> = []

    var starCountValue = 0
    
    var  imageValue = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    

    override func viewWillAppear(_ animated: Bool) {
        if fromPublic == "true"{
            
            print(indexNumber)
            pageNumber = Int(ceil((indexNumber/10)))
            
            //print(indexNumber/10)
            //print(Int(ceil((indexNumber/10))))
            print(pageNumber)
            
            
            if indexNumber >= 10{
                
                indexNumber = CGFloat(Int(indexNumber) % 10)
                
                print(indexNumber)
                
            }
        }
        self.navigationController?.navigationBar.isHidden = true
        self.inputViewController?.dismissKeyboard()
        emojiButtonPressed = false
        self.view.removeBlurEffect()
        if self.fromPublic == "true"{
            self.getRequesPublic()
            
        }
        else{
            self.getRequest()
            
        }
        

        //commentView.isHidden = true
        
        //postUserImage.setRounded()
        
        //tableViewComment.emptyDataSetSource = self
       // collectionViewComment.emptyDataSetSource = self
        
        //hiddenFloatingButtonsScrool()
        self.navigationController?.isNavigationBarHidden = false
        self.title = "All Public Photos "
//        commentText.layer.borderWidth = 1.0
//        commentText.layer.borderColor = UIColor.black.cgColor
//        commentText.layer.cornerRadius = 8.0
//        commentText.textContainer.lineBreakMode = .byTruncatingTail
        
//        if userPic == "/Content/Profile/Thumbs/   Male.jpg"{
//            profileButton.setBackgroundImage(UIImage(named:"male"), for: .normal)
//            postUserImage.image = UIImage(named: "male")
//        }
//        else if userPic == "/Content/Profile/Thumbs/   Female  .jpg"{
//            profileButton.setBackgroundImage(UIImage(named:"female"), for: .normal)
//            postUserImage.image = UIImage(named: "female")
//        }
//
//        else{
//            let urlString = URL(string:String(describing: UserDefaults.standard.value(forKey: "pic")!))
//            profileButton.af_setBackgroundImage(for: [] , url: urlString! )
//            postUserImage.af_setImage(withURL: urlString!)
//        }
        
//        floatingMainButton.setRounded()
//        notificationButton.setRounded()
//        uploadButton.setRounded()
//        achivementButton.setRounded()
//        homeButton.setRounded()
//        profileButton.setRounded()
//        searchButton.setRounded()
        
    }
    
    @objc func update() {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.panGestureRecognizer.translation(in: scrollView.superview).y > 0 {
            //scrolling downwards
            navigationController?.isNavigationBarHidden = false
            if scrollView.contentOffset.y < 0 {
                //this means we are at top of the scrollView
                navigationController?.isNavigationBarHidden = false
            }
        }
        else {
            //we are scrolling upward
            navigationController?.isNavigationBarHidden = true

        }
    }
    
    
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        
        let text = NSAttributedString(string: "No Comments")
        tableViewComment.separatorColor = UIColor.clear
        return text
        
    }
    func getRequesPublic(){
        
        
        
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/public/\(pageNumber)"
        
        Alamofire.request(url).responseJSON { response in
            
            if let jsonRoot = response.result.value as? [String:Any]!{
                //print(jsonRoot)
                if response.result.value != nil {
                    
                    if let resultPayload = jsonRoot!["resultPayload"] as? [String:Any]!{
                        
                        print(resultPayload)
                        
                        if let picReturn = resultPayload!["picsToReturn"] as? [[String:Any]]!{
                            
                            for data in picReturn! {
                                print(data)
                                
                                guard  let imageInfo = Mapper<PublicFeeedMapper>().map(JSON: data) else {
                                    
                                    
                                    continue
                                    
                                }
                                
                                self.PublicInfo.append(imageInfo)
                            }
                            
                            for dic in picReturn! {
                                
                                print(dic)
                                if let poster = dic["poster"] as? [String:Any]!{
                                    
                                    guard  let userinfo = Mapper<UserInfoMapper>().map(JSON: poster!) else {
                                        
                                        
                                        continue
                                        
                                    }
                                    self.UserInfoArray.append(userinfo)
                                    
                                }
                                if let comments = dic["comments"] as![[String : Any]]! {
                                    
                                    self.commentCount.append(comments.count)
                                    
                                    
                                    for array in comments {
                                        //print(comments)
                                        
                                        
                                        
                                        //print(self.commentArray)
                                        
                                        guard let comments = Mapper<CommentMapper>().map(JSON: array) else{
                                            
                                            continue
                                        }
                                        
                                        self.commentInfo.append(comments)
                                        
                                    }
                                    
                                }
                                if let comments = dic["editPhotos"] as![[String : Any]]! {
                                    
                                    
                                    
                                    for array in comments {
                                        //print(comments)
                                        
                                        
                                        
                                        //print(self.commentArray)
                                        
                                        guard let comments = Mapper<EditPhotoMapper>().map(JSON: array) else{
                                            
                                            continue
                                        }
                                        //                                        self.editPhotoArray.removeAll()
                                        self.editPhotoArray.append(comments)
                                        
                                    }
                                    
                                }
                                
                                
                                
                                
                                
                                
                            }
                            
                            
                            //print(picReturn)
                            
                        }
                        
                        //print(resultPayload)
                        
                        
                    }
                    
                    
                    
                    self.tableViewComment.reloadData()
                    print(Int(self.indexNumber))
                    
                    if  self.scrollEnable == "True"{
                        
                        self.tableViewComment.scrollToRow(at: IndexPath.init(row: Int(self.indexNumber), section: 0), at: .top, animated: true)
                    }
                    
                }
                
            }
                
            else {
                
                print("Network Error ")
                
            }
            
            
            
            
        }
        
    }
    

    func getRequest(){
        
        let parameter = [
            
            "ListPhoto": ListPhoto,
            
            ]
        
        
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/mutualpic/\(String(describing: UserDefaults.standard.value(forKey: "id")!))/\(pageNumber)"
        print(url)
        
        
        
        //        if (UserDefaults.standard.value(forKey: "access"))as? String == nil {
        
        let headers: HTTPHeaders = [
            
            //            "Authorization": tokenType + accessToken ,
            //            "Content-Type": "application/json",
            
            
            "Authorization": String(describing: UserDefaults.standard.value(forKey: "id")!) + String(describing: UserDefaults.standard.value(forKey: "access")!),
            "Content-Type": "application/json",
            
            ]
        
        Alamofire.request(url, method:.post, parameters : parameter,encoding:JSONEncoding.default,headers:headers).responseJSON { response in
            
            
            if response.result.value != nil{
                
                if let jsonRoot = response.result.value as? [String:Any]!{
                    
                    //print(jsonRoot)
                    if response.result.value != nil{
                        if let resultPayload = jsonRoot!["resultPayload"] as? [String: Any]!{
                            
                            let count = resultPayload!["count"] as! Int
                            print(count)
                            //self.totalItems = count
                            
                            
                            // print(jsonRoot)
                            
                            if let data = resultPayload!["data"] as! [[String:Any]]!{
                                
                                
                                print(data)
                                
                                
                                
                                
                                for array in data {
                                    
                                    
                                    if let comments = array["editPhotos"] as![[String : Any]]! {
                                        
                                        //print(comments)
                                        
                                        
                                        
                                        for array in comments {
                                            print(comments)
                                            
                                            
                                            
                                            guard let comments = Mapper<EditPhotoMapper>().map(JSON: array) else{
                                                
                                                continue
                                            }
                                            
                                            self.editPhotoArray.append(comments)
                                            
                                        }
                                        
                                    }
                                    
                                    
                                    //print(data.count)
                                    if let userName = array["poster"] as! [String:Any]!{
                                        
                                        print(userName)
                                        
                                        guard  let article1 = Mapper<InfoMapper>().map(JSON: userName) else {
                                            
                                            continue
                                            
                                            
                                        }
                                        self.ArticlesInfo.append(article1)
                                        
                                        
                                        
                                        
                                        
                                        
                                        guard  let article2 = Mapper<FeedMapper>().map(JSON: array) else {
                                            
                                            
                                            
                                            continue
                                            
                                            
                                        }
                                        self.Articles.append(article2)
                                        
                                        //                                        print(self.ListPhoto)
                                        //                                        self.ListPhoto.removeAll()
                                        self.ListPhoto.append(article2.idPhoto)
                                        
                                        //self.tableView.reloadData()
                                        
                                    }
                                    
                                    
                                    //self.Articles.append(article1)
                                    if let rating = array["ratings"] as![[String:Any]]!{
                                        
                                        // print(rating)
                                        
                                        
                                        
                                        if let comments = array["comments"] as![[String : Any]]! {
                                            
                                            self.commentCount.append(comments.count)
                                            
                                            
                                            for array in comments {
                                                //print(comments)
                                                
                                                
                                                
                                                //print(self.commentArray)
                                                
                                                guard let comments = Mapper<CommentMapper>().map(JSON: array) else{
                                                    
                                                    continue
                                                }
                                                
                                                self.commentInfo.append(comments)
                                                
                                            }
                                            
                                        }
                                        
                                        
                                        
                                    }
                                    
                                    
                                    
                                    
                                    
                                }
                                
                                
                                
                                
                            }
                        }
                        
                        //                        self.pageNumber = self.pageNumber + 1
                        
                        
                    }
                    else {
                        
                        print("Network error")
                        if self.fromPublic == "true"{
                            self.getRequesPublic()
                            
                        }
                        else{
                            self.getRequest()
                            
                        }
                        
                        
                        
                    }
                    //print(jsonRoot)
                    
                    
                    
                }
                self.tableViewComment.reloadData()
                
                
            }
            else {
                
                print("Internet Error")
                if self.fromPublic == "true"{
                    self.getRequesPublic()
                    
                }
                else{
                    self.getRequest()
                    
                }
                
                
            }
            
            self.tableViewComment.reloadData()
            for subViews in self.tableViewComment.visibleCells {
                let bottomAnimation = AnimationType.from(direction: .bottom, offSet: 30.0)
                subViews.contentView.animateAll(withType: [bottomAnimation])
            }
            
        }
        
        
        
    }
    
    func getImageInfo (){
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/picture/\(photoId)"
        let headers: HTTPHeaders = [
            
            "Authorization": tokenType + accessToken ,
            "Content-Type": "application/json",
            
            ]
        
        Alamofire.request(url, method:.get,encoding:JSONEncoding.default,headers:headers).responseJSON { response in
            
            
            if response.result.value != nil {
                
                if let jsonRoot = response.result.value as? [String:Any]!{
                    
                    print(jsonRoot)
                    
                    
                    if let ResultPayload = jsonRoot!["resultPayload"] as? [String:Any]!{
                        if let comments = ResultPayload!["comments"] as! [[String: Any]]!{
                            for array in comments{
                                print(array)
                                guard let allcomments = Mapper<PublicCommentMapper>().map(JSON: array) else{
                                    
                                    continue
                                }
                                self.CommentsPicArray.append(allcomments)
                            }
                            
                            
                        }
                        
                        
                        if let poster = ResultPayload!["poster"] as? [String:Any]!{

                        }
                        
                    }
                    
                    
                }
                self.tableViewComment.reloadData()
            }
            else {
                
                print("Internet error")
                
            }
            
            
        }
        
    }
    
    

}

extension NewFeedVC:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // return 10
        
//        if tableView == self.tableViewComment{
//            return self.CommentsPicArray.count
//        }
//        else{
//            if self.commentInfo.count < 1 {
//            }
             print(self.ArticlesInfo.count)
            return self.ArticlesInfo.count
       // }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsFeedTableViewCell", for: indexPath) as? NewsFeedTableViewCell
        cell?.registerCollectionView(datasource: self)
        cell?.collectionView.reloadData()

        return cell!
        
 
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if tableView == self.tableViewComment{
//        }
//        else{
//
//            tableViewComment.isHidden = true
//            PicURL = self.PublicInfo[indexPath.row].picUrl
//            if self.PublicInfo[indexPath.row].isCompeting == true {
////                let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
////                let passData = MainStoryboard.instantiateViewController(withIdentifier: "GaleryViewController") as! GaleryViewController
//                self.navigationController?.pushViewController(passData, animated: true)
//            }
//            else{
//
//
//            }
//
//        }
        
        
    }
    
    func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!)years"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!)month"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!)w"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1w"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!)d"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1d"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!)h"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1h"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 8) {
            return "\(components.minute!)m"
        } else if (components.minute! >= 8){
            if (numericDates){
                return "1m"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 0) {
            return "Just now"
        } else {
            return "Just now"
        }
    }
}


extension NewFeedVC:UICollectionViewDataSource,UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(editPhotoArray.count)
        return editPhotoArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsFeedCollectionViewCell", for: indexPath) as? NewsFeedCollectionViewCell
        
        let allinfo = self.ArticlesInfo[indexPath.row]
        let userinfo = self.ArticlesInfo[indexPath.row]
        let url = NSURL(string: allinfo.picUrl)
        let url2 = NSURL(string: userinfo.profilePicURL)
        print(url2!)
        
        cell?.starCount.text = allinfo.starcount.description
       
        if  userinfo.profilePicURL == "/Content/Profile/Thumbs/   Male.jpg"{
            
            cell?.proPicture.image = UIImage(named: "male")
            
            
        }
        else if  userinfo.profilePicURL == "/Content/Profile/Thumbs/   Female  .jpg"{
            
            cell?.proPicture.image = UIImage(named: "female")
            
        }
        else{
            
            cell?.proPicture.sd_setImage(with: url2! as URL, placeholderImage: nil , options: .scaleDownLargeImages, completed: nil)
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        
        if let  date = dateFormatter.date(from: allinfo.uploadedTime) {
            cell?.uploadedTime.text = timeAgoSinceDate(date, currentDate: Date(), numericDates: true)
        }
        
        
        if self.Articles[indexPath.row].featuredPicture == true {
            print("fretured")
//            tableViewCell.heightConstraintFeature.constant = 0.0
//            tableViewCell.featureTextOutlet.alpha = 0
//            tableViewCell.featuredImageOutlet.alpha = 0
//            tableViewCell.heightConstraintsFooterFeatureView.constant = 0.0
//
//            tableViewCell.seeAllPicOutlet.alpha = 0
        }
        
        
        else {
//            tableViewCell.heightConstraintFeature.constant = 00.00
//            tableViewCell.featureTextOutlet.alpha = 0
//            tableViewCell.featuredImageOutlet.alpha = 0
//            tableViewCell.heightConstraintsFooterFeatureView.constant = 00.00
//            tableViewCell.seeAllPicOutlet.alpha = 0
        }
        
        
        if self.Articles[indexPath.row].isCompeting == true  {
            
            cell?.privacyPicture.image = UIImage(named: "privacy_competition_photo")
            //tableViewCell.start1Outlet.isHidden = true
            cell?.starCount.isHidden = true
            cell?.commentCount.isHidden = true
            cell?.descriptionLabel.text = "See All Competition Pictures"
            cell?.commentButton.isHidden = true
            cell?.proPicture.isHidden = true
            cell?.userName.isHidden = true
//            tableViewCell.editButton.isHidden = false
//            tableViewCell.paintCount.isHidden = true
//
        }
        
        
        if self.Articles[indexPath.row].privacyPublic == "public" && self.Articles[indexPath.row].isCompeting == false {
            
            cell?.privacyPicture.image = UIImage(named: "Public w-shadow")
            
            cell?.star1Outlet.isHidden = false
            cell?.starCount.isHidden = false
            cell?.commentCount.isHidden = false
            cell?.descriptionLabel.text = Articles[indexPath.row].description
            cell?.commentButton.isHidden = false
            cell?.proPicture.isHidden = false
            cell?.userName.isHidden = false
//            tableViewCell.editButton.isHidden = false
//            tableViewCell.paintCount.isHidden = false

        }
        
        
        
        if self.Articles[indexPath.row].privacyFriend == "friends"  {
            
            cell?.privacyPicture.image = UIImage(named: "Mutual w-shadow")
            
            cell?.star1Outlet.isHidden = false
            cell?.starCount.isHidden = false
            cell?.commentCount.isHidden = false
            cell?.descriptionLabel.text = Articles[indexPath.row].description
            cell?.commentButton.isHidden = false
            cell?.proPicture.isHidden = false
            cell?.userName.isHidden = false
//            tableViewCell.paintCount.isHidden = false
            
        }
        
//        if userID == self.UserInfoArray[indexPath.row].userid{
////            tableViewCell.deleteButton.isHidden = true
////            tableViewCell.editButton.isHidden = true
////            tableViewCell.paintCount.isHidden = true
//        }
        
        
        if  self.Articles[indexPath.row].isCompeting == true {
            
            
        }
//        if allinfo.myStarCount == 0 {
//
//            let image = UIImage(named: "Star small 2") as UIImage?
//
//
//            cell?.star1Outlet.setImage(image, for: .normal)
//            self.imageValue = ""
//
//
//        }
//
//        if allinfo.myStarCount == 5 {
//
//            let image = UIImage(named: "Star filled w-shadow") as UIImage?
//            self.imageValue = "Checked"
//
//
//            cell?.star1Outlet.setImage(image, for: .normal)
//
//        }
//        if allinfo.myStarCount == 4 {
//
//            let image = UIImage(named: "Star filled w-shadow") as UIImage?
//            self.imageValue = "Checked"
//
//
//            cell?.star1Outlet.setImage(image, for: .normal)
//
//        }
//        if allinfo.myStarCount == 3 {
//
//            let image = UIImage(named: "Star filled w-shadow") as UIImage?
//            self.imageValue = "Checked"
//
//
//            cell?.star1Outlet.setImage(image, for: .normal)
//
//        }
//        if allinfo.myStarCount == 2 {
//
//            let image = UIImage(named: "Star filled w-shadow") as UIImage?
//            self.imageValue = "Checked"
//
//
//            cell?.star1Outlet.setImage(image, for: .normal)
//        }
//        if allinfo.myStarCount == 1 {
//
//            let image = UIImage(named: "Star filled w-shadow") as UIImage?
//            self.imageValue = "Checked"
//
//
//            cell?.star1Outlet.setImage(image, for: .normal)
//        }
        
        
        //cell.imgImage.af_imageDownloader?.download([url as! URLRequestConvertible])
        
        cell?.cellPhoto.sd_setShowActivityIndicatorView(true)
        cell?.cellPhoto.sd_setIndicatorStyle(.gray)
        cell?.cellPhoto.sd_setImage(with: url! as URL, placeholderImage: nil, options: .scaleDownLargeImages, completed: nil)
        
        //tableViewCell.descriptionLabel.text = allinfo.description
        //tableViewCell.proPic.af_setImage(withURL: url2! as URL)
        cell?.userName.text = userinfo.name
        
        
//        tableViewCell.onDeleteTap = {
//
//            self.photoId = allinfo.idPhoto
//            self.BackButtonClick("Warning", message: "Are you sure you want to delete this Photo")
//            print("Delete")
//
//        }
//        tableViewCell.onReport = {
//
//
//
//            let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//            if userID == userinfo.userid{
//                self.TitleShow = "Delete photo"
//            }
//            else{
//                self.TitleShow = "Report photo"
//            }
//
//
//            // create an action
//            let firstAction: UIAlertAction = UIAlertAction(title: self.TitleShow, style: .default) { action -> Void in
//
//
//                if userID == userinfo.userid{
//
//                    self.photoId = allinfo.idPhoto
//                    self.BackButtonClick("Warning", message: "Are you sure you want to delete this photo?")
//                    print("Delete")
//                }
//                else{
//
//                    let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//                    let passData = MainStoryboard.instantiateViewController(withIdentifier: "ReportPhoto") as! ReportPhoto
//
//                    self.navigationController?.pushViewController(passData, animated: true)
//                }
//
//
//
//            }
//
//            let secondAction: UIAlertAction = UIAlertAction(title: "Block this user", style: .default) { action -> Void in
//
//                print("Second Action pressed")
//            }
//            let thirdAction: UIAlertAction = UIAlertAction(title: "Share picture in facebook", style: .default) { action -> Void in
//
//                print("Share Photo")
//                // image to share
//                let text = allinfo.description
//                let image = tableViewCell.cellPhoto.image
//                let myWebsite = NSURL(string:"https://testglostarsdevelopers.azurewebsites.net/Home/ViewPicture/\(allinfo.idPhoto)")
//                let shareAll = [text , image!,myWebsite] as [Any]
//                let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
//                activityViewController.popoverPresentationController?.sourceView = self.view
//                self.present(activityViewController, animated: true, completion: nil)
//
//
//            }
//
//
//            let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
//
//            // add actions
//            actionSheetController.addAction(firstAction)
//            //actionSheetController.addAction(secondAction)
//            actionSheetController.addAction(thirdAction)
//            actionSheetController.addAction(cancelAction)
//
//            // present an actionSheet...
//            self.present(actionSheetController, animated: true, completion: nil)
//        }
//
//
//        tableViewCell.onEditTap = {
//
//
//            self.photoId = allinfo.idPhoto
//
//            let photoEditor = PhotoEditorViewController(nibName:"PhotoEditorViewController",bundle: Bundle(for: PhotoEditorViewController.self))
//
//            //PhotoEditorDelegate
//            photoEditor.photoEditorDelegate = self
//            //The image to be edited
//            photoEditor.image = tableViewCell.cellPhoto.image
//
//            //Stickers that the user will choose from to add on the image
//            for i in 0...17 {
//                photoEditor.stickers.append(UIImage(named: i.description )!)
//            }
//            //Optional: To hide controls - array of enum control
//            photoEditor.hiddenControls = [.crop, .share]
//
//            //Optional: Colors for drawing and Text, If not set default values will be used
//            //            photoEditor.colors = [.r]
//
//            //Present the View Controller
//            self.present(photoEditor, animated: true, completion: nil)
//
//
//        }
//
//
//
//
//
//
//
//        tableViewCell.onStarTapped1 = {
//
//            let imagevanish = UIImage(named: "Star filled w-shadow") as UIImage?
//
//
//
//
//
//
//            if self.imageValue == "Checked" {
//
//
//                let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/removerate/\(self.PublicInfo[indexPath.row].idPhoto)"
//                //            let headers: HTTPHeaders = [
//                //                "Content-Type": "application/json"
//                //            ]
//                Alamofire.request(url, method:.post,encoding: URLEncoding.default).responseJSON { response in
//                    switch response.result {
//                    case .success:
//
//                        let imagevanish = UIImage(named: "Star small 2") as UIImage?
//
//
//
//
//
//                        tableViewCell.start1Outlet.setImage(imagevanish, for: .normal)
//
//                        print(response)
//                        self.imageValue = ""
//                        // self.ArticlesInfo.removeAll()
//
//                        // self.getRequest()
//
//
//                        // self.tableView.reloadData()
//                        tableViewCell.startCount.text =  allinfo.starcount.description
//
//
//
//                    case .failure(let error):
//                        print(error)
//                    }
//
//
//                }
//
//
//
//
//
//            }
//            else{
//
//
//                let parameters: [String : Any]? = [
//                    "NumOfStars": 1 ,
//                    "photoId": self.PublicInfo[indexPath.row].idPhoto ,
//                    ]
//                let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/rating"
//                let headers: HTTPHeaders = [
//                    "Content-Type": "application/json"
//                ]
//                Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
//
//                    if let jsonRoot = response.result.value as? [String:Any]!{
//
//                        //print(jsonRoot)
//
//                        if let resultPayload = jsonRoot!["resultPayload"] as? [String: Any]!{
//
//                            print(resultPayload)
//
//                            tableViewCell.startCount.text =  String(describing: resultPayload!["totalRating"] as! Int)
//                            print(tableViewCell.startCount.text!)
//
//
//                            self.imageValue = "Checked"
//
//                        }
//
//                        let image = UIImage(named: "Star filled w-shadow") as UIImage?
//
//
//
//
//                        tableViewCell.start1Outlet.setImage(image, for: .normal)
//
//                    }
//
//
//
//
//
//
//                }
//                //self.getRequest()
//            }
//
//
//        }
//
//        tableViewCell.onProfileTapped = {
//            //self.stopRequest()
//
//            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
//
//
//            passData.getid = self.UserInfoArray[indexPath.row].userid
//            print(self.UserInfoArray[indexPath.row].userid)
//
//
//
//
//            self.navigationController?.pushViewController(passData, animated: true)
//
//        }
//
//
//        //tableViewCell.nameUser.text = UserInfo
//        // .name
//        //tableViewCell.proPic.sd_setImage(with: urlUser! as URL, placeholderImage: nil, options: .scaleDownLargeImages, completed: nil)
//
//        tableViewCell.onCommentsTapped = {
//            //                self.photoId = allinfo.idPhoto
//            //                self.commentView.isHidden = false
//            //                self.CommentsPicArray.removeAll()
//            //                self.getImageInfo()
//            //
//            //                self.tableViewComment.isHidden = false
//            //self.commentText.becomeFirstResponder()
//            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//            let passData = MainStoryboard.instantiateViewController(withIdentifier: "CommentPostVC") as! CommentPostVC
//            passData.getID = allinfo.idPhoto
//            passData.getImage = allinfo.picUrl
//            passData.photoId = allinfo.idPhoto
//
//            self.navigationController?.pushViewController(passData, animated: true)
//
//
//        }
//
//        //print(ListPhoto)
//        if indexPath.row == PublicInfo.count - 1 {
//            // last cell
//
//            if totalItems > ArticlesInfo.count {
//                pageNumber = pageNumber + 1
//                print(pageNumber)
//                // more items to fetch
//                self.scrollEnable = ""
//                getRequest() // increment `fromIndex` by 20 before server call
//            }
//        }
        
        
        
        return cell!
    }
}
