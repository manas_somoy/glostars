//
//  ImageViewController.swift
//  Glostars
//
//  Created by Sanzid Ashan on 4/4/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage


class ImageViewController: UIViewController ,UITextViewDelegate{
    
    @IBOutlet weak var interestLabel: UILabel!
    @IBOutlet weak var interestView: UIView!
    @IBOutlet var DescriptionTextView: UITextView!
    @IBOutlet weak var viewHide: UIView!
    var localPath: String?
    @IBOutlet weak var leftFabButton: UIButton!
    @IBOutlet weak var achivementButton: UIButton!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var uploadButton: UIButton!
    
    @IBOutlet weak var floatingMainButton: UIButton!
    
    var chooseCtegeory = ""
    var copetionPost = ""
    var newConpitition = ""
    var UpladURL = ""

    
    @IBOutlet weak var followingOvel: UIImageView!
    @IBOutlet weak var publicOval: UIImageView!

    @IBOutlet weak var compititionOval: UIImageView!
    
    var imagePicker = UIImagePickerController()
    
    @IBOutlet weak var chosenImage: UIImageView!
    let picker = UIImagePickerController()
    var pickedImagePath: URL?
    var pickedImageData: Data?

    var publicPost = ""
    var followingPost = ""
    var getImageData = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        chosenImage.image = getImageData
        self.title = "Publish Photo"

        hiddenFloatingButtons()
        DescriptionTextView.text = "Write something about the photo"
        DescriptionTextView.textColor = UIColor.lightGray
 

        //let urlString = URL(string: userPic)!
        self.navigationController?.navigationBar.barTintColor = UIColor.darkGray
        self.navigationItem.title = "Upload Image "
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.white,
             NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 18)!]
        
        interestView.addTapGestureRecognizer {
            
            let nextViewController =                                  self.storyboard!.instantiateViewController(withIdentifier:"InterestVC") as!InterestVC
               nextViewController.fromUpload = "true"
        self.navigationController?.pushViewController(nextViewController, animated: true)

            
        }
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
//        chosenImage.image = getImageData
        
        interestLabel.text! = interesrtValue
        interesrtValue = ""

        viewHide.isHidden = true
        self.navigationController?.isNavigationBarHidden = false

        
        if userPic == "/Content/Profile/Thumbs/   Male.jpg"{
            
            profileButton.setBackgroundImage(UIImage(named:"male"), for: .normal)
            
            
        }
        else if userPic == "/Content/Profile/Thumbs/   Female  .jpg"{
            
            profileButton.setBackgroundImage(UIImage(named:"female"), for: .normal)
        } 
            
        else{
            let urlString = URL(string:String(describing: UserDefaults.standard.value(forKey: "pic")!))
            profileButton.af_setBackgroundImage(for: [] , url: urlString! )
            
        }
        hiddenFloatingButtons()

    }
    
    
    @IBOutlet var editButtonClicked: UIBarButtonItem!
    
    @IBAction func editButtonAction(_ sender: Any) {
        
        
        
        
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        
        let nextViewController =                                  self.storyboard!.instantiateViewController(withIdentifier:"NewsVCViewController")
        self.navigationController?.pushViewController(nextViewController, animated: true)

    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
        //imagePicked = ""

    }
    
    @IBAction func imagePickAction(_ sender: Any) {
        
        
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        
        let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "FAImageCropperVC")
        
        let nvc = UINavigationController(rootViewController: nextViewController)
        present(nvc, animated: false, completion: nil)

    }
    
    @IBAction func leftFabButton(_ sender: Any){
        
        
        chosenImage.image = UIImage(named: "camera")
        
        
    }
    @IBAction func publicAction(_ sender: Any) {
        
        
        //chooseCtegeory = "public"
       // copetionPost = "false"

        hiddenFloatingButtons()

        print(chooseCtegeory)
        
    }
    
    @IBAction func followerAction(_ sender: Any) {
        
        
        chooseCtegeory = "friends"
        copetionPost = "false"
        hiddenFloatingButtons()

        print(chooseCtegeory)
        
    }
    
    
 
    
    
    
    @IBAction func compitetionAction(_ sender: Any) {
        
        
        hiddenFloatingButtons()

        copetionPost = "true"
        chooseCtegeory = "public"
        print(copetionPost)
        
    }
    
    func hiddenFloatingButtons(){
        
        uploadButton.isHidden = true
        achivementButton.isHidden = true
        profileButton.isHidden = true
        notificationButton.isHidden = true
        homeButton.isHidden = true
        
        
    }
    
    
    @IBAction func uploadImageButton(_ sender: UIButton) {
        
    
        if chooseCtegeory == ""{
            UploadEditPhotos()
        }
        else{
            uploadWithAlamofire()

        }
        
        
        
        
        //IJProgressView2.shared.showProgressView(view)
        
        //setCloseTimer()

        
    //hiddenFloatingButtons()
        
        view.isUserInteractionEnabled = false

        
        
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        hiddenFloatingButtons()

        if DescriptionTextView.text == "Write something about the photo"{
        DescriptionTextView.text = ""

        }
        else {
            
            print("Not Edited")

            
        }
    }


    @IBAction func publicButon(_ sender: Any) {
        
        hiddenFloatingButtons()

        publicOval.isHidden = !publicOval.isHidden
        followingOvel.isHidden = false
        compititionOval.isHidden = false
        
        
        
        
        
    }
    
 
    @IBAction func followingbutton(_ sender: Any) {
        hiddenFloatingButtons()

        followingOvel.isHidden = !followingOvel.isHidden
        publicOval.isHidden = false
        compititionOval.isHidden = false

        
        
    }
    @IBAction func compititionButton(_ sender: Any) {
        
        hiddenFloatingButtons()

        compititionOval.isHidden = !compititionOval.isHidden
        publicOval.isHidden = false
        followingOvel.isHidden = false

    }
    
    
    
    
    @IBAction func leftFabButtonClick(_ sender: Any) {
        
        floatingMainButton.isHidden = false
        leftFabButton.isHidden = true
        
        
        
    }
    
    func animatingPopUp()
    {
        UIView.animate(withDuration: 1, animations: {
            self.uploadButton.isHidden =  !self.uploadButton.isHidden
            
        }) { (true) in
            
            UIView.animate(withDuration: 1, animations: {
                self.achivementButton.isHidden = !self.achivementButton.isHidden
                
            }, completion: { (true) in
                UIView.animate(withDuration: 1, animations: {
                    
                    self.profileButton.isHidden = !self.profileButton.isHidden
                    
                }, completion: { (true) in
                    self.notificationButton.isHidden = !self.notificationButton.isHidden
                    
                    self.homeButton.isHidden = !self.homeButton.isHidden
                    
                    //                    let image1 = UIImage(named: "Circleopen")
                    //                    self.floatingMainButton.setBackgroundImage(image1, for: UIControlState.normal)
                    
                    
                    
                    
                })
            })
        }
        
        
        
    }

    
    
    
    
    
    
    @IBAction func mainFabAction(_ sender: UIButton) {
        
        
        animatingPopUp()

        
        
    }
    
   
    func uploadWithAlamofire2() {
        

        
        
//        let parameter = [
//            "Description": DescriptionTextView.text!,
//            "IsCompeting": "false",
//            "Privacy" : chooseCtegeory,
//            "file" : UIImage(named:"image0")! ,
//            ] as [String : Any]
//        
//        let headers: HTTPHeaders = [
//            
//            "Authorization": tokenType + accessToken ,
//            
//            ]
        
//        let url = "https://testglostarsdevelopers.azurewebsites.net/home/upload"
//        Alamofire.request(url, method:.post, parameters : parameter,encoding: URLEncoding.httpBody,headers:headers).responseJSON { response in
//            
//            print(response)
//            
//            
//            }
        
        
        
       
            

        
        }
    
    
    @IBAction func onClinkProfile(_ sender: Any) {
        
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        
        
        passData.getid = userID
        
        self.navigationController?.pushViewController(passData, animated: true)
        
        
    }
    
    
    func UploadEditPhotos(){

        
        let image = chosenImage.image!
        
        if DescriptionTextView.text == "Description"{
            DescriptionTextView.text = ""
        }
        // define parameters
        let parameter = [
            "PictureId": copetionPost,
            ]
        
        let headers: HTTPHeaders = [
            
            "Authorization": tokenType + accessToken ,
            
            ]
        let imageData = UIImageJPEGRepresentation(image, 0.2)!
        
        
        let URL = try! URLRequest(url: "https://testglostarsdevelopers.azurewebsites.net/home/UploadEdit", method: .post, headers: headers)
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imageData, withName: "signImg", fileName: "picture.png", mimeType: "image/png")
            for (key, value) in parameter {
                
                multipartFormData.append(value.data(using: .utf8)!, withName: key)
                
            }
            
            
            
        }, with: URL, encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    debugPrint("SUCCESS RESPONSE: \(response)")
                    
                    
                    if self.copetionPost == "true"{
                        
                        //                    let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                        //                    let passData = MainStoryboard.instantiateViewController(withIdentifier: "GaleryViewController") as! GaleryViewController
                        //                    self.present(passData, animated: true, completion:
                        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc : GaleryViewController = storyboard.instantiateViewController(withIdentifier: "GaleryViewController") as! GaleryViewController
                        
                        
                        let navigationController = UINavigationController(rootViewController: vc)
                        //                          imagePicked = ""
                        self.present(navigationController, animated: true, completion: nil)
                        //nil)
                        
                    }
                    else{
                        
                        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                        let passData = MainStoryboard.instantiateViewController(withIdentifier: "NewsVCViewController") as! NewsVCViewController
                        self.navigationController?.pushViewController(passData, animated: true)
                    }
                    
                }
                
                
            case .failure(let encodingError):
                // hide progressbas here
                print("ERROR RESPONSE: \(encodingError)")
            }
        })
        
    }
    

    func uploadWithAlamofire() {
        
//        if chooseCtegeory == "" && copetionPost == "false" {
//
//           displayMyAlertMessage("Please Choose Atleast One Categeory")
//
//
//        }
//        else {
        
        let image = chosenImage.image!
        
            if DescriptionTextView.text == "Description"{
                DescriptionTextView.text = ""
            }
        // define parameters
        let parameter = [
            "Description": DescriptionTextView.text!,
            "IsCompeting": copetionPost,
            "Privacy" : chooseCtegeory ,
            ]
        
        let headers: HTTPHeaders = [
            
            "Authorization": tokenType + accessToken ,
            
            ]
        let imageData = UIImageJPEGRepresentation(image, 0.2)!


        if newConpitition == "true"{
            UpladURL = "https://testglostarsdevelopers.azurewebsites.net/Home/UploadWeeklyCompetition"

        }
        else{
            
            UpladURL = "https://testglostarsdevelopers.azurewebsites.net/Home/Upload"
        }
        
        let URL = try! URLRequest(url: UpladURL , method: .post, headers: headers)
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imageData, withName: "signImg", fileName: "picture.png", mimeType: "image/png")
            for (key, value) in parameter {
                
                multipartFormData.append(value.data(using: .utf8)!, withName: key)
                
            }


            
        }, with: URL, encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    debugPrint("SUCCESS RESPONSE: \(response)")
                  
                    
                    if self.copetionPost == "true"{

//                    let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//                    let passData = MainStoryboard.instantiateViewController(withIdentifier: "GaleryViewController") as! GaleryViewController
//                    self.present(passData, animated: true, completion:
                        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc : GaleryViewController = storyboard.instantiateViewController(withIdentifier: "GaleryViewController") as! GaleryViewController
                        
                        
                        let navigationController = UINavigationController(rootViewController: vc)
//                          imagePicked = ""
                        self.present(navigationController, animated: true, completion: nil)
                        //nil)
                        
                }
                    else{
                        
                        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                        let passData = MainStoryboard.instantiateViewController(withIdentifier: "NewsVCViewController") as! NewsVCViewController
                        self.navigationController?.pushViewController(passData, animated: true)
                    }
                    
                }
                
              
            case .failure(let encodingError):
                // hide progressbas here
                print("ERROR RESPONSE: \(encodingError)")
            }
        })
    
    }
    func textViewDidEndEditing(_ textView: UITextView) {

        
    }
    func textViewShouldReturn(_ textView: UITextView) -> Bool {
        textView.resignFirstResponder()
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn _: NSRange, replacementText text: String) -> Bool {
        let resultRange = text.rangeOfCharacter(from: CharacterSet.newlines, options: .backwards)
        if text.characters.count == 1 && resultRange != nil {
            textView.resignFirstResponder()
            // Do any additional stuff here
            return false
        }
        return true
    }
}
