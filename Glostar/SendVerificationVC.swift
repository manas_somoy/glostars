//
//  SendVerificationVC.swift
//  MainWorkUnfinisedVC
//
//  Created by sadidur rahman on 31/10/18.
//  Copyright © 2018 sadidur rahman. All rights reserved.
//

import UIKit
import Alamofire
import SwiftMessages

var userEmail =  String()

class SendVerificationVC: UIViewController {
    
    
    
    @IBOutlet weak var emailTextField: DesignableTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe:)))
        rightSwipe.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(rightSwipe)
        emailTextField.keyboardAppearance = .dark
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.hideLoader()
    }
    
    
    func swipeAction(swipe:UISwipeGestureRecognizer)
    {
        
        //dismiss(animated: true, completion: nil)
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        
        let nextViewController = self.storyboard!.instantiateViewController(withIdentifier: "LoginVC")
        
        let nvc = UINavigationController(rootViewController: nextViewController)
        present(nvc, animated: false, completion: nil)
    }
    
    
    func warningMessage(title:String,subtitle:String){
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: .messageView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.warning)
        view.button?.isHidden = true
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        // Set message title, body, and icon. Here, we're overriding the default warning
        // image with an emoji character.
        let iconText = ["🤔", "😳", "🙄", "😶"].sm_random()!
        view.configureContent(title: title, body: subtitle, iconText: iconText)
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        
        // Show the message.
        SwiftMessages.show(view: view)
        
    }
    func successMessage(title:String,subtitle:String){
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: .messageView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.success)
        view.button?.isHidden = true
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        
        // Set message title, body, and icon. Here, we're overriding the default warning
        // image with an emoji character.
        let iconText = ["🤔", "😳", "🙄", "😶"].sm_random()!
        view.configureContent(title: title, body: subtitle, iconText: iconText)
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        
        SwiftMessages.show(view: view)
        
    }
    
    
    
    @IBAction func sendVarificationAction(_ sender: Any) {
        
        if emailTextField.text?.isEmpty == true{
            
            warningMessage(title: "Warning", subtitle: "Please Enter Email")
        }
            
        else {
           
            getEmailData()
            
        }
    }
    
    
    
    
    func  getEmailData() {
        DispatchQueue.main.async{
            self.successMessage(title: "Please Wait", subtitle: "Verifying Your Email Address")
            self.showLoader("")
        }
        
        
        let params2 : Parameters = [
            
            "userEmail" : emailTextField.text!,
            
            ]
        
        let url2 = "https://testglostarsdevelopers.azurewebsites.net/api/forgetpassword/EmailCheckSendCode?email=" + emailTextField.text!
        print(url2)
        
        userEmail = emailTextField.text!
        
        Alamofire.request(url2, method: .get, parameters: params2, encoding: URLEncoding.default)
            
            
            .responseJSON{ response2 in
                
                if response2.result.value != nil {
                    
                    if let jsonRoot = response2.result.value as? [String:Any]!{
                        print(jsonRoot)
                        
                        if let userinfo = jsonRoot!["resultPayload"] as? [String: Any]!{
                            
                            let transition = CATransition()
                            transition.duration = 0.3
                            transition.type = kCATransitionPush
                            transition.subtype = kCATransitionFromRight
                            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                            self.view.window?.layer.add(transition, forKey: kCATransition)
                            
                            
                            let nextViewController =                                  self.storyboard!.instantiateViewController(withIdentifier:"ForgotPasswordCodeVC")
                            
                            let nvc = UINavigationController(rootViewController: nextViewController)
                            self.present(nvc, animated: false, completion: nil)
                            
                        }
                    }
                    else{
                        
                        self.warningMessage(title: "Warning", subtitle: "Please Check Your Email")
                        self.hideLoader()
                    }
                }
                
                
                
                
        }
        
    }
}
