//
//  ProfileMapper.swift
//  Glostars
//
//  Created by Sanzid iOS on 5/26/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import Foundation
import ObjectMapper

class ProfileMapper : Mappable {
    
    
    dynamic var userName : String = ""
    dynamic var lastName : String = ""
    dynamic var proLink : String = ""
    dynamic var id : String = ""


    
    
    
    
    
    
    
    
    
    
    
    
    required convenience init?(map: Map) {
        
        
        self.init()
        
        
    }
    
    func mapping(map: Map) {
        
        userName <- map["name"]
        lastName <- map["lastName"]
        proLink <- map["profilemediumPath"]
        id <- map["id"]

        
        
        
        
        
    }
}
