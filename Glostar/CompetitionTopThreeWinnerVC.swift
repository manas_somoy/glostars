//
//  CompetitionTopThreeWinnerVC.swift
//  Glostars
//
//  Created by sadidur rahman on 23/1/19.
//  Copyright © 2019 Sanzid Ashan. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class CompetitionTopThreeWinnerVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    var weekNumber = 0
    var year = ""
    var PicImage = [URL]()
    var NameUser = [String]()
    var userPic = [URL]()
    var voteCount = [Int]()
    var userIDInfo = [String]()
    var descriptionPic = [String]()
    var titleReport = ""
    var userIDDetals = ""
    var descriptionPicture = ""
    var indexNumber :CGFloat = 0.0
    var ScrollingTimer = Timer()
    var x = 0







    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        navigationController?.navigationBar.isHidden = true
        let DownSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe:)))
        DownSwipe.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(DownSwipe)
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeActionLeft(swipeleft:)))
        leftSwipe.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(leftSwipe)
        let RightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeActionRight(swiperight:)))
        RightSwipe.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(RightSwipe)
        getRequest()
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        //ChangeAPP

        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.landscapeRight, andRotateTo: UIInterfaceOrientation.landscapeRight)
        setTimer()

        
        
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if scrollView.panGestureRecognizer.translation(in: scrollView).x < 0 {
            ScrollingTimer.invalidate()
            setTimer()
            print("left")
            
            
        } else {
            
            ScrollingTimer.invalidate()
            print("right")
            
        }
    }
    
    func setTimer() {
        ScrollingTimer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(CompetitionPicFullVC.autoScroll), userInfo: nil, repeats: true)
    }
    
    
    func autoScroll() {
        if self.x < PicImage.count {
            let indexPath = IndexPath(item: x, section: 0)
            self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            self.x = self.x + 1
        } else {
            self.x = 0
            self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
        }
    }

    
    override func viewWillDisappear(_ animated : Bool) {
        super.viewWillDisappear(animated)
        //ChangeAPP

        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
    }
    
    func swipeAction(swipe:UISwipeGestureRecognizer)
    {
        self.navigationController?.popViewController(animated: true)
        
    }
    func swipeActionLeft(swipeleft:UISwipeGestureRecognizer)
    {

    }
    func swipeActionRight(swiperight:UISwipeGestureRecognizer)
    {
        
        
    }
    
    
    func getRequest(){
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/weeklycompetitionsingleweek/\(weekNumber)/\(year)/1"
        
        Alamofire.request(url).responseJSON { response in
            
            
            if response.result.value != nil{
                
                if let jsonRoot = response.result.value as? [String:Any]{
                    print(jsonRoot)
                    ComptitionPic.removeAll()
                    
                    if let resultPayload = jsonRoot["resultPayload"] as? [String:Any]{
                        
                        
                        if let resultallPost = resultPayload["allpost"] as?[ [String:Any]]{
                            
                            print(resultallPost)
                            
                            
                            for data in resultallPost {
                                
                                guard  let CompititionImage = Mapper<GaleryMapper>().map(JSON: data) else {
                                    continue
                                    
                                }
                                
                                
                                

                            }
                            
                            
                        }
                        if let resultTopPost = resultPayload["topthree"] as?[[String:Any]]{
                            
                            print(resultTopPost)
                            
                            for  dic in resultTopPost{
                                if let one = dic["winnerOne"] as?[String:Any]{
                                    
                                    print(one)
                                    
                                    let image1 = one["photourlOne"] as? String
                                    let name = one["usernameOne"] as? String
                                    let userPic = one["userProfilePicLinkOne"] as? String
                                    let vote = one["votecounterOne"] as? Int
                                    let userID = one["uploadedUserIdOne"] as? String
                                    let photoDes = one["descriptionOne"] as? String
                                    self.userIDInfo.append(userID!)
                                    self.descriptionPic.append(photoDes!)

  

                                    
                                    self.NameUser.append(name!)
                                    self.voteCount.append(vote!)

                                    



                                    
                                    if image1 != nil {
                                        let url = URL(string: image1!)
                                        self.PicImage.append(url!)
                                        
                                        
                                    }
                                    if userPic != nil {
                                        let url = URL(string: userPic!)
                                        self.userPic.append(url!)
                                        
                                        
                                    }
                                        
                                    else{
                                        print("error")
                                    }
                                    
                                    
                                }
                                if let one = dic["winnerTwo"] as?[String:Any]{
                                    
                                    print(one)
                                    
                                    let image1 = one["photourlTwo"] as? String
                                    let name = one["usernameTwo"] as? String
                                    let userPic = one["userProfilePicLinkTwo"] as? String
                                    let vote = one["votecounterTwo"] as? Int
                                    let userID = one["uploadedUserIdTwo"] as? String
                                    let photoDes = one["descriptionTwo"] as? String
                                    self.userIDInfo.append(userID!)
                                    self.descriptionPic.append(photoDes!)
                                    self.NameUser.append(name!)
                                    self.voteCount.append(vote!)
                                    
                                    if image1 != nil {
                                        let url = URL(string: image1!)
                                        self.PicImage.append(url!)

                                    }
                                    if userPic != nil {
                                        let url = URL(string: userPic!)
                                        self.userPic.append(url!)
                                        
                                        
                                    }
                                        
                                    else{
                                        print("error")
                                    }
                                    
                                    
                                    
                                }
                                if let one = dic["winnerThree"] as?[String:Any]{
                                    
                                    print(one)
                                    
                                    let image1 = one["photourlThree"] as? String
                                    let name = one["usernameThree"] as? String
                                    let userPic = one["userProfilePicLinkThree"] as? String
                                    let vote = one["votecounterThree"] as? Int
                                    let userID = one["uploadedUserIdThree"] as? String
                                    let photoDes = one["descriptionThree"] as? String
                                    self.userIDInfo.append(userID!)
                                    self.descriptionPic.append(photoDes!)
                                    self.NameUser.append(name!)
                                    self.voteCount.append(vote!)
                                    
                                    if image1 != nil {
                                        let url = URL(string: image1!)
                                        self.PicImage.append(url!)

                                    }
                                    if userPic != nil {
                                        let url = URL(string: userPic!)
                                        self.userPic.append(url!)
                                        
                                        
                                    }
                                        
                                    else{
                                        print("error")
                                    }
                                    
                                    
                                    
                                    
                                }
                                
                                
                            }
                            
                            
                        }
                        self.collectionView.reloadData()
                        
                        
                    }
                    
                }
                    
                else {
                    
                    print("Internet Error")
                    self.getRequest()
                    
                }
            }
            
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return PicImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopThreeWinnerCell", for: indexPath) as! TopThreeWinnerCell
        cell.reportButton.isHidden = true
        cell.imageViewTop!.sd_setImage(with: PicImage[indexPath.row], completed: nil)
        cell.userImageView.sd_setImage(with: userPic[indexPath.row], completed: nil)
        cell.userNameLabel.text = NameUser[indexPath.row]
        cell.competitionPostionWithVoteNumberLabel.text = "Number " + "\(indexPath.row + 1)" + " with " +  String(voteCount[indexPath.row]) + " Votes"
        
        cell.userImageView.addTapGestureRecognizer {
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            passData.getid = self.userIDInfo[indexPath.row]
            self.navigationController?.pushViewController(passData, animated: true)
        }
        
        cell.userNameLabel.addTapGestureRecognizer {
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            passData.getid = self.userIDInfo[indexPath.row]
            self.navigationController?.pushViewController(passData, animated: true)
        }
        
        cell.reportButton.addTapGestureRecognizer {
            
            self.userIDDetals = self.userIDInfo[indexPath.row]
            self.descriptionPicture = self.descriptionPic[indexPath.row]
            
            
            let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            if self.userIDDetals != userID{
                self.titleReport = "Report photo"
                
            }
            else{
                
                self.titleReport = "Report photo"
                
            }
            let firstAction: UIAlertAction = UIAlertAction(title: self.titleReport, style: .default) { action -> Void in
                
                self.navigationController?.navigationBar.isHidden = false
                
                self.navigationController?.setNavigationBarHidden(false, animated: true)
                let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let passData = MainStoryboard.instantiateViewController(withIdentifier: "ReportPhoto") as! ReportPhoto
                self.navigationController?.pushViewController(passData, animated: true)
                
                
            }
            
            let thirdAction: UIAlertAction = UIAlertAction(title: "See description", style: .default) { action -> Void in
                
                // create the alert
                let alert = UIAlertController(title: "See description", message: self.descriptionPicture, preferredStyle: UIAlertController.Style.alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
            }
            let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
            
            // add actions
            actionSheetController.addAction(thirdAction)
            actionSheetController.addAction(firstAction)
            //actionSheetController.addAction(secondAction)
            actionSheetController.addAction(cancelAction)
            
            // present an actionSheet...
            self.present(actionSheetController, animated: true, completion: nil)

        }
        cell.imageViewTop.addTapGestureRecognizer {
            self.ScrollingTimer.invalidate()
            cell.reportButton.isHidden = false
            
            
        }
        
        cell.onReportTap = {
            
            
            
        }
        
       // cell.reportButton.isHidden = true
        //cell.reportView.isHidden = true
        
//        cell.onReportTap = {
//
//            let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//            if self.IDArray[indexPath.row].userId != userID{
//                self.titleReport = "Report photo"
//
//            }
//            else{
//
//                self.titleReport = "Delete photo"
//
//            }
//
//            let thirdAction: UIAlertAction = UIAlertAction(title: "See description", style: .default) { action -> Void in
//
//                // create the alert
//                let alert = UIAlertController(title: "See description", message: self.seeDescription, preferredStyle: UIAlertController.Style.alert)
//
//                // add an action (button)
//                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//
//                // show the alert
//                self.present(alert, animated: true, completion: nil)
//
//
//            }
//
//            // create an action
//            let firstAction: UIAlertAction = UIAlertAction(title: self.titleReport, style: .default) { action -> Void in
//
//                if self.IDArray[indexPath.row].userId == userID{
//
//                    self.galeryID = String(allphoto.photoId)
//                    self.PictureID = String(allphoto.pictureId)
//                    self.BackButtonClick("Warning", message: "Are you sure you want to delete this photo?")
//                    print("Delete")
//
//
//                }
//                else{
//
//                    self.navigationController?.navigationBar.isHidden = false
//
//                    self.navigationController?.setNavigationBarHidden(false, animated: true)
//                    let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//                    let passData = MainStoryboard.instantiateViewController(withIdentifier: "ReportPhoto") as! ReportPhoto
//                    self.navigationController?.pushViewController(passData, animated: true)
//
//                }
//
//
//            }
//
//
//
//
//            let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
//
//            // add actions
//            actionSheetController.addAction(thirdAction)
//            actionSheetController.addAction(firstAction)
//            //actionSheetController.addAction(secondAction)
//            actionSheetController.addAction(cancelAction)
//
//            // present an actionSheet...
//            self.present(actionSheetController, animated: true, completion: nil)
//        }
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.width
        let height = UIScreen.main.bounds.height
        
        return CGSize(width: width, height: height )
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
}
