

import UIKit

class FullGalaryCell: UICollectionViewCell {
    
    @IBOutlet weak var autoSlideIntroView: UIView!
    
    @IBOutlet weak var directionIntroView: UIView!
    
    @IBOutlet weak var centerDirectionImageView: UIImageView!
    
    @IBOutlet weak var leftDirectionImageView: UIImageView!
    
    @IBOutlet weak var rightDirectionImageView: UIImageView!
    
    @IBOutlet weak var autoSlideStackView: UIStackView!
    
    @IBOutlet weak var imgImage: UIImageView!
    
    @IBOutlet weak var reportButton: UIButton!
    
    @IBOutlet var starCount: UILabel!
    @IBOutlet var starCellButton: UIButton!
    
    var starCellButtonTap: (() -> Void)? = nil
    var onReportTap: (() -> Void)? = nil



    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
        autoSlideIntroView.isUserInteractionEnabled = true
        directionIntroView.isUserInteractionEnabled = true
        
        let leftImage = UIImage(named: "swipe")!
        let leftImageForImageView = UIImage(cgImage: leftImage.cgImage!, scale: CGFloat(1.0), orientation: .down)
         leftDirectionImageView.image = leftImageForImageView

        
        let centerImage = UIImage(named: "swipe")!
        let centerImageForImageView = UIImage(cgImage: centerImage.cgImage!, scale: CGFloat(1.0), orientation: .right)
        centerDirectionImageView.image = centerImageForImageView
    }

    
    
    @IBAction func reportButtonPressed(_ sender: Any) {
        print("ReportPressed")
        if let reportButtonTap = self.reportButton {
            
            onReportTap!()
            
        }
        
    }
    

    @IBAction func starCellButton(_ sender: UIButton) {
        
        if let starCellButtonTap = self.starCellButtonTap {
            
            starCellButtonTap()
            
        }

    
    
    
}
}
