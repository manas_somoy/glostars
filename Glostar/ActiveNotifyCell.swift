//
//  ActiveNotifyCell.swift
//  Glostars
//
//  Created by Sanzid iOS on 6/9/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit

class ActiveNotifyCell: UITableViewCell {
    
    
    @IBOutlet weak var proPicOutlet: UIImageView!
    
    @IBOutlet weak var imageViewOutlet: UIImageView!
    
    
    @IBOutlet var notifyPicClick: UIButton!
    @IBOutlet weak var userNameOutlet: UILabel!
    
    @IBOutlet weak var timeUploadOutlet: UILabel!
    
    
    
    @IBOutlet weak var descriptionOutlet: UILabel!
    
    var onProfileTapped: (() -> Void)? = nil
    var OnnotifyPicClick: (() -> Void)? = nil

    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        proPicOutlet.setRounded()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    @IBAction func ProfileClickAction(_ sender: UIButton) {
        
        if let onProfileTapped = self.onProfileTapped {
            
            onProfileTapped()
            
        }
        
    }
    
    @IBAction func notifyPicAction(_ sender: Any) {
        
        
        if let OnnotifyPicClick = self.OnnotifyPicClick {
            
            OnnotifyPicClick()
            
        }
        
        
    }
    
    

}
