//
//  NotificationViewController.swift
//  Glostars
//
//  Created by Sanzid Ashan on 3/30/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

import ObjectMapper
import SDWebImage


class CompetitionPicFullVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    
    @IBOutlet weak var reportButtonFull: UIButton!
    var  imageValue = ""
    var weekNumber = 0
    var year = ""

    var allPostWeekLy = ""
    var scrollEnable = "True"
    var indexNumber :CGFloat = 0.0
    var seeDescription = ""
    var x = 0
    var reportHide = ""
    var titleReport = ""
    var galeryID = ""
    var PictureID = ""



    @IBOutlet weak var ReportButtonNew: UIButton!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var ScrollingTimer = Timer()
    
   
    
    var ComptitionPicFull : Array<GaleryMapperFull> = []
    var IDArray : Array<ImageIdMapper> = []
    var pageNumber = 1
    
    var totalItems = 1000
    
    override func viewDidLoad() {
        
        
        
        super.viewDidLoad()
        print(pageNumber)
        print(indexNumber)
        pageNumber = Int(ceil((indexNumber/10)))
        
        //print(indexNumber/10)
        //print(Int(ceil((indexNumber/10))))
        print(pageNumber)
        
        if indexNumber >= 10{
            
            indexNumber = CGFloat(Int(indexNumber) % 10)

            print(indexNumber)
            
        }
    
        x = Int(indexNumber)
        getRequest()

        
        navigationController?.navigationBar.isHidden = true
        let DownSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe:)))
        DownSwipe.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(DownSwipe)
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeActionLeft(swipeleft:)))
        leftSwipe.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(leftSwipe)
        let RightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeActionRight(swiperight:)))
        RightSwipe.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(RightSwipe)

        
        //        let urlString = URL(string: userPic)!
        //
        //
        //        profileButton.af_setBackgroundImage(for: [] , url: urlString )
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        //ChangeAPP

     AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.landscapeRight, andRotateTo: UIInterfaceOrientation.landscapeRight)
        
        setTimer()
        
         }
    override func viewWillDisappear(_ animated : Bool) {
        super.viewWillDisappear(animated)
        //ChangeAPP

     AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
    }

    func swipeAction(swipe:UISwipeGestureRecognizer)
    {
        //performSegue(withIdentifier: "Dismiss", sender: self)
        self.navigationController?.popViewController(animated: true)
        
    }
    func swipeActionLeft(swipeleft:UISwipeGestureRecognizer)
    {
        //performSegue(withIdentifier: "Dismiss", sender: self)
        
        
    }
    func swipeActionRight(swiperight:UISwipeGestureRecognizer)
    {
        //performSegue(withIdentifier: "Dismiss", sender: self)
        
        
    }
    

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if scrollView.panGestureRecognizer.translation(in: scrollView).x < 0 {
            ScrollingTimer.invalidate()
            setTimer()
            print("left")
            
            
        } else {
            
            ScrollingTimer.invalidate()
            print("right")
            
        }
    }
    
    
    
    func getRequest(){
        
        var url = ""
        if allPostWeekLy == "true"{
        url = "https://testglostarsdevelopers.azurewebsites.net/api/images/weeklycompetitionsingleweek/\(weekNumber)/\(year)/1"
            allPostWeekLy = ""

        }else{
            
            url = "https://testglostarsdevelopers.azurewebsites.net/api/images/weeklycompetition/\(pageNumber)"

        }
        

        print(url)
        Alamofire.request(url).responseJSON { response in
            
            
            if response.result.value != nil{
                
                if let jsonRoot = response.result.value as? [String:Any]!{
                    print(jsonRoot)
                    
                    if let resultPayload = jsonRoot!["resultPayload"] as? [String:Any]!{
                        
                         print(resultPayload)
                        
                        if let resultallPost = resultPayload!["allpost"] as?[[String:Any]]{

                        
                            for data in resultallPost {
                            
                            guard  let CompititionImage = Mapper<GaleryMapperFull>().map(JSON: data) else {
                                continue}
                            
                            
                            self.ComptitionPicFull.append(CompititionImage)
                            
                            
                        }
                            for dic in resultallPost {
                            
                            
                            if let picID = dic["poster"] as? [String:Any]!{
                                
                                guard  let imageID = Mapper<ImageIdMapper>().map(JSON: picID!) else {
                                    
                                    
                                    continue
                                    
                                }
                                
                                self.IDArray.append(imageID)
                            }
                            
                            
                            
                            
                            
                        }
                        
                        //                    if let picReturn = resultPayload["picsToReturn"] as? [[String:Any]]!{
                        //
                        //                        for data in picReturn {
                        //
                        //                            guard  let CompititionImage = Mapper<GaleryMapper>().map(JSON: data) else {
                        //                                continue}
                        //
                        //
                        //                            self.ComptitionPic.append(CompititionImage)
                        //                        }
                        
                        //print(picReturn)
                        
                        //}
                        
                        //print(resultPayload)
                        
                        
                    }
                    
                }
                }
                self.collectionView.reloadData()
                if  self.scrollEnable == "True"{
                    
                    self.collectionView.scrollToItem(at: IndexPath.init(row: Int(self.indexNumber), section: 0), at: .centeredHorizontally, animated: true)
                }

            }
                
            else {
                
                print("Internet Error")
                
            }
        }
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ComptitionPicFull.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FullGalaryCell", for: indexPath) as! FullGalaryCell
        
        cell.reportButton.isHidden = true
        
        if ComptitionPic.count > 0 {
            cell.autoSlideIntroView.isHidden = true
            let allphoto = ComptitionPicFull[indexPath.row]
            
            
            //seeDescription = //ComptitionPicFull[indexPath.row - 1].description
            

            let url = NSURL(string: allphoto.picUrl)
            
            // cell.imgImage.af_imageDownloader?.download([url as! URLRequestConvertible])
            cell.imgImage.sd_setShowActivityIndicatorView(true)
            cell.imgImage.sd_setIndicatorStyle(.gray)
            cell.imgImage.sd_setImage(with: url! as URL, placeholderImage: nil, options: .scaleDownLargeImages, completed: nil)
            
            
            if !UserDefaults.standard.bool(forKey: "autoSlide") {
                UserDefaults.standard.set(false, forKey: "autoSlide")
                //autoSlideIntroView.isHidden = false
                cell.autoSlideIntroView.isHidden = false
                cell.autoSlideIntroView.addTapGestureRecognizer{
                    
                    UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                       // cell.autoSlideIntroView.isHidden = true
                        cell.autoSlideStackView.isHidden = true
                        cell.directionIntroView.isHidden = false
                        
                        
                        
                       // UserDefaults.standard.set(true, forKey: "autoSlide")
                        
                    })
                }
            }
            
            cell.directionIntroView.addTapGestureRecognizer{
            
            UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                cell.autoSlideIntroView.isHidden = true
                cell.autoSlideStackView.isHidden = true
                UserDefaults.standard.set(true, forKey: "autoSlide")
                
            })
            
            }

            
            
            
            
            
            cell.imgImage.addTapGestureRecognizer {
                self.ScrollingTimer.invalidate()
                cell.reportButton.isHidden = false
               

            }
            
            
            
           
            
            cell.imgImage.isUserInteractionEnabled = true
            cell.imgImage.tag = indexPath.row
            cell.starCount.isHidden = true

            
            if IDArray[indexPath.row].userId == userID{
                cell.starCount.text = "Total Star:  " + allphoto.starcount.description
                cell.starCount.isHidden = false
  
            }
            
            if allphoto.myStarCount == 0 {
                
                let image = UIImage(named: "Star empty w-shadow") as UIImage?
                
                
                cell.starCellButton.setImage(image, for: .normal)
                self.imageValue = ""
                
                
            }
            
            if allphoto.myStarCount == 5 {
                
                let image = UIImage(named: "Star filled w-shadow") as UIImage?
                self.imageValue = "Checked"
                
                
                cell.starCellButton.setImage(image, for: .normal)
                
            }
            if allphoto.myStarCount == 4 {
                
                let image = UIImage(named: "Star filled w-shadow") as UIImage?
                self.imageValue = "Checked"
                
                
                cell.starCellButton.setImage(image, for: .normal)
                
            }
            if allphoto.myStarCount == 3 {
                
                let image = UIImage(named: "Star filled w-shadow") as UIImage?
                self.imageValue = "Checked"
                
                
                cell.starCellButton.setImage(image, for: .normal)
                
            }
            if allphoto.myStarCount == 2 {
                
                let image = UIImage(named: "Star filled w-shadow") as UIImage?
                self.imageValue = "Checked"
                
                
                cell.starCellButton.setImage(image, for: .normal)
            }
            if allphoto.myStarCount == 1 {
                
                let image = UIImage(named: "Star filled w-shadow") as UIImage?
                self.imageValue = "Checked"
                
                
                cell.starCellButton.setImage(image, for: .normal)
            }
            

            cell.onReportTap = {
                
                self.seeDescription =  allphoto.description
                
                let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                if self.IDArray[indexPath.row].userId != userID{
                    self.titleReport = "Report photo"

                }
                else{
                    
                    self.titleReport = "Delete photo"

                }
                
                // create an action
                let firstAction: UIAlertAction = UIAlertAction(title: self.titleReport, style: .default) { action -> Void in
                    
                    if self.IDArray[indexPath.row].userId == userID{
                        
                        self.galeryID = String(allphoto.photoId)
                        self.PictureID = String(allphoto.pictureId)
                        self.BackButtonClick("Warning", message: "Are you sure you want to delete this photo?")
                        print("Delete")
                        
                        
                    }
                    else{
                        
                    self.navigationController?.navigationBar.isHidden = false

                self.navigationController?.setNavigationBarHidden(false, animated: true)
                    let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let passData = MainStoryboard.instantiateViewController(withIdentifier: "ReportPhoto") as! ReportPhoto
                    self.navigationController?.pushViewController(passData, animated: true)
                        
                    }
                    
                    
                }
               
                let thirdAction: UIAlertAction = UIAlertAction(title: "See description", style: .default) { action -> Void in
                    
//                    print("Share Photo")
//                    // image to share
//
//                    let text = allphoto.description
//                    let image = cell.imgImage.image
//                    let myWebsite = NSURL(string:"https://testglostarsdevelopers.azurewebsites.net/Home/ViewPicture/\(allphoto.photoId)")
//                    let shareAll = [text , image!,myWebsite] as [Any]
//                    let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
//                    activityViewController.popoverPresentationController?.sourceView = self.view
//                    self.present(activityViewController, animated: true, completion: nil)
                    
                    // create the alert
                    let alert = UIAlertController(title: "See description", message: self.seeDescription, preferredStyle: UIAlertController.Style.alert)
                    
                    // add an action (button)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                    

                }
                
                
                let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
                
                // add actions
                actionSheetController.addAction(thirdAction)
                actionSheetController.addAction(firstAction)
                //actionSheetController.addAction(secondAction)
                actionSheetController.addAction(cancelAction)
                
                // present an actionSheet...
                self.present(actionSheetController, animated: true, completion: nil)

                
            }
            cell.starCellButtonTap = {
                
                
                
                if self.imageValue == "Checked" {
                    
                    
                    let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/removerate/\(allphoto.photoId)"
                    //            let headers: HTTPHeaders = [
                    //                "Content-Type": "application/json"
                    //            ]
                    Alamofire.request(url, method:.post,encoding: URLEncoding.default).responseJSON { response in
                        switch response.result {
                        case .success:
                            
                            let imagevanish = UIImage(named: "Star small 2") as UIImage?
                            
                            
                            
                            
                            
                            cell.starCellButton.setImage(imagevanish, for: .normal)
                            
                            print(response)
                            
                            self.imageValue = ""
                            // self.ArticlesInfo.removeAll()
                            
                            // self.getRequest()
                            
                            
                            // self.tableView.reloadData()
                            cell.starCount.text = "Total Star:  " + allphoto.starcount.description
                            
                            
                            
                        case .failure(let error):
                            print(error)
                        }
                        
                        
                    }
                    
                    
                    
                    
                    
                }
                else{
                    
                    
                    let parameters: [String : Any]? = [
                        "NumOfStars": 1 ,
                        "WeekPictureId": allphoto.photoId ,
                        ]
                    let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/weeklycompetitionrating"
                    let headers: HTTPHeaders = [
                        "Content-Type": "application/json"
                    ]
                    Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
                        
                        if let jsonRoot = response.result.value as? [String:Any]!{
                            
                            //print(jsonRoot)
                            
                            if let resultPayload = jsonRoot!["resultPayload"] as? [String: Any]!{
                                
                                print(resultPayload)
                                let rating = resultPayload!["totalRating"]as! Int
                                
                                cell.starCount.text = "Total Star:  " + String(rating)
                                
                                self.imageValue = "Checked"
                                
                            }
                            
                            let image = UIImage(named: "Star filled w-shadow") as UIImage?
                            
                            
                            
                            
                            cell.starCellButton.setImage(image, for: .normal)
                            
                        }
                        
                        
                        
                        
                        
                        
                    }
                    //self.getRequest()
                }

                
            }
//            var rowIndex = indexPath.row
//            let numberOfRecords : Int = self.ComptitionPicFull.count - 1
//            if (rowIndex < numberOfRecords){
//                
//                
//                rowIndex = (rowIndex + 1 )
//            }
//            else{
//                
//                rowIndex = 0
//            }
            
            //ScrollingTimer = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(CompetitionPicFullVC.starTimer(theTimer:)), userInfo: rowIndex, repeats: true)
            
        }
        else {
            
            print("Error")
        }
        if indexPath.row == ComptitionPicFull.count - 1 {
            // last cell
            
            if totalItems > ComptitionPicFull.count {
                pageNumber = pageNumber + 1
                print(pageNumber)
                self.scrollEnable = ""

                // more items to fetch
                getRequest() // increment `fromIndex` by 20 before server call
            }
        }
        
        
        return cell
    }
    
    func BackButtonClick(_ title:String,message:String){
        
        let alertController = UIAlertController(title: title, message: message , preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            print("OK Pressed")
            
            var DeletePicUrl = ""
            
            
                DeletePicUrl = "https://testglostarsdevelopers.azurewebsites.net/api/images/weeklydelete/\(self.galeryID)"

            print(self.galeryID)
            Alamofire.request(DeletePicUrl, method: .post).responseJSON { response in
                
                switch response.result {
                    
                case .success:
                    
                    print(response)
                    
                    let nextViewController =                                  self.storyboard!.instantiateViewController(withIdentifier:"GaleryViewController")
                    
                    let nvc = UINavigationController(rootViewController: nextViewController)
                    self.present(nvc, animated: false, completion: nil)
                    
                case .failure(let error):
                    print(error)
                }
                
            }
            
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
            print("Cancel Pressed")
            //self.dismiss(animated: true, completion: nil)
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }

    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width
        let height = collectionView.frame.height
        
        return CGSize(width: width, height: height )
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func connected(_ sender:AnyObject){
        
        ScrollingTimer.invalidate()
        
        //print("you tap image number : \(sender.view.tag)")
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        ScrollingTimer.invalidate()
        
    }
    
    
    func setTimer() {
        ScrollingTimer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(CompetitionPicFullVC.autoScroll), userInfo: nil, repeats: true)
    }
    
    
     func autoScroll() {
        if self.x < ComptitionPicFull.count {
            
            if !UserDefaults.standard.bool(forKey: "autoSlide") {
                UserDefaults.standard.set(false, forKey: "autoSlide")
                self.x = 0
                self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
            }
            else {
                let indexPath = IndexPath(item: x, section: 0)
                self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                self.x = self.x + 1
            }
            
        } else {
            self.x = 0
            self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
    
  

    
   
        
    }


    



