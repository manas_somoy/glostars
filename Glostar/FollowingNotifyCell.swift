//
//  FollowingNotifyCell.swift
//  Glostars
//
//  Created by Sanzid iOS on 6/9/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit

class FollowingNotifyCell: UITableViewCell {
    
    
    @IBOutlet var followingButton: DesignableButton!
    @IBOutlet weak var statusButtonOutlet: DesignableButton!
    
    @IBOutlet weak var userPermissionButton: UIButton!
    @IBOutlet var imageClickOutlet: UIButton!
    
    @IBOutlet weak var proPicImage: UIImageView!
    
    @IBOutlet weak var nameProfile: UILabel!
    
    
    @IBOutlet weak var descriptionOutlet: UILabel!
    
    @IBOutlet var nameClickOutlet: UIButton!
    
    @IBOutlet weak var uploadTime: UILabel!
    
    var onFollowTapped: (() -> Void)? = nil
    var onPicTapped: (() -> Void)? = nil
    var onNameTapped: (() -> Void)? = nil
    var onUserPermissonTapped: (() -> Void)? = nil




    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        proPicImage.setRounded()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func FollowClickAction(_ sender: UIButton) {
        
        if let onFollowTapped = self.onFollowTapped {
            
            onFollowTapped()
            
        }
        
    }
    
    @IBAction func imageClickAction(_ sender: Any) {
        
        if let onPicTapped = self.onPicTapped {
            
            onPicTapped()
            
        }
    }
    @IBAction func UserPermissonAction(_ sender: Any) {
        
        if let onUserPermissonTapped = self.onUserPermissonTapped {
            
            onUserPermissonTapped()
            
        }
    }

    
    
    @IBAction func nameClickAction(_ sender: Any) {
        if let onNameTapped = self.onNameTapped {
            
            onNameTapped()
            
        }
        
    }
    

}
