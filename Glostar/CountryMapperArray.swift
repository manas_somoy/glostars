//
//  ProfileMapper.swift
//  Glostars
//
//  Created by Sanzid iOS on 5/26/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import Foundation
import ObjectMapper

class CountryMapperArray : Mappable {
    dynamic var Name : String = ""
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        Name <- map["name"]
    }
}
