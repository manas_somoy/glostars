//
//  ImageIdMapper.swift
//  Glostars
//
//  Created by Sanzid iOS on 2/2/18.
//  Copyright © 2018 Sanzid Ashan. All rights reserved.
//

//
//  ImageMapper.swift
//  Glostars
//
//  Created by Sanzid Ashan on 5/17/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import Foundation
import Foundation
import ObjectMapper

class ImageIdMapper : Mappable {
    dynamic var userId : String = ""
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        userId <- map["userId"]
    }
}

