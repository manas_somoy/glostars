//
//  EditPhotoMapper.swift
//  Glostars
//
//  Created by Sanzid on 2/3/19.
//  Copyright © 2019 Sanzid Ashan. All rights reserved.
//

import Foundation
import ObjectMapper

class EditPhotoMapper : Mappable {
    
    
    dynamic var disLikesCount : Int = 0
    dynamic var editPicUrl : String = ""
    dynamic var id : Int = 0
    dynamic var likesCount : Int = 0
    dynamic var myDislikesCount : Int = 0
    dynamic var myLikesCount : Int = 0
    dynamic var uploaded : String = ""
    dynamic var userId : String = ""
    dynamic var username : String = ""
    dynamic var getProfilePictureThumb : String = ""

    

    required convenience init?(map: Map) {
        
        
        self.init()
        
        
    }
    
    func mapping(map: Map) {
        disLikesCount <- map["disLikesCount"]
        editPicUrl <- map["editPicUrl"]
        id <- map["id"]
        likesCount <- map["likesCount"]
        myLikesCount <- map["myLikesCount"]
        myDislikesCount <- map["myDislikesCount"]
        uploaded <- map["uploaded"]
        userId <- map["userId"]
        username <- map["username"]
        getProfilePictureThumb <- map["getProfilePictureThumb"]

        
    }
}
