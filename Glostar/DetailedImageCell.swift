//
//  DetailedImageCell.swift
//  EditedProject
//
//  Created by sadidur rahman on 5/2/19.
//  Copyright © 2019 sadidur rahman. All rights reserved.
//

import UIKit

class DetailedImageCell: UITableViewCell {

    @IBOutlet weak var cellPhoto: UIImageView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var personImage: UIImageView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var personName: UILabel!
    @IBOutlet weak var thumbUpButton: UIButton!
    @IBOutlet weak var thumbDownButton: UIButton!
    @IBOutlet weak var thumbUPCountLabel: UILabel!
    @IBOutlet weak var thumbDownCountLabel: UILabel!
    
    
    var onThumbUpTapped: (() -> Void)?
    var onThumbDownTapped: (() -> Void)?
    var onDeleteTapped: (() -> Void)?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        userImage.setRounded()
        personImage.setRounded()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func thumbUpButtonPressed(_ sender: Any) {
        if self.onThumbUpTapped != nil{
            onThumbUpTapped!()
        }
        
    }
    
    
    @IBAction func thumbDownButtonPressed(_ sender: Any) {
        
        if self.onThumbDownTapped != nil{
            onThumbDownTapped!()
        }
    }
    
    @IBAction func deleteButtonPressed(_ sender: Any) {
        if self.onDeleteTapped != nil{
            onDeleteTapped!()
        }
        
        
    }
    
}
// if let onDeleteTap = self.onDeleteTap {
//onDeleteTap()
//}
