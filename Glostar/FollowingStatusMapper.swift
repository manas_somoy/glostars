//
//  FollowingStatusMapper.swift
//  Glostars
//
//  Created by Sanzid iOS on 6/13/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

//
//  FollowingMapper.swift
//  Glostars
//
//  Created by Sanzid iOS on 6/9/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import Foundation
import ObjectMapper

class FollowingStatusMapper : Mappable {
    
    
    dynamic var heFollows : Bool = true
    dynamic var isMutual : Bool = true
    dynamic var meFollows : Bool = true
   
    dynamic var  name : String = ""
    dynamic var lastName : String = ""
    dynamic var profilemediumPath : String = ""
    dynamic var id :String = ""
    
    
    
    
    required convenience init?(map: Map) {
        
        
        self.init()
        
        
    }
    
    func mapping(map: Map) {
        
        heFollows <- map["he_follow"]
        isMutual <- map["is_mutual"]
        meFollows <- map["me_follow"]
        name <- map["name"]
        lastName <- map["lastName"]
        profilemediumPath <- map["profilemediumPath"]
        id <- map["id"]
        
        
        
    }
}

