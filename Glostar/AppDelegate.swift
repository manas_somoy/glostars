//
//  AppDelegate.swift
//  Glostar
//
//  Created by Sanzid Ashan on 3/19/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import FBSDKCoreKit
import FBSDKLoginKit
import IQKeyboardManagerSwift
//import FacebookCore
//import GGLSignIn
import GoogleSignIn
import FirebaseCore
import FirebaseMessaging
import FirebaseInstanceID
import UserNotifications



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    
    var orientationLock = UIInterfaceOrientationMask.portrait
    
    static var shared: AppDelegate { return UIApplication.shared.delegate as! AppDelegate }
    
    //Notification UserDefault data Keys
    let appBagdeCount = "iconBadgeCount"
    let notificationCount = "notificationCount"
    
    //Notification Tap Flag
    fileprivate var didTapNotification = false
    
    //Notification data
    fileprivate var notification: [String: Any]?
    
    @objc(application:didFinishLaunchingWithOptions:) func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        
        //GIDSignIn.sharedInstance().clientID = FIRApp.defaultApp()?.options.clientID
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        UINavigationBar.appearance().barTintColor = UIColor.init(hex: "9a9a9a")
        //UINavigationBar.appearance().isTranslucent = true
        UINavigationBar.appearance().tintColor =  UIColor.white
        
        //        UINavigationBar.appearance().shadowImage = UIImage()
        //        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        //
        //        UINavigationBar.appearance().barTintColor = UIColor.white
        //        UINavigationBar.appearance().isTranslucent = false
        
        
        
        
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.3)
            // statusBar.backgroundColor = UIColor.clear
            statusBar.alpha = 0.5
            statusBar.tintColor = UIColor.red
        }
        
        
        //        DispatchQueue.main.async(execute: {
        //            var configureError: NSError?
        //            GGLContext.sharedInstance().configureWithError(&configureError)
        //            assert(configureError == nil, "Error configuring Google services: \(String(describing: configureError))")
        //
        //
        //           // SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        //
        //
        //        })
        
        FirebaseApp.configure()
        UNUserNotificationCenter.current().delegate = self
        Messaging.messaging().delegate = self
        
        Messaging.messaging().subscribe(toTopic: "topic/all") { error in
            
        }
        
        // Initialize Google sign-in
        GIDSignIn.sharedInstance().clientID = "387262906399-uakkip56fi9l8527cinhlubidlkiiqd7.apps.googleusercontent.com"
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        var vc : UIViewController
        
        if UserDefaults.standard.value(forKey: "E") == nil {
            
            vc = storyBoard.instantiateViewController(withIdentifier: "LoginViewController2")
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
            
            //Register Notification
        }else {
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            var vc : UIViewController
            vc = storyBoard.instantiateInitialViewController()!
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
            
            if let notification = launchOptions?[.remoteNotification] as? [String: Any] {
                didTapNotification = true
            }
            
            getRequest()
        }
        
        
        application.applicationIconBadgeNumber = 0
        UserDefaults.standard.removeObject(forKey: appBagdeCount)
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        
        // Mark: Custom Keyboard
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 70.0
        IQKeyboardManager.shared.shouldPlayInputClicks = true
        //        FIRMessaging.messaging().subscribe(toTopic: "all")
        
        
        
        return true
        
        
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return self.orientationLock
    }
    
    struct AppUtility {
        static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
            if let delegate = UIApplication.shared.delegate as? AppDelegate {
                delegate.orientationLock = orientation
            }
        }
        
        static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
            self.lockOrientation(orientation)
            UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
        }
    }
    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool
    {
        
        let faceBook =  FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        
        let Google =  GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApplication, annotation: annotation)
        
        return  faceBook || Google
    }
    
    
    
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // FBSDKAppEvents.activateApp()
        
        
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        UNUserNotificationCenter.current().getNotificationSettings(completionHandler: { settings in
            if settings.authorizationStatus == .authorized {
                //Already authorized
                print("PUSH NOTIFICATION ALLOWED")
            } else {
                // Either denied or notDetermined
                print("PUSH NOTIFICATION NOT ALLOWED")
            }
        })
        
        connecttoFCM(flag: false)
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        UNUserNotificationCenter.current().getNotificationSettings(completionHandler: { settings in
            if settings.authorizationStatus == .authorized {
                //Already authorized
                print("PUSH NOTIFICATION ALLOWED")
                
                
            } else {
                // Either denied or notDetermined
                print("PUSH NOTIFICATION NOT ALLOWED")
//                UserDefaults.standard.set("0", forKey: NOTIFICATION_COUNT)
            }
        })
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        connecttoFCM(flag: true)
        
        application.applicationIconBadgeNumber = 0
        UserDefaults.standard.removeObject(forKey: appBagdeCount)
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        // self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    
    
    // MARK: - Core Data Saving support
    
    //    func saveContext () {
    //        if #available(iOS 10.0, *) {
    //            let context = persistentContainer.viewContext
    //        } else {
    //            // Fallback on earlier versions
    //        }
    //        if context.hasChanges {
    //            do {
    //                try context.save()
    //            } catch {
    //                // Replace this implementation with code to handle the error appropriately.
    //                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
    //                let nserror = error as NSError
    //                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
    //            }
    //        }
    //    }
    func  getRequest(){
        
        
        let params : Parameters = [
            "grant_type" : "password",
            "password" : UserDefaults.standard.value(forKey: "P")!,
            "username" : UserDefaults.standard.value(forKey: "E")!,
            
            ]
        
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/Token"
        Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.httpBody)
            .responseJSON{ response in
                
                if response.result.value != nil{
                    
                    if let jsonRoot = response.result.value as? [String:Any]!{
                        
                        print(jsonRoot)
                        
                        if jsonRoot!["access_token"] != nil  && jsonRoot!["token_type"] != nil {
                            accessToken = jsonRoot!["access_token"] as! String!
                            UserDefaults.standard.set(accessToken, forKey: "access")
                            
                            tokenType = jsonRoot!["token_type"] as! String!
                            UserDefaults.standard.set(tokenType, forKey: "token")
                            self.getData()
                            
                            
                        }
                        else {
                            
                            DispatchQueue.main.async{
                                
                            }
                            
                            //self.displayMyAlertMessage("Wrong User Name or password ")
                            //SwiftMessageService.WarningMessage("Warning", description: "Wrong User Name or password ")
                        }
                        
                        
                        // self.localStorage()
                        
                        
                        
                    }
                    else {
                        
                        
                    }
                }
                
                
        }
        
    }
    
    
    
    
    func  getData() {
        
        
        
        
        let params2 : Parameters = [
            
            "userEmail" : UserDefaults.standard.value(forKey: "E")!,
            
            ]
        
        let url2 = "https://testglostarsdevelopers.azurewebsites.net/api/account/GetUserInfo"
        
        Alamofire.request(url2, method: .get, parameters: params2, encoding: URLEncoding.default)
            
            
            .responseJSON{ response2 in
                
                if response2.result.value != nil {
                    
                    if let jsonRoot = response2.result.value as? [String:Any]!{
                        print(jsonRoot)
                        
                        if let userinfo = jsonRoot!["resultPayload"] as? [String: Any]!{
                            
                            
                            //                            gender = userinfo["gender"] as! String
                            //                            print(gender)
                            //
                            userID = userinfo!["userId"] as! String
                            print(userID)
                            UserDefaults.standard.set(userID, forKey: "id")
                            userPic = userinfo!["profilePicURL"] as! String
                            print(userPic)
                            UserDefaults.standard.set(userPic, forKey: "pic")
                            
                            userNameR = userinfo!["name"] as! String
                            
                            if self.didTapNotification {
                                let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                let passData = MainStoryboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
                                
                                if let notification = self.notification, let data = notification["data"] as? [String: Any] {
                                    print(data)
                                }
                                
                                if let rootNav = self.window?.rootViewController as? UINavigationController {
                                    rootNav.pushViewController(passData, animated: true)
                                }
                            }
                            
                        }
                        
                        
                    }
                    else{
                        
                    }
                }
                
                
                
                
        }
        
        
        
    }
    
    //MARK:- Ask User For Notification Permission
    func registerForPushNotifications() {
        UNUserNotificationCenter.current()
            .requestAuthorization(options: [.alert, .sound, .badge]) {
                [weak self] granted, error in
                
                print("Permission granted: \(granted)")
                guard granted else { return }
                self?.getNotificationSettings()
        }
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            print("Notification settings: \(settings)")
            
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    //MARK: Send FCMToken To Server
    func sendNotificationToken(fcmToken: String) {
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/fcmtokens/tokens"
        let params : Parameters = [
            "Email" : UserDefaults.standard.string(forKey: "E") ?? "",
            "UserId" : UserDefaults.standard.string(forKey: "id") ?? "",
            "FcmToken" : fcmToken]
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(String(describing: UserDefaults.standard.value(forKey: "id")!) + String(describing: UserDefaults.standard.value(forKey: "access")!))"/* + (UserDefaults.standard.string(forKey: "access") ?? "")*/,
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        print(String(describing: UserDefaults.standard.value(forKey: "id")!) + String(describing: UserDefaults.standard.value(forKey: "access")!))
        
        Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                print(value)
            case .failure(let error):
                print(response)
                print(error)
            }
        }
    }
}

//MARK:- UNUserNotificationCenterDelegate
extension AppDelegate: UNUserNotificationCenterDelegate{
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if userInfo is [String: Any] {
            var badge = 0
            if UserDefaults.standard.object(forKey: appBagdeCount) != nil  {
                var count = UserDefaults.standard.integer(forKey: appBagdeCount)
                count = count + 1
                badge = count
                
                UserDefaults.standard.set(count, forKey: appBagdeCount)
            } else {
                UserDefaults.standard.set(1, forKey: appBagdeCount)
                badge = 1
            }
            UIApplication.shared.applicationIconBadgeNumber = badge
            
            if UserDefaults.standard.object(forKey: notificationCount) != nil  {
                var count = UserDefaults.standard.integer(forKey: notificationCount)
                count = count + 1
                UserDefaults.standard.set(count, forKey: notificationCount)
            } else {
                UserDefaults.standard.set(1, forKey: notificationCount)
            }
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([.alert, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        
        if let rootNav = self.window?.rootViewController as? UINavigationController {
            rootNav.pushViewController(passData, animated: true)
        }
        
        completionHandler()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        print("\nAPN Device Token: \(token)\n")
        
        Messaging.messaging().apnsToken = deviceToken
        
        InstanceID.instanceID().instanceID(handler: { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                self.sendNotificationToken(fcmToken: result.token)
            }
        })
        
        //MARK: Subscribe to notification topic
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
}

//MARK:- MessagingDelegate
extension AppDelegate: MessagingDelegate{
    
    func connecttoFCM(flag: Bool) {
        
        Messaging.messaging().shouldEstablishDirectChannel = flag
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        
        connecttoFCM(flag: true)
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
}
