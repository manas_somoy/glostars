//
//  SportArticleMapper.swift
//  NewsApp
//
//  Created by Sanzid Ashan on 5/2/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import Foundation
import ObjectMapper

class PublicFeeedMapper : Mappable {
    
    
//    dynamic var picUrl : String = ""
//    dynamic var profilePicURL : String = ""
//    dynamic var description : String = ""
//    dynamic var name : String = ""
//    dynamic var uploadedTime : String = ""
//    dynamic var totalmutualFollowerPics : String = ""
//    dynamic var starcount : Int = 0
//    dynamic var idPhoto : Int = 0
//    dynamic var featuredPicture : Bool = true
//    dynamic var isCompeting : Bool = true
//
//    dynamic var is_mutual : Bool = true
//
//    dynamic var privacyPublic : String = ""
//
//    dynamic var privacyFriend : String = ""
//    dynamic var myStarCount : Int = 0
//
//
//
//    required convenience init?(map: Map) {
//
//
//        self.init()
//
//
//    }
//
//    func mapping(map: Map) {
//        picUrl <- map["picUrl"]
//        profilePicURL <- map["profilePicURL"]
//        description <- map["description"]
//        name <- map["name"]
//        uploadedTime <- map["uploaded"]
//        totalmutualFollowerPics <- map["totalmutualFollowerPics"]
//        starcount <- map["starsCount"]
//        idPhoto <- map["id"]
//        featuredPicture <- map["isfeatured"]
//        isCompeting <- map["isCompeting"]
//        is_mutual <- map["is_mutual"]
//        privacyPublic <- map["privacy"]
//        privacyFriend <- map["privacy"]
//        myStarCount <- map["myStarCount"]
//
//    }
//}
    dynamic var picUrl : String = ""
    dynamic var profilePicURL : String = ""
    dynamic var description : String = ""
    dynamic var name : String = ""
    dynamic var uploadedTime : String = ""
    dynamic var totalmutualFollowerPics : String = ""
    dynamic var starcount : Int = 0
    dynamic var idPhoto : Int = 0
    dynamic var featuredPicture : Bool = true
    dynamic var isCompeting : Bool = true
    
    dynamic var is_mutual : Bool = true
    
    dynamic var privacyPublic : String = ""
    
    dynamic var privacyFriend : String = ""
    dynamic var myStarCount : Int = 0
    dynamic var totalEditPhoto : Int = 0
    
    
    
    
    required convenience init?(map: Map) {
        
        
        self.init()
        
        
    }
    
    func mapping(map: Map) {
        picUrl <- map["picUrl"]
        profilePicURL <- map["profilePicURL"]
        description <- map["description"]
        name <- map["name"]
        uploadedTime <- map["uploaded"]
        totalmutualFollowerPics <- map["totalmutualFollowerPics"]
        starcount <- map["starsCount"]
        idPhoto <- map["id"]
        featuredPicture <- map["isfeatured"]
        isCompeting <- map["isCompeting"]
        is_mutual <- map["is_mutual"]
        privacyPublic <- map["privacy"]
        privacyFriend <- map["privacy"]
        totalEditPhoto <- map["totalEditPhoto"]
        
}
}

