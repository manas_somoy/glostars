//
//  DesignableTextField.swift
//  Glostar
//
//  Created by Sanzid Ashan on 3/21/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

//
//  Button.swift
//  Glostar
//
//  Created by Sanzid Ashan on 3/20/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit

@IBDesignable class DesignableTextField : UITextField {
    
    
    @IBInspectable var borderWidth : CGFloat = 0 {
        
        didSet{
            self.layer.borderWidth = borderWidth
            
        }
    }
    @IBInspectable var borderColor : UIColor = UIColor.clear {
        
        didSet{
            self.layer.borderColor = borderColor.cgColor
            
        }
    }
    
    @IBInspectable var cornerRadious : CGFloat = 0 {
        
        didSet{
            self.layer.cornerRadius = cornerRadious
            
        }
    }
    
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSForegroundColorAttributeName: newValue!])
        }
    }
    
}


