//
//  FeedTableViewCell.swift
//  Glostars
//
//  Created by Sanzid Ashan on 3/30/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit

class PublicFeedCell: UITableViewCell {
    
    
    @IBOutlet var paintCount: UILabel!
    
    
    @IBOutlet var commentButtonOutlet: UIButton!
    
    @IBOutlet weak var start1Outlet: UIButton!
    @IBOutlet weak var startCount: UILabel!
    @IBOutlet weak var uploadedTime: UILabel!
    @IBOutlet weak var proPic: UIImageView!
    @IBOutlet weak var cellPhoto: UIImageView!
    
    @IBOutlet weak var nameUser: UILabel!
    
    @IBOutlet weak var commentCount: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var featuredImageOutlet: UIImageView!
    @IBOutlet weak var featureTextOutlet: UILabel!
    @IBOutlet weak var star2Outlet: UIButton!
    
    @IBOutlet weak var star3Outlet: UIButton!
    
    @IBOutlet weak var heightConstraintFeature: NSLayoutConstraint!
    
    @IBOutlet var vanishedSpace1: NSLayoutConstraint!
    
    @IBOutlet var vanishedSpaced2: NSLayoutConstraint!
    
    
    @IBOutlet var vanishedSpaced3: NSLayoutConstraint!
    
    
    
    @IBOutlet var vanished4Space: NSLayoutConstraint!
    
    @IBOutlet weak var heightConstraintsFooterFeatureView: NSLayoutConstraint!
    
    @IBOutlet weak var seeAllPicOutlet: UIButton!
    
    
    @IBOutlet weak var privacyPic: UIImageView!
    
    @IBOutlet weak var star4Outlet: UIButton!
    
    @IBOutlet weak var widthConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var star5Outlet: UIButton!
    
    @IBOutlet var deleteButton: UIButton!
    
    @IBOutlet var editButton: UIButton!
    
    @IBOutlet weak var reportButton: UIButton!
    
    var onProfileTapped: (() -> Void)? = nil
    var onStarTapped1: (() -> Void)? = nil
    var onStarTapped2: (() -> Void)? = nil
    var onStarTapped3: (() -> Void)? = nil
    var onStarTapped4: (() -> Void)? = nil
    var onStarTapped5: (() -> Void)? = nil
    var onCrossTapped: (() -> Void)? = nil
    var onCommentsTapped: (() -> Void)? = nil
    
    var ondescriptionLabel: (() -> Void)? = nil
    var onDeleteTap: (() -> Void)? = nil
    var onEditTap: (() -> Void)? = nil
    var onReport: (() -> Void)? = nil
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        proPic.setRounded()
        proPic.layer.borderColor = UIColor.white.cgColor
        proPic.layer.borderWidth = 1.0
        
        
        
        //cellPhoto.contentMode = UIViewContentMode.scaleAspectFit
        //cellPhoto.clipsToBounds = true
        
        // Configure the view for the selected state
    }
    
    
    @IBAction func ProfileClickAction(_ sender: UIButton) {
        
        if let onProfileTapped = self.onProfileTapped {
            
            onProfileTapped()
            
        }
        
    }
    
    
    @IBAction func deleteButtonAction(_ sender: Any) {
        
        if let onDeleteTap = self.onDeleteTap {
            
            onDeleteTap()
            
        }
    }
    
    
    @IBAction func editButtonAction(_ sender: Any) {
        
        if let onEditTap = self.onEditTap {
            
            onEditTap()
            
        }
        
    }
    
    @IBAction func CrossClickAction(_ sender: UIButton) {
        
        if let onCrossTapped = self.onCrossTapped {
            
            onCrossTapped()
            
        }
        
    }
    
    
    
    @IBAction func StarAction1(_ sender: UIButton) {
        
        if let onStarTapped1 = self.onStarTapped1 {
            
            onStarTapped1()
            
        }
        
        
        
    }
    
    @IBAction func starAction2(_ sender: Any) {
        
        if let onStarTapped2 = self.onStarTapped2 {
            
            onStarTapped2()
            
        }
        
    }
    
    
    
    @IBAction func DescriptionTap(_ sender: UILabel) {
        
        if let ondescriptionLabel = self.ondescriptionLabel {
            
            ondescriptionLabel()
            
        }
    }
    
    
    @IBAction func starAction4(_ sender: Any) {
        
        
        if let onStarTapped4 = self.onStarTapped4 {
            
            onStarTapped4()
            
        }
    }
    
    
    
    @IBAction func starAction5(_ sender: Any) {
        
        if let onStarTapped5 = self.onStarTapped5 {
            
            onStarTapped5()
            
        }
    }
    
    @IBAction func commentsTappedaction(_ sender: Any) {
        
        if let onCommentsTapped = self.onCommentsTapped {
            
            onCommentsTapped()
            
        }
    }
    
    @IBAction func reportButtonPressed(_ sender: Any) {
        if let onReportTapped = self.onReport{
            onReportTapped()
        }
    }
    
}

