//
//  BlockMapper.swift
//  Glostars
//
//  Created by Sanzid on 2/3/19.
//  Copyright © 2019 Sanzid Ashan. All rights reserved.
//

import Foundation
import ObjectMapper

class BlockMapper : Mappable {
    
    
    dynamic var id : String = ""
    dynamic var lastname : String = ""
    dynamic var name : String = ""
    dynamic var profilemediumPath : String = ""
   
    
    
    
    required convenience init?(map: Map) {
        
        
        self.init()
        
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        lastname <- map["lastname"]
        name <- map["name"]
        profilemediumPath <- map["profilemediumPath"]
        
        
    }
}
