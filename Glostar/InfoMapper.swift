//
//  InfoMapper.swift
//  Glostars
//
//  Created by Sanzid Ashan on 5/14/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import Foundation


import Foundation
import ObjectMapper

class InfoMapper : Mappable {
    
    
    dynamic var picUrl : String = ""
    dynamic var profilePicURL : String = ""
    dynamic var description : String = ""
    dynamic var name : String = ""
    dynamic var uploadedTime : String = ""
    dynamic var totalmutualFollowerPics : String = ""
    dynamic var starcount : String = ""
    dynamic var commentMessage : [String :Any] = [:]
    dynamic var id : String = " "
    dynamic var photoId :Int = 0

    required convenience init?(map: Map) {
        
        
        self.init()
        
        
    }
    
    func mapping(map: Map) {
        picUrl <- map["picUrl"]
        profilePicURL <- map["profilePicURL"]
        description <- map["description"]
        name <- map["name"]
        uploadedTime <- map["uploaded"]
        totalmutualFollowerPics <- map["totalmutualFollowerPics"]
        starcount <- map["starsCount"]

        commentMessage <- map["commentMessage"]
        id <- map["userId"]
        photoId <- map["id"]
        
        
    }
}

