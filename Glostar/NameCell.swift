//
//  NameCell.swift
//  Glostars
//
//  Created by Sanzid iOS on 5/26/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit

class NameCell: UITableViewCell {

    @IBOutlet weak var searchProfilePic: UIImageView!
    @IBOutlet weak var lastName: UILabel!
    @IBOutlet weak var userName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        searchProfilePic.setRounded()
    }

}
