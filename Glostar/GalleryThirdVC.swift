//
//  GalleryThirdVC.swift
//  Glostars
//
//  Created by sadidur rahman on 5/1/19.
//  Copyright © 2019 Sanzid Ashan. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SDWebImage
import ObjectMapper

class GalleryThirdVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    var weekNumber = 0
    var year = ""
    var imagepath = ""
    var themeName = ""
    
    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var image3: UIImageView!
    @IBOutlet weak var image2: UIImageView!
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionView1{
            return ComptitionTop.count
            
        }
        else{
            return ComptitionPic.count
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "CompetitionPicFullVC") as! CompetitionPicFullVC
        
        passData.indexNumber = CGFloat(indexPath.row)
        passData.allPostWeekLy = "true"
        passData.weekNumber = self.weekNumber
        passData.year = self.year
        self.navigationController?.pushViewController(passData, animated: true)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionView1{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCell1", for: indexPath) as! GalleryCell1
            let url = URL(string: ComptitionTop[indexPath.row].photourlTwo)
            cell.imageView.af_setImage(withURL: url!)
            
            return cell
            
        }
        else{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCell2", for: indexPath) as! GalleryCell2
            let url = URL(string: ComptitionPic[indexPath.row].picUrl)
            
            cell.imageView.af_setImage(withURL: url!)
            
            
            return cell
            
            
        }
    }
    
    
    
    var ComptitionPi : Array<SecondPageMapper> = []
    var ComptitionTop : Array<TopImageMappre> = []
    
    
    
    @IBOutlet weak var collectionView2: UICollectionView!
    @IBOutlet weak var collectionView1: UICollectionView!
    @IBOutlet weak var subcripTionLabel: UILabel!
    
    @IBOutlet weak var topImage: UIImageView!
    @IBOutlet weak var weekLabel: UILabel!
    
    @IBOutlet weak var themeLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.isUserInteractionEnabled = true
        self.collectionView2.isUserInteractionEnabled = true
        
        collectionView2.delegate = self
        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.getRequest()
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.white,
             NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 18)!]
    }
    
    func getRequest(){
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/weeklycompetitionsingleweek/\(weekNumber)/\(year)/1"
        
        Alamofire.request(url).responseJSON { response in
            
            
            if response.result.value != nil{
                
                if let jsonRoot = response.result.value as? [String:Any]{
                    print(jsonRoot)
                    ComptitionPic.removeAll()
                    
                    if let resultPayload = jsonRoot["resultPayload"] as? [String:Any]{
                        
                        print(resultPayload)
                        let path =  self.imagepath
                        let url = URL(string: path)
                        self.topImage.af_setImage(withURL: url!)
                        let submission = resultPayload["totalsubmission"] as! Int
                        self.subcripTionLabel.text = String(submission) + " SUBMISSIONS"
                        self.weekLabel.text = "WEEK " + String(self.weekNumber)
                        self.themeLabel.text = self.themeName.uppercased()
                        self.navigationItem.title = self.themeName
                        
                        if let resultallPost = resultPayload["allpost"] as?[ [String:Any]]{
                            
                            print(resultallPost)
                            
                            
                            for data in resultallPost {
                                
                                guard  let CompititionImage = Mapper<GaleryMapper>().map(JSON: data) else {
                                    continue
                                    
                                }
                                
                                
                                //ComptitionPic.removeAll()
                                ComptitionPic.append(CompititionImage)
                                self.collectionView2.reloadData()
                                
                                
                            }
                            
                            
                        }
                        if let resultTopPost = resultPayload["topthree"] as?[[String:Any]]{
                            
                            print(resultTopPost)
                            
                            for  dic in resultTopPost{
                                if let one = dic["winnerOne"] as?[String:Any]{
                                    
                                    print(one)
                                    
                                    let image1 = one["photourlOne"] as? String
                                    
                                    if image1 != nil {
                                        let url = URL(string: image1!)
                                        self.image1.af_setImage(withURL: url!)
                                        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(tapGestureRecognizer:)))
                                        self.image1.isUserInteractionEnabled = true
                                        self.image1.addGestureRecognizer(tapGestureRecognizer)
                                        
                                    }
                                        
                                    else{
                                        print("error")
                                    }
                                    
                                    
                                }
                                if let one = dic["winnerTwo"] as?[String:Any]{
                                    
                                    print(one)
                                    
                                    let image1 = one["photourlTwo"] as? String
                                    
                                    if image1 != nil {
                                        let url = URL(string: image1!)
                                        self.image2.af_setImage(withURL: url!)
                                        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(tapGestureRecognizer:)))
                                        self.image2.isUserInteractionEnabled = true
                                        self.image2.addGestureRecognizer(tapGestureRecognizer)
                                    }
                                        
                                    else{
                                        print("error")
                                    }
                                    
                                    
                                    
                                }
                                if let one = dic["winnerThree"] as?[String:Any]{
                                    
                                    print(one)
                                    
                                    let image1 = one["photourlThree"] as? String
                                    
                                    if image1 != nil {
                                        let url = URL(string: image1!)
                                        self.image3.af_setImage(withURL: url!)
                                        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(tapGestureRecognizer:)))
                                        self.image3.isUserInteractionEnabled = true
                                        self.image3.addGestureRecognizer(tapGestureRecognizer)
                                    }
                                        
                                    else{
                                        print("error")
                                    }
                                    
                                    
                                    
                                    
                                }
                                
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                }
                    
                else {
                    
                    print("Internet Error")
                    self.getRequest()
                    
                }
            }
            
        }
        
    }
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        print("tapped")
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "CompetitionTopThreeWinnerVC") as! CompetitionTopThreeWinnerVC
        passData.weekNumber = weekNumber
        passData.year = year
       // passData.indexNumber = 0
        self.navigationController?.pushViewController(passData, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width/3 - 1
        print(collectionView.frame.width)
        return CGSize(width: width , height: width)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
}
