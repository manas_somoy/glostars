//
//  DetailComMapper.swift
//  Glostars
//
//  Created by Sanzid iOS on 6/3/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import Foundation
import ObjectMapper

class DetailComMapper : Mappable {
    
    dynamic var comments : String = " "
    dynamic var userName : String = " "
    dynamic var userproPic : String = " "
    dynamic var uploadTime : String = " "
    dynamic var lastName : String = " "
    dynamic var commenterId : String = " "
    dynamic var commentId : Int = 0

    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
    
        comments <- map["commentMessage"]
        userName <- map["firstName"]
        userproPic <- map["profilePicUrl"]
        uploadTime <- map["commentTime"]
        lastName <- map["lastName"]
        commenterId <- map["commenterId"]
        commentId <- map["commentId"]

        
    }
}

