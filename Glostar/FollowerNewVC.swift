////
////  FollowerVC.swift
////  Glostars
////
////  Created by Sanzid iOS on 7/12/17.
////  Copyright © 2017 Sanzid Ashan. All rights reserved.
////
//
////
////  FollowingViewController.swift
////  Glostars
////
////  Created by Sanzid Ashan on 3/30/17.
////  Copyright © 2017 Sanzid Ashan. All rights reserved.
////
//
//import UIKit
//
//class FollowerNewVC: UIViewController, UITableViewDataSource,UITableViewDelegate {
//    
//    var FollowingNotifyArray : Array< FollowingMapper > = []
//    var FollowingstatusArray : Array <FollowingStatusMapper> = []
//    var FollowerArray : Array<FollowUserMapper> = []
//    
//    var getNewUserId = String()
//    var getUserName = String()
//    var is_mutual = Bool()
//    var me_follow = Bool()
//    var he_follow = Bool()
//     var pageNumber = 0
//    
//    @IBOutlet weak var tableView: UITableView!
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        gettingFollowerUser()
//        pageNumber = 1
//        gettingFollowerUser()
//        pageNumber = 2
//        gettingFollowerUser()
//        tableView.tableFooterView = UIView(frame: .zero)
//        
//        gettingFollowerUser()
//        
//       
//        
//        self.navigationItem.title = "\(getUserName)'s Followers"
//        self.navigationController?.navigationBar.titleTextAttributes =
//            [NSForegroundColorAttributeName: UIColor.white,
//             NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 18)!]
//
//        //cintainerView.isHidden = true
//        //tableView2.isHidden = true
//        
//        // Do any additional setup after loading the view.
//    }
//    
// 
//    
//    
//    func gettingFollowerUser() {
//        
//        
////        let url = "https://testglostarsdevelopers.azurewebsites.net/api/account/GetUserFollowById?userId=\(getNewUserId)&page=\(pageNumber)"
////        Alamofire.request(url, method: .get , encoding: JSONEncoding.default)
////
////
////            .responseJSON{ response in
////
////
////                if let jsonRoot = response.result.value as? [String:Any]{
////
////                    if let resultPayload = jsonRoot["resultPayload"] as? [String : Any]{
////
////                        if let followList = resultPayload["followerList"] as? [[String : Any]] {
////                            for data in followList{
////
////                                guard let followList = Mapper<FollowUserMapper>().map(JSON: data) else{
////
////                                    continue
////                                }
////
////                                self.FollowerArray.append(followList)
////                                //self.tableView.reloadData()
////
////                            }
////
////
////                        }
////
////                        if (resultPayload["followingList"] as? [[String : Any]]) != nil {
////
////
////
////                        }
////
////
////
////                    }
////                    self.tableView.reloadData()
////
////
////                }
////        }
////
////
//        
//    }
//    
//    
//    
//    func gettingFollowerUserWithout() {
//        
//        
////        let url = "https://testglostarsdevelopers.azurewebsites.net/api/account/GetUserFollowById?userId=\(getNewUserId)&page=\(pageNumber)"
////        Alamofire.request(url, method: .get , encoding: JSONEncoding.default)
////
////
////            .responseJSON{ response in
////
////
////                if let jsonRoot = response.result.value as? [String:Any]{
////
////                    if let resultPayload = jsonRoot["resultPayload"] as? [String : Any]{
////
////                        if let followList = resultPayload["followerList"] as? [[String : Any]] {
////                            for data in followList{
////
////                                guard let followList = Mapper<FollowUserMapper>().map(JSON: data) else{
////
////                                    continue
////                                }
////
////                                self.FollowerArray.append(followList)
////                                self.tableView.reloadData()
////
////                            }
////
////
////                        }
////
////                        if (resultPayload["followingList"] as? [[String : Any]]) != nil {
////
////
////
////                        }
////
////
////
////                    }
////                    self.tableView.reloadData()
////
////
////                }
////        }
////
//        
//        
//    }
//    
//
//    
//    
//    
//    
//    // this method will be called by the table view to know the count of rows for each section
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        
//        return FollowerArray.count
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        
//        
//        
//        
//        
//        // create the table view cell design from the prototype cell we have created in storyboard. Reuseidentifier will have same string as in storyboard
////        let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellTwo", for: indexPath) as! FollowerCell
////
////
////        tableViewCell.lastname.text = FollowerArray[indexPath.row].lastName
////        tableViewCell.nameProfile.text = FollowerArray[indexPath.row].name
////        let url = NSURL(string: FollowerArray[indexPath.row].proPic)
////
////        tableViewCell.proPicImage.af_setImage(withURL: url! as URL, placeholderImage: #imageLiteral(resourceName: "male"), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .crossDissolve(0.5), runImageTransitionIfCached: true, completion: nil)
////
////
////        if self.FollowerArray[indexPath.row].is_mutual == true {
////
////            tableViewCell.followButtonOutlet.setTitle("Mutual", for: .normal)
////            tableViewCell.followButtonOutlet.backgroundColor = UIColor(hex: 0x660066, alpha: 1)
////
////        }
////
////
////        if self.FollowerArray[indexPath.row].is_mutual == false && self.FollowerArray[indexPath.row].me_follow == false && self.FollowerArray[indexPath.row].he_follow == false {
////
////            tableViewCell.followButtonOutlet.setTitle("Follow", for: .normal)
////
////            tableViewCell.followButtonOutlet.backgroundColor = UIColor(hex: 0x4d94ff, alpha: 1)
////
////        }
////
////        if self.FollowerArray[indexPath.row].is_mutual == false && self.FollowerArray[indexPath.row].me_follow == true && self.FollowerArray[indexPath.row].he_follow == false {
////
////            tableViewCell.followButtonOutlet.setTitle("Following", for: .normal)
////            tableViewCell.followButtonOutlet.backgroundColor = UIColor(hex: 0xcc99ff, alpha: 1)
////
////        }
////        if self.FollowerArray[indexPath.row].is_mutual == false && self.FollowerArray[indexPath.row].me_follow == false && self.FollowerArray[indexPath.row].he_follow == true {
////
////            tableViewCell.followButtonOutlet.setTitle("Follow Back", for: .normal)
////            tableViewCell.followButtonOutlet.backgroundColor = UIColor.darkGray
////        }
////        tableViewCell.onFollowTapped = {
////
////
////
////            let url = "https://testglostarsdevelopers.azurewebsites.net/api/Follower/Following/\(self.FollowerArray[indexPath.row].id)"
////
////
////            Alamofire.request(url, method: .post).responseJSON { response in
////
////
////
////                if let jsonRoot = response.result.value as? [String:Any]{
////
////                    if let resultPayload = jsonRoot["resultPayload"] as? [String : Any]{
////
////                        self.is_mutual = (resultPayload["is_mutual"] as? Bool)!
////                        self.me_follow = (resultPayload["me_follow"] as? Bool)!
////                        self.he_follow = (resultPayload["he_follow"] as? Bool)!
////
////                        if self.is_mutual == true {
////
////                        tableViewCell.followButtonOutlet.setTitle("Mutual", for: .normal)
////                            tableViewCell.followButtonOutlet.backgroundColor = UIColor(hex: 0x660066, alpha: 1)
////
////                        }
////
////
////                        if self.is_mutual == false && self.me_follow == false && self.he_follow == false {
////
////                            tableViewCell.followButtonOutlet.setTitle("Follow", for: .normal)
////
////                            tableViewCell.followButtonOutlet.backgroundColor = UIColor(hex: 0x4d94ff, alpha: 1)
////                        }
////
////                        if self.is_mutual == false && self.me_follow == true && self.he_follow == false {
////
////                            tableViewCell.followButtonOutlet.setTitle("Following", for: .normal)
////                            tableViewCell.followButtonOutlet.backgroundColor = UIColor(hex: 0xcc99ff, alpha: 1)
////
////                        }
////                        if self.is_mutual == false && self.me_follow == false && self.he_follow == true {
////
////                            tableViewCell.followButtonOutlet.setTitle("Follow Back", for: .normal)
////                            tableViewCell.followButtonOutlet.backgroundColor = UIColor.gray
////
////                        }
////
////
////                    }
////                }
////
////
////
////
////
////
////
////
////            }
//        
//       // }
//
//        
//        //return tableViewCell
//    }
//    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        
//        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileSearchVC") as! ProfileSearchVC
//        
//        passData.getid = self.FollowerArray[indexPath.row].id
//        passData.getName = self.FollowerArray[indexPath.row].name
//        passData.getProfileUrl = self.FollowerArray[indexPath.row].proPic
//        passData.getLastName = self.FollowerArray[indexPath.row].lastName
//
//        
//        self.navigationController?.pushViewController(passData, animated: true)
//        
//    }
//    
//    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let offsetY = scrollView.contentOffset.y
//        let contentHeight = scrollView.contentSize.height
//        
//        if offsetY > contentHeight - scrollView.frame.size.height {
//            gettingFollowerUser()
//            pageNumber += 1
//            self.tableView.reloadData()
//            
//        }
//    }
//    
//    
//    /*
//     // MARK: - Navigation
//     
//     // In a storyboard-based application, you will often want to do a little preparation before navigation
//     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//     // Get the new view controller using segue.destinationViewController.
//     // Pass the selected object to the new view controller.
//     }
//     */
//    
//}
