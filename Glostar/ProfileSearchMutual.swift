//
//  NotificationViewController.swift
//  Glostars
//
//  Created by Sanzid Ashan on 3/30/17.
//  Copyright © 2017 Sanzid Ashan. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireImage
extension UITextField {
    
    func useUnderline3() {
        
        let border = CALayer()
        let borderWidth = CGFloat(1.0)
        border.borderColor = UIColor.gray.cgColor
        border.frame = CGRect(x:0, y: self.frame.size.height - borderWidth, width : self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = borderWidth
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}

class ProfileSearchMutual: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout , UITextFieldDelegate , UITableViewDataSource,UITableViewDelegate ,FloatyDelegate {
    
    @IBOutlet var hiddingView: UIView!
    var NameArray : Array<ProfileMapper> = []
    var totalItems = 100

    var fab = Floaty()


    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: DesignableTextField!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var achivementButton: UIButton!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var uploadButton: UIButton!
    
    @IBOutlet weak var floatingMainButton: UIButton!
    
    var publicInfo : Array<ImageMapper> = []
    
    
    
    
    var pageNumber = 1
    
    
    var getSearchUserid = String()
    var getTheName = String()
    
    var Compitition : Array<CompititionMapper> = []
    var PublicArray : Array<PublicMapper> = []
    var MutualArray : Array<MutualMapper> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hiddenFloatingButtons()
        
        
        tableView.isHidden = true
        getRequest()
        self.layoutFAB()
        print(getSearchUserid)
        
        let urlString = URL(string: userPic)!
        
        profileButton.af_setBackgroundImage(for: [] , url: urlString )
        
        self.navigationItem.title = getTheName + "'s" + " Mutual Pictures"
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.white,
             NSFontAttributeName: UIFont(name: "Ubuntu-Light", size: 20)!]
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func viewWillAppear(_ animated: Bool) {
        hiddingView.isHidden = true
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NameArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NameCell
        
        let array = NameArray[indexPath.row]
        
        let Url = NSURL(string: array.proLink)
        
        cell.userName.text = NameArray[indexPath.row].userName
        cell.lastName.text = NameArray[indexPath.row].lastName
        cell.searchProfilePic.af_setImage(withURL: Url! as URL, placeholderImage:  UIImage.gif(name: "load_old"), filter: nil, progress: nil, progressQueue: DispatchQueue.main , imageTransition: .crossDissolve(0.5), runImageTransitionIfCached: true, completion: nil)
        
        return cell
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    
        
        
        
    }
    func layoutFAB() {
        let item = FloatyItem()
        item.buttonColor = UIColor(displayP3Red: 1, green: 1, blue: 1, alpha: 0.7)
        //item.circleShadowColor = UIColor.red
        //item.titleShadowColor = UIColor.blue
        
        //fab.addItem(title: "I got a title")
        //fab.addItem("I got a icon", icon: UIImage(named: "icShare"))
        fab.addItem("", icon: UIImage(named: "notif+"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "photo-camera"), titlePosition: .left) { (item) in
            
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "FAImageCropperVC") as! FAImageCropperVC
            
            self.navigationController?.pushViewController(passData, animated: true)
            
            
            
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "search-1"), titlePosition: .left) { (item) in
            
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "competiton"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "GaleryViewController") as! GaleryViewController
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "Profile_male"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            passData.getid = userID
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        fab.addItem("", icon: UIImage(named: "home+"), titlePosition: .left) { (item) in
            let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let passData = MainStoryboard.instantiateViewController(withIdentifier: "NewsVCViewController") as! NewsVCViewController
            
            self.navigationController?.pushViewController(passData, animated: true)
            self.fab.close()
        }
        
        // For Image
        
        
        self.fab.sticky = true
        self.fab.paddingX = self.view.frame.width/20
        
        self.fab.fabDelegate = self
        
        print(tableView.frame)
        
        self.view.addSubview(self.fab)
    }
    
    
    func getRequest(){
        
        
        
        
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/images/user/\(getSearchUserid)/\(pageNumber)"
        Alamofire.request(url, method: .get,encoding: URLEncoding.default)
            
            
            .responseJSON{ response in
                
                
                
                if let jsonRoot = response.result.value as? [String:Any]{
                    
                    if let resultPaylaod = jsonRoot["resultPayload"] as? [String:Any]{
                        
                        
                        
                        
                        
                        if let model = resultPaylaod["model"] as? [String: Any]{
                            
                            if let mutPic = model["mutualFollowerPictures"] as! [[String: Any]]! {
                                
                                for data in mutPic{
                                    
                                    guard let mutpics = Mapper<MutualMapper>().map(JSON: data) else{
                                        
                                        continue
                                    }
                                    
                                    self.MutualArray.append(mutpics)
                                    //print(data)
                                    //self.collectionView.reloadData()
                                    
                                }
                                
                                
                                
                            }
                            
                            if let pubPic = model["publicPictures"] as! [[String: Any]]! {
                                
                                for data in pubPic{
                                    
                                    guard let pubpics = Mapper<PublicMapper>().map(JSON: data) else{
                                        
                                        continue
                                    }
                                    
                                    self.PublicArray.append(pubpics)
                                    self.collectionView.reloadData()
                                    
                                }
                                
                            }
                            
                            if let comPic = model["competitionPictures"] as! [[String: Any]]! {
                                
                                
                                for data in comPic{
                                    
                                    guard let pics = Mapper<CompititionMapper>().map(JSON: data) else{
                                        
                                        continue
                                    }
                                    
                                    self.Compitition.append(pics)
                                    //self.collectionView.reloadData()
                                    
                                }
                                
                                
                                
                                
                                
                                
                            }
                        }
                    }
                    //self.collectionView.reloadData()
                }
                
                
        }
        
        
        
        
    }
    
    
    func searchItem(){
        
        let url = "https://testglostarsdevelopers.azurewebsites.net/api/account/GetUserListByName?Name=\(searchTextField.text!)"
        Alamofire.request(url).responseJSON { response in
            
            if let jsonRoot = response.result.value as? [String:Any]!{
                
                if let resultPayload = jsonRoot!["resultPayload"] as? [[String:Any]]!{
                    
                    for array in resultPayload! {
                        
                        print(array)
                        
                        guard  let profile = Mapper<ProfileMapper>().map(JSON: array) else {
                            continue
                        }
                        self.NameArray.append(profile)
                        self.NameArray.reverse()
                        self.tableView.reloadData()
                        
                    }
                    
                    
                }
            }
            
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        tableView.isHidden = false
        
        
        if (string == " ") {
            return false
        }
        
        print(textField.text!)
        //self.tableView.reloadData()
        
        NameArray.removeAll()
        searchItem()
        //self.tableView.reloadData()
        
        
        return true
    }
    
    
    
    
    func hiddenFloatingButtons(){
        
        uploadButton.isHidden = true
        achivementButton.isHidden = true
        profileButton.isHidden = true
        notificationButton.isHidden = true
        homeButton.isHidden = true
        
        
    }
    
    func animatingPopUp()
    {
        UIView.animate(withDuration: 1, animations: {
            self.uploadButton.isHidden =  !self.uploadButton.isHidden
            
        }) { (true) in
            
            UIView.animate(withDuration: 1, animations: {
                self.achivementButton.isHidden = !self.achivementButton.isHidden
                
            }, completion: { (true) in
                UIView.animate(withDuration: 1, animations: {
                    
                    self.profileButton.isHidden = !self.profileButton.isHidden
                    
                }, completion: { (true) in
                    self.notificationButton.isHidden = !self.notificationButton.isHidden
                    
                    self.homeButton.isHidden = !self.homeButton.isHidden
                    
                    //                    let image1 = UIImage(named: "Circleopen")
                    //                    self.floatingMainButton.setBackgroundImage(image1, for: UIControlState.normal)
                    
                    
                    
                    
                })
            })
        }
        
        
        
    }
    
    
    
    
    @IBAction func mainButtonAction(_ sender: UIButton) {
        
        animatingPopUp()
        //blurView.isHidden = !blurView.isHidden
        
        
        
        
    }
    
    
    
    
    
    @IBAction func searchFieldButton(_ sender: Any) {
        
        
        
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return MutualArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : UserMutualCell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserMutualCell", for: indexPath) as! UserMutualCell
        
        
        let mutInfomation = self.MutualArray[indexPath.row]
        
        let url = NSURL(string: mutInfomation.picUrl)
        
        cell.userImage.sd_setShowActivityIndicatorView(true)
        cell.userImage.sd_setIndicatorStyle(.gray)
        cell.userImage.sd_setImage(with: url! as URL, placeholderImage: nil, options: .scaleDownLargeImages, completed: nil)
        if indexPath.row == MutualArray.count - 1 {
            // last cell
            
            if totalItems > MutualArray.count {
                pageNumber = pageNumber + 1
                print(pageNumber)
                // more items to fetch
                getRequest() // increment `fromIndex` by 20 before server call
            }
        }
        
        
        hiddenFloatingButtons()
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 3 - 1
        
        return CGSize(width: width, height: width)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.size.height {
            
            pageNumber += 1
            getRequest()
            self.collectionView.reloadData()
            
            hiddingView.isHidden = false

            IJProgressView.shared.showProgressView(view)
            
            setCloseTimer()
        }
    }
    
    func close() {
        
        IJProgressView.shared.hideProgressView()
        hiddingView.isHidden = true

    }
    
    func setCloseTimer() {
        
        _ = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(close), userInfo: nil, repeats: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let MainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let passData = MainStoryboard.instantiateViewController(withIdentifier: "ProfilePublicFeed") as! ProfilePublicFeed
        
        
        //        passData.getid = NameArray[indexPath.row].id
        //        passData.getName = NameArray[indexPath.row].userName
        //        passData.getLastName = NameArray[indexPath.row].lastName
        //        passData.getProfileUrl = NameArray[indexPath.row].proLink
        passData.getUserName = getTheName
        passData.userIDFeed = getSearchUserid
        passData.TypePhotos = "mutual"
        passData.getUserName = userNameR
        passData.indexNumber = CGFloat(indexPath.row)
        
        self.navigationController?.pushViewController(passData, animated: true)
        
        
        
    }
    
    
    
    
}

