//
//  ProfileVC.swift
//  Glostars
//
//  Created by Pathao on 21/7/18.
//  Copyright © 2018 Pathao. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
import ObjectMapper
import SwiftMessages

class InterestProfileVC: UIViewController ,UITextViewDelegate{
    
    @IBOutlet weak var currentConty: UITextField!
    @IBOutlet weak var currentCityTextField: UITextField!
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var aboutText: UITextView!
    @IBOutlet weak var currentCity: UILabel!
    
    @IBOutlet weak var profileImageOutlet: UIImageView!
    
    @IBOutlet weak var btnChooseImage: UIButton!
    
    var imagePicker = UIImagePickerController()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Provide Information"
        userName.text = userFName + " " + userLName
        let countryLocale = NSLocale.current as NSLocale
        let countryCode = countryLocale.object(forKey: .countryCode) as? String
        let country = countryLocale.displayName(forKey: .countryCode, value: countryCode ?? "")
        print("Country Code:\(countryCode ?? "") Name:\(country ?? "")")
        self.currentConty.text! = country ?? "" + "( \(countryCode ?? "") )" 
        //---left bar button---
//        let leftBarButtonItem = UIBarButtonItem(title: "Little about you", style: .plain, target: self, action: #selector(leftButtonAction))
//        leftBarButtonItem.tintColor = UIColor.black
//        leftBarButtonItem.setTitleTextAttributes([(kCTFontAttributeName as NSAttributedStringKey) as String: UIFont(name: "GillSans-Bold", size: 24)!], for: .normal)
//        navigationItem.leftBarButtonItem = leftBarButtonItem
//        //--right bar button--
//        let rightBarButtonItem = UIBarButtonItem(title: "Next >", style: .plain, target: self, action: #selector(rightButtonAction))
//        rightBarButtonItem.tintColor = UIColor.init(hex: "4D4D80")
//        rightBarButtonItem.setTitleTextAttributes([(kCTFontAttributeName as NSAttributedStringKey) as String: UIFont(name: "GillSans-Bold", size: 24)!], for: .normal)
//        navigationItem.rightBarButtonItem = rightBarButtonItem
        
        //----profile image----
        profileImageOutlet.circleView()
        profileImageOutlet.layer.borderWidth = 2
        profileImageOutlet.layer.borderColor = UIColor.darkGray.cgColor
        
        
        
        //--------------------to check the font name----------------
        //        for fontfamilyname: String in UIFont.familyNames {
        //            print("family:'\(fontfamilyname)'")
        //            for fontName: String in UIFont.fontNames(forFamilyName: fontfamilyname) {
        //                print("\tfont:'\(fontName)'")
        //            }
        //            print("-------------")
        //        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveInfo(_ sender: Any) {
        
        saveProfile()
        
        
    }
    
    
    @objc func leftButtonAction(){
        print("left bar button clicked")
    }
    
    @objc func rightButtonAction(){
        print("Right bar button clicked")
    }

    @IBAction func saveButton(_ sender: Any) {
        
        saveProfile()
        
        
    }
    func saveProfile() {
        
        
        let image =  profileImageOutlet.image
        //       imageProfile = image
        let imageData = UIImageJPEGRepresentation(image!, 0.2)!
        
        
        //        let userid = userID
        //        let Name = firstNameEditOutlet.text
        
        let parameters : NSDictionary = [
            "Id": userRegisterID,
            "AboutMe": aboutText.text,
            "Name": userFName,
            "LastName": userLName,
           "Ocupation":"Student",
            "OcupationOther": "Student",
            "Country" :currentConty.text!,
            "Location": currentCityTextField.text!
        ]
        
        //
        //
        //        let url = "https://testglostarsdevelopers.azurewebsites.net/Home/Edit"
        //
        //        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default)
        //            .responseJSON{ response in
        //
        //            switch response.result {
        //            case .success:
        //                print(response)
        //
        //            case .failure(let error):
        //                print(error)
        //            }
        //        }
        
        
        
        
        
        let URL = try! URLRequest(url: "https://testglostarsdevelopers.azurewebsites.net/Home/UserEdit", method: .post)
        
        Alamofire.upload(multipartFormData: {
            multipartFormData in
            
            multipartFormData.append(imageData, withName: "file", fileName: "file.png", mimeType: "image/png")
            
            for (key, value) in parameters {
                
                multipartFormData.append((value as! String).data(using:.utf8)!, withName: key as! String)
                
            }
            
            
            
        }, with: URL, encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    debugPrint("SUCCESS RESPONSE: \(response)")
                    
                let interestProfileVC = self.storyboard!.instantiateViewController(withIdentifier: "InterestVC")
                
            self.navigationController?.pushViewController(interestProfileVC, animated: true)
                    
 
                }
            case .failure(let encodingError):
                // hide progressbas here
                print("ERROR RESPONSE: \(encodingError)")
                
                
            }
        })

    }
    
    //upload button click action
    
    @IBAction func uploadButtonClicked(_ sender: Any) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Choose Photo", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        //If you want work actionsheet on ipad then you have to use popoverPresentationController to present the actionsheet, otherwise app will crash in iPad
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    //MARK: - Open the camera
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            //If you dont want to edit the photo then you can set allowsEditing to false
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: - Choose image from camera roll
    func openGallary(){
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        //If you dont want to edit the photo then you can set allowsEditing to false
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        aboutText.text = ""
        
        return true
    }
    
}



//MARK: - UIImagePickerControllerDelegate
extension InterestProfileVC:  UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        /*
         Get the image from the info dictionary.
         If no need to edit the photo, use `UIImagePickerControllerOriginalImage`
         instead of `UIImagePickerControllerEditedImage`
         */
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            self.profileImageOutlet.image = editedImage
        }
        
        //Dismiss the UIImagePicker after selection
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("Cancelled")
        picker.dismiss(animated: true, completion: nil)
    }
    
}
